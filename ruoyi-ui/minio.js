let Minio = require('minio')
let stream = require('stream')

export const minioClient = new Minio.Client({
  endPoint: '192.168.0.104',   //对象存储服务的URL
  port: 9000,//端口号
  useSSL: false,   //true代表使用HTTPS
  accessKey: 'minioadmin',   //账户id
  secretKey: 'minioadmin',   //密码
  partSize: '50M'
});

export function get_minio_url()
{
  return 'http://192.168.0.104:9000/erp/';
}

export function get_minio_bucketName()
{
   return 'erp';
}
export function random_string(len) {
  len = len || 32;
  let chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz12345678', maxPos = chars.length, pwd = '';
  for (let i = 0; i < len; i++) {
    pwd += chars.charAt(Math.floor(Math.random() * maxPos));
  }
  return pwd;
}
export default{
 minioClient,
  get_minio_url,
  get_minio_bucketName
}
