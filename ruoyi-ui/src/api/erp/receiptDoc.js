import request from '@/utils/request'

// 查询入库单列表
export function listReceiptDoc(query) {
  return request({
    url: '/system/receiptDoc/list',
    method: 'get',
    params: query
  })
}

// 查询入库单详细
export function getReceiptDoc(id) {
  return request({
    url: '/system/receiptDoc/' + id,
    method: 'get'
  })
}

// 新增入库单
export function addReceiptDoc(data) {
  return request({
    url: '/system/receiptDoc',
    method: 'post',
    data: data
  })
}

// 修改入库单
export function updateReceiptDoc(data) {
  return request({
    url: '/system/receiptDoc',
    method: 'put',
    data: data
  })
}

// 删除入库单
export function delReceiptDoc(id) {
  return request({
    url: '/system/receiptDoc/' + id,
    method: 'delete'
  })
}
