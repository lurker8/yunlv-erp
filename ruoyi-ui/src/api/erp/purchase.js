import request from '@/utils/request'

// 查询采购订单列表
export function listPurchase(query) {
  return request({
    url: '/system/purchase/list',
    method: 'get',
    params: query
  })
}

// 查询采购订单详细
export function getPurchase(id) {
  return request({
    url: '/system/purchase/' + id,
    method: 'get'
  })
}

// 新增采购订单
export function addPurchase(data) {
  return request({
    url: '/system/purchase',
    method: 'post',
    data: data
  })
}

// 修改采购订单
export function updatePurchase(data) {
  return request({
    url: '/system/purchase',
    method: 'put',
    data: data
  })
}

// 删除采购订单
export function delPurchase(id) {
  return request({
    url: '/system/purchase/' + id,
    method: 'delete'
  })
}

// 审核上下架
export function editFlag(data) {
  return request({
    url: '/system/purchase/editFlag',
    method: 'post',
    data: data
  })
}

// 采购入库修改专用
export function purchaseReceipt(data) {
  return request({
    url: '/system/purchase/purchaseReceipt',
    method: 'post',
    data: data
  })
}
