import request from '@/utils/request'

// 查询销售订单列表
export function listSale(query) {
  return request({
    url: '/system/sale/list',
    method: 'get',
    params: query
  })
}
// 查询销售订单列表
export function deliveryList(query) {
  return request({
    url: '/system/sale/deliveryList',
    method: 'get',
    params: query
  })
}

// 查询销售订单详细
export function getSale(id) {
  return request({
    url: '/system/sale/' + id,
    method: 'get'
  })
}

// 新增销售订单
export function addSale(data) {
  return request({
    url: '/system/sale',
    method: 'post',
    data: data
  })
}

// 修改销售订单
export function updateSale(data) {
  return request({
    url: '/system/sale',
    method: 'put',
    data: data
  })
}
// 删除销售订单商品
export function delSaleGood(data) {
  return request({
    url: '/system/sale/deleteGoods',
    method: 'delete',
    data: data
  })
}

// 新增销售订单商品
export function addSaleGood(data) {
  return request({
    url: '/system/saleGoods',
    method: 'post',
    data: data
  })
}

// 修改销售订单状态
export function updateSaleType(id,type,num) {
  return request({
    url: '/system/sale/updateType?saleId='+id+'&type='+type+'&num='+num,
    method: 'put',
  })
}


export function ConfirmDeliveryQuantity(id) {
  return request({
    url: '/system/sale/ConfirmDeliveryQuantity?saleId='+id,
    method: 'get',
  })
}

// 删除销售订单关联商品仓库信息
export function deleteGoodNumberInfo(goodId,saleId) {
  return request({
    url: '/system/goodWarehouse/deleteGoodNumberInfo?goodId='+goodId+"&saleId="+saleId,
    method: 'delete',
  })
}
// 删除销售订单关联商品仓库信息
export function checkGoodsSaleWarehouse(data) {
  return request({
    url: '/system/sale/checkGoodsSaleWarehouse',
    method: 'post',
    data: data,
  })
}

// 添加销售仓库商品关联表
export function addGoodsSaleWarehouse(data) {
  return request({
    url: '/system/sale/addGoodsSaleWarehouse',
    method: 'post',
    data: data
  })
}
//  修改销售仓库商品关联表
export function upGoodsSaleWarehouse(data) {
  return request({
    url: '/system/sale/upGoodsSaleWarehouse',
    method: 'post',
    data: data
  })
}

// 商品退货方法
export function returnGoods(data) {
  return request({
    url: '/system/sale/returnGoods',
    method: 'put',
    data: data
  })
}

 // 添加销售仓库商品关联表
export function confirmDelivery(data) {
  return request({
    url: '/system/sale/addGoodsSaleWarehouse',
    method: 'post',
    data: data
  })
}


// 删除销售订单
export function delSale(id) {
  return request({
    url: '/system/sale/' + id,
    method: 'delete'
  })
}
