import request from '@/utils/request'

// 查询采购商品仓库关联列表
export function listGoodsPurchaseWarehouse(query) {
  return request({
    url: '/system/goodsPurchaseWarehouse/list',
    method: 'get',
    params: query
  })
}

// 查询采购商品仓库关联详细
export function getGoodsPurchaseWarehouse(id) {
  return request({
    url: '/system/goodsPurchaseWarehouse/' + id,
    method: 'get'
  })
}

// 新增采购商品仓库关联
export function addGoodsPurchaseWarehouse(data) {
  return request({
    url: '/system/goodsPurchaseWarehouse',
    method: 'post',
    data: data
  })
}

// 修改采购商品仓库关联
export function updateGoodsPurchaseWarehouse(data) {
  return request({
    url: '/system/goodsPurchaseWarehouse',
    method: 'put',
    data: data
  })
}

// 删除采购商品仓库关联
export function delGoodsPurchaseWarehouse(id) {
  return request({
    url: '/system/goodsPurchaseWarehouse/' + id,
    method: 'delete'
  })
}
