import request from '@/utils/request'

// 查询库存调拨列表
export function listAllocation(query) {
  return request({
    url: '/system/allocation/list',
    method: 'get',
    params: query
  })
}

// 查询库存调拨详细
export function getAllocation(id) {
  return request({
    url: '/system/allocation/' + id,
    method: 'get'
  })
}

// 新增库存调拨
export function addAllocation(data) {
  return request({
    url: '/system/allocation',
    method: 'post',
    data: data
  })
}

// 修改库存调拨
export function updateAllocation(data) {
  return request({
    url: '/system/allocation',
    method: 'put',
    data: data
  })
}
//修改调拨状态
export function updateAllocationType(allocationId,type) {
  return request({
    url: '/system/allocation/updateAllocationType?allocationId='+allocationId+"&type="+type,
    method: 'put',
  })
}

// 删除库存调拨
export function delAllocation(id) {
  return request({
    url: '/system/allocation/' + id,
    method: 'delete'
  })
}
