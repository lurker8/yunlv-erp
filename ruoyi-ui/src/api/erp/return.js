import request from '@/utils/request'

// 查询【请填写功能名称】列表
export function listReturn(query) {
  return request({
    url: '/system/return/list',
    method: 'get',
    params: query
  })
}

// 查询【请填写功能名称】详细
export function getReturn(id) {
  return request({
    url: '/system/return/' + id,
    method: 'get'
  })
}

// 新增【请填写功能名称】
export function addReturn(data) {
  return request({
    url: '/system/return',
    method: 'post',
    data: data
  })
}

// 修改【请填写功能名称】
export function updateReturn(data) {
  return request({
    url: '/system/return',
    method: 'put',
    data: data
  })
}
// 修改【请填写功能名称】
export function returnGood(data) {
  return request({
    url: '/system/return/returnGood',
    method: 'post',
    data: data
  })
}

// 删除【请填写功能名称】
export function delReturn(id) {
  return request({
    url: '/system/return/' + id,
    method: 'delete'
  })
}
