import request from '@/utils/request'

// 查询采购退货列表
export function listReturnPurchase(query) {
  return request({
    url: '/system/returnPurchase/list',
    method: 'get',
    params: query
  })
}

// 查询采购退货详细
export function getReturnPurchase(id) {
  return request({
    url: '/system/returnPurchase/' + id,
    method: 'get'
  })
}

// 新增采购退货
export function addReturnPurchase(data) {
  return request({
    url: '/system/returnPurchase',
    method: 'post',
    data: data
  })
}

// 修改采购退货
export function updateReturnPurchase(data) {
  return request({
    url: '/system/returnPurchase',
    method: 'put',
    data: data
  })
}

// 删除采购退货
export function delReturnPurchase(id) {
  return request({
    url: '/system/returnPurchase/' + id,
    method: 'delete'
  })
}
