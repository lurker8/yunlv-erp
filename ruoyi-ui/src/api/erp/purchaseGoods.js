import request from '@/utils/request'

// 查询采购商品列表
export function listPurchaseoods(query) {
  return request({
    url: '/system/purchaseoods/list',
    method: 'get',
    params: query
  })
}

// 根据采购订单id获取采购商品详细信息列表
export function listPurchaseoodsInfo(query) {
  return request({
    url: '/system/purchaseoods/purchaseGoods',
    method: 'get',
    params: query
  })
}

// 查询采购商品详细
export function getPurchaseoods(id) {
  return request({
    url: '/system/purchaseoods/' + id,
    method: 'get'
  })
}

// 新增采购商品
export function addPurchaseoods(data) {
  return request({
    url: '/system/purchaseoods',
    method: 'post',
    data: data
  })
}

// 修改采购商品
export function updatePurchaseoods(data) {
  return request({
    url: '/system/purchaseoods',
    method: 'put',
    data: data
  })
}
// 修改采购商品数量专用
export function updatePurchaseGoods(data) {
  return request({
    url: '/system/purchaseoods/updatePurchaseGoods',
    method: 'put',
    data: data
  })
}

// 删除采购商品
export function delPurchaseoods(id) {
  return request({
    url: '/system/purchaseoods/' + id,
    method: 'delete'
  })
}
