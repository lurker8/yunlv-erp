import request from '@/utils/request'

// 查询库存盘点列表
export function listInventory(query) {
  return request({
    url: '/system/inventory/list',
    method: 'get',
    params: query
  })
}

// 查询库存盘点详细
export function getInventory(id) {
  return request({
    url: '/system/inventory/' + id,
    method: 'get'
  })
}

// 新增库存盘点
export function addInventory(data) {
  return request({
    url: '/system/inventory',
    method: 'post',
    data: data
  })
}

// 修改库存盘点
export function updateInventory(data) {
  return request({
    url: '/system/inventory',
    method: 'put',
    data: data
  })
}
// 修改库存盘点状态
export function updateInventoryType(id,type) {
  return request({
    url: '/system/inventory/updateInventoryType?inventoryId='+id+"&type="+type,
    method: 'put',
  })
}

// 删除库存盘点
export function delInventory(id) {
  return request({
    url: '/system/inventory/' + id,
    method: 'delete'
  })
}
