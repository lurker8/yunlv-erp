import request from '@/utils/request'

// 查询商品信息列表
export function listGoods(query) {
  return request({
    url: '/system/goods/list',
    method: 'get',
    params: query
  })
}
// 查询阈值商品信息列表
export function listGoodsThreshold(query) {
  return request({
    url: '/system/goods/listGoodsThreshold',
    method: 'get',
    params: query
  })
}

export function warehouseList(query) {
  return request({
    url: '/system/goods/warehouseList',
    method: 'get',
    params: query
  })
}

// 查询商品信息列表采购订单专用
export function listGoodsPurchase(query) {
  return request({
    url: '/system/goods/listGoodsPurchase',
    method: 'get',
    params: query
  })
}

// 查询商品信息详细
export function getGoods(id) {
  return request({
    url: '/system/goods/' + id,
    method: 'get'
  })
}

// 新增商品信息
export function addGoods(data) {
  return request({
    url: '/system/goods',
    method: 'post',
    data: data
  })
}

// 修改商品信息
export function updateGoods(data) {
  return request({
    url: '/system/goods',
    method: 'put',
    data: data
  })
}
// 修改商品信息(采购入库按钮专用)
export function updateErpGoodsWarehousing(data) {
  return request({
    url: '/system/goods/updateErpGoodsWarehousing',
    method: 'put',
    data: data
  })
}

// 删除商品信息
export function delGoods(id) {
  return request({
    url: '/system/goods/' + id,
    method: 'delete'
  })
}
