import request from '@/utils/request'

// 查询报损单列表
export function listReporting(query) {
  return request({
    url: '/system/reporting/list',
    method: 'get',
    params: query
  })
}

// 查询报损单详细
export function getReporting(id) {
  return request({
    url: '/system/reporting/' + id,
    method: 'get'
  })
}

// 新增报损单
export function addReporting(data) {
  return request({
    url: '/system/reporting',
    method: 'post',
    data: data
  })
}

// 修改报损单
export function updateReporting(data) {
  return request({
    url: '/system/reporting',
    method: 'put',
    data: data
  })
}
//确认报修单
export function confirmReporting(data) {
  return request({
    url: '/system/reporting/confirmReporting',
    method: 'put',
    data: data
  })
}

// 删除报损单
export function delReporting(id) {
  return request({
    url: '/system/reporting/' + id,
    method: 'delete'
  })
}
