import request from '@/utils/request'

// 查询出库单列表
export function listLssue(query) {
  return request({
    url: '/system/lssue/list',
    method: 'get',
    params: query
  })
}

