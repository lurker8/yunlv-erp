import request from '@/utils/request'

// 查询商品仓库管理列表
export function listWarehouse(query) {
  return request({
    url: '/system/goodWarehouse/list',
    method: 'get',
    params: query
  })
}

// 查询商品名称
export function listGoodsName(query) {
  return request({
    url: '/system/goodWarehouse/goodsName',
    method: 'get',
    params: query
  })
}
// 查询仓库名称
export function listWarehouseName(query) {
  return request({
    url: '/system/goodWarehouse/warehouseName',
    method: 'get',
    params: query
  })
}

// 查询商品仓库管理详细
export function getWarehouse(id) {
  return request({
    url: '/system/goodWarehouse/' + id,
    method: 'get'
  })
}

// 修改查询商品仓库管理详细
export function getWarehouseBySaleId(id,saleId) {
  return request({
    url: '/system/goodWarehouse/getGoodNumberInfo?id='+ id+"&saleId="+saleId,
    method: 'get'
  })
}

// 新增商品仓库管理
export function addWarehouse(data) {
  return request({
    url: '/system/goodWarehouse',
    method: 'post',
    data: data
  })
}
// 新增采购入库商品仓库管理
export function addPurchaseWarehouse(data) {
  return request({
    url: '/system/goodWarehouse/addPurchaseWarehouse',
    method: 'post',
    data: data
  })
}

// 修改商品仓库管理
export function updateWarehouse(data) {
  return request({
    url: '/system/goodWarehouse',
    method: 'put',
    data: data
  })
}

// 删除商品仓库管理
export function delWarehouse(id) {
  return request({
    url: '/system/goodWarehouse/' + id,
    method: 'delete'
  })
}
