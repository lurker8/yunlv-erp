import request from '@/utils/request'

// 查询付款记录列表
export function listPaymentRecord(query) {
  return request({
    url: '/system/paymentRecord/list',
    method: 'get',
    params: query
  })
}

// 查询付款记录详细
export function getPaymentRecord(id) {
  return request({
    url: '/system/paymentRecord/' + id,
    method: 'get'
  })
}

// 新增付款记录
export function addPaymentRecord(data) {
  return request({
    url: '/system/paymentRecord',
    method: 'post',
    data: data
  })
}

// 修改付款记录
export function updatePaymentRecord(data) {
  return request({
    url: '/system/paymentRecord',
    method: 'put',
    data: data
  })
}

// 删除付款记录
export function delPaymentRecord(id) {
  return request({
    url: '/system/paymentRecord/' + id,
    method: 'delete'
  })
}
