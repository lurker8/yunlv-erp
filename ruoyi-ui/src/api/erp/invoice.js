import request from '@/utils/request'

// 查询销售发货单列表
export function listInvoice(query) {
  return request({
    url: '/system/invoice/list',
    method: 'get',
    params: query
  })
}

// 查询销售发货单详细
export function getInvoice(id) {
  return request({
    url: '/system/invoice/' + id,
    method: 'get'
  })
}

// 新增销售发货单
export function addInvoice(data) {
  return request({
    url: '/system/invoice',
    method: 'post',
    data: data
  })
}

// 修改销售发货单
export function updateInvoice(data) {
  return request({
    url: '/system/invoice',
    method: 'put',
    data: data
  })
}

// 删除销售发货单
export function delInvoice(id) {
  return request({
    url: '/system/invoice/' + id,
    method: 'delete'
  })
}
