import request from '@/utils/request'

// 查询应收账款列表
export function listCollection(query) {
  return request({
    url: '/system/collection/list',
    method: 'get',
    params: query
  })
}

// 查询应收账款详细
export function getCollection(id) {
  return request({
    url: '/system/collection/' + id,
    method: 'get'
  })
}
// 查询应收账款详细
export function uploadFile(data) {
  return request({
    url: '/system/collection/uploadFile',
    method: 'post',
    data: data
  })
}

// 新增应收账款
export function addCollection(data) {
  return request({
    url: '/system/collection',
    method: 'post',
    data: data
  })
}


// 查询收款记录列表
export function listRecord(query) {
  return request({
    url: '/system/record/list',
    method: 'get',
    params: query
  })
}


// 修改应收账款
export function updateCollection(data) {
  return request({
    url: '/system/collection',
    method: 'put',
    data: data
  })
}

// 删除应收账款
export function delCollection(id) {
  return request({
    url: '/system/collection/' + id,
    method: 'delete'
  })
}
