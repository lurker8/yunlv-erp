import request from '@/utils/request'

// 查询应付账款列表
export function listCopeWith(query) {
  return request({
    url: '/system/copeWith/list',
    method: 'get',
    params: query
  })
}

// 查询应付账款详细
export function getCopeWith(id) {
  return request({
    url: '/system/copeWith/' + id,
    method: 'get'
  })
}
// 财务管理查看付款页面信息
export function getCopeWithLeftPaymentRecord(id) {
  return request({
    url: '/system/copeWith/LeftPaymentRecordLeftPaymentRecord/' + id,
    method: 'get'
  })
}

// 新增应付账款
export function addCopeWith(data) {
  return request({
    url: '/system/copeWith',
    method: 'post',
    data: data
  })
}

// 修改应付账款
export function updateCopeWith(data) {
  return request({
    url: '/system/copeWith',
    method: 'put',
    data: data
  })
}

// 删除应付账款
export function delCopeWith(id) {
  return request({
    url: '/system/copeWith/' + id,
    method: 'delete'
  })
}
