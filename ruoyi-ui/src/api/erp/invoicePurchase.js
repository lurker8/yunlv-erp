import request from '@/utils/request'

// 查询采购入库单列表
export function listInvoicePurchase(query) {
  return request({
    url: '/system/invoicePurchase/list',
    method: 'get',
    params: query
  })
}

// 查询采购入库单详细
export function getInvoicePurchase(id) {
  return request({
    url: '/system/invoicePurchase/' + id,
    method: 'get'
  })
}

// 新增采购入库单
export function addInvoicePurchase(data) {
  return request({
    url: '/system/invoicePurchase',
    method: 'post',
    data: data
  })
}

// 修改采购入库单
export function updateInvoicePurchase(data) {
  return request({
    url: '/system/invoicePurchase',
    method: 'put',
    data: data
  })
}

// 删除采购入库单
export function delInvoicePurchase(id) {
  return request({
    url: '/system/invoicePurchase/' + id,
    method: 'delete'
  })
}
