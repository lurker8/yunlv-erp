import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: 路由配置项
 *
 * hidden: true                     // 当设置 true 的时候该路由不会再侧边栏出现 如401，login等页面，或者如一些编辑页面/edit/1
 * alwaysShow: true                 // 当你一个路由下面的 children 声明的路由大于1个时，自动会变成嵌套的模式--如组件页面
 *                                  // 只有一个时，会将那个子路由当做根路由显示在侧边栏--如引导页面
 *                                  // 若你想不管路由下面的 children 声明的个数都显示你的根路由
 *                                  // 你可以设置 alwaysShow: true，这样它就会忽略之前定义的规则，一直显示根路由
 * redirect: noRedirect             // 当设置 noRedirect 的时候该路由在面包屑导航中不可被点击
 * name:'router-name'               // 设定路由的名字，一定要填写不然使用<keep-alive>时会出现各种问题
 * query: '{"id": 1, "name": "ry"}' // 访问路由的默认传递参数
 * roles: ['admin', 'common']       // 访问路由的角色权限
 * permissions: ['a:a:a', 'b:b:b']  // 访问路由的菜单权限
 * meta : {
    noCache: true                   // 如果设置为true，则不会被 <keep-alive> 缓存(默认 false)
    title: 'title'                  // 设置该路由在侧边栏和面包屑中展示的名字
    icon: 'svg-name'                // 设置该路由的图标，对应路径src/assets/icons/svg
    breadcrumb: false               // 如果设置为false，则不会在breadcrumb面包屑中显示
    activeMenu: '/system/user'      // 当路由设置了该属性，则会高亮相对应的侧边栏。
  }
 */

// 公共路由
export const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/views/redirect')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login'),
    hidden: true
  },
  {
    path: '/register',
    component: () => import('@/views/register'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/error/404'),
    hidden: true
  },
  {
    path: '/401',
    component: () => import('@/views/error/401'),
    hidden: true
  },
  {
    path: '',
    component: Layout,
    redirect: 'index',
    children: [
      {
        path: 'index',
        component: () => import('@/views/index'),
        name: 'Index',
        meta: { title: '首页', icon: 'dashboard', affix: true }
      }
    ]
  },
  {
    path: '/user',
    component: Layout,
    hidden: true,
    redirect: 'noredirect',
    children: [
      {
        path: 'profile',
        component: () => import('@/views/system/user/profile/index'),
        name: 'Profile',
        meta: { title: '个人中心', icon: 'user' }
      }
    ]
  },
  {
    path: '/addGoods',
    hidden: true,
    component: Layout,
    redirect: 'noredirect',
    children: [
      {
        path: '/addGoods',
        name: 'addGoods',
        component: () => import('@/views/erp/goods/add'),
        meta: { title: '添加商品', icon: 'index' }
      }
    ]
  },
  {
    path: '/addSale',
    hidden: true,
    component: Layout,
    redirect: 'noredirect',
    children: [
      {
        path: '/addSale',
        name: 'addSale',
        component: () => import('@/views/erp/sale/add'),
        meta: { title: '添加销售订单', icon: 'index' }
      }
    ]
  },
  {
    path: '/editSale',
    hidden: true,
    component: Layout,
    redirect: 'noredirect',
    children: [
      {
        path: '/editSale',
        name: 'editSale',
        component: () => import('@/views/erp/sale/edit'),
        meta: { title: '修改销售订单', icon: 'index' }
      }
    ]
  },
  {
    path: '/InfoSale',
    hidden: true,
    component: Layout,
    redirect: 'noredirect',
    children: [
      {
        path: '/InfoSale',
        name: 'InfoSale',
        component: () => import('@/views/erp/sale/info'),
        meta: { title: '查看销售订单', icon: 'index' }
      }
    ]
  },
  {
    path: '/InfoLssue',
    hidden: true,
    component: Layout,
    redirect: 'noredirect',
    children: [
      {
        path: '/InfoLssue',
        name: 'InfoLssue',
        component: () => import('@/views/erp/lssue/info'),
        meta: { title: '出库详情', icon: 'index' }
      }
    ]
  },
  {
    path: '/addAllocation',
    hidden: true,
    component: Layout,
    redirect: 'noredirect',
    children: [
      {
        path: '/addAllocation',
        name: 'addAllocation',
        component: () => import('@/views/erp/allocation/add'),
        meta: { title: '新增库间调拨单', icon: 'index' }
      }
    ]
  },
  {
    path: '/editAllocation',
    hidden: true,
    component: Layout,
    redirect: 'noredirect',
    children: [
      {
        path: '/editAllocation',
        name: 'editAllocation',
        component: () => import('@/views/erp/allocation/edit'),
        meta: { title: '修改库间调拨单', icon: 'index' }
      }
    ]
  },
  {
    path: '/InfoAllocation',
    hidden: true,
    component: Layout,
    redirect: 'noredirect',
    children: [
      {
        path: '/InfoAllocation',
        name: 'InfoAllocation',
        component: () => import('@/views/erp/allocation/info'),
        meta: { title: '查看确认库间调拨单', icon: 'index' }
      }
    ]
  },
  {
    path: '/LssueAllocation',
    hidden: true,
    component: Layout,
    redirect: 'noredirect',
    children: [
      {
        path: '/LssueAllocation',
        name: 'LssueAllocation',
        component: () => import('@/views/erp/lssue/allocationInfo'),
        meta: { title: '查看调拨出库', icon: 'index' }
      }
    ]
  },
  {
    path: '/receiptAllocation',
    hidden: true,
    component: Layout,
    redirect: 'noredirect',
    children: [
      {
        path: '/receiptAllocation',
        name: 'receiptAllocation',
        component: () => import('@/views/erp/receiptDoc/allocationInfo'),
        meta: { title: '查看调拨入库', icon: 'index' }
      }
    ]
  },
  {
    path: '/addInventory',
    hidden: true,
    component: Layout,
    redirect: 'noredirect',
    children: [
      {
        path: '/addInventory',
        name: 'addInventory',
        component: () => import('@/views/erp/inventory/add'),
        meta: { title: '新增库存盘点', icon: 'index' }
      }
    ]
  },
  {
    path: '/editInventory',
    hidden: true,
    component: Layout,
    redirect: 'noredirect',
    children: [
      {
        path: '/editInventory',
        name: 'editInventory',
        component: () => import('@/views/erp/inventory/edit'),
        meta: { title: '修改库存盘点', icon: 'index' }
      }
    ]
  },
  {
    path: '/infoInventory',
    hidden: true,
    component: Layout,
    redirect: 'noredirect',
    children: [
      {
        path: '/infoInventory',
        name: 'infoInventory',
        component: () => import('@/views/erp/inventory/info'),
        meta: { title: '查看库存盘点', icon: 'index' }
      }
    ]
  },
  {
    path: '/Deliver',
    hidden: true,
    component: Layout,
    redirect: 'noredirect',
    children: [
      {
        path: '/Deliver',
        name: 'Deliver',
        component: () => import('@/views/erp/sale/deliver'),
        meta: { title: '查看销售订单', icon: 'index' }
      }
    ]
  },
  {
    path: '/saleList',
    hidden: true,
    component: Layout,
    redirect: 'noredirect',
    children: [
      {
        path: '/saleList',
        name: 'saleList',
        component: () => import('@/views/erp/sale/index'),
        meta: { title: '销售订单', icon: 'index' }
      }
    ]
  },
  {
    path: '/invoiceList',
    hidden: true,
    component: Layout,
    redirect: 'noredirect',
    children: [
      {
        path: '/invoiceList',
        name: 'invoiceList',
        component: () => import('@/views/erp/invoice/index'),
        meta: { title: '销售发货单', icon: 'index' }
      }
    ]
  },
  {
    path: '/returnInfo',
    hidden: true,
    component: Layout,
    redirect: 'noredirect',
    children: [
      {
        path: '/returnInfo',
        name: 'returnInfo',
        component: () => import('@/views/erp/return/info'),
        meta: { title: '销售退货单', icon: 'index' }
      }
    ]
  },
  {
    path: '/editGoods',
    hidden: true,
    component: Layout,
    redirect: 'noredirect',
    children: [
      {
        path: '/editGoods',
        name: 'editGoods',
        component: () => import('@/views/erp/goods/edit'),
        meta: { title: '修改商品', icon: 'index' }
      }
    ]
  },
  {
    path: '/infoGoods',
    hidden: true,
    component: Layout,
    redirect: 'noredirect',
    children: [
      {
        path: '/infoGoods',
        name: 'infoGoods',
        component: () => import('@/views/erp/goods/info'),
        meta: { title: '商品详情', icon: 'index' }
      }
    ]
  },
  {
    path: '/goodsList',
    hidden: true,
    component: Layout,
    redirect: 'noredirect',
    children: [
      {
        path: '/goodsList',
        name: 'goodsList',
        component: () => import('@/views/erp/goods/index'),
        meta: { title: '商品列表', icon: 'index' }
      }
    ]
  },
  {
    path: '/addPurchase',
    hidden: true,
    component: Layout,
    redirect: 'noredirect',
    children: [
      {
        path: '/addPurchase',
        name: 'addPurchase',
        component: () => import('@/views/erp/purchase/add'),
        meta: { title: '添加采购订单', icon: 'index' }
      }
    ]
  },
  {
    path: '/editPurchase',
    hidden: true,
    component: Layout,
    redirect: 'noredirect',
    children: [
      {
        path: '/editPurchase',
        name: 'editPurchase',
        component: () => import('@/views/erp/purchase/edit'),
        meta: { title: '修改采购订单', icon: 'index' }
      }
    ]
  },
  {
    path: '/deliverPurchase',
    hidden: true,
    component: Layout,
    redirect: 'noredirect',
    children: [
      {
        path: '/deliverPurchase',
        name: 'deliverPurchase',
        component: () => import('@/views/erp/purchase/deliver'),
        meta: { title: '选择入库仓库', icon: 'index' }
      }
    ]
  },
  {
    path: '/inFoPurchase',
    hidden: true,
    component: Layout,
    redirect: 'noredirect',
    children: [
      {
        path: '/inFoPurchase',
        name: 'inFoPurchase',
        component: () => import('@/views/erp/purchase/info'),
        meta: { title: '采购订单详情', icon: 'index' }
      }
    ]
  },
  {
    path: '/returnGoods',
    hidden: true,
    component: Layout,
    redirect: 'noredirect',
    children: [
      {
        path: '/returnGoods',
        name: 'returnGoods',
        component: () => import('@/views/erp/return/returnGood'),
        meta: { title: '订单退货', icon: 'index' }
      }
    ]
  }, {
    path: '/returnInfo',
    hidden: true,
    component: Layout,
    redirect: 'noredirect',
    children: [
      {
        path: '/returnInfo',
        name: 'returnInfo',
        component: () => import('@/views/erp/return/info'),
        meta: { title: '订单退货', icon: 'index' }
      }
    ]
  },{
    path: '/returnList',
    hidden: true,
    component: Layout,
    redirect: 'noredirect',
    children: [
      {
        path: '/returnList',
        name: 'returnList',
        component: () => import('@/views/erp/return/index'),
        meta: { title: '订单退货', icon: 'index' }
      }
    ]
  },
  {
    path: '/invoiceInfo',
    hidden: true,
    component: Layout,
    redirect: 'noredirect',
    children: [
      {
        path: '/invoiceInfo',
        name: 'invoiceInfo',
        component: () => import('@/views/erp/invoice/info'),
        meta: { title: '发货详情', icon: 'index' }
      }
    ]
  },
  {
    path: '/collectionInfo',
    hidden: true,
    component: Layout,
    redirect: 'noredirect',
    children: [
      {
        path: '/collectionInfo',
        name: 'collectionInfo',
        component: () => import('@/views/erp/collection/info'),
        meta: { title: '收款详情', icon: 'index' }
      }
    ]
  },
  {
    path: '/collection',
    hidden: true,
    component: Layout,
    redirect: 'noredirect',
    children: [
      {
        path: '/collection',
        name: 'collection',
        component: () => import('@/views/erp/collection/collection'),
        meta: { title: '收款', icon: 'index' }
      }
    ]
  },
  {
    path: '/purchase/purchase',
    hidden: true,
    component: Layout,
    redirect: 'noredirect',
    children: [
      {
        path: '/purchase/purchase',
        name: 'purchasePurchase',
        component: () => import('@/views/erp/purchase/index'),
        meta: { title: '采购订单', icon: 'index' }
      }
    ]
  },
  {
    path: '/returnPurchase',
    hidden: true,
    component: Layout,
    redirect: 'noredirect',
    children: [
      {
        path: '/returnPurchase',
        name: 'returnPurchase',
        component: () => import('@/views/erp/returnPurchase/returnGoods'),
        meta: { title: '退货信息', icon: 'returnGoods' }
      }
    ]
  },
  {
    path: '/seePurchase',
    hidden: true,
    component: Layout,
    redirect: 'noredirect',
    children: [
      {
        path: '/seePurchase',
        name: 'seePurchase',
        component: () => import('@/views/erp/invoicePurchase/info'),
        meta: { title: '查看', icon: 'info' }
      }
    ]
  },
  {
    path: '/seereturnPurchase',
    hidden: true,
    component: Layout,
    redirect: 'noredirect',
    children: [
      {
        path: '/seereturnPurchase',
        name: 'seereturnPurchase',
        component: () => import('@/views/erp/returnPurchase/info'),
        meta: { title: '查看', icon: 'info' }
      }
    ]
  },
  {
    path: '/seeReceiptDoc',
    hidden: true,
    component: Layout,
    redirect: 'noredirect',
    children: [
      {
        path: '/seeReceiptDoc',
        name: 'seeReceiptDoc',
        component: () => import('@/views/erp/receiptDoc/info'),
        meta: { title: '查看', icon: 'info' }
      }
    ]
  },
  {
    path: '/copeWith',
    hidden: true,
    component: Layout,
    redirect: 'noredirect',
    children: [
      {
        path: '/copeWith',
        name: 'copeWith',
        component: () => import('@/views/erp/copeWith/copeWith'),
        meta: { title: '付款', icon: 'index' }
      }
    ]
  },
  {
    path: '/copeWithInfo',
    hidden: true,
    component: Layout,
    redirect: 'noredirect',
    children: [
      {
        path: '/copeWithInfo',
        name: 'copeWithInfo',
        component: () => import('@/views/erp/copeWith/info'),
        meta: { title: '付款详情', icon: 'index' }
      }
    ]
  },
]

// 动态路由，基于用户权限动态去加载
export const dynamicRoutes = [
  {
    path: '/system/user-auth',
    component: Layout,
    hidden: true,
    permissions: ['system:user:edit'],
    children: [
      {
        path: 'role/:userId(\\d+)',
        component: () => import('@/views/system/user/authRole'),
        name: 'AuthRole',
        meta: { title: '分配角色', activeMenu: '/system/user' }
      }
    ]
  },
  {
    path: '/system/role-auth',
    component: Layout,
    hidden: true,
    permissions: ['system:role:edit'],
    children: [
      {
        path: 'user/:roleId(\\d+)',
        component: () => import('@/views/system/role/authUser'),
        name: 'AuthUser',
        meta: { title: '分配用户', activeMenu: '/system/role' }
      }
    ]
  },
  {
    path: '/system/dict-data',
    component: Layout,
    hidden: true,
    permissions: ['system:dict:list'],
    children: [
      {
        path: 'index/:dictId(\\d+)',
        component: () => import('@/views/system/dict/data'),
        name: 'Data',
        meta: { title: '字典数据', activeMenu: '/system/dict' }
      }
    ]
  },
  {
    path: '/monitor/job-log',
    component: Layout,
    hidden: true,
    permissions: ['monitor:job:list'],
    children: [
      {
        path: 'index',
        component: () => import('@/views/monitor/job/log'),
        name: 'JobLog',
        meta: { title: '调度日志', activeMenu: '/monitor/job' }
      }
    ]
  },
  {
    path: '/tool/gen-edit',
    component: Layout,
    hidden: true,
    permissions: ['tool:gen:edit'],
    children: [
      {
        path: 'index/:tableId(\\d+)',
        component: () => import('@/views/tool/gen/editTable'),
        name: 'GenEdit',
        meta: { title: '修改生成配置', activeMenu: '/tool/gen' }
      }
    ]
  }
]

// 防止连续点击多次路由报错
let routerPush = Router.prototype.push;
Router.prototype.push = function push(location) {
  return routerPush.call(this, location).catch(err => err)
}

export default new Router({
  mode: 'history', // 去掉url中的#
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})
