package com.ruoyi.common.core.utils.html;

public class HtmlUtil {
	private String html;
	public String getHtml() {
		return html;
	}

	public void setHtml(String html) {
		this.html = html;
	}

	@Override
	public String toString() {
		return "HtmlUtils [html=" + html + "]";
	}
	public static String getLable(String title,String content){
		String agreementcontent = "<!DOCTYPE html>";
		agreementcontent += "<html>";
		agreementcontent += "<head>";
		agreementcontent += "<meta charset="+"UTF-8"+">";
		agreementcontent += "<meta name=\" viewport\" content=\"width=device-width   initial-scale=1.0  minimum-scale=1.0  miximum=1.0  user-scalable=yes\"/>";
		agreementcontent +=	"<link href=\"https://inatsk.oss-cn-beijing.aliyuncs.com/base/base.css\" rel=\"stylesheet\">";
		agreementcontent += "<title>"+title+"</title>";
		agreementcontent += "</head>";
		agreementcontent += "<body>";
		agreementcontent += "<div class=ql-editor>";
		agreementcontent+=content;
		agreementcontent += "</div>";
		agreementcontent += "</body>";
		agreementcontent += "</html>";
		return agreementcontent;
	}
	public static String getVideo(String title,String video){
		String agreementcontent = "<!DOCTYPE html>";
		agreementcontent += "<html>";
		agreementcontent += "<head>";
		agreementcontent += "<meta charset="+"UTF-8"+">";
		agreementcontent += "<meta name=\" viewport\" content=\"width=device-width   initial-scale=1.0  minimum-scale=1.0  miximum=1.0  user-scalable=yes\"/>";
		agreementcontent +=	"<link href=\"https://inatsk.oss-cn-beijing.aliyuncs.com/base/base.css\" rel=\"stylesheet\">";
		agreementcontent += "<title>"+title+"</title>";
		agreementcontent += "</head>";
		agreementcontent += "<body>";
		agreementcontent += "<div class=ql-editor>";
		agreementcontent += "<iframe class=\"ql-video\" frameborder=\"0\" allowfullscreen=\"true\" src=\"";
		agreementcontent+=video;
		agreementcontent += "\"></iframe>";
		agreementcontent += "</div>";
		agreementcontent += "</body>";
		agreementcontent += "</html>";
		return agreementcontent;
	}
}
