<p align="center">
	<img alt="logo" src="https://portrait.gitee.com/uploads/avatars/user/173/520646_iNatsk_1639380892.png!avatar200">
</p>
<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">YunLv ERP v1.0.1</h1>
<h4 align="center">基于若依扩展 Vue/Element UI 和 Spring Boot/Spring Cloud & Alibaba 前后端分离的分布式微服务架构</h4>

## 平台简介

云侣ERP是一套全部开源的ERP管理系统。 我们将持续升级，持续完善，欢迎友友们收藏和点赞 。
  感谢若依
* 后端采用Spring Boot、Spring Cloud & Alibaba。
* 注册中心、配置中心选型Nacos，权限认证使用Redis。
* 文件上传使用了minio高性能对象存储。
* 提供了技术栈（[Vue3](https://v3.cn.vuejs.org) [Element Plus](https://element-plus.org/zh-CN) [Vite](https://cn.vitejs.dev)
  ）保持同步更新。
  
## 系统模块

~~~
com.ruoyi     
├── ruoyi-ui              // 前端框架 [80]
        └── src           // 前端页面代码
            └── api       // ajax请求封装 
            └── views     // 页面代码
              └── erp     // erp基础数据,商品,采购,销售模块
              └── system  // 系统模块前端代码
├── ruoyi-gateway         // 网关模块 [8080]
├── ruoyi-auth            // 认证中心 [9200]
├── ruoyi-api             // 接口模块
│       └── ruoyi-api-system                          // 系统接口
├── ruoyi-common          // 通用模块
│       └── ruoyi-common-core                         // 核心模块
│       └── ruoyi-common-datascope                    // 权限范围
│       └── ruoyi-common-datasource                   // 多数据源
│       └── ruoyi-common-log                          // 日志记录
│       └── ruoyi-common-redis                        // 缓存服务
│       └── ruoyi-common-seata                        // 分布式事务
│       └── ruoyi-common-security                     // 安全模块
│       └── ruoyi-common-swagger                      // 系统接口
├── ruoyi-modules         // 业务模块
│       └── ruoyi-system                              // 系统模块 [9201]
│       └── ruoyi-gen                                 // 代码生成 [9202]
│       └── ruoyi-job                                 // 定时任务 [9203]
│       └── ruoyi-file                                // 文件服务 [9300]
├── ruoyi-visual          // 图形化管理模块
│       └── ruoyi-visual-monitor                      // 监控中心 [9100]
├──pom.xml                // 公共依赖
~~~

## 架构图

<img src="https://oscimg.oschina.net/oscnet/up-82e9722ecb846786405a904bafcf19f73f3.png"/>

## 内置功能

1. 基础信息： 对erp系统进行计量单位，客户管理，供应商管理，仓库管理等进行基础信息维护。
2. 商品管理： 对erp系统进行商品的基础信息，品牌，分类等进行维护。
3. 销售管理： 对erp系统进行销售单的维护。
4. 采购管理： 对erp系统进行采购单的维护。
5. 库存管理： 对erp系统商品出库入库进行查看，以及进行库存盘点。
6. 财务管理： 对erp系统的应收应付账款进行查看以及收款付款的操作。
7. 用户管理：用户是系统操作者，该功能主要完成系统用户配置。
8. 部门管理：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。
9. 岗位管理：配置系统用户所属担任职务。
10. 菜单管理：配置系统菜单，操作权限，按钮权限标识等。
11. 角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
12. 字典管理：对系统中经常使用的一些较为固定的数据进行维护。
13. 参数管理：对系统动态配置常用参数。
14. 通知公告：系统通知公告信息发布维护。
15. 操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
16. 登录日志：系统登录日志记录查询包含登录异常。
17. 在线用户：当前系统中活跃用户状态监控。
18. 定时任务：在线（添加、修改、删除)任务调度包含执行结果日志。
19. 代码生成：前后端代码的生成（java、html、xml、sql）支持CRUD下载 。
20. 系统接口：根据业务代码自动生成相关的api接口文档。
21. 服务监控：监视当前系统CPU、内存、磁盘、堆栈等相关信息。
22. 在线构建器：拖动表单元素生成相应的HTML代码。
23. 连接池监视：监视当前系统数据库连接池状态，可进行分析SQL找出系统性能瓶颈。

## 关于我们
我们擅长前端开发、后端架构，有一颗热爱开源的心，致力于打造企业级的通用产品设计，未来将持续关注前沿技术，持续推出高质量的开源产品。</br>
 官方QQ群：718608373 使用问题请入群由专人负责简答 </br>
 承接各种XX软件和APP定制开发，请让我发挥专业的技能（请对我有信心，超出你的期待哦）具体私聊
 
## 演示图

<table>
    <tr>
        <td><img src="https://gitee.com/iNatsk/yunlv-erp/raw/master/img/index.png"/></td>
        <td><img src="https://gitee.com/iNatsk/yunlv-erp/raw/master/img/homePage.png"/></td>
    </tr> 
    <tr>
        <td><img src="https://gitee.com/iNatsk/yunlv-erp/raw/master/img/basicInformation.png"/></td>
        <td><img src="https://gitee.com/iNatsk/yunlv-erp/raw/master/img/basicInformation(1).png"/></td>
    </tr> 
     <tr>
        <td><img src="https://gitee.com/iNatsk/yunlv-erp/raw/master/img/good.png"/></td>
        <td><img src="https://gitee.com/iNatsk/yunlv-erp/raw/master/img/sale.png"/></td>
    </tr> 
    <tr>
        <td><img src="https://gitee.com/iNatsk/yunlv-erp/raw/master/img/deliver.png"/></td>
        <td><img src="https://gitee.com/iNatsk/yunlv-erp/raw/master/img/purchase.png"/></td>
    </tr> 
    <tr>
        <td><img src="https://gitee.com/iNatsk/yunlv-erp/raw/master/img/inventory.png"/></td>
        <td><img src="https://gitee.com/iNatsk/yunlv-erp/raw/master/img/receiptDoc.png"/></td>
    </tr> 
    <tr>
        <td><img src="https://gitee.com/iNatsk/yunlv-erp/raw/master/img/copeWith.png"/></td>
        <td><img src="https://gitee.com/iNatsk/yunlv-erp/raw/master/img/allocation.png"/></td>
    </tr>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-8074972883b5ba0622e13246738ebba237a.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-9f88719cdfca9af2e58b352a20e23d43b12.png"/></td>
    </tr>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-39bf2584ec3a529b0d5a3b70d15c9b37646.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-4148b24f58660a9dc347761e4cf6162f28f.png"/></td>
    </tr>
	<tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-b2d62ceb95d2dd9b3fbe157bb70d26001e9.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-d67451d308b7a79ad6819723396f7c3d77a.png"/></td>
    </tr>	 
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/5e8c387724954459291aafd5eb52b456f53.jpg"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/644e78da53c2e92a95dfda4f76e6d117c4b.jpg"/></td>
    </tr>
	<tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-8370a0d02977eebf6dbf854c8450293c937.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-49003ed83f60f633e7153609a53a2b644f7.png"/></td>
    </tr>
	<tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-d4fe726319ece268d4746602c39cffc0621.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-c195234bbcd30be6927f037a6755e6ab69c.png"/></td>
    </tr>
</table>

## 环境部署

环境要求

* JDK >= 1.8
* MySQL >= 5.7
* Maven >= 3.0
* Node >= 12
* Redis >= 3
    
后端运行
1、前往Gitee下载页面(https://gitee.com/iNatsk/yunlv-erp)下载解压到工作目录。
2、使用idea打开工程，idea会自动加载Maven依赖包。
3、创建数据库erp_system并导入数据脚本erp_system.sql。
4、创建数据库ry-config并导入数据脚本ry-config_20220929.sql。
5、配置nacos持久化，修改conf/application.properties文件，增加支持mysql数据源配置。
6、启动服务RuoYiGenApplication（代码生成可选）,RuoYiJobApplication,（定时任务可选）,RuoYFileApplication（文件服务可选）。

前端运行
1、cd ruoyi-ui 进入项目目录
2、npm install --registry=https://registry.npmmirror.com 安装依赖，强烈建议不要用直接使用 cnpm 安装，会有各种诡异的 bug，可以通过重新指定 registry 来解决 npm 安装速度慢的问题。
3、npm run dev 本地开发 启动项目


