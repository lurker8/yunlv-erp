/*
 Navicat Premium Data Transfer

 Target Server Type    : MySQL
 Target Server Version : 80018
 File Encoding         : 65001

 Date: 24/11/2022 17:25:45
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for erp_allocation
-- ----------------------------
DROP TABLE IF EXISTS `erp_allocation`;
CREATE TABLE `erp_allocation`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '调拨id',
  `allocation_order` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '调拨单号',
  `lssue_warehouse` int(11) NULL DEFAULT NULL COMMENT '出库仓库',
  `warehousing_id` int(11) NULL DEFAULT NULL COMMENT '入库仓库id',
  `creat_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
  `type` int(11) NULL DEFAULT NULL COMMENT '调拨状态（1未审核，0已调拨）',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '完成时间',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 48 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '库存调拨' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for erp_allocation_goods
-- ----------------------------
DROP TABLE IF EXISTS `erp_allocation_goods`;
CREATE TABLE `erp_allocation_goods`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '库存调拨商品关联id',
  `allocation_id` int(11) NULL DEFAULT NULL COMMENT '调拨id',
  `good_id` int(11) NULL DEFAULT NULL COMMENT '商品id',
  `good_number` int(11) NULL DEFAULT NULL COMMENT '商品数量',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 56 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '仓库调拨商品关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for erp_brand
-- ----------------------------
DROP TABLE IF EXISTS `erp_brand`;
CREATE TABLE `erp_brand`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '品牌名称',
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '图标',
  `create_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人id',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 76 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '品牌管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for erp_category
-- ----------------------------
DROP TABLE IF EXISTS `erp_category`;
CREATE TABLE `erp_category`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '分类ID',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '分类名称',
  `parent_id` int(11) NULL DEFAULT NULL COMMENT '上级id',
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '分类图标',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 118 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '分类' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for erp_collection
-- ----------------------------
DROP TABLE IF EXISTS `erp_collection`;
CREATE TABLE `erp_collection`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `sale_order_num` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '销售单号',
  `pay_method` int(11) NULL DEFAULT NULL COMMENT '付款方式',
  `operator` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `sale_id` int(11) NULL DEFAULT NULL COMMENT '销售id',
  `amount_receivable` decimal(11, 2) NULL DEFAULT NULL COMMENT '应收金额',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '应收账款' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for erp_collection_record
-- ----------------------------
DROP TABLE IF EXISTS `erp_collection_record`;
CREATE TABLE `erp_collection_record`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `sale_id` int(11) NULL DEFAULT NULL COMMENT '销售id',
  `collection_time` datetime(0) NULL DEFAULT NULL COMMENT '收款时间',
  `amount_collected` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '收款金额',
  `payee` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '收款人',
  `collection_voucher` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '收款凭证',
  `collection_remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '收款备注',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `collected_account` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '收款账户',
  `collection_id` int(11) NULL DEFAULT NULL COMMENT '收款账单id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '收款记录表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for erp_company
-- ----------------------------
DROP TABLE IF EXISTS `erp_company`;
CREATE TABLE `erp_company`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '单位id',
  `company` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '计量单位',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '计量单位表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for erp_cope_with
-- ----------------------------
DROP TABLE IF EXISTS `erp_cope_with`;
CREATE TABLE `erp_cope_with`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `po_no` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '采购单号',
  `payment_type` int(11) NULL DEFAULT NULL COMMENT '付款类型(0应付账款1现金付款2预付款)',
  `operator` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `purchase_id` int(11) NULL DEFAULT NULL COMMENT '采购id',
  `amount_payable` decimal(11, 2) NULL DEFAULT NULL COMMENT '应付金额',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '应付账款' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for erp_customer
-- ----------------------------
DROP TABLE IF EXISTS `erp_customer`;
CREATE TABLE `erp_customer`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '客户id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '客户name',
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户电话',
  `customer_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户地址',
  `customer_category` int(11) NULL DEFAULT NULL COMMENT '客户等级',
  `customer_bank` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户开户银行',
  `customer_bank_account` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户银行卡号',
  `remarks` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '备注信息',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_user` int(11) NULL DEFAULT NULL COMMENT '创建人id',
  `province` int(11) NULL DEFAULT NULL COMMENT '省',
  `city` int(11) NULL DEFAULT NULL COMMENT '市',
  `area` int(11) NULL DEFAULT NULL COMMENT '区',
  `detailed_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '详细地址',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '客户管理表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for erp_fund_account
-- ----------------------------
DROP TABLE IF EXISTS `erp_fund_account`;
CREATE TABLE `erp_fund_account`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '资金账户id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '资金账户name',
  `account_bank` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '银行开户行',
  `bank_card` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '银行卡号',
  `start_balance` decimal(20, 3) NULL DEFAULT NULL COMMENT '期初余额',
  `capital_balance` decimal(20, 3) NULL DEFAULT NULL COMMENT '资金余额',
  `opening_time` datetime(0) NULL DEFAULT NULL COMMENT '开户时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_user` int(11) NULL DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '资金账户管理表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for erp_good_label
-- ----------------------------
DROP TABLE IF EXISTS `erp_good_label`;
CREATE TABLE `erp_good_label`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `label_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '唯一标识',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标签名称',
  `create_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品标签' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for erp_good_sale_warehouse
-- ----------------------------
DROP TABLE IF EXISTS `erp_good_sale_warehouse`;
CREATE TABLE `erp_good_sale_warehouse`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `warehouse_id` int(11) NULL DEFAULT NULL COMMENT '仓库id',
  `sale_id` int(11) NULL DEFAULT NULL COMMENT '销售单号id',
  `sale_good_id` int(11) NULL DEFAULT NULL COMMENT '销售商品id',
  `sale_good_number` int(11) NULL DEFAULT NULL COMMENT '销售商品数量',
  `return_good_number` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '退货商品数量',
  `return_good_bonus` decimal(11, 2) UNSIGNED NULL DEFAULT 0.00 COMMENT '退货商品金额',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 227 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '销售商品仓库关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for erp_goods
-- ----------------------------
DROP TABLE IF EXISTS `erp_goods`;
CREATE TABLE `erp_goods`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '商品名称',
  `thumbnail` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '缩略图',
  `banners` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '轮播图：json形式多张',
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '商品描述',
  `sort_description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '简要描述',
  `price` decimal(10, 0) NULL DEFAULT NULL COMMENT '商品价格（单位：分）',
  `brand_id` int(11) NULL DEFAULT NULL COMMENT '品牌id',
  `category_id` int(11) NULL DEFAULT NULL COMMENT '分类id',
  `cost` decimal(10, 0) NULL DEFAULT NULL COMMENT '成本价格（单位：分）',
  `details` mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '商品详情',
  `labels` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标签',
  `inventory` int(11) NULL DEFAULT NULL COMMENT '库存',
  `sales` int(255) NULL DEFAULT 0 COMMENT '销量',
  `weight` double NULL DEFAULT NULL COMMENT '重量(克)',
  `shelves` int(11) NULL DEFAULT 1 COMMENT '上下架（1上架0下架）',
  `company` int(11) NULL DEFAULT NULL COMMENT '单位id',
  `shop_id` bigint(20) NULL DEFAULT NULL COMMENT '店铺ID',
  `sort` int(11) NULL DEFAULT NULL COMMENT '默认排序（后台设置）',
  `create_ty` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `bar_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '条形码（预留字段）',
  `specList` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '（预留）规格列表',
  `specLabelList` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '（预留）属性列表',
  `inventory_threshold` int(11) NULL DEFAULT NULL COMMENT '库存阈值',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `name`(`name`) USING BTREE,
  INDEX `category_id`(`category_id`) USING BTREE,
  INDEX `labels`(`labels`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 301 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for erp_goods_purchase_warehouse
-- ----------------------------
DROP TABLE IF EXISTS `erp_goods_purchase_warehouse`;
CREATE TABLE `erp_goods_purchase_warehouse`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `warehouse_id` int(11) NULL DEFAULT NULL COMMENT '仓库id',
  `purchase_id` int(11) NULL DEFAULT NULL COMMENT '采购订单id',
  `purchase_good_id` int(11) NULL DEFAULT NULL COMMENT '采购商品id',
  `purchase_good_number` int(11) NULL DEFAULT NULL COMMENT '入库商品数量',
  `return_good_number` int(11) NULL DEFAULT NULL COMMENT '退货商品数量',
  `return_good_bonus` decimal(11, 2) NULL DEFAULT NULL COMMENT '退货商品金额',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 205 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '采购商品仓库关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for erp_goods_warehouse
-- ----------------------------
DROP TABLE IF EXISTS `erp_goods_warehouse`;
CREATE TABLE `erp_goods_warehouse`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `good_id` int(11) NULL DEFAULT NULL COMMENT '商品id',
  `warehouse_id` int(11) NULL DEFAULT NULL COMMENT '仓库id',
  `warehouse_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '仓库name',
  `goods_number` int(11) NULL DEFAULT NULL COMMENT '库存数量',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 35 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品仓库管理表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for erp_inventory
-- ----------------------------
DROP TABLE IF EXISTS `erp_inventory`;
CREATE TABLE `erp_inventory`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '盘点id',
  `Inventory_order` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '盘点单号',
  `warehouse_id` int(11) NULL DEFAULT NULL COMMENT '仓库id',
  `Inventory_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '盘点人',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `type` int(11) NULL DEFAULT NULL COMMENT '盘点状态',
  `create_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '库存盘点表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for erp_inventory_goods
-- ----------------------------
DROP TABLE IF EXISTS `erp_inventory_goods`;
CREATE TABLE `erp_inventory_goods`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '盘点商品关联表id',
  `inventory_id` int(11) NULL DEFAULT NULL COMMENT '盘点id',
  `good_id` int(11) NULL DEFAULT NULL COMMENT '商品id',
  `before_num` int(11) NULL DEFAULT NULL COMMENT '盘点前数量',
  `after_num` int(11) NULL DEFAULT NULL COMMENT '盘点后数量',
  `creat_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '库存盘点商品表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for erp_loss_reporting
-- ----------------------------
DROP TABLE IF EXISTS `erp_loss_reporting`;
CREATE TABLE `erp_loss_reporting`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '报损单id',
  `good_id` int(11) NULL DEFAULT NULL COMMENT '商品id',
  `warehouse_id` int(11) NULL DEFAULT NULL COMMENT '仓库id',
  `damage_num` int(11) NULL DEFAULT NULL COMMENT '损坏数量',
  `create_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `type` int(11) NULL DEFAULT NULL COMMENT '报损状态',
  `reason` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '报损原因',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '报损单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for erp_lssue
-- ----------------------------
DROP TABLE IF EXISTS `erp_lssue`;
CREATE TABLE `erp_lssue`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '出库id',
  `Issue_order` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '出库单号',
  `lssue_amount` decimal(11, 2) NULL DEFAULT NULL COMMENT '出库金额',
  `lssue_warehouse` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '出库仓库',
  `lssue_time` datetime(0) NULL DEFAULT NULL COMMENT '出库时间',
  `create_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作人',
  `sale_id` int(11) NULL DEFAULT NULL COMMENT '销售id',
  `is_sale_allocation` int(11) NULL DEFAULT NULL COMMENT '0销售订单1调拨订单',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 40 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '出库单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for erp_payment_record
-- ----------------------------
DROP TABLE IF EXISTS `erp_payment_record`;
CREATE TABLE `erp_payment_record`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `purchase_id` int(11) NULL DEFAULT NULL COMMENT '采购id',
  `payment_time` datetime(0) NULL DEFAULT NULL COMMENT '付款时间',
  `amount_payment` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '付款金额',
  `drawee` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '付款人',
  `payment_voucher` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '付款凭证',
  `payment_remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '付款备注',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `payment_account` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '付款账户',
  `cope_with_id` int(11) NULL DEFAULT NULL COMMENT '付款账单id',
  `payment_account_id` int(11) NULL DEFAULT NULL COMMENT '付款账户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '付款记录表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for erp_purchase
-- ----------------------------
DROP TABLE IF EXISTS `erp_purchase`;
CREATE TABLE `erp_purchase`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '资金账户id',
  `po_no` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '采购单号',
  `supplier_id` int(11) NULL DEFAULT NULL COMMENT '供应商id',
  `payment_type` int(11) NULL DEFAULT NULL COMMENT '付款类型(0应付账款1现金付款2预付款)',
  `warehousing_type` int(11) NULL DEFAULT NULL COMMENT '入库类型(停用)',
  `return_goods` int(11) NULL DEFAULT NULL COMMENT '退货状态(0有1无)',
  `supplieruser` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '供应商联系人',
  `supplierphone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '供应商手机号',
  `process_type` int(11) NULL DEFAULT NULL COMMENT '审核类型（0已审核，1未审核，3采购入库，3确认入库）',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 46 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '采购订单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for erp_purchase_goods
-- ----------------------------
DROP TABLE IF EXISTS `erp_purchase_goods`;
CREATE TABLE `erp_purchase_goods`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `purchase_id` int(11) NULL DEFAULT NULL COMMENT '采购订单Id',
  `goods_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品编号',
  `goods_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品名称',
  `goods_specs` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品规格',
  `company` int(11) NULL DEFAULT NULL COMMENT '商品单位',
  `monovalent` double(20, 2) NULL DEFAULT NULL COMMENT '采购单价',
  `quantity` int(11) NULL DEFAULT NULL COMMENT '采购数量',
  `taxes` double(20, 2) NULL DEFAULT NULL COMMENT '税金',
  `total` double(20, 2) NULL DEFAULT NULL COMMENT '采购总价',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `categoryName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品分类名称',
  `brandName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '品牌名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 111 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '采购商品' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for erp_purchase_invoice
-- ----------------------------
DROP TABLE IF EXISTS `erp_purchase_invoice`;
CREATE TABLE `erp_purchase_invoice`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `purchase_order_num` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '采购单号',
  `shipment_num` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '入库单号',
  `purchase_id` int(11) NULL DEFAULT NULL COMMENT '订单id',
  `supplier_id` int(11) NULL DEFAULT NULL COMMENT '供应商id',
  `payment_type` int(11) NULL DEFAULT NULL COMMENT '付款类型(0应付账款1现金付款2预付款)',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `is_return` int(11) NULL DEFAULT NULL COMMENT '有无退货(0有1无)',
  `supplieruser` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '供应商联系人',
  `supplierphone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '供应商手机号',
  `totalPrice` double(20, 2) NULL DEFAULT NULL COMMENT '采购总价',
  `isWarehousing` int(11) NULL DEFAULT NULL COMMENT '是否入库(0是1否)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '采购入库单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for erp_purchase_return
-- ----------------------------
DROP TABLE IF EXISTS `erp_purchase_return`;
CREATE TABLE `erp_purchase_return`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `purchase_id` int(11) NULL DEFAULT NULL COMMENT '订单id',
  `purchase_order_num` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单单号',
  `shipment_num` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '入库单号',
  `return_bonus` decimal(13, 2) NULL DEFAULT NULL COMMENT '退货总额',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `is_return` int(11) NULL DEFAULT NULL COMMENT '是否退货（0是1否）',
  `supplier_id` int(11) NULL DEFAULT NULL COMMENT '供应商id',
  `payment_type` int(11) NULL DEFAULT NULL COMMENT '付款类型(0应付账款1现金付款2预付款)',
  `supplieruser` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '供应商联系人',
  `supplierphone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '供应商手机号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '采购退货表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for erp_receipt_doc
-- ----------------------------
DROP TABLE IF EXISTS `erp_receipt_doc`;
CREATE TABLE `erp_receipt_doc`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '入库id',
  `receipt_doc_order` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '入库单号',
  `receipt_doc_amount` decimal(11, 2) NULL DEFAULT NULL COMMENT '入库金额',
  `receipt_doc_warehouse` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '入库仓库',
  `create_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作人',
  `purchaseId` int(11) NULL DEFAULT NULL COMMENT '采购id',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '出库时间',
  `isSaleAllocation` int(11) NULL DEFAULT NULL COMMENT '出库单类型',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '入库单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for erp_sale
-- ----------------------------
DROP TABLE IF EXISTS `erp_sale`;
CREATE TABLE `erp_sale`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `sale_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '销售单号',
  `customer_id` int(11) NULL DEFAULT NULL COMMENT '客户id',
  `payment_method` int(11) NULL DEFAULT NULL COMMENT '收款方式id',
  `customer_phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户手机',
  `fixed_telephone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '固定电话',
  `customer_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户地址',
  `province` int(11) NULL DEFAULT NULL COMMENT '省',
  `city` int(11) NULL DEFAULT NULL COMMENT '市',
  `area` int(11) NULL DEFAULT NULL COMMENT '区',
  `detailed_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '详细地址',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `sale_bonus` decimal(11, 2) NULL DEFAULT NULL COMMENT '销售总额',
  `customer_contact` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `type` int(11) NULL DEFAULT NULL COMMENT '状态(0已确认1未确认)',
  `return_goods` int(11) NULL DEFAULT NULL COMMENT '是否退货（0有退货 1 未退货）',
  `deliver_goods` int(11) NULL DEFAULT NULL COMMENT '是否发货（0已发货，1确认收货）',
  `taxes` decimal(11, 2) NULL DEFAULT NULL COMMENT '税金',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '销售订单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for erp_sale_goods
-- ----------------------------
DROP TABLE IF EXISTS `erp_sale_goods`;
CREATE TABLE `erp_sale_goods`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `sale_id` int(11) NULL DEFAULT NULL COMMENT '采购订单Id',
  `goods_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品编号',
  `goods_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品名称',
  `goods_specs` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品规格',
  `company` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品单位',
  `monovalent` double(20, 2) NULL DEFAULT NULL COMMENT '销售单价',
  `quantity` int(11) NULL DEFAULT NULL COMMENT '销售数量',
  `taxes` double(20, 2) NULL DEFAULT NULL COMMENT '税金',
  `one_good_price` double(20, 2) NULL DEFAULT NULL COMMENT '销售总价',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_user` int(11) NULL DEFAULT NULL COMMENT '创建人id',
  `warehouse_id` int(11) NULL DEFAULT NULL COMMENT '发货仓库id',
  `return_amount` decimal(11, 2) NULL DEFAULT NULL COMMENT '退货金额',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 179 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '销售商品' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for erp_sales_invoice
-- ----------------------------
DROP TABLE IF EXISTS `erp_sales_invoice`;
CREATE TABLE `erp_sales_invoice`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `sale_order_num` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '销售单号',
  `shipment_num` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发货单号',
  `sale_id` int(11) NULL DEFAULT NULL COMMENT '订单id',
  `customer_id` int(11) NULL DEFAULT NULL COMMENT '客户id',
  `payment_method` int(11) NULL DEFAULT NULL COMMENT '收款方式id',
  `customer_phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户电话',
  `fixed_telephone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '销售电话',
  `sale_bonus` decimal(13, 2) NULL DEFAULT NULL COMMENT '销售总额',
  `type` int(11) NULL DEFAULT NULL COMMENT '状态',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `is_return` int(11) NULL DEFAULT NULL COMMENT '有无退货',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 54 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '销售发货单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for erp_sales_return
-- ----------------------------
DROP TABLE IF EXISTS `erp_sales_return`;
CREATE TABLE `erp_sales_return`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `sale_id` int(11) NULL DEFAULT NULL COMMENT '销售id',
  `sale_order_num` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '销售单号',
  `shipment_num` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发货单号',
  `customer_id` int(11) NULL DEFAULT NULL COMMENT '客户id',
  `payment_method` int(11) NULL DEFAULT NULL COMMENT '收款方式id',
  `return_bonus` decimal(13, 2) NULL DEFAULT NULL COMMENT '退货总额',
  `customer_phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户电话',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `is_return` int(11) NULL DEFAULT NULL COMMENT '是否退货入库（0是1否）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '销售退货表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for erp_supplier
-- ----------------------------
DROP TABLE IF EXISTS `erp_supplier`;
CREATE TABLE `erp_supplier`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '供应商id',
  `supplier_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '供应商name',
  `supplier_contacts` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系人name',
  `supplier_phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系人电话',
  `supplier_category` int(11) NULL DEFAULT NULL COMMENT '供应商类别id',
  `supplier_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '供应商地址',
  `supplier_level` int(11) NULL DEFAULT NULL COMMENT '供应商等级',
  `supplier_bank` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '供应商开户银行',
  `supplier_bank_account` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '供应商银行账户',
  `supplier_duty_paragraph` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '供应商税号',
  `Remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  `creat_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_user` int(11) NULL DEFAULT NULL COMMENT '创建人id',
  `province` int(11) NULL DEFAULT NULL COMMENT '省id',
  `city` int(11) NULL DEFAULT NULL COMMENT '市id',
  `area` int(11) NULL DEFAULT NULL COMMENT '区id',
  `detailed_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '详细地址',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '供应商管理表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for erp_warehouse
-- ----------------------------
DROP TABLE IF EXISTS `erp_warehouse`;
CREATE TABLE `erp_warehouse`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '仓库id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '仓库name',
  `warehouse_administrators` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '仓库管理员',
  `administrators_phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '管理员电话',
  `warehouse_phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '仓库电话',
  `warehouse_category` int(11) NULL DEFAULT NULL COMMENT '仓库类别',
  `warehouse_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '仓库地址',
  `remarks` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '备注',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_user` int(11) NULL DEFAULT NULL COMMENT '创建人',
  `position` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '仓位(预留)',
  `province` int(11) NULL DEFAULT NULL COMMENT '省',
  `city` int(11) NULL DEFAULT NULL COMMENT '市',
  `area` int(11) NULL DEFAULT NULL COMMENT '区',
  `detailed_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '详细地址',
  `goodsId` int(11) NULL DEFAULT NULL COMMENT '商品Id(给采购入库用)',
  `goodsName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品名称(给采购入库用)',
  `warehousingQuantity` int(11) NULL DEFAULT NULL COMMENT '入库数量(给采购入库用)',
  `warehousingReturnQuantity` int(11) NULL DEFAULT NULL COMMENT '入库总数量(给采购入库用)',
  `warehousingId` int(11) NULL DEFAULT NULL COMMENT '修改入库id(给采购入库用)',
  `goodsMonovalent` double(25, 2) NULL DEFAULT NULL COMMENT '商品单价(给采购入库用)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '仓库管理表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 36 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '代码生成业务表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES (1, 'erp_customer', '客户管理表', NULL, NULL, 'ErpCustomer', 'crud', 'com.ruoyi.system', 'erp', 'customer', '客户管理', '刘策', '0', '/', '{\"parentMenuId\":\"1\"}', 'admin', '2022-10-19 16:14:26', '', '2022-10-24 17:14:44', NULL);
INSERT INTO `gen_table` VALUES (2, 'erp_fund_account', '资金账户管理表', NULL, NULL, 'ErpFundAccount', 'crud', 'com.ruoyi.system', 'system', 'account', '资金账户管理', 'ruoyi', '0', '/', '{\"parentMenuId\":1}', 'admin', '2022-10-19 16:14:27', '', '2022-10-26 11:27:43', NULL);
INSERT INTO `gen_table` VALUES (3, 'erp_supplier', '供应商管理表', NULL, NULL, 'ErpSupplier', 'crud', 'com.ruoyi.system', 'system', 'supplier', '供应商管理', '刘策', '0', '/', '{\"parentMenuId\":1}', 'admin', '2022-10-19 16:14:27', '', '2022-10-25 13:04:00', NULL);
INSERT INTO `gen_table` VALUES (4, 'erp_warehouse', '仓库管理表', NULL, NULL, 'ErpWarehouse', 'crud', 'com.ruoyi.system', 'system', 'warehouse', '仓库管理', '刘策', '0', '/', '{\"parentMenuId\":1}', 'admin', '2022-10-19 16:14:28', '', '2022-10-26 09:20:26', NULL);
INSERT INTO `gen_table` VALUES (9, 'erp_goods', '商品信息', NULL, NULL, 'ErpGoods', 'crud', 'com.ruoyi.system', 'system', 'goods', '商品信息', '刘策', '0', '/', '{\"parentMenuId\":1}', 'admin', '2022-10-26 14:03:50', '', '2022-10-26 14:04:17', NULL);
INSERT INTO `gen_table` VALUES (11, 'erp_brand', '品牌管理', NULL, NULL, 'ErpBrand', 'crud', 'com.ruoyi.system', 'system', 'brand', '品牌管理', '刘策', '0', '/', '{\"parentMenuId\":1}', 'admin', '2022-10-26 16:21:38', '', '2022-10-26 16:22:14', NULL);
INSERT INTO `gen_table` VALUES (12, 'erp_category', '分类', NULL, NULL, 'ErpCategory', 'crud', 'com.ruoyi.system', 'system', 'category', '分类', '刘策', '0', '/', '{\"parentMenuId\":1}', 'admin', '2022-10-26 17:26:26', '', '2022-10-26 17:26:56', NULL);
INSERT INTO `gen_table` VALUES (13, 'erp_good_label', '商品标签', NULL, NULL, 'ErpGoodLabel', 'crud', 'com.ruoyi.system', 'system', 'label', '商品标签', 'ruoyi', '0', '/', '{\"parentMenuId\":1}', 'admin', '2022-10-27 15:44:38', '', '2022-10-27 15:45:12', NULL);
INSERT INTO `gen_table` VALUES (14, 'erp_purchase', '采购订单', NULL, NULL, 'ErpPurchase', 'crud', 'com.ruoyi.system', 'system', 'purchase', '采购订单', 'ruoyi', '0', '/', '{\"parentMenuId\":\"2030\"}', 'admin', '2022-10-27 16:05:23', '', '2022-10-31 15:41:43', NULL);
INSERT INTO `gen_table` VALUES (15, 'erp_purchase_goods', '采购商品', NULL, NULL, 'ErpPurchaseGoods', 'crud', 'com.ruoyi.system', 'system', 'purchaseoods', '采购商品', 'ruoyi', '0', '/', '{}', 'admin', '2022-10-27 16:05:23', '', '2022-10-31 16:00:23', NULL);
INSERT INTO `gen_table` VALUES (16, 'erp_sale', '销售订单表', NULL, NULL, 'ErpSale', 'crud', 'com.ruoyi.system', 'system', 'sale', '销售订单', '刘策', '0', '/', '{\"parentMenuId\":\"1\"}', 'admin', '2022-10-28 16:41:12', '', '2022-10-31 09:23:37', NULL);
INSERT INTO `gen_table` VALUES (17, 'erp_sale_goods', '销售商品', NULL, NULL, 'ErpSaleGoods', 'crud', 'com.ruoyi.system', 'system', 'goods', '销售商品', 'liuce', '0', '/', '{}', 'admin', '2022-10-28 16:41:13', '', '2022-10-31 16:50:47', NULL);
INSERT INTO `gen_table` VALUES (18, 'erp_company', '计量单位表', NULL, NULL, 'ErpCompany', 'crud', 'com.ruoyi.system', 'system', 'company', 'company ', '刘策', '0', '/', '{\"parentMenuId\":\"2055\"}', 'admin', '2022-11-01 09:02:02', '', '2022-11-01 09:03:04', NULL);
INSERT INTO `gen_table` VALUES (19, 'erp_goods_warehouse', '商品仓库管理表', NULL, NULL, 'ErpGoodsWarehouse', 'crud', 'com.ruoyi.system', 'system', 'goodsWarehouse', '商品仓库管理', '刘策', '0', '/', '{\"parentMenuId\":\"2055\"}', 'admin', '2022-11-02 15:27:41', '', '2022-11-22 09:19:49', NULL);
INSERT INTO `gen_table` VALUES (20, 'erp_sales_invoice', '销售发货单', NULL, NULL, 'ErpSalesInvoice', 'crud', 'com.ruoyi.system', 'system', 'invoice', '销售发货单', 'liuce', '0', '/', '{\"parentMenuId\":2063}', 'admin', '2022-11-07 10:14:37', '', '2022-11-07 10:15:22', NULL);
INSERT INTO `gen_table` VALUES (21, 'erp_sales_return', '', NULL, NULL, 'ErpSalesReturn', 'crud', 'com.ruoyi.system', 'system', 'return', NULL, 'ruoyi', '0', '/', NULL, 'admin', '2022-11-07 11:29:31', '', NULL, NULL);
INSERT INTO `gen_table` VALUES (22, 'erp_goods_purchase_warehouse', '采购商品仓库关联表', NULL, NULL, 'ErpGoodsPurchaseWarehouse', 'crud', 'com.ruoyi.system', 'system', 'goodsPurchaseWarehouse', '采购商品仓库关联', 'ruoyi', '0', '/', '{}', 'admin', '2022-11-10 10:52:43', '', '2022-11-10 10:56:31', NULL);
INSERT INTO `gen_table` VALUES (23, 'erp_collection', '应收账款', NULL, NULL, 'ErpCollection', 'crud', 'com.ruoyi.system', 'system', 'collection', '应收账款', 'ruoyi', '0', '/', '{\"parentMenuId\":2063}', 'admin', '2022-11-11 10:11:38', '', '2022-11-11 10:12:45', NULL);
INSERT INTO `gen_table` VALUES (24, 'erp_purchase_invoice', '采购入库单', NULL, NULL, 'ErpPurchaseInvoice', 'crud', 'com.ruoyi.system', 'system', 'invoice', '采购入库单', 'ruoyi', '0', '/', '{}', 'admin', '2022-11-11 15:12:13', '', '2022-11-11 15:12:49', NULL);
INSERT INTO `gen_table` VALUES (25, 'erp_collection_record', '收款记录表', NULL, NULL, 'ErpCollectionRecord', 'crud', 'com.ruoyi.system', 'system', 'record', '收款记录', '刘策', '0', '/', '{\"parentMenuId\":2055}', 'admin', '2022-11-16 10:16:12', '', '2022-11-16 10:16:41', NULL);
INSERT INTO `gen_table` VALUES (26, 'erp_purchase_return', '采购退货表', NULL, NULL, 'ErpPurchaseReturn', 'crud', 'com.ruoyi.system', 'system', 'returnPurchase', '采购退货', 'ruoyi', '0', '/', '{\"parentMenuId\":\"2030\"}', 'admin', '2022-11-16 15:18:31', '', '2022-11-16 15:20:04', NULL);
INSERT INTO `gen_table` VALUES (27, 'erp_lssue', '出库单', NULL, NULL, 'ErpLssue', 'crud', 'com.ruoyi.system', 'system', 'lssue', '出库单', 'liuce', '0', '/', '{\"parentMenuId\":2055}', 'admin', '2022-11-17 09:13:43', '', '2022-11-17 09:14:19', NULL);
INSERT INTO `gen_table` VALUES (28, 'erp_allocation', '库存调拨', NULL, NULL, 'ErpAllocation', 'crud', 'com.ruoyi.system', 'system', 'allocation', '库存调拨', 'licue', '0', '/', '{\"parentMenuId\":2114}', 'admin', '2022-11-17 15:21:56', '', '2022-11-17 15:24:09', NULL);
INSERT INTO `gen_table` VALUES (29, 'erp_allocation_goods', '仓库调拨商品关联表', NULL, NULL, 'ErpAllocationGoods', 'crud', 'com.ruoyi.system', 'system', 'goods', '仓库调拨商品关联', 'ruoyi', '0', '/', NULL, 'admin', '2022-11-17 15:30:27', '', NULL, NULL);
INSERT INTO `gen_table` VALUES (30, 'erp_receipt_doc', '入库单', NULL, NULL, 'ErpReceiptDoc', 'crud', 'com.ruoyi.system', 'system', 'receiptDoc', '入库单', 'ruoyi', '0', '/', '{\"parentMenuId\":2114}', 'admin', '2022-11-17 15:32:02', '', '2022-11-17 15:32:52', NULL);
INSERT INTO `gen_table` VALUES (31, 'erp_cope_with', '应付账款', NULL, NULL, 'ErpCopeWith', 'crud', 'com.ruoyi.system', 'system', 'copeWith', '应付账款', 'ruoyi', '0', '/', '{\"parentMenuId\":2095}', 'admin', '2022-11-18 10:14:05', '', '2022-11-18 10:15:44', NULL);
INSERT INTO `gen_table` VALUES (32, 'erp_payment_record', '付款记录表', NULL, NULL, 'ErpPaymentRecord', 'crud', 'com.ruoyi.system', 'system', 'paymentRecord', '付款记录', 'ruoyi', '0', '/', '{}', 'admin', '2022-11-18 13:54:15', '', '2022-11-18 16:40:12', NULL);
INSERT INTO `gen_table` VALUES (33, 'erp_inventory', '库存盘点表', NULL, NULL, 'ErpInventory', 'crud', 'com.ruoyi.system', 'system', 'inventory', '库存盘点', 'ruoyi', '0', '/', NULL, 'admin', '2022-11-21 11:37:55', '', NULL, NULL);
INSERT INTO `gen_table` VALUES (34, 'erp_inventory_goods', '库存盘点商品表', NULL, NULL, 'ErpInventoryGoods', 'crud', 'com.ruoyi.system', 'system', 'goods', '库存盘点商品', 'ruoyi', '0', '/', '{\"parentMenuId\":2114}', 'admin', '2022-11-21 13:24:28', '', '2022-11-21 13:24:45', NULL);
INSERT INTO `gen_table` VALUES (35, 'erp_loss_reporting', '报损单', NULL, NULL, 'ErpLossReporting', 'crud', 'com.ruoyi.system', 'system', 'reporting', '报损单', 'liuce', '0', '/', '{\"parentMenuId\":2114}', 'admin', '2022-11-21 16:25:43', '', '2022-11-21 16:26:04', NULL);

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
  `column_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 388 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '代码生成业务表字段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES (1, '1', 'id', '客户id', 'int(11)', 'Long', 'id', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-10-19 16:14:26', '', '2022-10-24 17:14:44');
INSERT INTO `gen_table_column` VALUES (2, '1', 'name', '客户名称', 'varchar(255)', 'String', 'name', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2022-10-19 16:14:26', '', '2022-10-24 17:14:44');
INSERT INTO `gen_table_column` VALUES (3, '1', 'phone', '客户电话', 'varchar(255)', 'String', 'phone', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-10-19 16:14:26', '', '2022-10-24 17:14:44');
INSERT INTO `gen_table_column` VALUES (4, '1', 'customer_address', '客户地址', 'varchar(255)', 'String', 'customerAddress', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 4, 'admin', '2022-10-19 16:14:26', '', '2022-10-24 17:14:44');
INSERT INTO `gen_table_column` VALUES (5, '1', 'customer_category', '客户等级', 'int(11)', 'Long', 'customerCategory', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'select', 'customer_grade', 5, 'admin', '2022-10-19 16:14:26', '', '2022-10-24 17:14:44');
INSERT INTO `gen_table_column` VALUES (6, '1', 'customer_bank', '客户开户银行', 'varchar(255)', 'String', 'customerBank', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2022-10-19 16:14:26', '', '2022-10-24 17:14:44');
INSERT INTO `gen_table_column` VALUES (7, '1', 'customer_bank_account', '客户银行卡号', 'varchar(255)', 'String', 'customerBankAccount', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2022-10-19 16:14:26', '', '2022-10-24 17:14:44');
INSERT INTO `gen_table_column` VALUES (8, '1', 'remarks', '备注信息', 'varchar(255)', 'String', 'remarks', '0', '0', '0', '1', '1', '1', '1', 'EQ', 'textarea', '', 8, 'admin', '2022-10-19 16:14:26', '', '2022-10-24 17:14:44');
INSERT INTO `gen_table_column` VALUES (9, '1', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 9, 'admin', '2022-10-19 16:14:26', '', '2022-10-24 17:14:44');
INSERT INTO `gen_table_column` VALUES (10, '1', 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 10, 'admin', '2022-10-19 16:14:27', '', '2022-10-24 17:14:44');
INSERT INTO `gen_table_column` VALUES (11, '1', 'create_user', '创建人id', 'int(11)', 'Long', 'createUser', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 11, 'admin', '2022-10-19 16:14:27', '', '2022-10-24 17:14:44');
INSERT INTO `gen_table_column` VALUES (12, '2', 'id', '资金账户id', 'int(11)', 'Long', 'id', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-10-19 16:14:27', '', '2022-10-26 11:27:43');
INSERT INTO `gen_table_column` VALUES (13, '2', 'name', '资金账户name', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2022-10-19 16:14:27', '', '2022-10-26 11:27:43');
INSERT INTO `gen_table_column` VALUES (14, '2', 'account_bank', '银行开户行', 'varchar(255)', 'String', 'accountBank', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-10-19 16:14:27', '', '2022-10-26 11:27:43');
INSERT INTO `gen_table_column` VALUES (15, '2', 'bank_card', '银行卡号', 'varchar(255)', 'String', 'bankCard', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-10-19 16:14:27', '', '2022-10-26 11:27:43');
INSERT INTO `gen_table_column` VALUES (16, '2', 'start_balance', '期初余额', 'decimal(20,3)', 'BigDecimal', 'startBalance', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-10-19 16:14:27', '', '2022-10-26 11:27:43');
INSERT INTO `gen_table_column` VALUES (17, '2', 'capital_balance', '资金余额', 'decimal(20,3)', 'BigDecimal', 'capitalBalance', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2022-10-19 16:14:27', '', '2022-10-26 11:27:43');
INSERT INTO `gen_table_column` VALUES (18, '2', 'opening_time', '开户时间', 'datetime', 'Date', 'openingTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 7, 'admin', '2022-10-19 16:14:27', '', '2022-10-26 11:27:43');
INSERT INTO `gen_table_column` VALUES (19, '2', 'remarks', '备注信息', 'varchar(255)', 'String', 'remarks', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2022-10-19 16:14:27', '', '2022-10-26 11:27:43');
INSERT INTO `gen_table_column` VALUES (20, '2', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 9, 'admin', '2022-10-19 16:14:27', '', '2022-10-26 11:27:43');
INSERT INTO `gen_table_column` VALUES (21, '2', 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 10, 'admin', '2022-10-19 16:14:27', '', '2022-10-26 11:27:43');
INSERT INTO `gen_table_column` VALUES (22, '2', 'create_user', '创建人', 'int(11)', 'Long', 'createUser', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 11, 'admin', '2022-10-19 16:14:27', '', '2022-10-26 11:27:43');
INSERT INTO `gen_table_column` VALUES (23, '3', 'id', '供应商id', 'int(11)', 'Long', 'id', '1', '0', '0', '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-10-19 16:14:27', '', '2022-10-25 13:04:00');
INSERT INTO `gen_table_column` VALUES (24, '3', 'supplier_name', '供应商名称', 'varchar(255)', 'String', 'supplierName', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2022-10-19 16:14:27', '', '2022-10-25 13:04:00');
INSERT INTO `gen_table_column` VALUES (25, '3', 'supplier_contacts', '联系人', 'varchar(255)', 'String', 'supplierContacts', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-10-19 16:14:27', '', '2022-10-25 13:04:00');
INSERT INTO `gen_table_column` VALUES (26, '3', 'supplier_phone', '联系人电话', 'varchar(255)', 'String', 'supplierPhone', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-10-19 16:14:27', '', '2022-10-25 13:04:00');
INSERT INTO `gen_table_column` VALUES (27, '3', 'supplier_category', '供应商分类', 'int(11)', 'Long', 'supplierCategory', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'select', 'supplier_classification', 5, 'admin', '2022-10-19 16:14:27', '', '2022-10-25 13:04:00');
INSERT INTO `gen_table_column` VALUES (28, '3', 'supplier_address', '供应商地址', 'varchar(255)', 'String', 'supplierAddress', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2022-10-19 16:14:27', '', '2022-10-25 13:04:00');
INSERT INTO `gen_table_column` VALUES (29, '3', 'supplier_bank', '供应商开户银行', 'varchar(255)', 'String', 'supplierBank', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2022-10-19 16:14:28', '', '2022-10-25 13:04:00');
INSERT INTO `gen_table_column` VALUES (30, '3', 'supplier_bank_account', '供应商银行账户', 'varchar(255)', 'String', 'supplierBankAccount', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2022-10-19 16:14:28', '', '2022-10-25 13:04:00');
INSERT INTO `gen_table_column` VALUES (31, '3', 'supplier_duty_paragraph', '供应商税号', 'varchar(255)', 'String', 'supplierDutyParagraph', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2022-10-19 16:14:28', '', '2022-10-25 13:04:00');
INSERT INTO `gen_table_column` VALUES (32, '3', 'Remarks', '备注信息', 'varchar(255)', 'String', 'remarks', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 10, 'admin', '2022-10-19 16:14:28', '', '2022-10-25 13:04:00');
INSERT INTO `gen_table_column` VALUES (33, '3', 'creat_time', '创建时间', 'datetime', 'Date', 'creatTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 11, 'admin', '2022-10-19 16:14:28', '', '2022-10-25 13:04:00');
INSERT INTO `gen_table_column` VALUES (34, '3', 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 12, 'admin', '2022-10-19 16:14:28', '', '2022-10-25 13:04:00');
INSERT INTO `gen_table_column` VALUES (35, '3', '创建人', '创建人', 'int(11)', 'Long', '创建人', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 13, 'admin', '2022-10-19 16:14:28', '', '2022-10-25 13:04:00');
INSERT INTO `gen_table_column` VALUES (36, '4', 'id', '仓库id', 'int(11)', 'Long', 'id', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-10-19 16:14:28', '', '2022-10-26 09:20:26');
INSERT INTO `gen_table_column` VALUES (37, '4', 'name', '仓库name', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2022-10-19 16:14:28', '', '2022-10-26 09:20:26');
INSERT INTO `gen_table_column` VALUES (38, '4', 'warehouse_administrators', '仓库管理员', 'varchar(255)', 'String', 'warehouseAdministrators', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-10-19 16:14:28', '', '2022-10-26 09:20:26');
INSERT INTO `gen_table_column` VALUES (39, '4', 'administrators_phone', '管理员电话', 'varchar(255)', 'String', 'administratorsPhone', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-10-19 16:14:28', '', '2022-10-26 09:20:26');
INSERT INTO `gen_table_column` VALUES (40, '4', 'warehouse_phone', '仓库电话', 'varchar(255)', 'String', 'warehousePhone', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-10-19 16:14:28', '', '2022-10-26 09:20:26');
INSERT INTO `gen_table_column` VALUES (41, '4', 'warehouse_category', '仓库类型', 'int(11)', 'Long', 'warehouseCategory', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'warehouse_type', 6, 'admin', '2022-10-19 16:14:28', '', '2022-10-26 09:20:26');
INSERT INTO `gen_table_column` VALUES (43, '4', 'warehouse_address', '仓库地址', 'varchar(255)', 'String', 'warehouseAddress', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2022-10-19 16:14:28', '', '2022-10-26 09:20:26');
INSERT INTO `gen_table_column` VALUES (45, '4', 'remarks', '备注', 'varchar(255)', 'String', 'remarks', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', '', 8, 'admin', '2022-10-19 16:14:28', '', '2022-10-26 09:20:27');
INSERT INTO `gen_table_column` VALUES (47, '4', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 9, 'admin', '2022-10-19 16:14:28', '', '2022-10-26 09:20:27');
INSERT INTO `gen_table_column` VALUES (49, '4', 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 10, 'admin', '2022-10-19 16:14:28', '', '2022-10-26 09:20:27');
INSERT INTO `gen_table_column` VALUES (51, '4', 'create_user', '创建人', 'int(11)', 'Long', 'createUser', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 11, 'admin', '2022-10-19 16:14:28', '', '2022-10-26 09:20:27');
INSERT INTO `gen_table_column` VALUES (53, '4', 'Position', '仓位(预留)', 'varchar(255)', 'String', 'position', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 12, 'admin', '2022-10-19 16:14:28', '', '2022-10-26 09:20:27');
INSERT INTO `gen_table_column` VALUES (95, '9', 'id', 'ID', 'int(11) unsigned', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-10-26 14:03:51', '', '2022-10-26 14:04:17');
INSERT INTO `gen_table_column` VALUES (96, '9', 'name', '商品名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2022-10-26 14:03:51', '', '2022-10-26 14:04:17');
INSERT INTO `gen_table_column` VALUES (97, '9', 'thumbnail', '缩略图', 'varchar(255)', 'String', 'thumbnail', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-10-26 14:03:51', '', '2022-10-26 14:04:17');
INSERT INTO `gen_table_column` VALUES (98, '9', 'banners', '轮播图：json形式多张', 'text', 'String', 'banners', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', '', 4, 'admin', '2022-10-26 14:03:51', '', '2022-10-26 14:04:17');
INSERT INTO `gen_table_column` VALUES (99, '9', 'description', '商品描述', 'text', 'String', 'description', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', '', 5, 'admin', '2022-10-26 14:03:51', '', '2022-10-26 14:04:17');
INSERT INTO `gen_table_column` VALUES (100, '9', 'sort_description', '简要描述', 'varchar(255)', 'String', 'sortDescription', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2022-10-26 14:03:51', '', '2022-10-26 14:04:17');
INSERT INTO `gen_table_column` VALUES (101, '9', 'price', '商品价格（单位：分）', 'decimal(10,0)', 'Long', 'price', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2022-10-26 14:03:51', '', '2022-10-26 14:04:17');
INSERT INTO `gen_table_column` VALUES (102, '9', 'brand_id', '品牌id', 'int(11)', 'Long', 'brandId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2022-10-26 14:03:51', '', '2022-10-26 14:04:17');
INSERT INTO `gen_table_column` VALUES (103, '9', 'category_id', '分类id', 'int(11)', 'Long', 'categoryId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2022-10-26 14:03:51', '', '2022-10-26 14:04:17');
INSERT INTO `gen_table_column` VALUES (104, '9', 'cost', '成本价格（单位：分）', 'decimal(10,0)', 'Long', 'cost', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 10, 'admin', '2022-10-26 14:03:51', '', '2022-10-26 14:04:17');
INSERT INTO `gen_table_column` VALUES (105, '9', 'type', '商品分类', 'int(10)', 'Integer', 'type', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 11, 'admin', '2022-10-26 14:03:51', '', '2022-10-26 14:04:17');
INSERT INTO `gen_table_column` VALUES (106, '9', 'details', '商品详情', 'mediumtext', 'String', 'details', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', '', 12, 'admin', '2022-10-26 14:03:51', '', '2022-10-26 14:04:17');
INSERT INTO `gen_table_column` VALUES (107, '9', 'labels', '标签', 'varchar(255)', 'String', 'labels', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 13, 'admin', '2022-10-26 14:03:51', '', '2022-10-26 14:04:17');
INSERT INTO `gen_table_column` VALUES (108, '9', 'inventory', '库存', 'int(10)', 'Integer', 'inventory', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 14, 'admin', '2022-10-26 14:03:51', '', '2022-10-26 14:04:18');
INSERT INTO `gen_table_column` VALUES (109, '9', 'sales', '销量', 'int(255)', 'Long', 'sales', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 15, 'admin', '2022-10-26 14:03:51', '', '2022-10-26 14:04:18');
INSERT INTO `gen_table_column` VALUES (110, '9', 'weight', '重量(克)', 'double', 'Long', 'weight', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 16, 'admin', '2022-10-26 14:03:52', '', '2022-10-26 14:04:18');
INSERT INTO `gen_table_column` VALUES (111, '9', 'supplier_id', '供应商id', 'int(20)', 'Long', 'supplierId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 17, 'admin', '2022-10-26 14:03:52', '', '2022-10-26 14:04:18');
INSERT INTO `gen_table_column` VALUES (112, '9', 'shelves', '上下架', 'int(11)', 'Long', 'shelves', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 18, 'admin', '2022-10-26 14:03:52', '', '2022-10-26 14:04:18');
INSERT INTO `gen_table_column` VALUES (113, '9', 'shop_id', '店铺ID', 'bigint(20)', 'Long', 'shopId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 19, 'admin', '2022-10-26 14:03:52', '', '2022-10-26 14:04:18');
INSERT INTO `gen_table_column` VALUES (114, '9', 'sort', '默认排序（后台设置）', 'int(11)', 'Long', 'sort', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 20, 'admin', '2022-10-26 14:03:52', '', '2022-10-26 14:04:18');
INSERT INTO `gen_table_column` VALUES (115, '9', 'create_ty', '创建人', 'varchar(255)', 'String', 'createTy', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 21, 'admin', '2022-10-26 14:03:52', '', '2022-10-26 14:04:18');
INSERT INTO `gen_table_column` VALUES (117, '9', 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 22, 'admin', '2022-10-26 14:03:52', '', '2022-10-26 14:04:18');
INSERT INTO `gen_table_column` VALUES (119, '9', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', '1', '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 23, 'admin', '2022-10-26 14:03:52', '', '2022-10-26 14:04:18');
INSERT INTO `gen_table_column` VALUES (122, '9', 'bar_code', '条形码（预留字段）', 'varchar(255)', 'String', 'barCode', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 24, 'admin', '2022-10-26 14:03:52', '', '2022-10-26 14:04:18');
INSERT INTO `gen_table_column` VALUES (124, '9', 'specList', '（预留）规格列表', 'text', 'String', 'speclist', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', '', 25, 'admin', '2022-10-26 14:03:52', '', '2022-10-26 14:04:18');
INSERT INTO `gen_table_column` VALUES (126, '9', 'specLabelList', '（预留）属性列表', 'text', 'String', 'speclabellist', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', '', 26, 'admin', '2022-10-26 14:03:52', '', '2022-10-26 14:04:18');
INSERT INTO `gen_table_column` VALUES (147, '11', 'id', 'id', 'int(11) unsigned', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-10-26 16:21:38', '', '2022-10-26 16:22:14');
INSERT INTO `gen_table_column` VALUES (148, '11', 'name', '品牌名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2022-10-26 16:21:38', '', '2022-10-26 16:22:14');
INSERT INTO `gen_table_column` VALUES (149, '11', 'icon', '图标', 'varchar(255)', 'String', 'icon', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-10-26 16:21:38', '', '2022-10-26 16:22:14');
INSERT INTO `gen_table_column` VALUES (150, '11', 'create_user', '创建人id', 'varchar(255)', 'String', 'createUser', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-10-26 16:21:38', '', '2022-10-26 16:22:14');
INSERT INTO `gen_table_column` VALUES (151, '11', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 5, 'admin', '2022-10-26 16:21:38', '', '2022-10-26 16:22:14');
INSERT INTO `gen_table_column` VALUES (152, '11', 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 6, 'admin', '2022-10-26 16:21:38', '', '2022-10-26 16:22:14');
INSERT INTO `gen_table_column` VALUES (153, '12', 'id', '分类ID', 'int(10) unsigned', 'Integer', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-10-26 17:26:26', '', '2022-10-26 17:26:56');
INSERT INTO `gen_table_column` VALUES (154, '12', 'name', '分类名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2022-10-26 17:26:26', '', '2022-10-26 17:26:56');
INSERT INTO `gen_table_column` VALUES (155, '12', 'parent_id', '上级id', 'int(11)', 'Long', 'parentId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-10-26 17:26:26', '', '2022-10-26 17:26:56');
INSERT INTO `gen_table_column` VALUES (156, '12', 'icon', '分类图标', 'varchar(255)', 'String', 'icon', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-10-26 17:26:26', '', '2022-10-26 17:26:56');
INSERT INTO `gen_table_column` VALUES (157, '12', 'sort', '排序', 'int(11)', 'Long', 'sort', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-10-26 17:26:26', '', '2022-10-26 17:26:56');
INSERT INTO `gen_table_column` VALUES (158, '12', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 6, 'admin', '2022-10-26 17:26:26', '', '2022-10-26 17:26:56');
INSERT INTO `gen_table_column` VALUES (159, '12', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 7, 'admin', '2022-10-26 17:26:26', '', '2022-10-26 17:26:56');
INSERT INTO `gen_table_column` VALUES (160, '13', 'id', 'ID', 'int(10) unsigned', 'Integer', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-10-27 15:44:38', '', '2022-10-27 15:45:12');
INSERT INTO `gen_table_column` VALUES (161, '13', 'label_key', '唯一标识', 'varchar(255)', 'String', 'labelKey', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-10-27 15:44:38', '', '2022-10-27 15:45:12');
INSERT INTO `gen_table_column` VALUES (162, '13', 'name', '标签名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 3, 'admin', '2022-10-27 15:44:39', '', '2022-10-27 15:45:12');
INSERT INTO `gen_table_column` VALUES (163, '13', 'create_user', '创建人', 'varchar(255)', 'String', 'createUser', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-10-27 15:44:39', '', '2022-10-27 15:45:12');
INSERT INTO `gen_table_column` VALUES (164, '13', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 5, 'admin', '2022-10-27 15:44:39', '', '2022-10-27 15:45:12');
INSERT INTO `gen_table_column` VALUES (165, '13', 'update_time', NULL, 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 6, 'admin', '2022-10-27 15:44:39', '', '2022-10-27 15:45:12');
INSERT INTO `gen_table_column` VALUES (166, '14', 'id', '资金账户id', 'int(11)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-10-27 16:05:23', '', '2022-10-31 15:41:43');
INSERT INTO `gen_table_column` VALUES (167, '14', 'po_no', '采购单号', 'varchar(255)', 'String', 'poNo', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-10-27 16:05:23', '', '2022-10-31 15:41:43');
INSERT INTO `gen_table_column` VALUES (168, '14', 'supplier_id', '供应商id', 'int(11)', 'Long', 'supplierId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-10-27 16:05:23', '', '2022-10-31 15:41:44');
INSERT INTO `gen_table_column` VALUES (169, '14', 'payment_type', '付款类型(0应付账款1现金付款2预付款)', 'int(11)', 'Long', 'paymentType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'erp_payment_type', 4, 'admin', '2022-10-27 16:05:23', '', '2022-10-31 15:41:44');
INSERT INTO `gen_table_column` VALUES (171, '14', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 10, 'admin', '2022-10-27 16:05:23', '', '2022-10-31 15:41:44');
INSERT INTO `gen_table_column` VALUES (172, '14', 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 11, 'admin', '2022-10-27 16:05:23', '', '2022-10-31 15:41:44');
INSERT INTO `gen_table_column` VALUES (173, '14', 'create_user', '创建人', 'int(11)', 'String', 'createUser', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 12, 'admin', '2022-10-27 16:05:23', '', '2022-10-31 15:41:44');
INSERT INTO `gen_table_column` VALUES (174, '15', 'id', '资金账户id', 'int(11)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-10-27 16:05:24', '', '2022-10-31 16:00:23');
INSERT INTO `gen_table_column` VALUES (175, '15', 'purchase_id', '采购订单Id', 'int(11)', 'Long', 'purchaseId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-10-27 16:05:24', '', '2022-10-31 16:00:23');
INSERT INTO `gen_table_column` VALUES (176, '15', 'goods_number', '商品编号', 'varchar(255)', 'String', 'goodsNumber', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-10-27 16:05:24', '', '2022-10-31 16:00:23');
INSERT INTO `gen_table_column` VALUES (177, '15', 'goods_name', '商品名称', 'varchar(255)', 'String', 'goodsName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 4, 'admin', '2022-10-27 16:05:24', '', '2022-10-31 16:00:23');
INSERT INTO `gen_table_column` VALUES (178, '15', 'goods_specs', '商品规格', 'varchar(255)', 'String', 'goodsSpecs', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-10-27 16:05:24', '', '2022-10-31 16:00:23');
INSERT INTO `gen_table_column` VALUES (179, '15', 'company', '商品单位', 'int(11)', 'Long', 'company', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2022-10-27 16:05:24', '', '2022-10-31 16:00:23');
INSERT INTO `gen_table_column` VALUES (180, '15', 'monovalent', '采购单价', 'double(20,2)', 'BigDecimal', 'monovalent', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2022-10-27 16:05:24', '', '2022-10-31 16:00:23');
INSERT INTO `gen_table_column` VALUES (181, '15', 'quantity', '采购数量', 'int(11)', 'Long', 'quantity', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2022-10-27 16:05:24', '', '2022-10-31 16:00:23');
INSERT INTO `gen_table_column` VALUES (182, '15', 'taxes', '税金', 'double(20,2)', 'BigDecimal', 'taxes', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2022-10-27 16:05:24', '', '2022-10-31 16:00:23');
INSERT INTO `gen_table_column` VALUES (183, '15', 'total', '采购总价', 'double(20,2)', 'BigDecimal', 'total', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 10, 'admin', '2022-10-27 16:05:24', '', '2022-10-31 16:00:24');
INSERT INTO `gen_table_column` VALUES (184, '15', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 11, 'admin', '2022-10-27 16:05:24', '', '2022-10-31 16:00:24');
INSERT INTO `gen_table_column` VALUES (185, '15', 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 12, 'admin', '2022-10-27 16:05:24', '', '2022-10-31 16:00:24');
INSERT INTO `gen_table_column` VALUES (186, '15', 'create_user', '创建人id', 'int(11)', 'Long', 'createUser', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 13, 'admin', '2022-10-27 16:05:24', '', '2022-10-31 16:00:24');
INSERT INTO `gen_table_column` VALUES (187, '14', 'warehousing_type', '入库类型(0已入库，1未入库)', 'int(11)', 'Long', 'warehousingType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'erp_warehousing_type', 5, '', '2022-10-27 16:40:47', '', '2022-10-31 15:41:44');
INSERT INTO `gen_table_column` VALUES (188, '14', 'return_goods', '退货状态(0有1无)', 'int(11)', 'Long', 'returnGoods', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', 'erp_return_goods', 6, '', '2022-10-27 16:40:47', '', '2022-10-31 15:41:44');
INSERT INTO `gen_table_column` VALUES (189, '14', 'process_type', '审核类型（0已审核，1未审核）', 'int(11)', 'Long', 'processType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'erp_process_type', 9, '', '2022-10-27 16:40:48', '', '2022-10-31 15:41:44');
INSERT INTO `gen_table_column` VALUES (190, '16', 'id', 'id', 'int(11)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-10-28 16:41:12', '', '2022-10-31 09:23:37');
INSERT INTO `gen_table_column` VALUES (191, '16', 'sale_number', '销售单号', 'varchar(255)', 'String', 'saleNumber', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-10-28 16:41:12', '', '2022-10-31 09:23:37');
INSERT INTO `gen_table_column` VALUES (192, '16', 'customer_id', '客户id', 'int(11)', 'Long', 'customerId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-10-28 16:41:12', '', '2022-10-31 09:23:37');
INSERT INTO `gen_table_column` VALUES (193, '16', 'payment_method', '收款方式id', 'int(11)', 'Long', 'paymentMethod', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'erp_payment_type', 4, 'admin', '2022-10-28 16:41:12', '', '2022-10-31 09:23:37');
INSERT INTO `gen_table_column` VALUES (194, '16', 'customer_contact', '客户联系人', 'varchar(255)', 'String', 'customerContact', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-10-28 16:41:12', '', '2022-10-31 09:23:37');
INSERT INTO `gen_table_column` VALUES (195, '16', 'customer_phone', '客户手机', 'varchar(255)', 'String', 'customerPhone', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2022-10-28 16:41:12', '', '2022-10-31 09:23:37');
INSERT INTO `gen_table_column` VALUES (196, '16', 'fixed_telephone', '固定电话', 'varchar(255)', 'String', 'fixedTelephone', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2022-10-28 16:41:12', '', '2022-10-31 09:23:37');
INSERT INTO `gen_table_column` VALUES (197, '16', 'customer_address', '客户地址', 'varchar(255)', 'String', 'customerAddress', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2022-10-28 16:41:12', '', '2022-10-31 09:23:37');
INSERT INTO `gen_table_column` VALUES (198, '16', 'province', '省', 'int(11)', 'Long', 'province', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2022-10-28 16:41:12', '', '2022-10-31 09:23:37');
INSERT INTO `gen_table_column` VALUES (199, '16', 'city', '市', 'int(11)', 'Long', 'city', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 10, 'admin', '2022-10-28 16:41:12', '', '2022-10-31 09:23:38');
INSERT INTO `gen_table_column` VALUES (200, '16', 'area', '区', 'int(11)', 'Long', 'area', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 11, 'admin', '2022-10-28 16:41:12', '', '2022-10-31 09:23:38');
INSERT INTO `gen_table_column` VALUES (201, '16', 'detailed_address', '详细地址', 'varchar(255)', 'String', 'detailedAddress', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 12, 'admin', '2022-10-28 16:41:12', '', '2022-10-31 09:23:38');
INSERT INTO `gen_table_column` VALUES (202, '16', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 13, 'admin', '2022-10-28 16:41:12', '', '2022-10-31 09:23:38');
INSERT INTO `gen_table_column` VALUES (203, '16', 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 14, 'admin', '2022-10-28 16:41:13', '', '2022-10-31 09:23:38');
INSERT INTO `gen_table_column` VALUES (204, '16', 'create_user', '创建人', 'varchar(255)', 'String', 'createUser', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 15, 'admin', '2022-10-28 16:41:13', '', '2022-10-31 09:23:38');
INSERT INTO `gen_table_column` VALUES (205, '16', 'sale_bonus', '销售总额', 'decimal(11,3)', 'BigDecimal', 'saleBonus', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 16, 'admin', '2022-10-28 16:41:13', '', '2022-10-31 09:23:38');
INSERT INTO `gen_table_column` VALUES (206, '16', 'type', '状态(0已确认1未确认)', 'int(11)', 'Long', 'type', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'erp_process_type', 17, 'admin', '2022-10-28 16:41:13', '', '2022-10-31 09:23:38');
INSERT INTO `gen_table_column` VALUES (207, '16', 'return_goods', '是否退货（0有退货 1 未退货）', 'int(11)', 'Long', 'returnGoods', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'erp_return_goods', 18, 'admin', '2022-10-28 16:41:13', '', '2022-10-31 09:23:38');
INSERT INTO `gen_table_column` VALUES (208, '16', 'deliver_goods', '是否发货（0已发货，1确认收货）', 'int(11)', 'Long', 'deliverGoods', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'receiving_status', 19, 'admin', '2022-10-28 16:41:13', '', '2022-10-31 09:23:38');
INSERT INTO `gen_table_column` VALUES (209, '16', 'taxes', '税金', 'decimal(11,3)', 'BigDecimal', 'taxes', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 20, 'admin', '2022-10-28 16:41:13', '', '2022-10-31 09:23:38');
INSERT INTO `gen_table_column` VALUES (210, '17', 'id', '资金账户id', 'int(11)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-10-28 16:41:13', '', '2022-10-31 16:50:48');
INSERT INTO `gen_table_column` VALUES (211, '17', 'purchase_id', '采购订单Id', 'int(11)', 'Long', 'purchaseId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-10-28 16:41:13', '', '2022-10-31 16:50:48');
INSERT INTO `gen_table_column` VALUES (212, '17', 'goods_number', '商品编号', 'varchar(255)', 'String', 'goodsNumber', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-10-28 16:41:13', '', '2022-10-31 16:50:48');
INSERT INTO `gen_table_column` VALUES (213, '17', 'goods_name', '商品名称', 'varchar(255)', 'String', 'goodsName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 4, 'admin', '2022-10-28 16:41:13', '', '2022-10-31 16:50:48');
INSERT INTO `gen_table_column` VALUES (214, '17', 'goods_specs', '商品规格', 'varchar(255)', 'String', 'goodsSpecs', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-10-28 16:41:13', '', '2022-10-31 16:50:48');
INSERT INTO `gen_table_column` VALUES (215, '17', 'company', '商品单位', 'int(11)', 'Long', 'company', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2022-10-28 16:41:13', '', '2022-10-31 16:50:48');
INSERT INTO `gen_table_column` VALUES (216, '17', 'monovalent', '采购单价', 'double(20,2)', 'BigDecimal', 'monovalent', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2022-10-28 16:41:13', '', '2022-10-31 16:50:48');
INSERT INTO `gen_table_column` VALUES (217, '17', 'quantity', '采购数量', 'int(11)', 'Long', 'quantity', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2022-10-28 16:41:13', '', '2022-10-31 16:50:48');
INSERT INTO `gen_table_column` VALUES (218, '17', 'taxes', '税金', 'double(20,2)', 'BigDecimal', 'taxes', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2022-10-28 16:41:13', '', '2022-10-31 16:50:48');
INSERT INTO `gen_table_column` VALUES (219, '17', 'total', '销售总价', 'double(20,2)', 'BigDecimal', 'total', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 10, 'admin', '2022-10-28 16:41:13', '', '2022-10-31 16:50:48');
INSERT INTO `gen_table_column` VALUES (220, '17', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 11, 'admin', '2022-10-28 16:41:13', '', '2022-10-31 16:50:48');
INSERT INTO `gen_table_column` VALUES (221, '17', 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 12, 'admin', '2022-10-28 16:41:13', '', '2022-10-31 16:50:48');
INSERT INTO `gen_table_column` VALUES (222, '17', 'create_user', '创建人id', 'int(11)', 'Long', 'createUser', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 13, 'admin', '2022-10-28 16:41:13', '', '2022-10-31 16:50:48');
INSERT INTO `gen_table_column` VALUES (223, '14', 'supplieruser', '供应商联系人', 'varchar(255)', 'String', 'supplieruser', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, '', '2022-10-31 15:41:30', '', '2022-10-31 15:41:44');
INSERT INTO `gen_table_column` VALUES (224, '14', 'supplierphone', '供应商手机号', 'varchar(255)', 'String', 'supplierphone', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, '', '2022-10-31 15:41:30', '', '2022-10-31 15:41:44');
INSERT INTO `gen_table_column` VALUES (225, '14', 'remarks', '备注', 'varchar(255)', 'String', 'remarks', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', '', 13, '', '2022-10-31 15:41:30', '', '2022-10-31 15:41:44');
INSERT INTO `gen_table_column` VALUES (226, '18', 'id', '单位id', 'int(11)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-11-01 09:02:02', '', '2022-11-01 09:03:04');
INSERT INTO `gen_table_column` VALUES (227, '18', 'company', '计量单位', 'varchar(255)', 'String', 'company', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-11-01 09:02:02', '', '2022-11-01 09:03:04');
INSERT INTO `gen_table_column` VALUES (228, '18', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 3, 'admin', '2022-11-01 09:02:02', '', '2022-11-01 09:03:04');
INSERT INTO `gen_table_column` VALUES (229, '18', 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 4, 'admin', '2022-11-01 09:02:02', '', '2022-11-01 09:03:04');
INSERT INTO `gen_table_column` VALUES (230, '19', 'id', 'id', 'int(11)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-11-02 15:27:41', '', '2022-11-22 09:19:49');
INSERT INTO `gen_table_column` VALUES (231, '19', 'good_id', '商品id', 'int(11)', 'Long', 'goodId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-11-02 15:27:41', '', '2022-11-22 09:19:49');
INSERT INTO `gen_table_column` VALUES (232, '19', 'warehouse_id', '仓库id', 'int(11)', 'Long', 'warehouseId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-11-02 15:27:41', '', '2022-11-22 09:19:49');
INSERT INTO `gen_table_column` VALUES (233, '19', 'warehouse_name', '仓库', 'varchar(255)', 'String', 'warehouseName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 4, 'admin', '2022-11-02 15:27:41', '', '2022-11-22 09:19:49');
INSERT INTO `gen_table_column` VALUES (234, '19', 'goods_number', '库存数量', 'int(11)', 'Long', 'goodsNumber', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-11-02 15:27:41', '', '2022-11-22 09:19:49');
INSERT INTO `gen_table_column` VALUES (235, '19', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 6, 'admin', '2022-11-02 15:27:41', '', '2022-11-22 09:19:49');
INSERT INTO `gen_table_column` VALUES (236, '19', 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 7, 'admin', '2022-11-02 15:27:41', '', '2022-11-22 09:19:49');
INSERT INTO `gen_table_column` VALUES (237, '19', 'create_user', '创建人', 'varchar(255)', 'String', 'createUser', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2022-11-02 15:27:41', '', '2022-11-22 09:19:49');
INSERT INTO `gen_table_column` VALUES (238, '20', 'id', 'id', 'int(11)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-11-07 10:14:37', '', '2022-11-07 10:15:22');
INSERT INTO `gen_table_column` VALUES (239, '20', 'sale_order_num', '销售单号', 'varchar(255)', 'String', 'saleOrderNum', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-11-07 10:14:37', '', '2022-11-07 10:15:22');
INSERT INTO `gen_table_column` VALUES (240, '20', 'shipment_num', '发货单号', 'varchar(255)', 'String', 'shipmentNum', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-11-07 10:14:37', '', '2022-11-07 10:15:22');
INSERT INTO `gen_table_column` VALUES (241, '20', 'sale_id', '订单id', 'int(11)', 'Long', 'saleId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-11-07 10:14:37', '', '2022-11-07 10:15:22');
INSERT INTO `gen_table_column` VALUES (242, '20', 'customer_id', '客户id', 'int(11)', 'Long', 'customerId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-11-07 10:14:37', '', '2022-11-07 10:15:22');
INSERT INTO `gen_table_column` VALUES (243, '20', 'payment_method', '收款方式id', 'int(11)', 'Long', 'paymentMethod', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2022-11-07 10:14:37', '', '2022-11-07 10:15:22');
INSERT INTO `gen_table_column` VALUES (244, '20', 'customer_phone', '客户电话', 'varchar(255)', 'String', 'customerPhone', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2022-11-07 10:14:38', '', '2022-11-07 10:15:22');
INSERT INTO `gen_table_column` VALUES (245, '20', 'fixed_telephone', '销售电话', 'varchar(255)', 'String', 'fixedTelephone', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2022-11-07 10:14:38', '', '2022-11-07 10:15:22');
INSERT INTO `gen_table_column` VALUES (246, '20', 'sale_bonus', '销售总额', 'decimal(13,0)', 'Long', 'saleBonus', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2022-11-07 10:14:38', '', '2022-11-07 10:15:23');
INSERT INTO `gen_table_column` VALUES (247, '20', 'type', '状态', 'int(11)', 'Long', 'type', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 10, 'admin', '2022-11-07 10:14:38', '', '2022-11-07 10:15:23');
INSERT INTO `gen_table_column` VALUES (248, '20', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 11, 'admin', '2022-11-07 10:14:38', '', '2022-11-07 10:15:23');
INSERT INTO `gen_table_column` VALUES (249, '20', 'uodate_time', '修改时间', 'datetime', 'Date', 'uodateTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 12, 'admin', '2022-11-07 10:14:38', '', '2022-11-07 10:15:23');
INSERT INTO `gen_table_column` VALUES (250, '20', 'create_user', '创建人', 'varchar(255)', 'String', 'createUser', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 13, 'admin', '2022-11-07 10:14:38', '', '2022-11-07 10:15:23');
INSERT INTO `gen_table_column` VALUES (251, '21', 'id', 'id', 'int(11)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-11-07 11:29:31', '', '2022-11-07 11:37:18');
INSERT INTO `gen_table_column` VALUES (252, '21', 'sale_id', '销售id', 'int(11)', 'Long', 'saleId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-11-07 11:29:31', '', '2022-11-07 11:37:18');
INSERT INTO `gen_table_column` VALUES (253, '21', 'sale_order_num', '销售单号', 'varchar(255)', 'String', 'saleOrderNum', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-11-07 11:29:32', '', '2022-11-07 11:37:18');
INSERT INTO `gen_table_column` VALUES (254, '21', 'shipment_num', '发货单号', 'varchar(255)', 'String', 'shipmentNum', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-11-07 11:29:32', '', '2022-11-07 11:37:19');
INSERT INTO `gen_table_column` VALUES (255, '21', 'customer_id', '客户id', 'int(11)', 'Long', 'customerId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-11-07 11:29:32', '', '2022-11-07 11:37:19');
INSERT INTO `gen_table_column` VALUES (256, '21', 'payment_method', '收款方式id', 'int(11)', 'Long', 'paymentMethod', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2022-11-07 11:29:32', '', '2022-11-07 11:37:19');
INSERT INTO `gen_table_column` VALUES (257, '21', 'return_bonus', '退货总额', 'decimal(13,0)', 'Long', 'returnBonus', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2022-11-07 11:29:32', '', '2022-11-07 11:37:19');
INSERT INTO `gen_table_column` VALUES (258, '21', 'customer_phone', '客户电话', 'varchar(255)', 'String', 'customerPhone', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2022-11-07 11:29:32', '', '2022-11-07 11:37:19');
INSERT INTO `gen_table_column` VALUES (259, '21', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 9, 'admin', '2022-11-07 11:29:32', '', '2022-11-07 11:37:19');
INSERT INTO `gen_table_column` VALUES (260, '21', 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 10, 'admin', '2022-11-07 11:29:32', '', '2022-11-07 11:37:19');
INSERT INTO `gen_table_column` VALUES (261, '21', 'create_user', '创建人', 'varchar(255)', 'String', 'createUser', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 11, 'admin', '2022-11-07 11:29:32', '', '2022-11-07 11:37:19');
INSERT INTO `gen_table_column` VALUES (262, '22', 'id', 'id', 'int(11)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-11-10 10:52:43', '', '2022-11-10 10:56:31');
INSERT INTO `gen_table_column` VALUES (263, '22', 'warehouse_id', '仓库id', 'int(11)', 'Long', 'warehouseId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-11-10 10:52:43', '', '2022-11-10 10:56:31');
INSERT INTO `gen_table_column` VALUES (264, '22', 'purchase_id', '采购订单id', 'int(11)', 'Long', 'purchaseId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-11-10 10:52:44', '', '2022-11-10 10:56:31');
INSERT INTO `gen_table_column` VALUES (265, '22', 'purchase_good_id', '采购商品id', 'int(11)', 'Long', 'purchaseGoodId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-11-10 10:52:44', '', '2022-11-10 10:56:31');
INSERT INTO `gen_table_column` VALUES (266, '22', 'purchase_good_number', '入库商品数量', 'int(11)', 'Long', 'purchaseGoodNumber', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-11-10 10:52:44', '', '2022-11-10 10:56:31');
INSERT INTO `gen_table_column` VALUES (267, '22', 'return_good_number', '退货商品数量', 'int(11)', 'Long', 'returnGoodNumber', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2022-11-10 10:52:44', '', '2022-11-10 10:56:31');
INSERT INTO `gen_table_column` VALUES (268, '22', 'return_good_bonus', '退货商品金额', 'decimal(11,2)', 'BigDecimal', 'returnGoodBonus', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2022-11-10 10:52:44', '', '2022-11-10 10:56:31');
INSERT INTO `gen_table_column` VALUES (269, '22', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 8, 'admin', '2022-11-10 10:52:44', '', '2022-11-10 10:56:31');
INSERT INTO `gen_table_column` VALUES (270, '22', 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 9, 'admin', '2022-11-10 10:52:44', '', '2022-11-10 10:56:32');
INSERT INTO `gen_table_column` VALUES (271, '23', 'id', 'Id', 'int(11)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-11-11 10:11:38', '', '2022-11-11 10:17:37');
INSERT INTO `gen_table_column` VALUES (272, '23', 'saleOrderNum', '销售单号', 'varchar(255)', 'String', 'saleordernum', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-11-11 10:11:38', '', '2022-11-11 10:17:37');
INSERT INTO `gen_table_column` VALUES (273, '23', 'collection_time', '收款时间', 'datetime', 'Date', 'collectionTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 3, 'admin', '2022-11-11 10:11:38', '', '2022-11-11 10:17:37');
INSERT INTO `gen_table_column` VALUES (274, '23', 'amount_collected', '收款金额', 'decimal(12,2)', 'BigDecimal', 'amountCollected', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-11-11 10:11:38', '', '2022-11-11 10:17:37');
INSERT INTO `gen_table_column` VALUES (276, '23', 'collection_voucher', '收款凭证', 'varchar(255)', 'String', 'collectionVoucher', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2022-11-11 10:11:38', '', '2022-11-11 10:17:37');
INSERT INTO `gen_table_column` VALUES (277, '23', 'collection_remarks', '收款备注', 'varchar(255)', 'String', 'collectionRemarks', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2022-11-11 10:11:38', '', '2022-11-11 10:17:37');
INSERT INTO `gen_table_column` VALUES (278, '23', 'collection_type', '收款状态', 'int(11)', 'Long', 'collectionType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'erp_erp_collection_type', 8, 'admin', '2022-11-11 10:11:38', '', '2022-11-11 10:17:37');
INSERT INTO `gen_table_column` VALUES (279, '23', 'operator', '操作人', 'varchar(255)', 'String', 'operator', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2022-11-11 10:11:38', '', '2022-11-11 10:17:37');
INSERT INTO `gen_table_column` VALUES (280, '23', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 10, 'admin', '2022-11-11 10:11:38', '', '2022-11-11 10:17:37');
INSERT INTO `gen_table_column` VALUES (281, '23', 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 11, 'admin', '2022-11-11 10:11:38', '', '2022-11-11 10:17:37');
INSERT INTO `gen_table_column` VALUES (282, '23', 'payee', '收款人name', 'varchar(255)', 'String', 'payee', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, '', '2022-11-11 10:17:37', '', NULL);
INSERT INTO `gen_table_column` VALUES (283, '24', 'id', 'id', 'int(11)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-11-11 15:12:13', '', '2022-11-11 15:12:49');
INSERT INTO `gen_table_column` VALUES (284, '24', 'purchase_order_num', '采购单号', 'varchar(255)', 'String', 'purchaseOrderNum', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-11-11 15:12:13', '', '2022-11-11 15:12:49');
INSERT INTO `gen_table_column` VALUES (285, '24', 'shipment_num', '入库单号', 'varchar(255)', 'String', 'shipmentNum', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-11-11 15:12:13', '', '2022-11-11 15:12:49');
INSERT INTO `gen_table_column` VALUES (286, '24', 'purchase_id', '订单id', 'int(11)', 'Long', 'purchaseId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-11-11 15:12:13', '', '2022-11-11 15:12:49');
INSERT INTO `gen_table_column` VALUES (287, '24', 'supplier_id', '供应商id', 'int(11)', 'Long', 'supplierId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-11-11 15:12:13', '', '2022-11-11 15:12:49');
INSERT INTO `gen_table_column` VALUES (288, '24', 'payment_type', '付款类型(0应付账款1现金付款2预付款)', 'int(11)', 'Long', 'paymentType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'erp_payment_type', 6, 'admin', '2022-11-11 15:12:13', '', '2022-11-11 15:12:49');
INSERT INTO `gen_table_column` VALUES (289, '24', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 7, 'admin', '2022-11-11 15:12:13', '', '2022-11-11 15:12:49');
INSERT INTO `gen_table_column` VALUES (290, '24', 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 8, 'admin', '2022-11-11 15:12:13', '', '2022-11-11 15:12:49');
INSERT INTO `gen_table_column` VALUES (291, '24', 'create_user', '创建人', 'varchar(255)', 'String', 'createUser', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2022-11-11 15:12:13', '', '2022-11-11 15:12:49');
INSERT INTO `gen_table_column` VALUES (292, '24', 'is_return', '有无退货', 'int(11)', 'Long', 'isReturn', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', 'erp_return_goods', 10, 'admin', '2022-11-11 15:12:13', '', '2022-11-11 15:12:49');
INSERT INTO `gen_table_column` VALUES (293, '25', 'id', 'id', 'int(11)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-11-16 10:16:13', '', '2022-11-16 10:16:41');
INSERT INTO `gen_table_column` VALUES (294, '25', 'sale_id', '销售id', 'int(11)', 'Long', 'saleId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-11-16 10:16:13', '', '2022-11-16 10:16:41');
INSERT INTO `gen_table_column` VALUES (295, '25', 'collection_time', '收款时间', 'datetime', 'Date', 'collectionTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 3, 'admin', '2022-11-16 10:16:13', '', '2022-11-16 10:16:41');
INSERT INTO `gen_table_column` VALUES (296, '25', 'amount_collected', '收款金额', 'decimal(11,2)', 'BigDecimal', 'amountCollected', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-11-16 10:16:13', '', '2022-11-16 10:16:41');
INSERT INTO `gen_table_column` VALUES (297, '25', 'payee', '收款人', 'varchar(255)', 'String', 'payee', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-11-16 10:16:13', '', '2022-11-16 10:16:41');
INSERT INTO `gen_table_column` VALUES (298, '25', 'collection_voucher', '收款凭证', 'varchar(255)', 'String', 'collectionVoucher', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2022-11-16 10:16:13', '', '2022-11-16 10:16:41');
INSERT INTO `gen_table_column` VALUES (299, '25', 'collection_remarks', '收款备注', 'varchar(255)', 'String', 'collectionRemarks', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2022-11-16 10:16:13', '', '2022-11-16 10:16:41');
INSERT INTO `gen_table_column` VALUES (300, '25', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 8, 'admin', '2022-11-16 10:16:13', '', '2022-11-16 10:16:41');
INSERT INTO `gen_table_column` VALUES (301, '25', 'create_user', '创建人', 'varchar(255)', 'String', 'createUser', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2022-11-16 10:16:13', '', '2022-11-16 10:16:42');
INSERT INTO `gen_table_column` VALUES (302, '25', 'collected_account', '收款账户', 'varchar(255)', 'String', 'collectedAccount', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 10, 'admin', '2022-11-16 10:16:13', '', '2022-11-16 10:16:42');
INSERT INTO `gen_table_column` VALUES (303, '26', 'id', 'id', 'int(11)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-11-16 15:18:31', '', '2022-11-16 15:20:04');
INSERT INTO `gen_table_column` VALUES (304, '26', 'purchase_id', '销售id', 'int(11)', 'Long', 'purchaseId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-11-16 15:18:31', '', '2022-11-16 15:20:04');
INSERT INTO `gen_table_column` VALUES (305, '26', 'purchase_order_num', '销售单号', 'varchar(255)', 'String', 'purchaseOrderNum', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-11-16 15:18:31', '', '2022-11-16 15:20:04');
INSERT INTO `gen_table_column` VALUES (306, '26', 'shipment_num', '发货单号', 'varchar(255)', 'String', 'shipmentNum', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-11-16 15:18:31', '', '2022-11-16 15:20:04');
INSERT INTO `gen_table_column` VALUES (307, '26', 'return_bonus', '退货总额', 'decimal(13,2)', 'BigDecimal', 'returnBonus', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-11-16 15:18:31', '', '2022-11-16 15:20:04');
INSERT INTO `gen_table_column` VALUES (308, '26', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 6, 'admin', '2022-11-16 15:18:31', '', '2022-11-16 15:20:04');
INSERT INTO `gen_table_column` VALUES (309, '26', 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 7, 'admin', '2022-11-16 15:18:31', '', '2022-11-16 15:20:04');
INSERT INTO `gen_table_column` VALUES (310, '26', 'create_user', '创建人', 'varchar(255)', 'String', 'createUser', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2022-11-16 15:18:31', '', '2022-11-16 15:20:04');
INSERT INTO `gen_table_column` VALUES (311, '26', 'is_return', '是否退货扣除库存（0是1否）', 'int(11)', 'Long', 'isReturn', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'erp_return_goods', 9, 'admin', '2022-11-16 15:18:31', '', '2022-11-16 15:20:04');
INSERT INTO `gen_table_column` VALUES (312, '26', 'supplier_id', '供应商id', 'int(11)', 'Long', 'supplierId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 10, 'admin', '2022-11-16 15:18:31', '', '2022-11-16 15:20:04');
INSERT INTO `gen_table_column` VALUES (313, '26', 'payment_type', '付款类型', 'int(11)', 'Long', 'paymentType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'erp_payment_type', 11, 'admin', '2022-11-16 15:18:31', '', '2022-11-16 15:20:04');
INSERT INTO `gen_table_column` VALUES (314, '26', 'supplieruser', '供应商联系人', 'varchar(255)', 'String', 'supplieruser', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 12, 'admin', '2022-11-16 15:18:31', '', '2022-11-16 15:20:04');
INSERT INTO `gen_table_column` VALUES (315, '26', 'supplierphone', '供应商手机号', 'varchar(255)', 'String', 'supplierphone', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 13, 'admin', '2022-11-16 15:18:31', '', '2022-11-16 15:20:04');
INSERT INTO `gen_table_column` VALUES (316, '27', 'id', '出库id', 'int(11)', 'Long', 'id', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-11-17 09:13:44', '', '2022-11-17 09:14:19');
INSERT INTO `gen_table_column` VALUES (317, '27', 'Issue_order', '出库单号', 'varchar(255)', 'String', 'issueOrder', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-11-17 09:13:44', '', '2022-11-17 09:14:19');
INSERT INTO `gen_table_column` VALUES (318, '27', 'lssue_amount', '出库金额', 'decimal(11,2)', 'BigDecimal', 'lssueAmount', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-11-17 09:13:44', '', '2022-11-17 09:14:19');
INSERT INTO `gen_table_column` VALUES (319, '27', 'lssue_warehouse', '出库仓库', 'int(11)', 'Long', 'lssueWarehouse', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-11-17 09:13:44', '', '2022-11-17 09:14:19');
INSERT INTO `gen_table_column` VALUES (320, '27', 'lssue_time', '出库时间', 'datetime', 'Date', 'lssueTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 5, 'admin', '2022-11-17 09:13:44', '', '2022-11-17 09:14:19');
INSERT INTO `gen_table_column` VALUES (321, '27', 'create_user', '操作人', 'varchar(255)', 'String', 'createUser', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2022-11-17 09:13:44', '', '2022-11-17 09:14:19');
INSERT INTO `gen_table_column` VALUES (322, '27', 'sale_id', '销售id', 'int(11)', 'Long', 'saleId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2022-11-17 09:13:44', '', '2022-11-17 09:14:20');
INSERT INTO `gen_table_column` VALUES (323, '28', 'id', '调拨id', 'int(11)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-11-17 15:21:56', '', '2022-11-17 15:24:09');
INSERT INTO `gen_table_column` VALUES (324, '28', 'allocation_order', '调拨单号', 'varchar(255)', 'String', 'allocationOrder', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-11-17 15:21:56', '', '2022-11-17 15:24:09');
INSERT INTO `gen_table_column` VALUES (325, '28', 'lssue_warehouse', '出库仓库', 'int(11)', 'Long', 'lssueWarehouse', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-11-17 15:21:56', '', '2022-11-17 15:24:09');
INSERT INTO `gen_table_column` VALUES (326, '28', 'warehousing_id', '入库仓库id', 'int(11)', 'Long', 'warehousingId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-11-17 15:21:56', '', '2022-11-17 15:24:09');
INSERT INTO `gen_table_column` VALUES (327, '28', 'creat_user', '操作人', 'varchar(255)', 'String', 'creatUser', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-11-17 15:21:56', '', '2022-11-17 15:24:09');
INSERT INTO `gen_table_column` VALUES (328, '28', 'create_time', '操作时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 6, 'admin', '2022-11-17 15:21:56', '', '2022-11-17 15:24:09');
INSERT INTO `gen_table_column` VALUES (329, '28', 'type', '调拨状态（1未审核，0已调拨）', 'int(11)', 'Long', 'type', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 7, 'admin', '2022-11-17 15:21:56', '', '2022-11-17 15:24:09');
INSERT INTO `gen_table_column` VALUES (330, '28', 'update_time', '完成时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 8, '', '2022-11-17 15:23:46', '', '2022-11-17 15:24:10');
INSERT INTO `gen_table_column` VALUES (331, '28', 'remark', '备注', 'varchar(255)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 9, '', '2022-11-17 15:23:46', '', '2022-11-17 15:24:10');
INSERT INTO `gen_table_column` VALUES (332, '29', 'id', '库存调拨商品关联id', 'int(11)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-11-17 15:30:27', '', NULL);
INSERT INTO `gen_table_column` VALUES (333, '29', 'allocation_id', '调拨id', 'int(11)', 'Long', 'allocationId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-11-17 15:30:27', '', NULL);
INSERT INTO `gen_table_column` VALUES (334, '29', 'good_id', '商品id', 'int(11)', 'Long', 'goodId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-11-17 15:30:27', '', NULL);
INSERT INTO `gen_table_column` VALUES (335, '29', 'good_number', '商品数量', 'int(11)', 'Long', 'goodNumber', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-11-17 15:30:27', '', NULL);
INSERT INTO `gen_table_column` VALUES (336, '29', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 5, 'admin', '2022-11-17 15:30:27', '', NULL);
INSERT INTO `gen_table_column` VALUES (337, '29', 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 6, 'admin', '2022-11-17 15:30:27', '', NULL);
INSERT INTO `gen_table_column` VALUES (338, '29', 'create_user', '创建人', 'varchar(255)', 'String', 'createUser', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2022-11-17 15:30:28', '', NULL);
INSERT INTO `gen_table_column` VALUES (339, '30', 'id', '入库id', 'int(11)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-11-17 15:32:02', '', '2022-11-17 15:32:52');
INSERT INTO `gen_table_column` VALUES (340, '30', 'receipt_doc_order', '入库单号', 'varchar(255)', 'String', 'receiptDocOrder', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-11-17 15:32:02', '', '2022-11-17 15:32:52');
INSERT INTO `gen_table_column` VALUES (341, '30', 'receipt_doc_amount', '入库金额', 'decimal(11,2)', 'BigDecimal', 'receiptDocAmount', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-11-17 15:32:02', '', '2022-11-17 15:32:52');
INSERT INTO `gen_table_column` VALUES (342, '30', 'receipt_doc_warehouse', '入库仓库', 'text', 'String', 'receiptDocWarehouse', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', '', 4, 'admin', '2022-11-17 15:32:02', '', '2022-11-17 15:32:52');
INSERT INTO `gen_table_column` VALUES (343, '30', 'create_user', '操作人', 'varchar(255)', 'String', 'createUser', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-11-17 15:32:02', '', '2022-11-17 15:32:52');
INSERT INTO `gen_table_column` VALUES (344, '30', 'sale_id', '采购id', 'int(11)', 'Long', 'saleId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2022-11-17 15:32:03', '', '2022-11-17 15:32:52');
INSERT INTO `gen_table_column` VALUES (345, '30', 'create_time', '出库时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 7, 'admin', '2022-11-17 15:32:03', '', '2022-11-17 15:32:52');
INSERT INTO `gen_table_column` VALUES (346, '31', 'id', 'Id', 'int(11)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-11-18 10:14:05', '', '2022-11-18 10:15:44');
INSERT INTO `gen_table_column` VALUES (347, '31', 'po_no', '采购单号', 'varchar(255)', 'String', 'poNo', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2022-11-18 10:14:05', '', '2022-11-18 10:15:44');
INSERT INTO `gen_table_column` VALUES (348, '31', 'payment_type', '付款类型', 'int(11)', 'Long', 'paymentType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'erp_payment_type', 3, 'admin', '2022-11-18 10:14:05', '', '2022-11-18 10:15:44');
INSERT INTO `gen_table_column` VALUES (349, '31', 'operator', '操作人', 'varchar(255)', 'String', 'operator', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 4, 'admin', '2022-11-18 10:14:05', '', '2022-11-18 10:15:44');
INSERT INTO `gen_table_column` VALUES (350, '31', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 5, 'admin', '2022-11-18 10:14:05', '', '2022-11-18 10:15:44');
INSERT INTO `gen_table_column` VALUES (351, '31', 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 6, 'admin', '2022-11-18 10:14:05', '', '2022-11-18 10:15:44');
INSERT INTO `gen_table_column` VALUES (352, '31', 'purchase_id', '销售id', 'int(11)', 'Long', 'purchaseId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2022-11-18 10:14:05', '', '2022-11-18 10:15:44');
INSERT INTO `gen_table_column` VALUES (353, '31', 'amount_payable', '应付金额', 'decimal(11,2)', 'BigDecimal', 'amountPayable', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2022-11-18 10:14:05', '', '2022-11-18 10:15:44');
INSERT INTO `gen_table_column` VALUES (354, '32', 'id', 'id', 'int(11)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-11-18 13:54:15', '', '2022-11-18 16:40:12');
INSERT INTO `gen_table_column` VALUES (355, '32', 'purchase_id', '采购id', 'int(11)', 'Long', 'purchaseId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-11-18 13:54:15', '', '2022-11-18 16:40:12');
INSERT INTO `gen_table_column` VALUES (356, '32', 'payment_time', '付款时间', 'datetime', 'Date', 'paymentTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 3, 'admin', '2022-11-18 13:54:16', '', '2022-11-18 16:40:12');
INSERT INTO `gen_table_column` VALUES (357, '32', 'amount_payment', '付款金额', 'decimal(11,2)', 'BigDecimal', 'amountPayment', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-11-18 13:54:16', '', '2022-11-18 16:40:12');
INSERT INTO `gen_table_column` VALUES (358, '32', 'drawee', '付款人', 'varchar(255)', 'String', 'drawee', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-11-18 13:54:16', '', '2022-11-18 16:40:12');
INSERT INTO `gen_table_column` VALUES (359, '32', 'payment_voucher', '付款凭证', 'varchar(255)', 'String', 'paymentVoucher', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2022-11-18 13:54:16', '', '2022-11-18 16:40:12');
INSERT INTO `gen_table_column` VALUES (360, '32', 'payment_remarks', '付款备注', 'varchar(255)', 'String', 'paymentRemarks', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2022-11-18 13:54:16', '', '2022-11-18 16:40:12');
INSERT INTO `gen_table_column` VALUES (361, '32', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 8, 'admin', '2022-11-18 13:54:16', '', '2022-11-18 16:40:12');
INSERT INTO `gen_table_column` VALUES (362, '32', 'create_user', '创建人', 'varchar(255)', 'String', 'createUser', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2022-11-18 13:54:16', '', '2022-11-18 16:40:12');
INSERT INTO `gen_table_column` VALUES (363, '32', 'payment_account', '付款账户', 'varchar(255)', 'String', 'paymentAccount', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 10, 'admin', '2022-11-18 13:54:16', '', '2022-11-18 16:40:13');
INSERT INTO `gen_table_column` VALUES (364, '32', 'cope_with_id', '付款账单id', 'int(11)', 'Long', 'copeWithId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 11, 'admin', '2022-11-18 13:54:16', '', '2022-11-18 16:40:13');
INSERT INTO `gen_table_column` VALUES (365, '33', 'id', '盘点id', 'int(11)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-11-21 11:37:55', '', NULL);
INSERT INTO `gen_table_column` VALUES (366, '33', 'Inventory_order', '盘点单号', 'varchar(255)', 'String', 'inventoryOrder', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-11-21 11:37:55', '', NULL);
INSERT INTO `gen_table_column` VALUES (367, '33', 'warehouse_id', '仓库id', 'int(11)', 'Long', 'warehouseId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-11-21 11:37:55', '', NULL);
INSERT INTO `gen_table_column` VALUES (368, '33', 'Inventory_user', '盘点人', 'varchar(255)', 'String', 'inventoryUser', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-11-21 11:37:55', '', NULL);
INSERT INTO `gen_table_column` VALUES (369, '33', 'remark', '备注', 'varchar(255)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 5, 'admin', '2022-11-21 11:37:55', '', NULL);
INSERT INTO `gen_table_column` VALUES (370, '33', 'type', '盘点状态', 'int(11)', 'Long', 'type', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 6, 'admin', '2022-11-21 11:37:55', '', NULL);
INSERT INTO `gen_table_column` VALUES (371, '33', 'create_user', '创建人', 'varchar(255)', 'String', 'createUser', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2022-11-21 11:37:55', '', NULL);
INSERT INTO `gen_table_column` VALUES (372, '33', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 8, 'admin', '2022-11-21 11:37:55', '', NULL);
INSERT INTO `gen_table_column` VALUES (373, '34', 'id', '盘点商品关联表id', 'int(11)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-11-21 13:24:28', '', '2022-11-21 13:24:46');
INSERT INTO `gen_table_column` VALUES (374, '34', 'inventory_id', '盘点id', 'int(11)', 'Long', 'inventoryId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-11-21 13:24:28', '', '2022-11-21 13:24:46');
INSERT INTO `gen_table_column` VALUES (375, '34', 'good_id', '商品id', 'int(11)', 'Long', 'goodId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-11-21 13:24:28', '', '2022-11-21 13:24:46');
INSERT INTO `gen_table_column` VALUES (376, '34', 'before_num', '盘点前数量', 'int(11)', 'Long', 'beforeNum', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-11-21 13:24:28', '', '2022-11-21 13:24:46');
INSERT INTO `gen_table_column` VALUES (377, '34', 'after_num', '盘点后数量', 'int(11)', 'Long', 'afterNum', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-11-21 13:24:28', '', '2022-11-21 13:24:46');
INSERT INTO `gen_table_column` VALUES (378, '34', 'creat_user', '创建人', 'varchar(255)', 'String', 'creatUser', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2022-11-21 13:24:28', '', '2022-11-21 13:24:46');
INSERT INTO `gen_table_column` VALUES (379, '34', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 7, 'admin', '2022-11-21 13:24:28', '', '2022-11-21 13:24:46');
INSERT INTO `gen_table_column` VALUES (380, '34', 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 8, 'admin', '2022-11-21 13:24:28', '', '2022-11-21 13:24:46');
INSERT INTO `gen_table_column` VALUES (381, '35', 'id', '报损单id', 'int(11)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-11-21 16:25:44', '', '2022-11-21 16:26:04');
INSERT INTO `gen_table_column` VALUES (382, '35', 'good_id', '商品id', 'int(11)', 'Long', 'goodId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-11-21 16:25:44', '', '2022-11-21 16:26:04');
INSERT INTO `gen_table_column` VALUES (383, '35', 'warehouse_id', '仓库id', 'int(11)', 'Long', 'warehouseId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-11-21 16:25:44', '', '2022-11-21 16:26:04');
INSERT INTO `gen_table_column` VALUES (384, '35', 'damage_num', '损坏数量', 'int(11)', 'Long', 'damageNum', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-11-21 16:25:44', '', '2022-11-21 16:26:04');
INSERT INTO `gen_table_column` VALUES (385, '35', 'create_user', '创建人', 'varchar(255)', 'String', 'createUser', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-11-21 16:25:44', '', '2022-11-21 16:26:04');
INSERT INTO `gen_table_column` VALUES (386, '35', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 6, 'admin', '2022-11-21 16:25:44', '', '2022-11-21 16:26:04');
INSERT INTO `gen_table_column` VALUES (387, '35', 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 7, 'admin', '2022-11-21 16:25:44', '', '2022-11-21 16:26:04');

-- ----------------------------
-- Table structure for purchase_order
-- ----------------------------
DROP TABLE IF EXISTS `purchase_order`;
CREATE TABLE `purchase_order`  (
  `id` int(11) NOT NULL COMMENT '订单id',
  `supplier
_id` int(11) NULL DEFAULT NULL COMMENT '供应商id',
  `supplier
_contact` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '供应商联系人',
  `supplier
_phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '供应商联系人电话',
  `payment_method` int(11) NULL DEFAULT NULL COMMENT '付款方式',
  `purchase_amount` decimal(20, 3) NULL DEFAULT NULL COMMENT '采购金额',
  `creat_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_user` int(11) NULL DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '采购订单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '参数配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2022-10-19 14:09:17', '', NULL, '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2022-10-19 14:09:17', '', NULL, '初始化密码 123456');
INSERT INTO `sys_config` VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2022-10-19 14:09:17', '', NULL, '深色主题theme-dark，浅色主题theme-light');
INSERT INTO `sys_config` VALUES (4, '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'false', 'Y', 'admin', '2022-10-19 14:09:17', '', NULL, '是否开启注册用户功能（true开启，false关闭）');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父部门id',
  `ancestors` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 200 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (100, 0, '0', '若依科技', 0, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-10-19 14:09:17', '', NULL);
INSERT INTO `sys_dept` VALUES (101, 100, '0,100', '深圳总公司', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-10-19 14:09:17', '', NULL);
INSERT INTO `sys_dept` VALUES (102, 100, '0,100', '长沙分公司', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-10-19 14:09:17', '', NULL);
INSERT INTO `sys_dept` VALUES (103, 101, '0,100,101', '研发部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-10-19 14:09:17', '', NULL);
INSERT INTO `sys_dept` VALUES (104, 101, '0,100,101', '市场部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-10-19 14:09:17', '', NULL);
INSERT INTO `sys_dept` VALUES (105, 101, '0,100,101', '测试部门', 3, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-10-19 14:09:17', '', NULL);
INSERT INTO `sys_dept` VALUES (106, 101, '0,100,101', '财务部门', 4, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-10-19 14:09:17', '', NULL);
INSERT INTO `sys_dept` VALUES (107, 101, '0,100,101', '运维部门', 5, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-10-19 14:09:17', '', NULL);
INSERT INTO `sys_dept` VALUES (108, 102, '0,100,102', '市场部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-10-19 14:09:17', '', NULL);
INSERT INTO `sys_dept` VALUES (109, 102, '0,100,102', '财务部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-10-19 14:09:17', '', NULL);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 139 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2022-10-19 14:09:17', '', NULL, '性别男');
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2022-10-19 14:09:17', '', NULL, '性别女');
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2022-10-19 14:09:17', '', NULL, '性别未知');
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2022-10-19 14:09:17', '', NULL, '显示菜单');
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2022-10-19 14:09:17', '', NULL, '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2022-10-19 14:09:17', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2022-10-19 14:09:17', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2022-10-19 14:09:17', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2022-10-19 14:09:17', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2022-10-19 14:09:17', '', NULL, '默认分组');
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2022-10-19 14:09:17', '', NULL, '系统分组');
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2022-10-19 14:09:17', '', NULL, '系统默认是');
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2022-10-19 14:09:17', '', NULL, '系统默认否');
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2022-10-19 14:09:17', '', NULL, '通知');
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2022-10-19 14:09:17', '', NULL, '公告');
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2022-10-19 14:09:17', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2022-10-19 14:09:17', '', NULL, '关闭状态');
INSERT INTO `sys_dict_data` VALUES (18, 99, '其他', '0', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2022-10-19 14:09:17', '', NULL, '其他操作');
INSERT INTO `sys_dict_data` VALUES (19, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2022-10-19 14:09:17', '', NULL, '新增操作');
INSERT INTO `sys_dict_data` VALUES (20, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2022-10-19 14:09:17', '', NULL, '修改操作');
INSERT INTO `sys_dict_data` VALUES (21, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-10-19 14:09:17', '', NULL, '删除操作');
INSERT INTO `sys_dict_data` VALUES (22, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2022-10-19 14:09:17', '', NULL, '授权操作');
INSERT INTO `sys_dict_data` VALUES (23, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-10-19 14:09:17', '', NULL, '导出操作');
INSERT INTO `sys_dict_data` VALUES (24, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-10-19 14:09:17', '', NULL, '导入操作');
INSERT INTO `sys_dict_data` VALUES (25, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-10-19 14:09:17', '', NULL, '强退操作');
INSERT INTO `sys_dict_data` VALUES (26, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-10-19 14:09:17', '', NULL, '生成操作');
INSERT INTO `sys_dict_data` VALUES (27, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-10-19 14:09:17', '', NULL, '清空操作');
INSERT INTO `sys_dict_data` VALUES (28, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2022-10-19 14:09:17', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (29, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2022-10-19 14:09:17', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (102, 0, '青铜', '0', 'customer_grade', NULL, 'default', 'N', '0', 'admin', '2022-10-24 13:37:57', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (103, 0, '白银', '1', 'customer_grade', NULL, 'default', 'N', '0', 'admin', '2022-10-24 13:38:06', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (104, 0, '黄金', '2', 'customer_grade', NULL, 'default', 'N', '0', 'admin', '2022-10-24 13:38:14', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (105, 0, '铂金', '3', 'customer_grade', NULL, 'default', 'N', '0', 'admin', '2022-10-24 13:38:21', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (106, 0, '钻石', '4', 'customer_grade', NULL, 'default', 'N', '0', 'admin', '2022-10-24 13:38:28', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (107, 0, '服饰', '0', 'supplier_classification', NULL, 'default', 'N', '0', 'admin', '2022-10-25 13:01:40', 'admin', '2022-10-25 13:01:54', NULL);
INSERT INTO `sys_dict_data` VALUES (108, 0, '鞋靴', '1', 'supplier_classification', NULL, 'default', 'N', '0', 'admin', '2022-10-25 13:01:48', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (109, 0, '一级仓库', '0', 'warehouse_type', NULL, 'default', 'N', '0', 'admin', '2022-10-26 09:18:29', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (110, 0, '二级仓库', '1', 'warehouse_type', NULL, 'default', 'N', '0', 'admin', '2022-10-26 09:18:38', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (111, 0, '应付账款', '0', 'erp_payment_type', NULL, 'default', 'N', '0', 'admin', '2022-10-27 16:46:50', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (112, 1, '现金付款', '1', 'erp_payment_type', NULL, 'default', 'N', '0', 'admin', '2022-10-27 16:47:00', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (113, 2, '预付款', '2', 'erp_payment_type', NULL, 'default', 'N', '0', 'admin', '2022-10-27 16:47:12', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (114, 0, '已选择入库仓库', '0', 'erp_warehousing_type', NULL, 'success', 'N', '0', 'admin', '2022-10-27 16:47:30', 'admin', '2022-11-17 10:30:15', NULL);
INSERT INTO `sys_dict_data` VALUES (115, 1, '未选择入库仓库', '1', 'erp_warehousing_type', NULL, 'danger', 'N', '0', 'admin', '2022-10-27 16:47:40', 'admin', '2022-11-17 10:30:21', NULL);
INSERT INTO `sys_dict_data` VALUES (116, 0, '有', '0', 'erp_return_goods', NULL, 'danger', 'N', '0', 'admin', '2022-10-27 16:47:59', 'admin', '2022-11-17 11:16:36', NULL);
INSERT INTO `sys_dict_data` VALUES (117, 1, '无', '1', 'erp_return_goods', NULL, 'success', 'N', '0', 'admin', '2022-10-27 16:48:06', 'admin', '2022-11-17 11:16:41', NULL);
INSERT INTO `sys_dict_data` VALUES (118, 0, '已审核', '0', 'erp_process_type', NULL, 'success', 'N', '0', 'admin', '2022-10-27 16:48:24', 'admin', '2022-11-16 13:34:59', NULL);
INSERT INTO `sys_dict_data` VALUES (119, 1, '未审核', '1', 'erp_process_type', NULL, 'danger', 'N', '0', 'admin', '2022-10-27 16:48:34', 'admin', '2022-11-16 13:34:51', NULL);
INSERT INTO `sys_dict_data` VALUES (120, 0, '确认收货', '0', 'receiving_status', NULL, 'default', 'N', '0', 'admin', '2022-10-28 16:43:20', 'admin', '2022-10-28 16:43:44', NULL);
INSERT INTO `sys_dict_data` VALUES (121, 0, '发货出库', '1', 'receiving_status', NULL, 'default', 'N', '0', 'admin', '2022-10-28 16:43:30', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (122, 0, '发货出库', '2', 'erp_process_type', NULL, 'primary', 'N', '0', 'admin', '2022-11-02 10:22:29', 'admin', '2022-11-22 10:29:23', NULL);
INSERT INTO `sys_dict_data` VALUES (123, 0, '确认收货', '3', 'erp_process_type', NULL, 'success', 'N', '0', 'admin', '2022-11-02 10:22:44', 'admin', '2022-11-22 10:29:29', NULL);
INSERT INTO `sys_dict_data` VALUES (124, 0, '已收款', '0', 'erp_erp_collection_type', NULL, 'default', 'N', '0', 'admin', '2022-11-11 10:11:17', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (125, 0, '未收款', '1', 'erp_erp_collection_type', NULL, 'default', 'N', '0', 'admin', '2022-11-11 10:11:25', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (126, 0, '是', '0', 'erp_warehousing', NULL, 'default', 'N', '0', 'admin', '2022-11-17 11:34:37', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (127, 1, '否', '1', 'erp_warehousing', NULL, 'default', 'N', '0', 'admin', '2022-11-17 11:34:46', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (128, 0, '未审核', '1', 'erp_allocation', NULL, 'primary', 'N', '0', 'admin', '2022-11-17 15:16:30', 'admin', '2022-11-22 13:03:37', NULL);
INSERT INTO `sys_dict_data` VALUES (129, 0, '已调拨', '0', 'erp_allocation', NULL, 'success', 'N', '0', 'admin', '2022-11-17 15:16:41', 'admin', '2022-11-22 13:03:32', NULL);
INSERT INTO `sys_dict_data` VALUES (130, 0, '销售单', '0', 'erp_sale_allocation', NULL, 'success', 'N', '0', 'admin', '2022-11-19 15:31:03', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (131, 0, '调拨单', '1', 'erp_sale_allocation', NULL, 'primary', 'N', '0', 'admin', '2022-11-19 15:31:21', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (132, 0, '采购单', '2', 'erp_sale_allocation', NULL, 'success', 'N', '0', 'admin', '2022-11-21 11:19:54', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (133, 0, '已盘点', '0', 'erp_inventory', NULL, 'success', 'N', '0', 'admin', '2022-11-21 14:20:13', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (134, 0, '未确认', '1', 'erp_inventory', NULL, 'primary', 'N', '0', 'admin', '2022-11-21 14:20:24', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (135, 0, '已审核', '0', 'erp_purchase_type', NULL, 'success', 'N', '0', 'admin', '2022-11-22 13:38:45', 'admin', '2022-11-22 16:03:16', NULL);
INSERT INTO `sys_dict_data` VALUES (136, 1, '未审核', '1', 'erp_purchase_type', NULL, 'danger', 'N', '0', 'admin', '2022-11-22 13:39:08', 'admin', '2022-11-22 16:03:31', NULL);
INSERT INTO `sys_dict_data` VALUES (137, 2, '采购入库', '2', 'erp_purchase_type', NULL, 'primary', 'N', '0', 'admin', '2022-11-22 13:39:23', 'admin', '2022-11-22 16:03:38', NULL);
INSERT INTO `sys_dict_data` VALUES (138, 3, '确认入库', '3', 'erp_purchase_type', NULL, 'success', 'N', '0', 'admin', '2022-11-22 13:39:37', 'admin', '2022-11-22 16:03:45', NULL);

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 115 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2022-10-19 14:09:17', '', NULL, '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2022-10-19 14:09:17', '', NULL, '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2022-10-19 14:09:17', '', NULL, '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2022-10-19 14:09:17', '', NULL, '任务状态列表');
INSERT INTO `sys_dict_type` VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', '2022-10-19 14:09:17', '', NULL, '任务分组列表');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2022-10-19 14:09:17', '', NULL, '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', '2022-10-19 14:09:17', '', NULL, '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2022-10-19 14:09:17', '', NULL, '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2022-10-19 14:09:17', '', NULL, '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2022-10-19 14:09:17', '', NULL, '登录状态列表');
INSERT INTO `sys_dict_type` VALUES (101, '客户等级', 'customer_grade', '0', 'admin', '2022-10-24 13:37:34', 'admin', '2022-10-24 13:38:40', '客户等级');
INSERT INTO `sys_dict_type` VALUES (102, '供应商分类', 'supplier_classification', '0', 'admin', '2022-10-25 13:01:17', '', NULL, '供应商分类');
INSERT INTO `sys_dict_type` VALUES (103, '仓库类型', 'warehouse_type', '0', 'admin', '2022-10-26 09:18:10', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (104, '付款类型', 'erp_payment_type', '0', 'admin', '2022-10-27 16:45:21', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (105, '选择入库仓库类型', 'erp_warehousing_type', '0', 'admin', '2022-10-27 16:45:45', 'admin', '2022-11-17 11:33:38', NULL);
INSERT INTO `sys_dict_type` VALUES (106, '退货状态', 'erp_return_goods', '0', 'admin', '2022-10-27 16:46:06', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (107, '审核类型', 'erp_process_type', '0', 'admin', '2022-10-27 16:46:27', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (108, '收货状态', 'receiving_status', '0', 'admin', '2022-10-28 16:43:06', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (109, '收款状态', 'erp_erp_collection_type', '0', 'admin', '2022-11-11 10:10:59', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (110, '是否入库', 'erp_warehousing', '0', 'admin', '2022-11-17 11:34:23', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (111, '仓库调拨状态', 'erp_allocation', '0', 'admin', '2022-11-17 15:16:11', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (112, '出库单类型', 'erp_sale_allocation', '0', 'admin', '2022-11-19 15:30:29', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (113, '盘点状态', 'erp_inventory', '0', 'admin', '2022-11-21 14:19:55', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (114, '采购类型', 'erp_purchase_type', '0', 'admin', '2022-11-22 13:38:07', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '定时任务调度表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_job` VALUES (2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_job` VALUES (3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2022-10-19 14:09:17', '', NULL, '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log`  (
  `job_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日志信息',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '异常信息',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '定时任务调度日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor`  (
  `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `user_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户账号',
  `ipaddr` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录IP地址',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '提示信息',
  `access_time` datetime(0) NULL DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 235 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统访问记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父菜单ID',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `path` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组件路径',
  `query` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '路由参数',
  `is_frame` int(1) NULL DEFAULT 1 COMMENT '是否为外链（0是 1否）',
  `is_cache` int(1) NULL DEFAULT 0 COMMENT '是否缓存（0缓存 1不缓存）',
  `menu_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  `perms` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2158 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, 6, 'system', NULL, '', 1, 0, 'M', '0', '0', '', 'system', 'admin', '2022-10-19 14:09:17', 'admin', '2022-11-22 09:42:09', '系统管理目录');
INSERT INTO `sys_menu` VALUES (2, '系统监控', 0, 7, 'monitor', NULL, '', 1, 0, 'M', '0', '0', '', 'monitor', 'admin', '2022-10-19 14:09:17', 'admin', '2022-11-22 09:42:16', '系统监控目录');
INSERT INTO `sys_menu` VALUES (3, '系统工具', 0, 8, 'tool', NULL, '', 1, 0, 'M', '1', '0', '', 'tool', 'admin', '2022-10-19 14:09:17', 'admin', '2022-11-24 16:32:06', '系统工具目录');
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1, 1, 'user', 'system/user/index', '', 1, 0, 'C', '0', '0', 'system:user:list', 'user', 'admin', '2022-10-19 14:09:17', '', NULL, '用户管理菜单');
INSERT INTO `sys_menu` VALUES (101, '角色管理', 1, 2, 'role', 'system/role/index', '', 1, 0, 'C', '0', '0', 'system:role:list', 'peoples', 'admin', '2022-10-19 14:09:17', '', NULL, '角色管理菜单');
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 1, 3, 'menu', 'system/menu/index', '', 1, 0, 'C', '0', '0', 'system:menu:list', 'tree-table', 'admin', '2022-10-19 14:09:17', '', NULL, '菜单管理菜单');
INSERT INTO `sys_menu` VALUES (103, '部门管理', 1, 4, 'dept', 'system/dept/index', '', 1, 0, 'C', '0', '0', 'system:dept:list', 'tree', 'admin', '2022-10-19 14:09:17', '', NULL, '部门管理菜单');
INSERT INTO `sys_menu` VALUES (104, '岗位管理', 1, 5, 'post', 'system/post/index', '', 1, 0, 'C', '0', '0', 'system:post:list', 'post', 'admin', '2022-10-19 14:09:17', '', NULL, '岗位管理菜单');
INSERT INTO `sys_menu` VALUES (105, '字典管理', 1, 6, 'dict', 'system/dict/index', '', 1, 0, 'C', '0', '0', 'system:dict:list', 'dict', 'admin', '2022-10-19 14:09:17', '', NULL, '字典管理菜单');
INSERT INTO `sys_menu` VALUES (106, '参数设置', 1, 7, 'config', 'system/config/index', '', 1, 0, 'C', '0', '0', 'system:config:list', 'edit', 'admin', '2022-10-19 14:09:17', '', NULL, '参数设置菜单');
INSERT INTO `sys_menu` VALUES (107, '通知公告', 1, 8, 'notice', 'system/notice/index', '', 1, 0, 'C', '0', '0', 'system:notice:list', 'message', 'admin', '2022-10-19 14:09:17', '', NULL, '通知公告菜单');
INSERT INTO `sys_menu` VALUES (108, '日志管理', 1, 9, 'log', '', '', 1, 0, 'M', '0', '0', '', 'log', 'admin', '2022-10-19 14:09:17', '', NULL, '日志管理菜单');
INSERT INTO `sys_menu` VALUES (109, '在线用户', 2, 1, 'online', 'monitor/online/index', '', 1, 0, 'C', '0', '0', 'monitor:online:list', 'online', 'admin', '2022-10-19 14:09:17', '', NULL, '在线用户菜单');
INSERT INTO `sys_menu` VALUES (110, '定时任务', 2, 2, 'job', 'monitor/job/index', '', 1, 0, 'C', '0', '0', 'monitor:job:list', 'job', 'admin', '2022-10-19 14:09:17', '', NULL, '定时任务菜单');
INSERT INTO `sys_menu` VALUES (111, 'Sentinel控制台', 2, 3, 'http://localhost:8718', '', '', 0, 0, 'C', '0', '0', 'monitor:sentinel:list', 'sentinel', 'admin', '2022-10-19 14:09:17', '', NULL, '流量控制菜单');
INSERT INTO `sys_menu` VALUES (112, 'Nacos控制台', 2, 4, 'http://localhost:8848/nacos', '', '', 0, 0, 'C', '0', '0', 'monitor:nacos:list', 'nacos', 'admin', '2022-10-19 14:09:17', '', NULL, '服务治理菜单');
INSERT INTO `sys_menu` VALUES (113, 'Admin控制台', 2, 5, 'http://localhost:9100/login', '', '', 0, 0, 'C', '0', '0', 'monitor:server:list', 'server', 'admin', '2022-10-19 14:09:17', '', NULL, '服务监控菜单');
INSERT INTO `sys_menu` VALUES (114, '表单构建', 3, 1, 'build', 'tool/build/index', '', 1, 0, 'C', '0', '0', 'tool:build:list', 'build', 'admin', '2022-10-19 14:09:17', '', NULL, '表单构建菜单');
INSERT INTO `sys_menu` VALUES (115, '代码生成', 3, 2, 'gen', 'tool/gen/index', '', 1, 0, 'C', '0', '0', 'tool:gen:list', 'code', 'admin', '2022-10-19 14:09:17', '', NULL, '代码生成菜单');
INSERT INTO `sys_menu` VALUES (116, '系统接口', 3, 3, 'http://localhost:8080/swagger-ui/index.html', '', '', 0, 0, 'C', '0', '0', 'tool:swagger:list', 'swagger', 'admin', '2022-10-19 14:09:17', '', NULL, '系统接口菜单');
INSERT INTO `sys_menu` VALUES (500, '操作日志', 108, 1, 'operlog', 'system/operlog/index', '', 1, 0, 'C', '0', '0', 'system:operlog:list', 'form', 'admin', '2022-10-19 14:09:17', '', NULL, '操作日志菜单');
INSERT INTO `sys_menu` VALUES (501, '登录日志', 108, 2, 'logininfor', 'system/logininfor/index', '', 1, 0, 'C', '0', '0', 'system:logininfor:list', 'logininfor', 'admin', '2022-10-19 14:09:17', '', NULL, '登录日志菜单');
INSERT INTO `sys_menu` VALUES (1000, '用户查询', 100, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:user:query', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1001, '用户新增', 100, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:user:add', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1002, '用户修改', 100, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:user:edit', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1003, '用户删除', 100, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:user:remove', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1004, '用户导出', 100, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:user:export', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1005, '用户导入', 100, 6, '', '', '', 1, 0, 'F', '0', '0', 'system:user:import', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1006, '重置密码', 100, 7, '', '', '', 1, 0, 'F', '0', '0', 'system:user:resetPwd', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1007, '角色查询', 101, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:role:query', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1008, '角色新增', 101, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:role:add', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1009, '角色修改', 101, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:role:edit', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1010, '角色删除', 101, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:role:remove', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1011, '角色导出', 101, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:role:export', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1012, '菜单查询', 102, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:query', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1013, '菜单新增', 102, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:add', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1014, '菜单修改', 102, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:edit', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1015, '菜单删除', 102, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:remove', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1016, '部门查询', 103, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:query', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1017, '部门新增', 103, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:add', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1018, '部门修改', 103, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:edit', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1019, '部门删除', 103, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:remove', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1020, '岗位查询', 104, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:post:query', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1021, '岗位新增', 104, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:post:add', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1022, '岗位修改', 104, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:post:edit', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1023, '岗位删除', 104, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:post:remove', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1024, '岗位导出', 104, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:post:export', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1025, '字典查询', 105, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:query', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1026, '字典新增', 105, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:add', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1027, '字典修改', 105, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:edit', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1028, '字典删除', 105, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:remove', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1029, '字典导出', 105, 5, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:export', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1030, '参数查询', 106, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:query', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1031, '参数新增', 106, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:add', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1032, '参数修改', 106, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:edit', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1033, '参数删除', 106, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:remove', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1034, '参数导出', 106, 5, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:export', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1035, '公告查询', 107, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:query', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1036, '公告新增', 107, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:add', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1037, '公告修改', 107, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:edit', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1038, '公告删除', 107, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:remove', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1039, '操作查询', 500, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:operlog:query', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1040, '操作删除', 500, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:operlog:remove', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1041, '日志导出', 500, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:operlog:export', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1042, '登录查询', 501, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:logininfor:query', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1043, '登录删除', 501, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:logininfor:remove', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1044, '日志导出', 501, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:logininfor:export', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1045, '账户解锁', 501, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:logininfor:unlock', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1046, '在线查询', 109, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:query', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1047, '批量强退', 109, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:batchLogout', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1048, '单条强退', 109, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:forceLogout', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1049, '任务查询', 110, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:query', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1050, '任务新增', 110, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:add', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1051, '任务修改', 110, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:edit', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1052, '任务删除', 110, 4, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:remove', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1053, '状态修改', 110, 5, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:changeStatus', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1054, '任务导出', 110, 6, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:export', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1055, '生成查询', 115, 1, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:query', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1056, '生成修改', 115, 2, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:edit', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1057, '生成删除', 115, 3, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:remove', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1058, '导入代码', 115, 2, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:import', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1059, '预览代码', 115, 4, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:preview', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1060, '生成代码', 115, 5, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:code', '#', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2000, '客户管理', 2055, 1, 'customer', 'erp/customer/index', NULL, 1, 0, 'C', '0', '0', 'erp:customer:list', 'peoples', 'admin', '2022-10-24 13:47:22', 'admin', '2022-10-28 14:40:44', '客户管理菜单');
INSERT INTO `sys_menu` VALUES (2001, '客户管理查询', 2000, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'erp:customer:query', '#', 'admin', '2022-10-24 13:47:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2002, '客户管理新增', 2000, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'erp:customer:add', '#', 'admin', '2022-10-24 13:47:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2003, '客户管理修改', 2000, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'erp:customer:edit', '#', 'admin', '2022-10-24 13:47:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2004, '客户管理删除', 2000, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'erp:customer:remove', '#', 'admin', '2022-10-24 13:47:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2005, '客户管理导出', 2000, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'erp:customer:export', '#', 'admin', '2022-10-24 13:47:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2006, '供应商管理', 2055, 1, 'supplier', 'erp/supplier/index', NULL, 1, 0, 'C', '0', '0', 'system:supplier:list', '供应商代理商', 'admin', '2022-10-25 13:12:15', 'admin', '2022-10-28 15:20:41', '供应商管理菜单');
INSERT INTO `sys_menu` VALUES (2007, '供应商管理查询', 2006, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:supplier:query', '#', 'admin', '2022-10-25 13:12:15', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2008, '供应商管理新增', 2006, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:supplier:add', '#', 'admin', '2022-10-25 13:12:15', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2009, '供应商管理修改', 2006, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:supplier:edit', '#', 'admin', '2022-10-25 13:12:15', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2010, '供应商管理删除', 2006, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:supplier:remove', '#', 'admin', '2022-10-25 13:12:15', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2011, '供应商管理导出', 2006, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:supplier:export', '#', 'admin', '2022-10-25 13:12:15', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2012, '仓库管理', 2055, 1, 'warehouse', 'erp/warehouse/index', NULL, 1, 0, 'C', '0', '0', 'system:warehouse:list', 'chukudan', 'admin', '2022-10-26 09:28:52', 'admin', '2022-10-28 15:15:35', '仓库管理菜单');
INSERT INTO `sys_menu` VALUES (2013, '仓库管理查询', 2012, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:warehouse:query', '#', 'admin', '2022-10-26 09:28:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2014, '仓库管理新增', 2012, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:warehouse:add', '#', 'admin', '2022-10-26 09:28:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2015, '仓库管理修改', 2012, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:warehouse:edit', '#', 'admin', '2022-10-26 09:28:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2016, '仓库管理删除', 2012, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:warehouse:remove', '#', 'admin', '2022-10-26 09:28:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2017, '仓库管理导出', 2012, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:warehouse:export', '#', 'admin', '2022-10-26 09:28:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2018, '资金账户管理', 2055, 1, 'account', 'erp/account/index', NULL, 1, 0, 'C', '0', '0', 'system:account:list', '金融', 'admin', '2022-10-26 11:32:01', 'admin', '2022-10-28 15:15:43', '资金账户管理菜单');
INSERT INTO `sys_menu` VALUES (2019, '资金账户管理查询', 2018, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:account:query', '#', 'admin', '2022-10-26 11:32:01', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2020, '资金账户管理新增', 2018, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:account:add', '#', 'admin', '2022-10-26 11:32:01', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2021, '资金账户管理修改', 2018, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:account:edit', '#', 'admin', '2022-10-26 11:32:01', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2022, '资金账户管理删除', 2018, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:account:remove', '#', 'admin', '2022-10-26 11:32:01', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2023, '资金账户管理导出', 2018, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:account:export', '#', 'admin', '2022-10-26 11:32:01', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2024, '商品信息', 2056, 1, 'goods', 'erp/goods/index', NULL, 1, 0, 'C', '0', '0', 'system:goods:list', '商品管理', 'admin', '2022-10-26 14:07:29', 'admin', '2022-10-28 15:23:49', '商品信息菜单');
INSERT INTO `sys_menu` VALUES (2025, '商品信息查询', 2024, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:goods:query', '#', 'admin', '2022-10-26 14:07:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2026, '商品信息新增', 2024, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:goods:add', '#', 'admin', '2022-10-26 14:07:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2027, '商品信息修改', 2024, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:goods:edit', '#', 'admin', '2022-10-26 14:07:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2028, '商品信息删除', 2024, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:goods:remove', '#', 'admin', '2022-10-26 14:07:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2029, '商品信息导出', 2024, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:goods:export', '#', 'admin', '2022-10-26 14:07:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2030, '采购', 0, 3, 'purchase', NULL, NULL, 1, 0, 'M', '0', '0', '', 'cascader', 'admin', '2022-10-26 14:20:47', 'admin', '2022-11-22 09:41:48', '');
INSERT INTO `sys_menu` VALUES (2031, '品牌管理', 2056, 1, 'brand', 'erp/brand/index', NULL, 1, 0, 'C', '0', '0', 'system:brand:list', 'logo-buffer', 'admin', '2022-10-26 16:26:29', 'admin', '2022-10-28 15:29:06', '品牌管理菜单');
INSERT INTO `sys_menu` VALUES (2032, '品牌管理查询', 2031, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:brand:query', '#', 'admin', '2022-10-26 16:26:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2033, '品牌管理新增', 2031, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:brand:add', '#', 'admin', '2022-10-26 16:26:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2034, '品牌管理修改', 2031, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:brand:edit', '#', 'admin', '2022-10-26 16:26:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2035, '品牌管理删除', 2031, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:brand:remove', '#', 'admin', '2022-10-26 16:26:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2036, '品牌管理导出', 2031, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:brand:export', '#', 'admin', '2022-10-26 16:26:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2037, '商品分类', 2056, 1, 'category', 'erp/category/index', NULL, 1, 0, 'C', '0', '0', 'system:category:list', '科普', 'admin', '2022-10-27 12:53:56', 'admin', '2022-10-28 15:29:14', '分类菜单');
INSERT INTO `sys_menu` VALUES (2038, '分类查询', 2037, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:category:query', '#', 'admin', '2022-10-27 12:53:56', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2039, '分类新增', 2037, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:category:add', '#', 'admin', '2022-10-27 12:53:56', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2040, '分类修改', 2037, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:category:edit', '#', 'admin', '2022-10-27 12:53:56', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2041, '分类删除', 2037, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:category:remove', '#', 'admin', '2022-10-27 12:53:56', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2042, '分类导出', 2037, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:category:export', '#', 'admin', '2022-10-27 12:53:56', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2043, '商品标签', 2056, 1, 'label', 'erp/label/index', NULL, 1, 0, 'C', '0', '0', 'system:label:list', '标题-面性', 'admin', '2022-10-27 15:49:39', 'admin', '2022-10-28 15:30:35', '商品标签菜单');
INSERT INTO `sys_menu` VALUES (2044, '商品标签查询', 2043, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:label:query', '#', 'admin', '2022-10-27 15:49:39', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2045, '商品标签新增', 2043, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:label:add', '#', 'admin', '2022-10-27 15:49:39', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2046, '商品标签修改', 2043, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:label:edit', '#', 'admin', '2022-10-27 15:49:39', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2047, '商品标签删除', 2043, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:label:remove', '#', 'admin', '2022-10-27 15:49:39', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2048, '商品标签导出', 2043, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:label:export', '#', 'admin', '2022-10-27 15:49:39', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2049, '采购订单', 2030, 1, 'purchase', 'erp/purchase/index', NULL, 1, 0, 'C', '0', '0', 'system:purchase:list', 'chart', 'admin', '2022-10-27 16:54:51', 'admin', '2022-11-21 10:28:13', '采购订单菜单');
INSERT INTO `sys_menu` VALUES (2050, '采购订单查询', 2049, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:purchase:query', '#', 'admin', '2022-10-27 16:54:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2051, '采购订单新增', 2049, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:purchase:add', '#', 'admin', '2022-10-27 16:54:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2052, '采购订单修改', 2049, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:purchase:edit', '#', 'admin', '2022-10-27 16:54:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2053, '采购订单删除', 2049, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:purchase:remove', '#', 'admin', '2022-10-27 16:54:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2054, '采购订单导出', 2049, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:purchase:export', '#', 'admin', '2022-10-27 16:54:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2055, '基础信息', 0, 0, 'information', NULL, NULL, 1, 0, 'M', '0', '0', '', 'list', 'admin', '2022-10-28 14:34:06', 'admin', '2022-10-28 14:39:49', '');
INSERT INTO `sys_menu` VALUES (2056, '商品管理', 0, 1, 'commodity', NULL, NULL, 1, 0, 'M', '0', '0', '', 'monitor', 'admin', '2022-10-28 14:37:23', 'admin', '2022-10-28 14:40:13', '');
INSERT INTO `sys_menu` VALUES (2057, '销售订单', 2063, 1, 'sale', 'erp/sale/index', NULL, 1, 0, 'C', '0', '0', 'system:sale:list', '待审核订单', 'admin', '2022-10-28 16:49:39', 'admin', '2022-11-10 11:18:19', '销售订单菜单');
INSERT INTO `sys_menu` VALUES (2058, '销售订单查询', 2057, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:sale:query', '#', 'admin', '2022-10-28 16:49:39', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2059, '销售订单新增', 2057, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:sale:add', '#', 'admin', '2022-10-28 16:49:39', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2060, '销售订单修改', 2057, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:sale:edit', '#', 'admin', '2022-10-28 16:49:39', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2061, '销售订单删除', 2057, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:sale:remove', '#', 'admin', '2022-10-28 16:49:39', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2062, '销售订单导出', 2057, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:sale:export', '#', 'admin', '2022-10-28 16:49:39', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2063, '销售管理', 0, 2, 'sale', NULL, NULL, 1, 0, 'M', '0', '0', '', '订单管理', 'admin', '2022-10-28 16:56:44', 'admin', '2022-11-22 09:41:42', '');
INSERT INTO `sys_menu` VALUES (2064, '计量单位', 2055, 1, 'company', 'erp/company/index', NULL, 1, 0, 'C', '0', '0', 'system:company:list', '计量单位', 'admin', '2022-11-01 09:08:00', 'admin', '2022-11-11 09:09:23', 'company 菜单');
INSERT INTO `sys_menu` VALUES (2065, 'company 查询', 2064, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:company:query', '#', 'admin', '2022-11-01 09:08:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2066, 'company 新增', 2064, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:company:add', '#', 'admin', '2022-11-01 09:08:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2067, 'company 修改', 2064, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:company:edit', '#', 'admin', '2022-11-01 09:08:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2068, 'company 删除', 2064, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:company:remove', '#', 'admin', '2022-11-01 09:08:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2069, 'company 导出', 2064, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:company:export', '#', 'admin', '2022-11-01 09:08:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2071, '销售发货单', 2063, 1, 'invoice', 'erp/invoice/index', NULL, 1, 0, 'C', '0', '0', 'system:invoice:list', '发货出库', 'admin', '2022-11-07 10:21:29', 'admin', '2022-11-11 09:14:45', '销售发货单菜单');
INSERT INTO `sys_menu` VALUES (2072, '销售发货单查询', 2071, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:invoice:query', '#', 'admin', '2022-11-07 10:21:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2073, '销售发货单新增', 2071, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:invoice:add', '#', 'admin', '2022-11-07 10:21:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2074, '销售发货单修改', 2071, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:invoice:edit', '#', 'admin', '2022-11-07 10:21:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2075, '销售发货单删除', 2071, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:invoice:remove', '#', 'admin', '2022-11-07 10:21:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2076, '销售发货单导出', 2071, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:invoice:export', '#', 'admin', '2022-11-07 10:21:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2077, '销售退货单', 2063, 1, 'return', 'erp/return/index', NULL, 1, 0, 'C', '0', '0', 'system:return:list', '退订', 'admin', '2022-11-07 13:45:38', 'admin', '2022-11-10 11:18:43', '【请填写功能名称】菜单');
INSERT INTO `sys_menu` VALUES (2078, '【请填写功能名称】查询', 2077, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:return:query', '#', 'admin', '2022-11-07 13:45:38', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2079, '【请填写功能名称】新增', 2077, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:return:add', '#', 'admin', '2022-11-07 13:45:38', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2080, '【请填写功能名称】修改', 2077, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:return:edit', '#', 'admin', '2022-11-07 13:45:38', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2081, '【请填写功能名称】删除', 2077, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:return:remove', '#', 'admin', '2022-11-07 13:45:38', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2082, '【请填写功能名称】导出', 2077, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:return:export', '#', 'admin', '2022-11-07 13:45:38', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2089, '应收账款', 2095, 1, 'collection', 'erp/collection/index', NULL, 1, 0, 'C', '0', '0', 'system:collection:list', 'money', 'admin', '2022-11-11 10:20:20', 'admin', '2022-11-22 10:23:36', '应收账款菜单');
INSERT INTO `sys_menu` VALUES (2090, '应收账款查询', 2089, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:collection:query', '#', 'admin', '2022-11-11 10:20:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2091, '应收账款新增', 2089, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:collection:add', '#', 'admin', '2022-11-11 10:20:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2092, '应收账款修改', 2089, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:collection:edit', '#', 'admin', '2022-11-11 10:20:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2093, '应收账款删除', 2089, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:collection:remove', '#', 'admin', '2022-11-11 10:20:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2094, '应收账款导出', 2089, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:collection:export', '#', 'admin', '2022-11-11 10:20:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2095, '财务管理', 0, 5, 'finace', NULL, NULL, 1, 0, 'M', '0', '0', '', '金融', 'admin', '2022-11-11 10:22:55', 'admin', '2022-11-22 09:41:57', '');
INSERT INTO `sys_menu` VALUES (2096, '采购入库单', 2030, 1, 'invoicePurchase', 'erp/invoicePurchase/index', NULL, 1, 0, 'C', '0', '0', 'system:invoicePurchase:list', 'build', 'admin', '2022-11-14 09:44:29', 'admin', '2022-11-21 10:28:44', '采购入库单菜单');
INSERT INTO `sys_menu` VALUES (2097, '采购入库单查询', 2096, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:invoicePurchase:query', '#', 'admin', '2022-11-14 09:44:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2098, '采购入库单新增', 2096, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:invoicePurchase:add', '#', 'admin', '2022-11-14 09:44:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2099, '采购入库单修改', 2096, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:invoicePurchase:edit', '#', 'admin', '2022-11-14 09:44:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2100, '采购入库单删除', 2096, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:invoicePurchase:remove', '#', 'admin', '2022-11-14 09:44:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2101, '采购入库单导出', 2096, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:invoicePurchase:export', '#', 'admin', '2022-11-14 09:44:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2102, '采购退货', 2030, 1, 'returnPurchase', 'erp/returnPurchase/index', NULL, 1, 0, 'C', '0', '0', 'system:returnPurchase:list', 'edit', 'admin', '2022-11-16 15:26:04', 'admin', '2022-11-21 10:29:19', '采购退货菜单');
INSERT INTO `sys_menu` VALUES (2103, '采购退货查询', 2102, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:returnPurchase:query', '#', 'admin', '2022-11-16 15:26:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2104, '采购退货新增', 2102, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:returnPurchase:add', '#', 'admin', '2022-11-16 15:26:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2105, '采购退货修改', 2102, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:returnPurchase:edit', '#', 'admin', '2022-11-16 15:26:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2106, '采购退货删除', 2102, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:returnPurchase:remove', '#', 'admin', '2022-11-16 15:26:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2107, '采购退货导出', 2102, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:returnPurchase:export', '#', 'admin', '2022-11-16 15:26:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2108, '出库单', 2114, 1, 'lssue', 'erp/lssue/index', NULL, 1, 0, 'C', '0', '0', 'system:lssue:list', 'zhcc_出库', 'admin', '2022-11-17 09:24:29', 'admin', '2022-11-22 09:48:46', '出库单菜单');
INSERT INTO `sys_menu` VALUES (2109, '出库单查询', 2108, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:lssue:query', '#', 'admin', '2022-11-17 09:24:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2110, '出库单新增', 2108, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:lssue:add', '#', 'admin', '2022-11-17 09:24:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2111, '出库单修改', 2108, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:lssue:edit', '#', 'admin', '2022-11-17 09:24:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2112, '出库单删除', 2108, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:lssue:remove', '#', 'admin', '2022-11-17 09:24:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2113, '出库单导出', 2108, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:lssue:export', '#', 'admin', '2022-11-17 09:24:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2114, '库存管理', 0, 4, 'lssue', NULL, NULL, 1, 0, 'M', '0', '0', '', '店铺', 'admin', '2022-11-17 09:25:41', 'admin', '2022-11-22 09:46:11', '');
INSERT INTO `sys_menu` VALUES (2115, '库存调拨', 2114, 3, 'allocation', 'erp/allocation/index', NULL, 1, 0, 'C', '0', '0', 'system:allocation:list', '跨仓移库', 'admin', '2022-11-17 15:29:26', 'admin', '2022-11-22 09:52:18', '库存调拨菜单');
INSERT INTO `sys_menu` VALUES (2116, '库存调拨查询', 2115, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:allocation:query', '#', 'admin', '2022-11-17 15:29:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2117, '库存调拨新增', 2115, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:allocation:add', '#', 'admin', '2022-11-17 15:29:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2118, '库存调拨修改', 2115, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:allocation:edit', '#', 'admin', '2022-11-17 15:29:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2119, '库存调拨删除', 2115, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:allocation:remove', '#', 'admin', '2022-11-17 15:29:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2120, '库存调拨导出', 2115, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:allocation:export', '#', 'admin', '2022-11-17 15:29:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2121, '入库单', 2114, 2, 'receiptDoc', 'erp/receiptDoc/index', NULL, 1, 0, 'C', '0', '0', 'system:receiptDoc:list', 'zhcc_入库', 'admin', '2022-11-17 15:38:37', 'admin', '2022-11-22 09:49:04', '入库单菜单');
INSERT INTO `sys_menu` VALUES (2122, '入库单查询', 2121, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:receiptDoc:query', '#', 'admin', '2022-11-17 15:38:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2123, '入库单新增', 2121, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:receiptDoc:add', '#', 'admin', '2022-11-17 15:38:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2124, '入库单修改', 2121, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:receiptDoc:edit', '#', 'admin', '2022-11-17 15:38:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2125, '入库单删除', 2121, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:receiptDoc:remove', '#', 'admin', '2022-11-17 15:38:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2126, '入库单导出', 2121, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:receiptDoc:export', '#', 'admin', '2022-11-17 15:38:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2127, '应付账款', 2095, 1, 'copeWith', 'erp/copeWith/index', NULL, 1, 0, 'C', '0', '0', 'system:copeWith:list', '汇款', 'admin', '2022-11-18 10:26:21', 'admin', '2022-11-22 10:23:25', '应付账款菜单');
INSERT INTO `sys_menu` VALUES (2128, '应付账款查询', 2127, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:copeWith:query', '#', 'admin', '2022-11-18 10:26:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2129, '应付账款新增', 2127, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:copeWith:add', '#', 'admin', '2022-11-18 10:26:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2130, '应付账款修改', 2127, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:copeWith:edit', '#', 'admin', '2022-11-18 10:26:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2131, '应付账款删除', 2127, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:copeWith:remove', '#', 'admin', '2022-11-18 10:26:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2132, '应付账款导出', 2127, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:copeWith:export', '#', 'admin', '2022-11-18 10:26:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2133, '付款记录', 3, 1, 'paymentRecord', 'erp/paymentRecord/index', NULL, 1, 0, 'C', '1', '1', 'system:paymentRecord:list', '#', 'admin', '2022-11-18 13:58:34', 'admin', '2022-11-18 16:39:51', '付款记录菜单');
INSERT INTO `sys_menu` VALUES (2134, '付款记录查询', 2133, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:paymentRecord:query', '#', 'admin', '2022-11-18 13:58:34', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2135, '付款记录新增', 2133, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:paymentRecord:add', '#', 'admin', '2022-11-18 13:58:34', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2136, '付款记录修改', 2133, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:paymentRecord:edit', '#', 'admin', '2022-11-18 13:58:34', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2137, '付款记录删除', 2133, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:paymentRecord:remove', '#', 'admin', '2022-11-18 13:58:34', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2138, '付款记录导出', 2133, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:paymentRecord:export', '#', 'admin', '2022-11-18 13:58:34', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2139, '库存盘点', 2114, 5, 'inventory', 'erp/inventory/index', NULL, 1, 0, 'C', '0', '0', 'system:inventory:list', '库存查询', 'admin', '2022-11-21 11:38:51', 'admin', '2022-11-22 09:52:41', '库存盘点菜单');
INSERT INTO `sys_menu` VALUES (2140, '库存盘点查询', 2139, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:inventory:query', '#', 'admin', '2022-11-21 11:38:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2141, '库存盘点新增', 2139, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:inventory:add', '#', 'admin', '2022-11-21 11:38:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2142, '库存盘点修改', 2139, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:inventory:edit', '#', 'admin', '2022-11-21 11:38:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2143, '库存盘点删除', 2139, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:inventory:remove', '#', 'admin', '2022-11-21 11:38:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2144, '库存盘点导出', 2139, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:inventory:export', '#', 'admin', '2022-11-21 11:38:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2145, '库存预警', 2056, 9, 'goods/thresholdInfo', 'erp/goods/thresholdInfo', NULL, 1, 0, 'C', '0', '0', '', '库存预警', 'admin', '2022-11-21 14:53:57', 'admin', '2022-11-22 09:54:49', '');
INSERT INTO `sys_menu` VALUES (2146, '报损单', 2114, 4, 'reporting', 'erp/reporting/index', NULL, 1, 0, 'C', '0', '0', 'system:reporting:list', '销退收货', 'admin', '2022-11-21 16:32:09', 'admin', '2022-11-22 09:52:31', '报损单菜单');
INSERT INTO `sys_menu` VALUES (2147, '报损单查询', 2146, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:reporting:query', '#', 'admin', '2022-11-21 16:32:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2148, '报损单新增', 2146, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:reporting:add', '#', 'admin', '2022-11-21 16:32:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2149, '报损单修改', 2146, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:reporting:edit', '#', 'admin', '2022-11-21 16:32:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2150, '报损单删除', 2146, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:reporting:remove', '#', 'admin', '2022-11-21 16:32:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2151, '报损单导出', 2146, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:reporting:export', '#', 'admin', '2022-11-21 16:32:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2152, '商品信息', 2114, 6, 'goodsWarehouse', 'erp/goodsWarehouse/index', NULL, 1, 0, 'C', '0', '0', 'system:goodsWarehouse:list', '多卖商品', 'admin', '2022-11-22 09:22:15', 'admin', '2022-11-22 09:54:12', '商品仓库管理菜单');
INSERT INTO `sys_menu` VALUES (2153, '商品仓库管理查询', 2152, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:goodsWarehouse:query', '#', 'admin', '2022-11-22 09:22:15', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2154, '商品仓库管理新增', 2152, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:goodsWarehouse:add', '#', 'admin', '2022-11-22 09:22:15', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2155, '商品仓库管理修改', 2152, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:goodsWarehouse:edit', '#', 'admin', '2022-11-22 09:22:15', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2156, '商品仓库管理删除', 2152, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:goodsWarehouse:remove', '#', 'admin', '2022-11-22 09:22:15', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2157, '商品仓库管理导出', 2152, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:goodsWarehouse:export', '#', 'admin', '2022-11-22 09:22:15', '', NULL, '');

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `notice_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公告标题',
  `notice_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` longblob NULL COMMENT '公告内容',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '通知公告表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
  `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求方式',
  `operator_type` int(1) NULL DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '返回参数',
  `status` int(1) NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`oper_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1660 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '操作日志记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO `sys_oper_log` VALUES (1, '操作日志', 9, 'com.ruoyi.system.controller.SysOperlogController.clean()', 'DELETE', 1, 'admin', NULL, '/operlog/clean', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-11-24 16:32:20');
INSERT INTO `sys_oper_log` VALUES (2, '登录日志', 3, 'com.ruoyi.system.controller.SysLogininforController.clean()', 'DELETE', 1, 'admin', NULL, '/logininfor/clean', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-11-24 16:32:26');
INSERT INTO `sys_oper_log` VALUES (3, '通知公告', 3, 'com.ruoyi.system.controller.SysNoticeController.remove()', 'DELETE', 1, 'admin', NULL, '/notice/1,2', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-11-24 16:32:41');

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `post_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位名称',
  `post_sort` int(4) NOT NULL COMMENT '显示顺序',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '岗位信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, 'ceo', '董事长', 1, '0', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_post` VALUES (2, 'se', '项目经理', 2, '0', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_post` VALUES (3, 'hr', '人力资源', 3, '0', 'admin', '2022-10-19 14:09:17', '', NULL, '');
INSERT INTO `sys_post` VALUES (4, 'user', '普通员工', 4, '0', 'admin', '2022-10-19 14:09:17', '', NULL, '');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `menu_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '菜单树选择项是否关联显示',
  `dept_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '部门树选择项是否关联显示',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'admin', 1, '1', 1, 1, '0', '0', 'admin', '2022-10-19 14:09:17', '', NULL, '超级管理员');
INSERT INTO `sys_role` VALUES (2, '普通角色', 'common', 2, '2', 1, 1, '0', '0', 'admin', '2022-10-19 14:09:17', 'admin', '2022-11-22 09:42:49', '普通角色');

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色和部门关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES (2, 100);
INSERT INTO `sys_role_dept` VALUES (2, 101);
INSERT INTO `sys_role_dept` VALUES (2, 105);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (2, 1);
INSERT INTO `sys_role_menu` VALUES (2, 2);
INSERT INTO `sys_role_menu` VALUES (2, 3);
INSERT INTO `sys_role_menu` VALUES (2, 100);
INSERT INTO `sys_role_menu` VALUES (2, 101);
INSERT INTO `sys_role_menu` VALUES (2, 102);
INSERT INTO `sys_role_menu` VALUES (2, 103);
INSERT INTO `sys_role_menu` VALUES (2, 104);
INSERT INTO `sys_role_menu` VALUES (2, 105);
INSERT INTO `sys_role_menu` VALUES (2, 106);
INSERT INTO `sys_role_menu` VALUES (2, 107);
INSERT INTO `sys_role_menu` VALUES (2, 108);
INSERT INTO `sys_role_menu` VALUES (2, 109);
INSERT INTO `sys_role_menu` VALUES (2, 110);
INSERT INTO `sys_role_menu` VALUES (2, 111);
INSERT INTO `sys_role_menu` VALUES (2, 112);
INSERT INTO `sys_role_menu` VALUES (2, 113);
INSERT INTO `sys_role_menu` VALUES (2, 114);
INSERT INTO `sys_role_menu` VALUES (2, 115);
INSERT INTO `sys_role_menu` VALUES (2, 116);
INSERT INTO `sys_role_menu` VALUES (2, 500);
INSERT INTO `sys_role_menu` VALUES (2, 501);
INSERT INTO `sys_role_menu` VALUES (2, 1000);
INSERT INTO `sys_role_menu` VALUES (2, 1001);
INSERT INTO `sys_role_menu` VALUES (2, 1002);
INSERT INTO `sys_role_menu` VALUES (2, 1003);
INSERT INTO `sys_role_menu` VALUES (2, 1004);
INSERT INTO `sys_role_menu` VALUES (2, 1005);
INSERT INTO `sys_role_menu` VALUES (2, 1006);
INSERT INTO `sys_role_menu` VALUES (2, 1007);
INSERT INTO `sys_role_menu` VALUES (2, 1008);
INSERT INTO `sys_role_menu` VALUES (2, 1009);
INSERT INTO `sys_role_menu` VALUES (2, 1010);
INSERT INTO `sys_role_menu` VALUES (2, 1011);
INSERT INTO `sys_role_menu` VALUES (2, 1012);
INSERT INTO `sys_role_menu` VALUES (2, 1013);
INSERT INTO `sys_role_menu` VALUES (2, 1014);
INSERT INTO `sys_role_menu` VALUES (2, 1015);
INSERT INTO `sys_role_menu` VALUES (2, 1016);
INSERT INTO `sys_role_menu` VALUES (2, 1017);
INSERT INTO `sys_role_menu` VALUES (2, 1018);
INSERT INTO `sys_role_menu` VALUES (2, 1019);
INSERT INTO `sys_role_menu` VALUES (2, 1020);
INSERT INTO `sys_role_menu` VALUES (2, 1021);
INSERT INTO `sys_role_menu` VALUES (2, 1022);
INSERT INTO `sys_role_menu` VALUES (2, 1023);
INSERT INTO `sys_role_menu` VALUES (2, 1024);
INSERT INTO `sys_role_menu` VALUES (2, 1025);
INSERT INTO `sys_role_menu` VALUES (2, 1026);
INSERT INTO `sys_role_menu` VALUES (2, 1027);
INSERT INTO `sys_role_menu` VALUES (2, 1028);
INSERT INTO `sys_role_menu` VALUES (2, 1029);
INSERT INTO `sys_role_menu` VALUES (2, 1030);
INSERT INTO `sys_role_menu` VALUES (2, 1031);
INSERT INTO `sys_role_menu` VALUES (2, 1032);
INSERT INTO `sys_role_menu` VALUES (2, 1033);
INSERT INTO `sys_role_menu` VALUES (2, 1034);
INSERT INTO `sys_role_menu` VALUES (2, 1035);
INSERT INTO `sys_role_menu` VALUES (2, 1036);
INSERT INTO `sys_role_menu` VALUES (2, 1037);
INSERT INTO `sys_role_menu` VALUES (2, 1038);
INSERT INTO `sys_role_menu` VALUES (2, 1039);
INSERT INTO `sys_role_menu` VALUES (2, 1040);
INSERT INTO `sys_role_menu` VALUES (2, 1041);
INSERT INTO `sys_role_menu` VALUES (2, 1042);
INSERT INTO `sys_role_menu` VALUES (2, 1043);
INSERT INTO `sys_role_menu` VALUES (2, 1044);
INSERT INTO `sys_role_menu` VALUES (2, 1045);
INSERT INTO `sys_role_menu` VALUES (2, 1046);
INSERT INTO `sys_role_menu` VALUES (2, 1047);
INSERT INTO `sys_role_menu` VALUES (2, 1048);
INSERT INTO `sys_role_menu` VALUES (2, 1049);
INSERT INTO `sys_role_menu` VALUES (2, 1050);
INSERT INTO `sys_role_menu` VALUES (2, 1051);
INSERT INTO `sys_role_menu` VALUES (2, 1052);
INSERT INTO `sys_role_menu` VALUES (2, 1053);
INSERT INTO `sys_role_menu` VALUES (2, 1054);
INSERT INTO `sys_role_menu` VALUES (2, 1055);
INSERT INTO `sys_role_menu` VALUES (2, 1056);
INSERT INTO `sys_role_menu` VALUES (2, 1057);
INSERT INTO `sys_role_menu` VALUES (2, 1058);
INSERT INTO `sys_role_menu` VALUES (2, 1059);
INSERT INTO `sys_role_menu` VALUES (2, 1060);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint(20) NULL DEFAULT NULL COMMENT '部门ID',
  `user_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户账号',
  `nick_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户昵称',
  `user_type` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '00' COMMENT '用户类型（00系统用户）',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '头像地址',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '密码',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime(0) NULL DEFAULT NULL COMMENT '最后登录时间',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 103, 'admin', '若依', '00', 'ry@163.com', '15888888888', '1', '', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', '2022-10-19 14:09:17', 'admin', '2022-10-19 14:09:17', '', NULL, '管理员');
INSERT INTO `sys_user` VALUES (2, 105, 'ry', '若依', '00', 'ry@qq.com', '15666666666', '1', '', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', '2022-10-19 14:09:17', 'admin', '2022-10-19 14:09:17', '', NULL, '测试员');

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户与岗位关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES (1, 1);
INSERT INTO `sys_user_post` VALUES (2, 2);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户和角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (2, 2);

SET FOREIGN_KEY_CHECKS = 1;
