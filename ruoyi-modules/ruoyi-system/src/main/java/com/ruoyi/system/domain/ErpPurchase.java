package com.ruoyi.system.domain;

import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 采购订单对象 erp_purchase
 *
 * @author ruoyi
 * @date 2022-10-27
 */
public class ErpPurchase extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 资金账户id */
    private Long id;

    /** 采购单号 */
    @Excel(name = "采购单号")
    private String poNo;
    /*入库单号*/
    private String shipmentNum;
    /*采购总价*/
    private String totalPurchase;
    /*供应商*/
    private String supplierName;

    /** 供应商id */
    @Excel(name = "供应商id")
    private Long supplierId;

    /** 付款类型 */
    @Excel(name = "付款类型")
    private Long paymentType;

    /** 入库类型 */
    @Excel(name = "入库类型")
    private Long warehousingType;

    /** 退货状态 */
    @Excel(name = "退货状态")
    private Long returnGoods;

    /** 供应商联系人 */
    private String supplieruser;
    /** 供应商手机号 */
    private String supplierphone;

    /** 审核类型 */
    @Excel(name = "审核类型")
    private Long processType;

    /** 创建人id */
    @Excel(name = "创建人")
    private String createUser;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    /*商品多选ID*/
    private String[] goodsIds;
    /*采购单价数组*/
    private String[] monovalents;
    /*采购数量数组*/
    private String[] quantitys;
    /*采购总价数组*/
    private String[] totalPrices;

    public String getTotalPurchase() {
        return totalPurchase;
    }

    public void setTotalPurchase(String totalPurchase) {
        this.totalPurchase = totalPurchase;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getShipmentNum() {
        return shipmentNum;
    }

    public void setShipmentNum(String shipmentNum) {
        this.shipmentNum = shipmentNum;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setPoNo(String poNo)
    {
        this.poNo = poNo;
    }

    public String getPoNo()
    {
        return poNo;
    }
    public void setSupplierId(Long supplierId)
    {
        this.supplierId = supplierId;
    }

    public Long getSupplierId()
    {
        return supplierId;
    }
    public void setPaymentType(Long paymentType)
    {
        this.paymentType = paymentType;
    }

    public Long getPaymentType()
    {
        return paymentType;
    }
    public void setWarehousingType(Long warehousingType)
    {
        this.warehousingType = warehousingType;
    }

    public Long getWarehousingType()
    {
        return warehousingType;
    }
    public void setReturnGoods(Long returnGoods)
    {
        this.returnGoods = returnGoods;
    }

    public Long getReturnGoods()
    {
        return returnGoods;
    }

    public String getSupplieruser() {
        return supplieruser;
    }

    public void setSupplieruser(String supplieruser) {
        this.supplieruser = supplieruser;
    }

    public String getSupplierphone() {
        return supplierphone;
    }

    public void setSupplierphone(String supplierphone) {
        this.supplierphone = supplierphone;
    }

    public void setProcessType(Long processType)
    {
        this.processType = processType;
    }

    public Long getProcessType()
    {
        return processType;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String[] getGoodsIds() {
        return goodsIds;
    }

    public void setGoodsIds(String[] goodsIds) {
        this.goodsIds = goodsIds;
    }

    public String[] getMonovalents() {
        return monovalents;
    }

    public void setMonovalents(String[] monovalents) {
        this.monovalents = monovalents;
    }

    public String[] getQuantitys() {
        return quantitys;
    }

    public void setQuantitys(String[] quantitys) {
        this.quantitys = quantitys;
    }

    public String[] getTotalPrices() {
        return totalPrices;
    }

    public void setTotalPrices(String[] totalPrices) {
        this.totalPrices = totalPrices;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("poNo", getPoNo())
            .append("supplierId", getSupplierId())
            .append("paymentType", getPaymentType())
            .append("warehousingType", getWarehousingType())
            .append("returnGoods", getReturnGoods())
            .append("supplieruser", getSupplieruser())
            .append("supplierphone", getSupplierphone())
            .append("processType", getProcessType())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createUser", getCreateUser())
            .append("remarks", getRemarks())
            .toString();
    }
}
