package com.ruoyi.system.domain;

import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;

/**
 * 采购商品对象 erp_purchase_goods
 *
 * @author ruoyi
 * @date 2022-10-31
 */
public class ErpPurchaseGoods extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 资金账户id */
    private Long id;

    /** 采购订单Id */
    @Excel(name = "采购订单Id")
    private Long purchaseId;

    /** 商品编号 */
    @Excel(name = "商品编号")
    private String goodsNumber;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String goodsName;

    /** 商品规格 */
    @Excel(name = "商品规格")
    private String goodsSpecs;

    /** 商品单位 */
    @Excel(name = "商品单位")
    private Long company;

    /** 采购单价 */
    @Excel(name = "采购单价")
    private BigDecimal monovalent;

    /** 采购数量 */
    @Excel(name = "采购数量")
    private Long quantity;

    /** 税金 */
    @Excel(name = "税金")
    private BigDecimal taxes;

    /** 采购总价 */
    @Excel(name = "采购总价")
    private BigDecimal total;

    /** 创建人 */
    @Excel(name = "创建人")
    private String createUser;

    /** 商品分类名称 */
    private String categoryName;
    /** 品牌名称 */
    private String brandName;


    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setPurchaseId(Long purchaseId)
    {
        this.purchaseId = purchaseId;
    }

    public Long getPurchaseId()
    {
        return purchaseId;
    }
    public void setGoodsNumber(String goodsNumber)
    {
        this.goodsNumber = goodsNumber;
    }

    public String getGoodsNumber()
    {
        return goodsNumber;
    }
    public void setGoodsName(String goodsName)
    {
        this.goodsName = goodsName;
    }

    public String getGoodsName()
    {
        return goodsName;
    }
    public void setGoodsSpecs(String goodsSpecs)
    {
        this.goodsSpecs = goodsSpecs;
    }

    public String getGoodsSpecs()
    {
        return goodsSpecs;
    }
    public void setCompany(Long company)
    {
        this.company = company;
    }

    public Long getCompany()
    {
        return company;
    }
    public void setMonovalent(BigDecimal monovalent)
    {
        this.monovalent = monovalent;
    }

    public BigDecimal getMonovalent()
    {
        return monovalent;
    }
    public void setQuantity(Long quantity)
    {
        this.quantity = quantity;
    }

    public Long getQuantity()
    {
        return quantity;
    }
    public void setTaxes(BigDecimal taxes)
    {
        this.taxes = taxes;
    }

    public BigDecimal getTaxes()
    {
        return taxes;
    }
    public void setTotal(BigDecimal total)
    {
        this.total = total;
    }

    public BigDecimal getTotal()
    {
        return total;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("purchaseId", getPurchaseId())
            .append("goodsNumber", getGoodsNumber())
            .append("goodsName", getGoodsName())
            .append("goodsSpecs", getGoodsSpecs())
            .append("company", getCompany())
            .append("monovalent", getMonovalent())
            .append("quantity", getQuantity())
            .append("taxes", getTaxes())
            .append("total", getTotal())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createUser", getCreateUser())
            .toString();
    }
}
