package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.system.api.model.LoginUser;
import com.ruoyi.system.domain.ErpGoodLabel;
import com.ruoyi.system.mapper.ErpGoodLabelMapper;
import com.ruoyi.system.service.IErpGoodLabelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.ruoyi.common.security.utils.SecurityUtils.getLoginUser;

/**
 * 商品标签Service业务层处理
 *
 * @author ruoyi
 * @date 2022-10-27
 */
@Service
public class ErpGoodLabelServiceImpl implements IErpGoodLabelService
{
    @Autowired
    private ErpGoodLabelMapper erpGoodLabelMapper;

    /**
     * 查询商品标签
     *
     * @param id 商品标签主键
     * @return 商品标签
     */
    @Override
    public ErpGoodLabel selectErpGoodLabelById(Integer id)
    {
        return erpGoodLabelMapper.selectErpGoodLabelById(id);
    }

    /**
     * 查询商品标签列表
     *
     * @param erpGoodLabel 商品标签
     * @return 商品标签
     */
    @Override
    public List<ErpGoodLabel> selectErpGoodLabelList(ErpGoodLabel erpGoodLabel)
    {
        return erpGoodLabelMapper.selectErpGoodLabelList(erpGoodLabel);
    }

    /**
     * 新增商品标签
     *
     * @param erpGoodLabel 商品标签
     * @return 结果
     */
    @Override
    public int insertErpGoodLabel(ErpGoodLabel erpGoodLabel)
    {
        erpGoodLabel.setCreateTime(DateUtils.getNowDate());
        LoginUser loginUser = getLoginUser();
        erpGoodLabel.setCreateUser(loginUser.getUsername());
        return erpGoodLabelMapper.insertErpGoodLabel(erpGoodLabel);
    }

    /**
     * 修改商品标签
     *
     * @param erpGoodLabel 商品标签
     * @return 结果
     */
    @Override
    public int updateErpGoodLabel(ErpGoodLabel erpGoodLabel)
    {
        erpGoodLabel.setUpdateTime(DateUtils.getNowDate());
        return erpGoodLabelMapper.updateErpGoodLabel(erpGoodLabel);
    }

    /**
     * 批量删除商品标签
     *
     * @param ids 需要删除的商品标签主键
     * @return 结果
     */
    @Override
    public int deleteErpGoodLabelByIds(Integer[] ids)
    {
        return erpGoodLabelMapper.deleteErpGoodLabelByIds(ids);
    }

    /**
     * 删除商品标签信息
     *
     * @param id 商品标签主键
     * @return 结果
     */
    @Override
    public int deleteErpGoodLabelById(Integer id)
    {
        return erpGoodLabelMapper.deleteErpGoodLabelById(id);
    }

    @Override
    public Long selectGoodLabelAssociation(String id) {
        return erpGoodLabelMapper.selectGoodLabelAssociation(id);
    }
}
