package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.ErpSupplier;

import java.util.List;

/**
 * 供应商管理Mapper接口
 *
 * @author 刘策
 * @date 2022-10-25
 */
public interface ErpSupplierMapper
{
    /**
     * 查询供应商管理
     *
     * @param id 供应商管理主键
     * @return 供应商管理
     */
    public ErpSupplier selectErpSupplierById(Long id);

    /**
     * 查询供应商管理列表
     *
     * @param erpSupplier 供应商管理
     * @return 供应商管理集合
     */
    public List<ErpSupplier> selectErpSupplierList(ErpSupplier erpSupplier);

    /*首页供应商数量统计*/
    public int selectSupplierNum();

    /**
     * 新增供应商管理
     *
     * @param erpSupplier 供应商管理
     * @return 结果
     */
    public int insertErpSupplier(ErpSupplier erpSupplier);

    /**
     * 修改供应商管理
     *
     * @param erpSupplier 供应商管理
     * @return 结果
     */
    public int updateErpSupplier(ErpSupplier erpSupplier);

    /**
     * 删除供应商管理
     *
     * @param id 供应商管理主键
     * @return 结果
     */
    public int deleteErpSupplierById(Long id);

    /**
     * 批量删除供应商管理
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteErpSupplierByIds(Long[] ids);

    /**
     * 查询供应商的关联关系
     * @param id 供应商id
     *
     * @return 结果
     * */
    Long selectSupplierAssociation(Long id );
}
