package com.ruoyi.system.domain;

import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.annotation.Excels;
import com.ruoyi.common.core.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;

/**
 * 商品信息对象 erp_goods
 *
 * @author 刘策
 * @date 2022-10-26
 */
public class ErpGoods extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String name;

    /** 缩略图 */
    @Excel(name = "缩略图",cellType = Excel.ColumnType.IMAGE,height = 60,width = 40)
    private String thumbnail;

    /** 轮播图：json形式多张 */
    private String banners;

    /** 商品描述 */
    @Excel(name = "商品描述")
    private String description;

    /** 简要描述 */
    private String sortDescription;

    /** 商品价格（单位：分） */
    @Excel(name = "商品价格")
    private BigDecimal price;

    /*商品价格（单位：分）采购订单专用*/
    private BigDecimal monovalent;

    /** 品牌id */
    private Long brandId;

    /** 品牌name */
    @Excel(name = "品牌")
    private String brandName;

    /** 分类id */
    private Long categoryId;

    /** 分类name */
    @Excel(name = "分类")
    private String categoryName;

    /** 成本价格（单位：分） */
    private Long cost;


    /** 商品详情 */
    private String details;

    /** 标签 */
    private String labels;

    /** 库存 */
    @Excel(name = "库存")
    private Integer inventory;

    /** 单位name*/
    @Excel(name = "单位")
    private String companyName;

    /** 默认排序（后台设置） */
    @Excel(name = "默认排序")
    private Long sort;

    /** 销量 */
    private Long sales;

    /** 重量(克) */
    private Long weight;


    /** 上下架 */
    @Excel(name = "上下架", readConverterExp = "1=是,0=否")
    private Long shelves;

    /** 店铺ID */
    private Long shopId;


    /** 创建人 */
    private String createTy;

    /** 条形码（预留字段） */
    private String barCode;

    /** （预留）规格列表 */
    private String speclist;

    /** （预留）属性列表 */
    private String speclabellist;

    /** 单位id */
    private Long company;


    /** 库存阈值*/
    private Long inventoryThreshold;

    /** 仓库name*/
    private String warehouseName;

    /** 仓库id*/

    private String warehouseId;

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public Long getInventoryThreshold() {
        return inventoryThreshold;
    }

    public void setInventoryThreshold(Long inventoryThreshold) {
        this.inventoryThreshold = inventoryThreshold;
    }

    public BigDecimal getMonovalent() {
        return monovalent;
    }

    public void setMonovalent(BigDecimal monovalent) {
        this.monovalent = monovalent;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Long getCompany() {
        return company;
    }

    public void setCompany(Long company) {
        this.company = company;
    }

    public String getBrandName() {
        return brandName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setThumbnail(String thumbnail)
    {
        this.thumbnail = thumbnail;
    }

    public String getThumbnail()
    {
        return thumbnail;
    }
    public void setBanners(String banners)
    {
        this.banners = banners;
    }

    public String getBanners()
    {
        return banners;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getDescription()
    {
        return description;
    }
    public void setSortDescription(String sortDescription)
    {
        this.sortDescription = sortDescription;
    }

    public String getSortDescription()
    {
        return sortDescription;
    }
    public void setPrice(BigDecimal price)
    {
        this.price = price;
    }

    public BigDecimal getPrice()
    {
        return price;
    }
    public void setBrandId(Long brandId)
    {
        this.brandId = brandId;
    }

    public Long getBrandId()
    {
        return brandId;
    }
    public void setCategoryId(Long categoryId)
    {
        this.categoryId = categoryId;
    }

    public Long getCategoryId()
    {
        return categoryId;
    }
    public void setCost(Long cost)
    {
        this.cost = cost;
    }

    public Long getCost()
    {
        return cost;
    }

    public void setDetails(String details)
    {
        this.details = details;
    }

    public String getDetails()
    {
        return details;
    }
    public void setLabels(String labels)
    {
        this.labels = labels;
    }

    public String getLabels()
    {
        return labels;
    }
    public void setInventory(Integer inventory)
    {
        this.inventory = inventory;
    }

    public Integer getInventory()
    {
        return inventory;
    }
    public void setSales(Long sales)
    {
        this.sales = sales;
    }

    public Long getSales()
    {
        return sales;
    }
    public void setWeight(Long weight)
    {
        this.weight = weight;
    }

    public Long getWeight()
    {
        return weight;
    }

    public void setShelves(Long shelves)
    {
        this.shelves = shelves;
    }

    public Long getShelves()
    {
        return shelves;
    }
    public void setShopId(Long shopId)
    {
        this.shopId = shopId;
    }

    public Long getShopId()
    {
        return shopId;
    }
    public void setSort(Long sort)
    {
        this.sort = sort;
    }

    public Long getSort()
    {
        return sort;
    }
    public void setCreateTy(String createTy)
    {
        this.createTy = createTy;
    }

    public String getCreateTy()
    {
        return createTy;
    }
    public void setBarCode(String barCode)
    {
        this.barCode = barCode;
    }

    public String getBarCode()
    {
        return barCode;
    }
    public void setSpeclist(String speclist)
    {
        this.speclist = speclist;
    }

    public String getSpeclist()
    {
        return speclist;
    }
    public void setSpeclabellist(String speclabellist)
    {
        this.speclabellist = speclabellist;
    }

    public String getSpeclabellist()
    {
        return speclabellist;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("thumbnail", getThumbnail())
            .append("banners", getBanners())
            .append("description", getDescription())
            .append("sortDescription", getSortDescription())
            .append("price", getPrice())
            .append("brandId", getBrandId())
            .append("categoryId", getCategoryId())
            .append("cost", getCost())
            .append("details", getDetails())
            .append("labels", getLabels())
            .append("inventory", getInventory())
            .append("sales", getSales())
            .append("weight", getWeight())
            .append("shelves", getShelves())
            .append("shopId", getShopId())
            .append("sort", getSort())
            .append("createTy", getCreateTy())
            .append("updateTime", getUpdateTime())
            .append("createTime", getCreateTime())
            .append("barCode", getBarCode())
            .append("speclist", getSpeclist())
            .append("speclabellist", getSpeclabellist())
            .toString();
    }
}
