package com.ruoyi.system.service.impl;

import java.util.List;

import com.alibaba.nacos.shaded.org.checkerframework.checker.units.qual.A;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.system.domain.ErpInventoryGoods;
import com.ruoyi.system.mapper.ErpInventoryGoodsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ErpInventoryMapper;
import com.ruoyi.system.domain.ErpInventory;
import com.ruoyi.system.service.IErpInventoryService;

import static com.ruoyi.common.security.utils.SecurityUtils.getLoginUser;

/**
 * 库存盘点Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-11-21
 */
@Service
public class ErpInventoryServiceImpl implements IErpInventoryService 
{
    @Autowired
    private ErpInventoryMapper erpInventoryMapper;

    @Autowired
    private ErpInventoryGoodsMapper erpInventoryGoodsMapper;

    /**
     * 查询库存盘点
     * 
     * @param id 库存盘点主键
     * @return 库存盘点
     */
    @Override
    public ErpInventory selectErpInventoryById(Long id)
    {
        return erpInventoryMapper.selectErpInventoryById(id);
    }

    /**
     * 查询库存盘点列表
     * 
     * @param erpInventory 库存盘点
     * @return 库存盘点
     */
    @Override
    public List<ErpInventory> selectErpInventoryList(ErpInventory erpInventory)
    {
        return erpInventoryMapper.selectErpInventoryList(erpInventory);
    }

    /**
     * 新增库存盘点
     * 
     * @param erpInventory 库存盘点
     * @return 结果
     */
    @Override
    public int insertErpInventory(ErpInventory erpInventory)
    {
        erpInventory.setCreateTime(DateUtils.getNowDate());
        erpInventory.setCreateUser(getLoginUser().getUsername());
        erpInventory.setType(1L);
        int i = erpInventoryMapper.insertErpInventory(erpInventory);
        List<ErpInventoryGoods> erpInventoryGoodsList = erpInventory.getErpInventoryGoodsList();
        for (ErpInventoryGoods erpInventoryGoods : erpInventoryGoodsList) {
            erpInventoryGoods.setInventoryId(erpInventory.getId());
            erpInventoryGoods.setCreateTime(DateUtils.getNowDate());
            erpInventoryGoods.setCreatUser(getLoginUser().getUsername());
            erpInventoryGoodsMapper.insertErpInventoryGoods(erpInventoryGoods);
        }
        return i;
    }

    /**
     * 修改库存盘点
     * 
     * @param erpInventory 库存盘点
     * @return 结果
     */
    @Override
    public int updateErpInventory(ErpInventory erpInventory)
    {
        erpInventoryGoodsMapper.deleteErpInventoryGoodsById(erpInventory.getId());
        List<ErpInventoryGoods> erpInventoryGoodsList = erpInventory.getErpInventoryGoodsList();
        for (ErpInventoryGoods erpInventoryGoods : erpInventoryGoodsList) {
            erpInventoryGoods.setInventoryId(erpInventory.getId());
            erpInventoryGoods.setCreateTime(DateUtils.getNowDate());
            erpInventoryGoods.setCreatUser(getLoginUser().getUsername());
            erpInventoryGoodsMapper.insertErpInventoryGoods(erpInventoryGoods);
        }

        return erpInventoryMapper.updateErpInventory(erpInventory);
    }

    /**
     * 批量删除库存盘点
     * 
     * @param ids 需要删除的库存盘点主键
     * @return 结果
     */
    @Override
    public int deleteErpInventoryByIds(Long[] ids)
    {
        return erpInventoryMapper.deleteErpInventoryByIds(ids);
    }

    /**
     * 删除库存盘点信息
     * 
     * @param id 库存盘点主键
     * @return 结果
     */
    @Override
    public int deleteErpInventoryById(Long id)
    {
        return erpInventoryMapper.deleteErpInventoryById(id);
    }

    @Override
    public int updateInventoryType(Long inventoryId, Long type) {
        /*ErpInventory erpInventory = erpInventoryMapper.selectErpInventoryById(inventoryId);*/
        return erpInventoryMapper.updateInventoryType(inventoryId, type);
    }
}
