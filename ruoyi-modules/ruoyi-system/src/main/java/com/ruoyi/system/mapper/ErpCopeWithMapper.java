package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.ErpCopeWith;

/**
 * 应付账款Mapper接口
 * 
 * @author ruoyi
 * @date 2022-11-18
 */
public interface ErpCopeWithMapper 
{
    /**
     * 查询应付账款
     * 
     * @param id 应付账款主键
     * @return 应付账款
     */
    public ErpCopeWith selectErpCopeWithById(Long id);
    /*财务管理查看付款页面信息*/
    public ErpCopeWith selectErpCopeWithByIdLeftPaymentRecord(Long id);

    /**
     * 查询应付账款列表
     * 
     * @param erpCopeWith 应付账款
     * @return 应付账款集合
     */
    public List<ErpCopeWith> selectErpCopeWithList(ErpCopeWith erpCopeWith);

    /**
     * 新增应付账款
     * 
     * @param erpCopeWith 应付账款
     * @return 结果
     */
    public int insertErpCopeWith(ErpCopeWith erpCopeWith);

    /**
     * 修改应付账款
     * 
     * @param erpCopeWith 应付账款
     * @return 结果
     */
    public int updateErpCopeWith(ErpCopeWith erpCopeWith);

    /**
     * 删除应付账款
     * 
     * @param id 应付账款主键
     * @return 结果
     */
    public int deleteErpCopeWithById(Long id);

    /**
     * 批量删除应付账款
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteErpCopeWithByIds(Long[] ids);
}
