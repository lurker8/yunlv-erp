package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ErpGoodsPurchaseWarehouseMapper;
import com.ruoyi.system.domain.ErpGoodsPurchaseWarehouse;
import com.ruoyi.system.service.IErpGoodsPurchaseWarehouseService;

/**
 * 采购商品仓库关联Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-11-10
 */
@Service
public class ErpGoodsPurchaseWarehouseServiceImpl implements IErpGoodsPurchaseWarehouseService 
{
    @Autowired
    private ErpGoodsPurchaseWarehouseMapper erpGoodsPurchaseWarehouseMapper;

    /**
     * 查询采购商品仓库关联
     * 
     * @param id 采购商品仓库关联主键
     * @return 采购商品仓库关联
     */
    @Override
    public ErpGoodsPurchaseWarehouse selectErpGoodsPurchaseWarehouseById(Long id)
    {
        return erpGoodsPurchaseWarehouseMapper.selectErpGoodsPurchaseWarehouseById(id);
    }

    /**
     * 查询采购商品仓库关联列表
     * 
     * @param erpGoodsPurchaseWarehouse 采购商品仓库关联
     * @return 采购商品仓库关联
     */
    @Override
    public List<ErpGoodsPurchaseWarehouse> selectErpGoodsPurchaseWarehouseList(ErpGoodsPurchaseWarehouse erpGoodsPurchaseWarehouse)
    {
        return erpGoodsPurchaseWarehouseMapper.selectErpGoodsPurchaseWarehouseList(erpGoodsPurchaseWarehouse);
    }

    /**
     * 新增采购商品仓库关联
     * 
     * @param erpGoodsPurchaseWarehouse 采购商品仓库关联
     * @return 结果
     */
    @Override
    public int insertErpGoodsPurchaseWarehouse(ErpGoodsPurchaseWarehouse erpGoodsPurchaseWarehouse)
    {
        erpGoodsPurchaseWarehouse.setCreateTime(DateUtils.getNowDate());
        return erpGoodsPurchaseWarehouseMapper.insertErpGoodsPurchaseWarehouse(erpGoodsPurchaseWarehouse);
    }

    /**
     * 修改采购商品仓库关联
     * 
     * @param erpGoodsPurchaseWarehouse 采购商品仓库关联
     * @return 结果
     */
    @Override
    public int updateErpGoodsPurchaseWarehouse(ErpGoodsPurchaseWarehouse erpGoodsPurchaseWarehouse)
    {
        erpGoodsPurchaseWarehouse.setUpdateTime(DateUtils.getNowDate());
        return erpGoodsPurchaseWarehouseMapper.updateErpGoodsPurchaseWarehouse(erpGoodsPurchaseWarehouse);
    }

    /**
     * 批量删除采购商品仓库关联
     * 
     * @param ids 需要删除的采购商品仓库关联主键
     * @return 结果
     */
    @Override
    public int deleteErpGoodsPurchaseWarehouseByIds(Long[] ids)
    {
        return erpGoodsPurchaseWarehouseMapper.deleteErpGoodsPurchaseWarehouseByIds(ids);
    }

    /**
     * 删除采购商品仓库关联信息
     * 
     * @param id 采购商品仓库关联主键
     * @return 结果
     */
    @Override
    public int deleteErpGoodsPurchaseWarehouseById(Long id)
    {
        return erpGoodsPurchaseWarehouseMapper.deleteErpGoodsPurchaseWarehouseById(id);
    }
}
