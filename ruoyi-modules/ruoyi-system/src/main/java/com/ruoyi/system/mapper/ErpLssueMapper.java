package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.ErpLssue;

/**
 * 出库单Mapper接口
 * 
 * @author liuce
 * @date 2022-11-17
 */
public interface ErpLssueMapper 
{
    /**
     * 查询出库单
     * 
     * @param id 出库单主键
     * @return 出库单
     */
    public ErpLssue selectErpLssueById(Long id);

    /**
     * 查询出库单列表
     * 
     * @param erpLssue 出库单
     * @return 出库单集合
     */
    public List<ErpLssue> selectErpLssueList(ErpLssue erpLssue);

    /**
     * 新增出库单
     * 
     * @param erpLssue 出库单
     * @return 结果
     */
    public int insertErpLssue(ErpLssue erpLssue);

    /**
     * 修改出库单
     * 
     * @param erpLssue 出库单
     * @return 结果
     */
    public int updateErpLssue(ErpLssue erpLssue);

    /**
     * 删除出库单
     * 
     * @param id 出库单主键
     * @return 结果
     */
    public int deleteErpLssueById(Long id);

    /**
     * 批量删除出库单
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteErpLssueByIds(Long[] ids);
}
