package com.ruoyi.system.domain;

import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;

/**
 * 仓库管理对象 erp_warehouse
 *
 * @author 刘策
 * @date 2022-10-26
 */
public class ErpWarehouse extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 仓库id
     */
    private Long id;

    /**
     * 仓库name
     */
    @Excel(name = "仓库")
    private String name;

    /**
     * 仓库管理员
     */
    @Excel(name = "仓库管理员")
    private String warehouseAdministrators;

    /**
     * 管理员电话
     */
    @Excel(name = "管理员电话")
    private String administratorsPhone;

    /**
     * 仓库电话
     */
    @Excel(name = "仓库电话")
    private String warehousePhone;

    /**
     * 仓库类型
     */
    @Excel(name = "仓库类型"  ,readConverterExp = "0=一级仓库,1=二级仓库")
    private Long warehouseCategory;

    /**
     * 仓库地址
     */
    @Excel(name = "仓库地址")
    private String warehouseAddress;

    /**
     * 详细地址
     */
    @Excel(name = "详细地址")
    private String detailedAddress;

    /**
     * 备注
     */
    @Excel(name = "备注")
    private String remarks;

    /**
     * 创建人
     */
    private Long createUser;

    /**
     * 仓位(预留)
     */
    private String position;

    /**
     * 省
     */
    private Long province;

    /**
     * 市
     */
    private Long city;

    /**
     * 区
     */
    private Long area;


    /**
     * 创建人name
     */
    private String createUserName;

    /*订单入库商品Id(给采购入库用)*/
    private Long goodsId;
    /*订单入库商品名称(给采购入库用)*/
    private String goodsName;
    /*订单入库数量(给采购入库用)*/
    private Long warehousingQuantity;
    /*订单入库退货数量(给采购入库用)*/
    private Long warehousingReturnQuantity;
    /*修改入库id(给采购入库用)*/
    private Long warehousingId;

    /*商品单价(给采购入库用)*/
    private BigDecimal goodsMonovalent;

    public BigDecimal getGoodsMonovalent() {
        return goodsMonovalent;
    }

    public void setGoodsMonovalent(BigDecimal goodsMonovalent) {
        this.goodsMonovalent = goodsMonovalent;
    }

    public Long getWarehousingId() {
        return warehousingId;
    }

    public void setWarehousingId(Long warehousingId) {
        this.warehousingId = warehousingId;
    }

    public Long getWarehousingReturnQuantity() {
        return warehousingReturnQuantity;
    }

    public void setWarehousingReturnQuantity(Long warehousingReturnQuantity) {
        this.warehousingReturnQuantity = warehousingReturnQuantity;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public Long getWarehousingQuantity() {
        return warehousingQuantity;
    }

    public void setWarehousingQuantity(Long warehousingQuantity) {
        this.warehousingQuantity = warehousingQuantity;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }


    public Long getProvince() {
        return province;
    }

    public void setProvince(Long province) {
        this.province = province;
    }

    public Long getCity() {
        return city;
    }

    public void setCity(Long city) {
        this.city = city;
    }

    public Long getArea() {
        return area;
    }

    public void setArea(Long area) {
        this.area = area;
    }

    public String getDetailedAddress() {
        return detailedAddress;
    }

    public void setDetailedAddress(String detailedAddress) {
        this.detailedAddress = detailedAddress;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setWarehouseAdministrators(String warehouseAdministrators) {
        this.warehouseAdministrators = warehouseAdministrators;
    }

    public String getWarehouseAdministrators() {
        return warehouseAdministrators;
    }

    public void setAdministratorsPhone(String administratorsPhone) {
        this.administratorsPhone = administratorsPhone;
    }

    public String getAdministratorsPhone() {
        return administratorsPhone;
    }

    public void setWarehousePhone(String warehousePhone) {
        this.warehousePhone = warehousePhone;
    }

    public String getWarehousePhone() {
        return warehousePhone;
    }

    public void setWarehouseCategory(Long warehouseCategory) {
        this.warehouseCategory = warehouseCategory;
    }

    public Long getWarehouseCategory() {
        return warehouseCategory;
    }

    public void setWarehouseAddress(String warehouseAddress) {
        this.warehouseAddress = warehouseAddress;
    }

    public String getWarehouseAddress() {
        return warehouseAddress;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setCreateUser(Long createUser) {
        this.createUser = createUser;
    }

    public Long getCreateUser() {
        return createUser;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPosition() {
        return position;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("name", getName())
                .append("warehouseAdministrators", getWarehouseAdministrators())
                .append("administratorsPhone", getAdministratorsPhone())
                .append("warehousePhone", getWarehousePhone())
                .append("warehouseCategory", getWarehouseCategory())
                .append("warehouseAddress", getWarehouseAddress())
                .append("remarks", getRemarks())
                .append("createTime", getCreateTime())
                .append("updateTime", getUpdateTime())
                .append("createUser", getCreateUser())
                .append("position", getPosition())
                .toString();
    }
}
