package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.ErpGoodsSaleWarehouse;
import com.ruoyi.system.domain.ErpSale;
import com.ruoyi.system.domain.ErpSaleGoodNum;
import com.ruoyi.system.domain.SaleGoods;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 销售订单Mapper接口
 *
 * @author 刘策
 * @date 2022-10-28
 */
public interface ErpSaleMapper {
    /**
     * 查询销售订单
     *
     * @param id 销售订单主键
     * @return 销售订单
     */
    public ErpSale selectErpSaleById(Long id);

    /**
     * 查询销售订单列表
     *
     * @param erpSale 销售订单
     * @return 销售订单集合
     */
    public List<ErpSale> selectErpSaleList(ErpSale erpSale);

    /*首页折线图销售单数*/
    public List<Integer> selectSaleCount(Map map);

/*    *//**
     * 查询销售发货订单列表
     *
     * @param erpSale 销售订单
     * @return 销售订单集合
     *//*
    public List<ErpSale> selectErpSaleDeliveryList(ErpSale erpSale);*/

    /**
     * 新增销售订单
     *
     * @param erpSale 销售订单
     * @return 结果
     */
    public int insertErpSale(ErpSale erpSale);

    /**
     * 修改销售订单
     *
     * @param erpSale 销售订单
     * @return 结果
     */
    public int updateErpSale(ErpSale erpSale);

    /**
     * 删除销售订单
     *
     * @param id 销售订单主键
     * @return 结果
     */
    public int deleteErpSaleById(Long id);

    /**
     * 批量删除销售订单
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteErpSaleByIds(Long[] ids);

    /**
     * 删除销售订单关联商品信息
     *
     * @param saleGoods 销售订单
     * @return 结果
     */
    public int deleteErpSaleGoodById(SaleGoods saleGoods);


    /**
     * 删除销售订单关联商品信息
     *
     * @param saleId 销售订单
     * @return 结果
     */
    public int deleteErpSaleGoodBySaleId(Long saleId);

    /**
     * 修改销售订单状态
     *
     * @param saleId 销售订单id
     * @param type   状态
     * @return 结果
     */
    public int updateErpSaleType(@Param("saleId") Long saleId, @Param("type") Long type);

    /**
     * 添加销售仓库商品关联表
     *
     * @param erpGoodsSaleWarehouse 销售仓库商品关联
     * @return 结果
     */
    public int addGoodsSaleWarehouse(ErpGoodsSaleWarehouse erpGoodsSaleWarehouse);

    /**
     * 修改销售仓库商品关联表
     *
     * @param erpGoodsSaleWarehouse 销售仓库商品关联
     * @return 结果
     */
    public int returnGoods(ErpGoodsSaleWarehouse erpGoodsSaleWarehouse);

    /**
     * 根据销售id查询一个销售订单商品从各仓库各发货多少件商品
     *
     * @param saleId 销售id
     * @return 返回结果list
     */
    List<ErpSaleGoodNum> selectGoodNumBySaleIdFromSale(Long saleId);

    /**
     * 根据销售id查询一个销售订单商品各出售多少件
     *
     * @param saleId 销售id
     * @return 返回结果list
     */
    List<ErpSaleGoodNum> selectGoodNumBySaleIdFromWarehouse(Long saleId);

    /**
     * 根据销售id查询商品出库数量
     *
     * @param saleId 销售id
     * @return 结果
     */
    List<ErpGoodsSaleWarehouse> sSaleGood(Long saleId);

    /**
     * 退货时修改订单的销售额
     * @param saleId 销售id
     * @param returnAmount 退货金额
     * @return 结果
     * */
    int modifySales(@Param("saleId") Long saleId,@Param("returnAmount")BigDecimal returnAmount);

    /**
     * 修改退货单是否确认退货商品已入库状态
     * @param saleId 销售id
     * @return 结果
     * */
    int updateReturnType(Long saleId);

    /**
     * 查寻销售订单的出库仓库
     * @param saleId 销售id
     * @return  结果
     * */
    List<String> selectSaleLssueWarehouse(Long saleId);
}
