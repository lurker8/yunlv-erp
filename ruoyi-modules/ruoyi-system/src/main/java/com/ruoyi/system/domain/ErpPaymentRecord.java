package com.ruoyi.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 付款记录对象 erp_payment_record
 * 
 * @author ruoyi
 * @date 2022-11-18
 */
public class ErpPaymentRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 采购id */
    @Excel(name = "采购id")
    private Long purchaseId;

    /** 付款时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "付款时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date paymentTime;

    /** 付款金额 */
    @Excel(name = "付款金额")
    private BigDecimal amountPayment;

    /** 付款人 */
    @Excel(name = "付款人")
    private String drawee;

    /** 付款凭证 */
    @Excel(name = "付款凭证")
    private String paymentVoucher;

    /** 付款备注 */
    @Excel(name = "付款备注")
    private String paymentRemarks;

    /** 创建人 */
    @Excel(name = "创建人")
    private String createUser;

    /** 付款账户 */
    @Excel(name = "付款账户")
    private String paymentAccount;

    private String paymentAccountName;

    /** 付款账单id */
    @Excel(name = "付款账单id")
    private Long copeWithId;

    public String getPaymentAccountName() {
        return paymentAccountName;
    }

    public void setPaymentAccountName(String paymentAccountName) {
        this.paymentAccountName = paymentAccountName;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setPurchaseId(Long purchaseId) 
    {
        this.purchaseId = purchaseId;
    }

    public Long getPurchaseId() 
    {
        return purchaseId;
    }
    public void setPaymentTime(Date paymentTime) 
    {
        this.paymentTime = paymentTime;
    }

    public Date getPaymentTime() 
    {
        return paymentTime;
    }
    public void setAmountPayment(BigDecimal amountPayment) 
    {
        this.amountPayment = amountPayment;
    }

    public BigDecimal getAmountPayment() 
    {
        return amountPayment;
    }
    public void setDrawee(String drawee) 
    {
        this.drawee = drawee;
    }

    public String getDrawee() 
    {
        return drawee;
    }
    public void setPaymentVoucher(String paymentVoucher) 
    {
        this.paymentVoucher = paymentVoucher;
    }

    public String getPaymentVoucher() 
    {
        return paymentVoucher;
    }
    public void setPaymentRemarks(String paymentRemarks) 
    {
        this.paymentRemarks = paymentRemarks;
    }

    public String getPaymentRemarks() 
    {
        return paymentRemarks;
    }
    public void setCreateUser(String createUser) 
    {
        this.createUser = createUser;
    }

    public String getCreateUser() 
    {
        return createUser;
    }
    public void setPaymentAccount(String paymentAccount) 
    {
        this.paymentAccount = paymentAccount;
    }

    public String getPaymentAccount() 
    {
        return paymentAccount;
    }
    public void setCopeWithId(Long copeWithId) 
    {
        this.copeWithId = copeWithId;
    }

    public Long getCopeWithId() 
    {
        return copeWithId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("purchaseId", getPurchaseId())
            .append("paymentTime", getPaymentTime())
            .append("amountPayment", getAmountPayment())
            .append("drawee", getDrawee())
            .append("paymentVoucher", getPaymentVoucher())
            .append("paymentRemarks", getPaymentRemarks())
            .append("createTime", getCreateTime())
            .append("createUser", getCreateUser())
            .append("paymentAccount", getPaymentAccount())
            .append("copeWithId", getCopeWithId())
            .toString();
    }
}
