package com.ruoyi.system.domain;

import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 客户管理对象 erp_customer
 *
 * @author 刘策
 * @date 2022-10-24
 */
public class ErpCustomer extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 客户id */
    private Long id;

    /** 客户名称 */
    @Excel(name = "客户名称")
    private String name;

    /** 客户电话 */
    @Excel(name = "客户电话")
    private String phone;

    /** 客户地址 */
    @Excel(name = "客户地址")
    private String customerAddress;

    /** 详细地址 */
    @Excel(name = "详细地址")
    private String detailedAddress;

    /** 客户等级 */
    @Excel(name = "客户等级"  ,readConverterExp = "0=青铜,1=白银,2=黄金,3=铂金,4=钻石")
    private Long customerCategory;

    /** 客户开户银行 */
    @Excel(name = "客户开户银行")
    private String customerBank;

    /** 客户银行卡号 */
    @Excel(name = "客户银行卡号")
    private String customerBankAccount;

    /** 备注信息 */
    @Excel(name = "备注信息")
    private String remarks;

    /** 创建人id */
    private Long createUser;

    /** 创建人Name*/
    private String createUserName;

    /** 省 */
    private Long province;

    /** 市 */
    private Long city;

    /** 区 */
    private Long area;


    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public String getDetailedAddress() {
        return detailedAddress;
    }

    public void setDetailedAddress(String detailedAddress) {
        this.detailedAddress = detailedAddress;
    }

    public Long getProvince() {
        return province;
    }

    public void setProvince(Long province) {
        this.province = province;
    }

    public Long getCity() {
        return city;
    }

    public void setCity(Long city) {
        this.city = city;
    }

    public Long getArea() {
        return area;
    }

    public void setArea(Long area) {
        this.area = area;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public String getPhone()
    {
        return phone;
    }
    public void setCustomerAddress(String customerAddress)
    {
        this.customerAddress = customerAddress;
    }

    public String getCustomerAddress()
    {
        return customerAddress;
    }
    public void setCustomerCategory(Long customerCategory)
    {
        this.customerCategory = customerCategory;
    }

    public Long getCustomerCategory()
    {
        return customerCategory;
    }
    public void setCustomerBank(String customerBank)
    {
        this.customerBank = customerBank;
    }

    public String getCustomerBank()
    {
        return customerBank;
    }
    public void setCustomerBankAccount(String customerBankAccount)
    {
        this.customerBankAccount = customerBankAccount;
    }

    public String getCustomerBankAccount()
    {
        return customerBankAccount;
    }
    public void setRemarks(String remarks)
    {
        this.remarks = remarks;
    }

    public String getRemarks()
    {
        return remarks;
    }
    public void setCreateUser(Long createUser)
    {
        this.createUser = createUser;
    }

    public Long getCreateUser()
    {
        return createUser;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("phone", getPhone())
            .append("customerAddress", getCustomerAddress())
            .append("customerCategory", getCustomerCategory())
            .append("customerBank", getCustomerBank())
            .append("customerBankAccount", getCustomerBankAccount())
            .append("remarks", getRemarks())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createUser", getCreateUser())
            .toString();
    }
}
