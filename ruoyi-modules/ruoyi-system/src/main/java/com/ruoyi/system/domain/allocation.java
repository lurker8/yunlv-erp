package com.ruoyi.system.domain;

public class allocation {

    private Long goodNum;

    private Long warehousingId;

    private Long lssueId;

    private Long goodId;

    public Long getWarehousingId() {
        return warehousingId;
    }

    public void setWarehousingId(Long warehousingId) {
        this.warehousingId = warehousingId;
    }

    public Long getGoodNum() {
        return goodNum;
    }

    public void setGoodNum(Long goodNum) {
        this.goodNum = goodNum;
    }

    public Long getLssueId() {
        return lssueId;
    }

    public void setLssueId(Long lssueId) {
        this.lssueId = lssueId;
    }

    public Long getGoodId() {
        return goodId;
    }

    public void setGoodId(Long goodId) {
        this.goodId = goodId;
    }
}
