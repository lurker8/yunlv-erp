package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 仓库调拨商品关联对象 erp_allocation_goods
 *
 * @author ruoyi
 * @date 2022-11-17
 */
public class ErpAllocationGoods extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 库存调拨商品关联id */
    private Long id;

    /** 调拨id */
    @Excel(name = "调拨id")
    private Long allocationId;

    /** 商品id */
    @Excel(name = "商品id")
    private Long goodId;

    /**商品name*/
    private String goodName;

    /** 商品数量 */
    @Excel(name = "商品数量")
    private Long goodNumber;

    /** 创建人 */
    @Excel(name = "创建人")
    private String createUser;

    /** 品牌name*/
    private String brandName;

    /** 分类name*/
    private String categoryName;

    /** 单位name*/
    private String companyName;

    /** 仓库name*/
    private String warehouseName;

    /** 仓库商品的库存*/
    private Long stock;

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public Long getStock() {
        return stock;
    }

    public void setStock(Long stock) {
        this.stock = stock;
    }

    public String getGoodName() {
        return goodName;
    }

    public void setGoodName(String goodName) {
        this.goodName = goodName;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setAllocationId(Long allocationId)
    {
        this.allocationId = allocationId;
    }

    public Long getAllocationId()
    {
        return allocationId;
    }
    public void setGoodId(Long goodId)
    {
        this.goodId = goodId;
    }

    public Long getGoodId()
    {
        return goodId;
    }
    public void setGoodNumber(Long goodNumber)
    {
        this.goodNumber = goodNumber;
    }

    public Long getGoodNumber()
    {
        return goodNumber;
    }
    public void setCreateUser(String createUser)
    {
        this.createUser = createUser;
    }

    public String getCreateUser()
    {
        return createUser;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("allocationId", getAllocationId())
                .append("goodId", getGoodId())
                .append("goodNumber", getGoodNumber())
                .append("createTime", getCreateTime())
                .append("updateTime", getUpdateTime())
                .append("createUser", getCreateUser())
                .toString();
    }
}
