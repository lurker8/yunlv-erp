package com.ruoyi.system.service;

import com.ruoyi.system.domain.ErpWarehouse;

import java.util.List;

/**
 * 仓库管理Service接口
 *
 * @author 刘策
 * @date 2022-10-26
 */
public interface IErpWarehouseService
{
    /**
     * 查询仓库管理
     *
     * @param id 仓库管理主键
     * @return 仓库管理
     */
    public ErpWarehouse selectErpWarehouseById(Long id);

    /**
     * 查询仓库管理列表
     *
     * @param erpWarehouse 仓库管理
     * @return 仓库管理集合
     */
    public List<ErpWarehouse> selectErpWarehouseList(ErpWarehouse erpWarehouse);

    /**
     * 新增仓库管理
     *
     * @param erpWarehouse 仓库管理
     * @return 结果
     */
    public int insertErpWarehouse(ErpWarehouse erpWarehouse);

    /**
     * 修改仓库管理
     *
     * @param erpWarehouse 仓库管理
     * @return 结果
     */
    public int updateErpWarehouse(ErpWarehouse erpWarehouse);

    /**
     * 批量删除仓库管理
     *
     * @param ids 需要删除的仓库管理主键集合
     * @return 结果
     */
    public int deleteErpWarehouseByIds(Long[] ids);

    /**
     * 删除仓库管理信息
     *
     * @param id 仓库管理主键
     * @return 结果
     */
    public int deleteErpWarehouseById(Long id);

    /**
     * 查询仓库关联关系
     *
     * @param id 仓库id
     *
     * @return 结果
     * */
    Long selectWarehouseAssociation(Long id);
}
