package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ErpPaymentRecordMapper;
import com.ruoyi.system.domain.ErpPaymentRecord;
import com.ruoyi.system.service.IErpPaymentRecordService;

/**
 * 付款记录Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-11-18
 */
@Service
public class ErpPaymentRecordServiceImpl implements IErpPaymentRecordService 
{
    @Autowired
    private ErpPaymentRecordMapper erpPaymentRecordMapper;

    /**
     * 查询付款记录
     * 
     * @param id 付款记录主键
     * @return 付款记录
     */
    @Override
    public ErpPaymentRecord selectErpPaymentRecordById(Long id)
    {
        return erpPaymentRecordMapper.selectErpPaymentRecordById(id);
    }

    /**
     * 查询付款记录列表
     * 
     * @param erpPaymentRecord 付款记录
     * @return 付款记录
     */
    @Override
    public List<ErpPaymentRecord> selectErpPaymentRecordList(ErpPaymentRecord erpPaymentRecord)
    {
        return erpPaymentRecordMapper.selectErpPaymentRecordList(erpPaymentRecord);
    }

    /**
     * 新增付款记录
     * 
     * @param erpPaymentRecord 付款记录
     * @return 结果
     */
    @Override
    public int insertErpPaymentRecord(ErpPaymentRecord erpPaymentRecord)
    {
        erpPaymentRecord.setCreateTime(DateUtils.getNowDate());
        return erpPaymentRecordMapper.insertErpPaymentRecord(erpPaymentRecord);
    }

    /**
     * 修改付款记录
     * 
     * @param erpPaymentRecord 付款记录
     * @return 结果
     */
    @Override
    public int updateErpPaymentRecord(ErpPaymentRecord erpPaymentRecord)
    {
        return erpPaymentRecordMapper.updateErpPaymentRecord(erpPaymentRecord);
    }

    /**
     * 批量删除付款记录
     * 
     * @param ids 需要删除的付款记录主键
     * @return 结果
     */
    @Override
    public int deleteErpPaymentRecordByIds(Long[] ids)
    {
        return erpPaymentRecordMapper.deleteErpPaymentRecordByIds(ids);
    }

    /**
     * 删除付款记录信息
     * 
     * @param id 付款记录主键
     * @return 结果
     */
    @Override
    public int deleteErpPaymentRecordById(Long id)
    {
        return erpPaymentRecordMapper.deleteErpPaymentRecordById(id);
    }
}
