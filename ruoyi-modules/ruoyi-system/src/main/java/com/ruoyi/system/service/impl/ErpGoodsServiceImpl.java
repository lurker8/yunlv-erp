package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.system.api.model.LoginUser;
import com.ruoyi.system.domain.ErpGoods;
import com.ruoyi.system.mapper.ErpGoodsMapper;
import com.ruoyi.system.service.IErpGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import static com.ruoyi.common.security.utils.SecurityUtils.getLoginUser;

/**
 * 商品信息Service业务层处理
 *
 * @author 刘策
 * @date 2022-10-26
 */
@Service
public class ErpGoodsServiceImpl implements IErpGoodsService
{
    @Autowired
    private ErpGoodsMapper erpGoodsMapper;

    /**
     * 查询商品信息
     *
     * @param id 商品信息主键
     * @return 商品信息
     */
    @Override
    public ErpGoods selectErpGoodsById(Long id)
    {
        return erpGoodsMapper.selectErpGoodsById(id);
    }

    /**
     * 查询商品信息列表
     *
     * @param erpGoods 商品信息
     * @return 商品信息
     */
    @Override
    public List<ErpGoods> selectErpGoodsList(ErpGoods erpGoods)
    {
        return erpGoodsMapper.selectErpGoodsList(erpGoods);
    }

    /**
     * 查询阈值商品信息列表
     *
     * @param erpGoods 商品信息
     * @return 商品信息
     */
    @Override
    public List<ErpGoods> selectThresholdGoodsList(ErpGoods erpGoods)
    {
        return erpGoodsMapper.selectThresholdGoodsList(erpGoods);
    }

    /*首页商品量统计*/
    @Override
    public int selectGoodsNum() {
        return erpGoodsMapper.selectGoodsNum();
    }

    /*首页阈值商品量统计*/
    @Override
    public int selectThresholdNum() {
        return erpGoodsMapper.selectThresholdNum();
    }
    /*首页饼状图商品品牌名称*/
    @Override
    public List<String> selectBrandName(Map map) {
        return erpGoodsMapper.selectBrandName(map);
    }

    /*首页饼状图商品品牌数量*/
    @Override
    public List<Integer> selectBrandCount(Map map) {
        return erpGoodsMapper.selectBrandCount(map);
    }

    @Override
    public List<ErpGoods> selectErpGoodsWarehouseList(ErpGoods erpGoods) {
        return erpGoodsMapper.selectErpGoodsWarehouseList(erpGoods);
    }

    // 查询商品信息列表采购订单专用
    @Override
    public List<ErpGoods> listGoodsPurchase(ErpGoods erpGoods)
    {
        return erpGoodsMapper.listGoodsPurchase(erpGoods);
    }

    /**
     * 新增商品信息
     *
     * @param erpGoods 商品信息
     * @return 结果
     */
    @Override
    public int insertErpGoods(ErpGoods erpGoods)
    {
        erpGoods.setCreateTime(DateUtils.getNowDate());
        LoginUser loginUser = getLoginUser();
        erpGoods.setCreateTy(loginUser.getUsername());
        return erpGoodsMapper.insertErpGoods(erpGoods);
    }

    /**
     * 修改商品信息
     *
     * @param erpGoods 商品信息
     * @return 结果
     */
    @Override
    public int updateErpGoods(ErpGoods erpGoods)
    {
        erpGoods.setUpdateTime(DateUtils.getNowDate());
        return erpGoodsMapper.updateErpGoods(erpGoods);
    }

    /*修改商品信息(采购入库按钮专用)*/
    @Override
    public int updateErpGoodsWarehousing(ErpGoods erpGoods)
    {
        erpGoods.setUpdateTime(DateUtils.getNowDate());
        return erpGoodsMapper.updateErpGoodsWarehousing(erpGoods);
    }

    /**
     * 批量删除商品信息
     *
     * @param ids 需要删除的商品信息主键
     * @return 结果
     */
    @Override
    public int deleteErpGoodsByIds(Long[] ids)
    {
        return erpGoodsMapper.deleteErpGoodsByIds(ids);
    }

    /**
     * 删除商品信息信息
     *
     * @param id 商品信息主键
     * @return 结果
     */
    @Override
    public int deleteErpGoodsById(Long id)
    {
        return erpGoodsMapper.deleteErpGoodsById(id);
    }

    @Override
    public Long selectGoodAssociation(Long id) {
        return erpGoodsMapper.selectGoodAssociation(id);
    }
}
