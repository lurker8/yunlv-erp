package com.ruoyi.system.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 采购商品仓库关联对象 erp_goods_purchase_warehouse
 * 
 * @author ruoyi
 * @date 2022-11-10
 */
public class ErpGoodsPurchaseWarehouse extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 仓库id */
    @Excel(name = "仓库id")
    private Long warehouseId;

    /** 采购订单id */
    @Excel(name = "采购订单id")
    private Long purchaseId;

    /** 采购商品id */
    @Excel(name = "采购商品id")
    private Long purchaseGoodId;

    /** 入库商品数量 */
    @Excel(name = "入库商品数量")
    private Long purchaseGoodNumber;

    /** 退货商品数量 */
    @Excel(name = "退货商品数量")
    private Long returnGoodNumber;

    /** 退货商品金额 */
    @Excel(name = "退货商品金额")
    private BigDecimal returnGoodBonus;

    /**
     * 仓库name
     * */
    private String warehouseName;

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setWarehouseId(Long warehouseId) 
    {
        this.warehouseId = warehouseId;
    }

    public Long getWarehouseId() 
    {
        return warehouseId;
    }
    public void setPurchaseId(Long purchaseId) 
    {
        this.purchaseId = purchaseId;
    }

    public Long getPurchaseId() 
    {
        return purchaseId;
    }
    public void setPurchaseGoodId(Long purchaseGoodId) 
    {
        this.purchaseGoodId = purchaseGoodId;
    }

    public Long getPurchaseGoodId() 
    {
        return purchaseGoodId;
    }
    public void setPurchaseGoodNumber(Long purchaseGoodNumber) 
    {
        this.purchaseGoodNumber = purchaseGoodNumber;
    }

    public Long getPurchaseGoodNumber() 
    {
        return purchaseGoodNumber;
    }
    public void setReturnGoodNumber(Long returnGoodNumber) 
    {
        this.returnGoodNumber = returnGoodNumber;
    }

    public Long getReturnGoodNumber() 
    {
        return returnGoodNumber;
    }
    public void setReturnGoodBonus(BigDecimal returnGoodBonus) 
    {
        this.returnGoodBonus = returnGoodBonus;
    }

    public BigDecimal getReturnGoodBonus() 
    {
        return returnGoodBonus;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("warehouseId", getWarehouseId())
            .append("purchaseId", getPurchaseId())
            .append("purchaseGoodId", getPurchaseGoodId())
            .append("purchaseGoodNumber", getPurchaseGoodNumber())
            .append("returnGoodNumber", getReturnGoodNumber())
            .append("returnGoodBonus", getReturnGoodBonus())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
