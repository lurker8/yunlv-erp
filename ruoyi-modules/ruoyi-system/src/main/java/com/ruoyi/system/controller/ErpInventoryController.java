package com.ruoyi.system.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.nacos.shaded.org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.domain.ErpInventory;
import com.ruoyi.system.service.IErpInventoryService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 库存盘点Controller
 * 
 * @author ruoyi
 * @date 2022-11-21
 */
@RestController
@RequestMapping("/inventory")
public class ErpInventoryController extends BaseController
{
    @Autowired
    private IErpInventoryService erpInventoryService;

    /**
     * 查询库存盘点列表
     */
    @RequiresPermissions("system:inventory:list")
    @GetMapping("/list")
    public TableDataInfo list(ErpInventory erpInventory)
    {
        startPage();
        List<ErpInventory> list = erpInventoryService.selectErpInventoryList(erpInventory);
        return getDataTable(list);
    }

    /**
     * 导出库存盘点列表
     */
    @RequiresPermissions("system:inventory:export")
    @Log(title = "库存盘点", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ErpInventory erpInventory)
    {
        List<ErpInventory> list = erpInventoryService.selectErpInventoryList(erpInventory);
        ExcelUtil<ErpInventory> util = new ExcelUtil<ErpInventory>(ErpInventory.class);
        util.exportExcel(response, list, "库存盘点数据");
    }

    /**
     * 获取库存盘点详细信息
     */
    @RequiresPermissions("system:inventory:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(erpInventoryService.selectErpInventoryById(id));
    }

    /**
     * 新增库存盘点
     */
    @RequiresPermissions("system:inventory:add")
    @Log(title = "库存盘点", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ErpInventory erpInventory)
    {
        return toAjax(erpInventoryService.insertErpInventory(erpInventory));
    }

    /**
     * 修改库存盘点
     */
    @RequiresPermissions("system:inventory:edit")
    @Log(title = "库存盘点", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ErpInventory erpInventory)
    {
        return toAjax(erpInventoryService.updateErpInventory(erpInventory));
    }

    @PutMapping("/updateInventoryType")
    public AjaxResult updateInventoryType(Long inventoryId,Long type){
     return toAjax(erpInventoryService.updateInventoryType(inventoryId, type));
    }

    /**
     * 删除库存盘点
     */
    @RequiresPermissions("system:inventory:remove")
    @Log(title = "库存盘点", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(erpInventoryService.deleteErpInventoryByIds(ids));
    }
}
