package com.ruoyi.system.service;

import com.ruoyi.system.domain.ErpGoodsSaleWarehouse;
import com.ruoyi.system.domain.ErpSale;
import com.ruoyi.system.domain.ErpSaleGoodNum;
import com.ruoyi.system.domain.SaleGoods;

import java.util.List;
import java.util.Map;

/**
 * 销售订单Service接口
 *
 * @author 刘策
 * @date 2022-10-28
 */
public interface IErpSaleService
{
    /**
     * 查询销售订单
     *
     * @param id 销售订单主键
     * @return 销售订单
     */
    public ErpSale selectErpSaleById(Long id);

    /**
     * 查询销售订单列表
     *
     * @param erpSale 销售订单
     * @return 销售订单集合
     */
    public List<ErpSale> selectErpSaleList(ErpSale erpSale);

    /*首页折线图销售单数*/
    public List<Integer> selectSaleCount(Map map);

/*    *//**
     * 查询销售发货订单列表
     *
     * @param erpSale 销售订单
     * @return 销售订单集合
     *//*
    public List<ErpSale> selectErpSaleDeliveryList(ErpSale erpSale);*/

    /**
     * 新增销售订单
     *
     * @param erpSale 销售订单
     * @return 结果
     */
    public int insertErpSale(ErpSale erpSale);

    /**
     * 修改销售订单
     *
     * @param erpSale 销售订单
     * @return 结果
     */
    public int updateErpSale(ErpSale erpSale);

    /**
     * 批量删除销售订单
     *
     * @param ids 需要删除的销售订单主键集合
     * @return 结果
     */
    public int deleteErpSaleByIds(Long[] ids);

    /**
     * 删除销售订单信息
     *
     * @param id 销售订单主键
     * @return 结果
     */
    public int deleteErpSaleById(Long id);

    /**
     * 删除销售订单关联商品信息
     *
     * @param saleGoods 销售订单
     * @return 结果
     */
    public int deleteErpSaleGoodById(SaleGoods saleGoods);

    /**
     * 删除销售订单关联商品信息
     *
     * @param saleId 销售订单
     * @return 结果
     */
    public int deleteErpSaleGoodBySaleId(Long saleId);

    /**
     * 修改销售订单状态
     *
     * @param saleId 销售订单id
     * @param type 状态
     * @param num 发货单号
     * @return 结果
     */
    public int updateErpSaleType(Long saleId,Long type,String num);

    /**
     * 添加销售仓库商品关联表
     *
     * @param erpGoodsSaleWarehouse 销售仓库商品关联
     * @return 结果
     */
    public int addGoodsSaleWarehouse(ErpGoodsSaleWarehouse erpGoodsSaleWarehouse);

    /**
     * 商品退货方法
     *
     * @param erpGoodsSaleWarehouse 销售仓库商品关联
     * @return 结果
     */
    public int returnGoods(ErpGoodsSaleWarehouse erpGoodsSaleWarehouse);

    List<ErpSaleGoodNum> selectGoodNumBySaleIdFromSale(Long saleId);

    List<ErpSaleGoodNum> selectGoodNumBySaleIdFromWarehouse(Long saleId);
}
