package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ErpReceiptDocMapper;
import com.ruoyi.system.domain.ErpReceiptDoc;
import com.ruoyi.system.service.IErpReceiptDocService;

/**
 * 入库单Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-11-17
 */
@Service
public class ErpReceiptDocServiceImpl implements IErpReceiptDocService 
{
    @Autowired
    private ErpReceiptDocMapper erpReceiptDocMapper;

    /**
     * 查询入库单
     * 
     * @param id 入库单主键
     * @return 入库单
     */
    @Override
    public ErpReceiptDoc selectErpReceiptDocById(Long id)
    {
        return erpReceiptDocMapper.selectErpReceiptDocById(id);
    }

    /**
     * 查询入库单列表
     * 
     * @param erpReceiptDoc 入库单
     * @return 入库单
     */
    @Override
    public List<ErpReceiptDoc> selectErpReceiptDocList(ErpReceiptDoc erpReceiptDoc)
    {
        return erpReceiptDocMapper.selectErpReceiptDocList(erpReceiptDoc);
    }

    /**
     * 新增入库单
     * 
     * @param erpReceiptDoc 入库单
     * @return 结果
     */
    @Override
    public int insertErpReceiptDoc(ErpReceiptDoc erpReceiptDoc)
    {
        erpReceiptDoc.setCreateTime(DateUtils.getNowDate());
        return erpReceiptDocMapper.insertErpReceiptDoc(erpReceiptDoc);
    }

    /**
     * 修改入库单
     * 
     * @param erpReceiptDoc 入库单
     * @return 结果
     */
    @Override
    public int updateErpReceiptDoc(ErpReceiptDoc erpReceiptDoc)
    {
        return erpReceiptDocMapper.updateErpReceiptDoc(erpReceiptDoc);
    }

    /**
     * 批量删除入库单
     * 
     * @param ids 需要删除的入库单主键
     * @return 结果
     */
    @Override
    public int deleteErpReceiptDocByIds(Long[] ids)
    {
        return erpReceiptDocMapper.deleteErpReceiptDocByIds(ids);
    }

    /**
     * 删除入库单信息
     * 
     * @param id 入库单主键
     * @return 结果
     */
    @Override
    public int deleteErpReceiptDocById(Long id)
    {
        return erpReceiptDocMapper.deleteErpReceiptDocById(id);
    }
}
