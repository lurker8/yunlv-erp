package com.ruoyi.system.service;

import com.ruoyi.system.domain.ErpGoodLabel;

import java.util.List;

/**
 * 商品标签Service接口
 *
 * @author ruoyi
 * @date 2022-10-27
 */
public interface IErpGoodLabelService
{
    /**
     * 查询商品标签
     *
     * @param id 商品标签主键
     * @return 商品标签
     */
    public ErpGoodLabel selectErpGoodLabelById(Integer id);

    /**
     * 查询商品标签列表
     *
     * @param erpGoodLabel 商品标签
     * @return 商品标签集合
     */
    public List<ErpGoodLabel> selectErpGoodLabelList(ErpGoodLabel erpGoodLabel);

    /**
     * 新增商品标签
     *
     * @param erpGoodLabel 商品标签
     * @return 结果
     */
    public int insertErpGoodLabel(ErpGoodLabel erpGoodLabel);

    /**
     * 修改商品标签
     *
     * @param erpGoodLabel 商品标签
     * @return 结果
     */
    public int updateErpGoodLabel(ErpGoodLabel erpGoodLabel);

    /**
     * 批量删除商品标签
     *
     * @param ids 需要删除的商品标签主键集合
     * @return 结果
     */
    public int deleteErpGoodLabelByIds(Integer[] ids);

    /**
     * 删除商品标签信息
     *
     * @param id 商品标签主键
     * @return 结果
     */
    public int deleteErpGoodLabelById(Integer id);

    /**
     * 查询商品标签关联关系
     *
     * @param id 标签key
     * @return 结果
     * */
    Long selectGoodLabelAssociation(String id );
}
