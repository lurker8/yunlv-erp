package com.ruoyi.system.service;

import com.ruoyi.system.domain.ErpFundAccount;

import java.util.List;

/**
 * 资金账户管理Service接口
 *
 * @author ruoyi
 * @date 2022-10-26
 */
public interface IErpFundAccountService
{
    /**
     * 查询资金账户管理
     *
     * @param id 资金账户管理主键
     * @return 资金账户管理
     */
    public ErpFundAccount selectErpFundAccountById(Long id);

    /**
     * 查询资金账户管理列表
     *
     * @param erpFundAccount 资金账户管理
     * @return 资金账户管理集合
     */
    public List<ErpFundAccount> selectErpFundAccountList(ErpFundAccount erpFundAccount);

    /**
     * 新增资金账户管理
     *
     * @param erpFundAccount 资金账户管理
     * @return 结果
     */
    public int insertErpFundAccount(ErpFundAccount erpFundAccount);

    /**
     * 修改资金账户管理
     *
     * @param erpFundAccount 资金账户管理
     * @return 结果
     */
    public int updateErpFundAccount(ErpFundAccount erpFundAccount);

    /**
     * 批量删除资金账户管理
     *
     * @param ids 需要删除的资金账户管理主键集合
     * @return 结果
     */
    public int deleteErpFundAccountByIds(Long[] ids);

    /**
     * 删除资金账户管理信息
     *
     * @param id 资金账户管理主键
     * @return 结果
     */
    public int deleteErpFundAccountById(Long id);

    /** 查询资金账户关联关系
     *
     * @param id 资金账户id
     * @return 结果
     * */
    Long selectAccountAssociation(Long id);
}
