package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.system.api.model.LoginUser;
import com.ruoyi.system.domain.ErpCollectionRecord;
import com.ruoyi.system.domain.ErpPaymentRecord;
import com.ruoyi.system.mapper.ErpPaymentRecordMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ErpCopeWithMapper;
import com.ruoyi.system.domain.ErpCopeWith;
import com.ruoyi.system.service.IErpCopeWithService;

import static com.ruoyi.common.security.utils.SecurityUtils.getLoginUser;

/**
 * 应付账款Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-11-18
 */
@Service
public class ErpCopeWithServiceImpl implements IErpCopeWithService 
{
    @Autowired
    private ErpCopeWithMapper erpCopeWithMapper;

    @Autowired
    private ErpPaymentRecordMapper erpPaymentRecordMapper;

    /**
     * 查询应付账款
     * 
     * @param id 应付账款主键
     * @return 应付账款
     */
    @Override
    public ErpCopeWith selectErpCopeWithById(Long id)
    {
        return erpCopeWithMapper.selectErpCopeWithById(id);
    }
    /*财务管理查看付款页面信息*/
    @Override
    public ErpCopeWith selectErpCopeWithByIdLeftPaymentRecord(Long id)
    {
        return erpCopeWithMapper.selectErpCopeWithByIdLeftPaymentRecord(id);
    }

    /**
     * 查询应付账款列表
     * 
     * @param erpCopeWith 应付账款
     * @return 应付账款
     */
    @Override
    public List<ErpCopeWith> selectErpCopeWithList(ErpCopeWith erpCopeWith)
    {
        return erpCopeWithMapper.selectErpCopeWithList(erpCopeWith);
    }

    /**
     * 新增应付账款
     * 
     * @param erpCopeWith 应付账款
     * @return 结果
     */
    @Override
    public int insertErpCopeWith(ErpCopeWith erpCopeWith)
    {
        erpCopeWith.setCreateTime(DateUtils.getNowDate());
        return erpCopeWithMapper.insertErpCopeWith(erpCopeWith);
    }

    /**
     * 修改应付账款
     * 
     * @param erpCopeWith 应付账款
     * @return 结果
     */
    @Override
    public int updateErpCopeWith(ErpCopeWith erpCopeWith)
    {
        erpCopeWith.setUpdateTime(DateUtils.getNowDate());
        ErpPaymentRecord erpPaymentRecord = new ErpPaymentRecord();
        erpPaymentRecord.setPurchaseId(erpCopeWith.getPurchaseId());
        erpPaymentRecord.setPaymentTime(erpCopeWith.getPaymentTime());
        erpPaymentRecord.setAmountPayment(erpCopeWith.getAmountPayment());
        erpPaymentRecord.setDrawee(erpCopeWith.getDrawee());
        erpPaymentRecord.setPaymentVoucher(erpCopeWith.getPaymentVoucher());
        erpPaymentRecord.setPaymentRemarks(erpCopeWith.getPaymentRemarks());
        erpPaymentRecord.setCreateTime(DateUtils.getNowDate());
        LoginUser loginUser = getLoginUser();
        erpPaymentRecord.setCreateUser(loginUser.getUsername());
        erpPaymentRecord.setPaymentAccount(erpCopeWith.getPaymentAccount());
        erpPaymentRecord.setCopeWithId(erpCopeWith.getId());
        erpPaymentRecordMapper.insertErpPaymentRecord(erpPaymentRecord);
        return erpCopeWithMapper.updateErpCopeWith(erpCopeWith);
    }

    /**
     * 批量删除应付账款
     * 
     * @param ids 需要删除的应付账款主键
     * @return 结果
     */
    @Override
    public int deleteErpCopeWithByIds(Long[] ids)
    {
        return erpCopeWithMapper.deleteErpCopeWithByIds(ids);
    }

    /**
     * 删除应付账款信息
     * 
     * @param id 应付账款主键
     * @return 结果
     */
    @Override
    public int deleteErpCopeWithById(Long id)
    {
        return erpCopeWithMapper.deleteErpCopeWithById(id);
    }
}
