package com.ruoyi.system.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 应付账款对象 erp_cope_with
 * 
 * @author ruoyi
 * @date 2022-11-18
 */
public class ErpCopeWith extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** Id */
    private Long id;

    /** 采购单号 */
    @Excel(name = "采购单号")
    private String poNo;

    /** 付款类型 */
    @Excel(name = "付款类型")
    private Long paymentType;

    /** 操作人 */
    @Excel(name = "操作人")
    private String operator;

    /*供应商名称*/
    private String supplierName;

    /** 采购id */
    @Excel(name = "采购id")
    private Long purchaseId;

    /** 应付金额 */
    @Excel(name = "应付金额")
    private BigDecimal amountPayable;

    /** 付款时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "付款时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date paymentTime;

    /** 付款人name */
    @Excel(name = "付款人name")
    private String drawee;

    /** 付款凭证 */
    @Excel(name = "付款凭证")
    private String paymentVoucher;

    /** 付款备注 */
    @Excel(name = "付款备注")
    private String paymentRemarks;

    /** 付款账户 */
    @Excel(name = "付款账户")
    private String paymentAccount;
    /** 付款金额 */
    @Excel(name = "付款金额")
    private BigDecimal amountPayment;

    /*已付金额*/
    private BigDecimal amountPaid;
    /*未付金额*/
    private BigDecimal unpaidAmount;

    public BigDecimal getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(BigDecimal amountPaid) {
        this.amountPaid = amountPaid;
    }

    public BigDecimal getUnpaidAmount() {
        return unpaidAmount;
    }

    public void setUnpaidAmount(BigDecimal unpaidAmount) {
        this.unpaidAmount = unpaidAmount;
    }

    public BigDecimal getAmountPayment() {
        return amountPayment;
    }

    public void setAmountPayment(BigDecimal amountPayment) {
        this.amountPayment = amountPayment;
    }

    public String getPaymentAccount() {
        return paymentAccount;
    }

    public void setPaymentAccount(String paymentAccount) {
        this.paymentAccount = paymentAccount;
    }

    public String getPaymentVoucher() {
        return paymentVoucher;
    }

    public void setPaymentVoucher(String paymentVoucher) {
        this.paymentVoucher = paymentVoucher;
    }

    public String getPaymentRemarks() {
        return paymentRemarks;
    }

    public void setPaymentRemarks(String paymentRemarks) {
        this.paymentRemarks = paymentRemarks;
    }

    public String getDrawee() {
        return drawee;
    }

    public void setDrawee(String drawee) {
        this.drawee = drawee;
    }

    public Date getPaymentTime() {
        return paymentTime;
    }

    public void setPaymentTime(Date paymentTime) {
        this.paymentTime = paymentTime;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setPoNo(String poNo) 
    {
        this.poNo = poNo;
    }

    public String getPoNo() 
    {
        return poNo;
    }
    public void setPaymentType(Long paymentType) 
    {
        this.paymentType = paymentType;
    }

    public Long getPaymentType() 
    {
        return paymentType;
    }
    public void setOperator(String operator) 
    {
        this.operator = operator;
    }

    public String getOperator() 
    {
        return operator;
    }
    public void setPurchaseId(Long purchaseId) 
    {
        this.purchaseId = purchaseId;
    }

    public Long getPurchaseId() 
    {
        return purchaseId;
    }
    public void setAmountPayable(BigDecimal amountPayable) 
    {
        this.amountPayable = amountPayable;
    }

    public BigDecimal getAmountPayable() 
    {
        return amountPayable;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("poNo", getPoNo())
            .append("paymentType", getPaymentType())
            .append("operator", getOperator())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("purchaseId", getPurchaseId())
            .append("amountPayable", getAmountPayable())
            .toString();
    }
}
