package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ErpInventoryGoodsMapper;
import com.ruoyi.system.domain.ErpInventoryGoods;
import com.ruoyi.system.service.IErpInventoryGoodsService;

/**
 * 库存盘点商品Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-11-21
 */
@Service
public class ErpInventoryGoodsServiceImpl implements IErpInventoryGoodsService 
{
    @Autowired
    private ErpInventoryGoodsMapper erpInventoryGoodsMapper;

    /**
     * 查询库存盘点商品
     * 
     * @param id 库存盘点商品主键
     * @return 库存盘点商品
     */
    @Override
    public ErpInventoryGoods selectErpInventoryGoodsById(Long id)
    {
        return erpInventoryGoodsMapper.selectErpInventoryGoodsById(id);
    }

    /**
     * 查询库存盘点商品列表
     * 
     * @param erpInventoryGoods 库存盘点商品
     * @return 库存盘点商品
     */
    @Override
    public List<ErpInventoryGoods> selectErpInventoryGoodsList(ErpInventoryGoods erpInventoryGoods)
    {
        return erpInventoryGoodsMapper.selectErpInventoryGoodsList(erpInventoryGoods);
    }

    /**
     * 新增库存盘点商品
     * 
     * @param erpInventoryGoods 库存盘点商品
     * @return 结果
     */
    @Override
    public int insertErpInventoryGoods(ErpInventoryGoods erpInventoryGoods)
    {
        erpInventoryGoods.setCreateTime(DateUtils.getNowDate());


        return erpInventoryGoodsMapper.insertErpInventoryGoods(erpInventoryGoods);
    }

    /**
     * 修改库存盘点商品
     * 
     * @param erpInventoryGoods 库存盘点商品
     * @return 结果
     */
    @Override
    public int updateErpInventoryGoods(ErpInventoryGoods erpInventoryGoods)
    {
        erpInventoryGoods.setUpdateTime(DateUtils.getNowDate());
        return erpInventoryGoodsMapper.updateErpInventoryGoods(erpInventoryGoods);
    }

    /**
     * 批量删除库存盘点商品
     * 
     * @param ids 需要删除的库存盘点商品主键
     * @return 结果
     */
    @Override
    public int deleteErpInventoryGoodsByIds(Long[] ids)
    {
        return erpInventoryGoodsMapper.deleteErpInventoryGoodsByIds(ids);
    }

    /**
     * 删除库存盘点商品信息
     * 
     * @param id 库存盘点商品主键
     * @return 结果
     */
    @Override
    public int deleteErpInventoryGoodsById(Long id)
    {
        return erpInventoryGoodsMapper.deleteErpInventoryGoodsById(id);
    }
}
