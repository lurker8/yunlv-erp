package com.ruoyi.system.controller;

import java.math.BigDecimal;
import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import javax.swing.plaf.synth.SynthOptionPaneUI;

import com.alibaba.nacos.shaded.org.checkerframework.checker.units.qual.A;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.system.api.RemoteFileService;
import com.ruoyi.system.api.RemoteUserService;
import com.ruoyi.system.api.domain.SysFile;
import org.aspectj.weaver.loadtime.Aj;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.domain.ErpCollection;
import com.ruoyi.system.service.IErpCollectionService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 应收账款Controller
 * 
 * @author ruoyi
 * @date 2022-11-11
 */
@RestController
@RequestMapping("/collection")
public class ErpCollectionController extends BaseController
{

    @Autowired
    private RemoteFileService remoteFileService;

    @Autowired
    private IErpCollectionService erpCollectionService;

    /**
     * 查询应收账款列表
     */
    @RequiresPermissions("system:collection:list")
    @GetMapping("/list")
    public TableDataInfo list(ErpCollection erpCollection)
    {
        startPage();
        List<ErpCollection> list = erpCollectionService.selectErpCollectionList(erpCollection);
        return getDataTable(list);
    }


    /**
     * 导出应收账款列表
     */
    @RequiresPermissions("system:collection:export")
    @Log(title = "应收账款", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ErpCollection erpCollection)
    {
        List<ErpCollection> list = erpCollectionService.selectErpCollectionList(erpCollection);
        ExcelUtil<ErpCollection> util = new ExcelUtil<ErpCollection>(ErpCollection.class);
        util.exportExcel(response, list, "应收账款数据");
    }

    /**
     * 获取应收账款详细信息
     */
    @RequiresPermissions("system:collection:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(erpCollectionService.selectErpCollectionById(id));
    }

    /**
     * 新增应收账款
     */
    @RequiresPermissions("system:collection:add")
    @Log(title = "应收账款", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ErpCollection erpCollection)
    {
        return toAjax(erpCollectionService.insertErpCollection(erpCollection));
    }

    /**
     * 修改应收账款
     */
    @RequiresPermissions("system:collection:edit")
    @Log(title = "应收账款", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ErpCollection erpCollection)
    {
        if (erpCollection.getAmountCollected() == null){
            return AjaxResult.error("请填写收款金额");
        }
        if (erpCollection.getCollectionTime() == null){
            return AjaxResult.error("请填写收款日期");
        }
        if (erpCollection.getPayee() == null){
            return AjaxResult.error("请填写收款人!");
        }
        int i = erpCollection.getAmountCollected().compareTo(erpCollection.getAmountNotReceived());
        System.out.println(i);
        if(erpCollection.getAmountCollected().compareTo(erpCollection.getAmountNotReceived()) == 1){
            return AjaxResult.error("收款金额有误请重新核对收款金额!");
        }
        return toAjax(erpCollectionService.updateErpCollection(erpCollection));
    }

    /**
     * 删除应收账款
     */
    @RequiresPermissions("system:collection:remove")
    @Log(title = "应收账款", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(erpCollectionService.deleteErpCollectionByIds(ids));
    }
}
