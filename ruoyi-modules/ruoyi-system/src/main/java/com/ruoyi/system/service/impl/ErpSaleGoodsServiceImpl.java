package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.system.domain.ErpSaleGoods;
import com.ruoyi.system.mapper.ErpSaleGoodsMapper;
import com.ruoyi.system.service.IErpSaleGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 销售商品Service业务层处理
 *
 * @author liuce
 * @date 2022-10-31
 */
@Service
public class ErpSaleGoodsServiceImpl implements IErpSaleGoodsService
{
    @Autowired
    private ErpSaleGoodsMapper erpSaleGoodsMapper;

    /**
     * 查询销售商品
     *
     * @param id 销售商品主键
     * @return 销售商品
     */
    @Override
    public ErpSaleGoods selectErpSaleGoodsById(Long saleId,Long goodId)
    {
        return erpSaleGoodsMapper.selectErpSaleGoodsById(saleId, goodId);
    }

    /**
     * 查询销售商品列表
     *
     * @param erpSaleGoods 销售商品
     * @return 销售商品
     */
    @Override
    public List<ErpSaleGoods> selectErpSaleGoodsList(ErpSaleGoods erpSaleGoods)
    {
        return erpSaleGoodsMapper.selectErpSaleGoodsList(erpSaleGoods);
    }

    /**
     * 新增销售商品
     *
     * @param erpSaleGoods 销售商品
     * @return 结果
     */
    @Override
    public int insertErpSaleGoods(ErpSaleGoods erpSaleGoods)
    {
        erpSaleGoods.setCreateTime(DateUtils.getNowDate());
        return erpSaleGoodsMapper.insertErpSaleGoods(erpSaleGoods);
    }

    /**
     * 修改销售商品
     *
     * @param erpSaleGoods 销售商品
     * @return 结果
     */
    @Override
    public int updateErpSaleGoods(ErpSaleGoods erpSaleGoods)
    {
        erpSaleGoods.setUpdateTime(DateUtils.getNowDate());
        return erpSaleGoodsMapper.updateErpSaleGoods(erpSaleGoods);
    }

    /**
     * 批量删除销售商品
     *
     * @param ids 需要删除的销售商品主键
     * @return 结果
     */
    @Override
    public int deleteErpSaleGoodsByIds(Long[] ids)
    {
        return erpSaleGoodsMapper.deleteErpSaleGoodsByIds(ids);
    }

    /**
     * 删除销售商品信息
     *
     * @param id 销售商品主键
     * @return 结果
     */
    @Override
    public int deleteErpSaleGoodsById(Long id)
    {
        return erpSaleGoodsMapper.deleteErpSaleGoodsById(id);
    }
}
