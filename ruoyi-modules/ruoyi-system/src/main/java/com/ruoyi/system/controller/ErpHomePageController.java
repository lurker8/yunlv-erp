package com.ruoyi.system.controller;

import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.system.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 首页统计Controller
 *
 * @author ruoyi
 * @date 2022-11-21
 */
@RestController
@RequestMapping("/HomePage")
public class ErpHomePageController extends BaseController {
    @Autowired
    private IErpCustomerService erpCustomerService;
    @Autowired
    private IErpSupplierService erpSupplierService;
    @Autowired
    private IErpGoodsService erpGoodsService;
    @Autowired
    private IErpPurchaseService erpPurchaseService;
    @Autowired
    private IErpSaleService erpSaleService;

    /*首页客户数量统计*/
    @GetMapping("/customerNum")
    public AjaxResult selectCustomerNum() {
        return AjaxResult.success(erpCustomerService.selectCustomerNum());
    }

    /*首页供应商数量统计*/
    @GetMapping("/supplierNum")
    public AjaxResult selectSupplierNum() {
        return AjaxResult.success(erpSupplierService.selectSupplierNum());
    }

    /*首页商品数量统计*/
    @GetMapping("/goodsNums")
    public AjaxResult selectGoodsNum() {
        return AjaxResult.success(erpGoodsService.selectGoodsNum());
    }

    /*首页阈值商品数量统计*/
    @GetMapping("/thresholdNum")
    public AjaxResult selectThresholdNum() {
        return AjaxResult.success(erpGoodsService.selectThresholdNum());
    }

    /*首页饼状图商品品牌名称*/
    @PostMapping("/brandName")
    public TableDataInfo brandName(String brandName) {
        Map map = new HashMap();
        map.put("brandName", brandName);
        List<String> maps = erpGoodsService.selectBrandName(map);
        return getDataTable(maps);
    }

    /*首页饼状图商品品牌数量*/
    @PostMapping("/brandCount")
    public TableDataInfo brandCount(String name,String value) {
        Map map = new HashMap();
        map.put("name", name);
        map.put("value", value);
        List<Integer> maps = erpGoodsService.selectBrandCount(map);
        return getDataTable(maps);
    }
    /*首页折线图日期数据*/
    @PostMapping("/xDate")
    public TableDataInfo selectxDate( Date clickDate) {
        Map map = new HashMap();
        map.put("clickDate", clickDate);
        List<String> maps = erpPurchaseService.selectxDate(map);
        return getDataTable(maps);
    }

    /*首页折线图采购单数*/
    @PostMapping("/purchaseCount")
    public TableDataInfo purchaseCount(String yCount1, Date clickDate) {
        Map map = new HashMap();
        map.put("clickDate", clickDate);
        map.put("yCount1", yCount1);
        List<Integer> maps = erpPurchaseService.selectPurchaseCount(map);
        return getDataTable(maps);
    }
    /*首页折线图销售单数*/
    @PostMapping("/saleCount")
    public TableDataInfo saleCount(String yCount2, Date clickDate) {
        Map map = new HashMap();
        map.put("clickDate", clickDate);
        map.put("yCount2", yCount2);
        List<Integer> maps = erpSaleService.selectSaleCount(map);
        return getDataTable(maps);
    }
}
