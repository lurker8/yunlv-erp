package com.ruoyi.system.controller;

import com.ruoyi.common.core.utils.html.HtmlUtil;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.domain.ErpGoods;
import com.ruoyi.system.service.IErpGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 商品信息Controller
 *
 * @author 刘策
 * @date 2022-10-26
 */
@RestController
@RequestMapping("/goods")
public class ErpGoodsController extends BaseController
{
    @Autowired
    private IErpGoodsService erpGoodsService;

    /**
     * 查询商品信息列表
     */
    @RequiresPermissions("system:goods:list")
    @GetMapping("/list")
    public TableDataInfo list(ErpGoods erpGoods)
    {
        startPage();
        List<ErpGoods> list = erpGoodsService.selectErpGoodsList(erpGoods);
        return getDataTable(list);
    }

    /**
     * 查询阈值商品信息列表
     */
    @GetMapping("/listGoodsThreshold")
    public TableDataInfo listGoodsThreshold(ErpGoods erpGoods)
    {
        startPage();
        List<ErpGoods> list = erpGoodsService.selectThresholdGoodsList(erpGoods);
        return getDataTable(list);
    }

    @GetMapping("/warehouseList")
    public TableDataInfo warehouseList(ErpGoods erpGoods)
    {
        startPage();
        List<ErpGoods> list = erpGoodsService.selectErpGoodsWarehouseList(erpGoods);
        return getDataTable(list);
    }

    // 查询商品信息列表采购订单专用
    @GetMapping("/listGoodsPurchase")
    public TableDataInfo listGoodsPurchase(ErpGoods erpGoods)
    {
        startPage();
        List<ErpGoods> list = erpGoodsService.listGoodsPurchase(erpGoods);
        return getDataTable(list);
    }

    /**
     * 导出商品信息列表
     */
    @RequiresPermissions("system:goods:export")
    @Log(title = "商品信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ErpGoods erpGoods)
    {
        List<ErpGoods> list = erpGoodsService.selectErpGoodsList(erpGoods);
        ExcelUtil<ErpGoods> util = new ExcelUtil<ErpGoods>(ErpGoods.class);
        util.exportExcel(response, list, "商品信息数据");
    }

    /**
     * 获取商品信息详细信息
     */
    @RequiresPermissions("system:goods:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(erpGoodsService.selectErpGoodsById(id));
    }

    /**
     * 新增商品信息
     */
    @RequiresPermissions("system:goods:add")
    @Log(title = "商品信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ErpGoods erpGoods)
    {
        Long company = erpGoods.getCompany();
        erpGoods.setDetails( HtmlUtil.getLable("", erpGoods.getDetails()));
        return toAjax(erpGoodsService.insertErpGoods(erpGoods));
    }

    /**
     * 修改商品信息
     */
    @RequiresPermissions("system:goods:edit")
    @Log(title = "商品信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ErpGoods erpGoods)
    {
        return toAjax(erpGoodsService.updateErpGoods(erpGoods));
    }

    /*修改商品信息(采购入库按钮专用)*/
    @PutMapping("/updateErpGoodsWarehousing")
    public AjaxResult updateErpGoodsWarehousing(@RequestBody ErpGoods erpGoods)
    {
        return toAjax(erpGoodsService.updateErpGoodsWarehousing(erpGoods));
    }

    /**
     * 删除商品信息
     */
    @RequiresPermissions("system:goods:remove")
    @Log(title = "商品信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        for (Long id : ids) {
            Long aLong = erpGoodsService.selectGoodAssociation(id);
            if (aLong != null){
                return AjaxResult.error("存在商品订单禁止删除");
            }
        }
        return toAjax(erpGoodsService.deleteErpGoodsByIds(ids));
    }
}
