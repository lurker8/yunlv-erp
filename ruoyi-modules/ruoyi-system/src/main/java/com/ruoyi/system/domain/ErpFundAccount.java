package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 资金账户管理对象 erp_fund_account
 *
 * @author ruoyi
 * @date 2022-10-26
 */
public class ErpFundAccount extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 资金账户id */
    private Long id;

    /** 资金账户name */
    @Excel(name = "资金账户name")
    private String name;

    /** 银行开户行 */
    @Excel(name = "银行开户行")
    private String accountBank;

    /** 银行卡号 */
    @Excel(name = "银行卡号")
    private String bankCard;

    /** 期初余额 */
    private BigDecimal startBalance;

    /** 资金余额 */
    private BigDecimal capitalBalance;

    /** 开户时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开户时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date openingTime;

    /** 备注信息 */
    @Excel(name = "备注信息")
    private String remarks;

    /** 创建人 */
    private Long createUser;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setAccountBank(String accountBank)
    {
        this.accountBank = accountBank;
    }

    public String getAccountBank()
    {
        return accountBank;
    }
    public void setBankCard(String bankCard)
    {
        this.bankCard = bankCard;
    }

    public String getBankCard()
    {
        return bankCard;
    }
    public void setStartBalance(BigDecimal startBalance)
    {
        this.startBalance = startBalance;
    }

    public BigDecimal getStartBalance()
    {
        return startBalance;
    }
    public void setCapitalBalance(BigDecimal capitalBalance)
    {
        this.capitalBalance = capitalBalance;
    }

    public BigDecimal getCapitalBalance()
    {
        return capitalBalance;
    }
    public void setOpeningTime(Date openingTime)
    {
        this.openingTime = openingTime;
    }

    public Date getOpeningTime()
    {
        return openingTime;
    }
    public void setRemarks(String remarks)
    {
        this.remarks = remarks;
    }

    public String getRemarks()
    {
        return remarks;
    }
    public void setCreateUser(Long createUser)
    {
        this.createUser = createUser;
    }

    public Long getCreateUser()
    {
        return createUser;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("accountBank", getAccountBank())
            .append("bankCard", getBankCard())
            .append("startBalance", getStartBalance())
            .append("capitalBalance", getCapitalBalance())
            .append("openingTime", getOpeningTime())
            .append("remarks", getRemarks())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createUser", getCreateUser())
            .toString();
    }
}
