package com.ruoyi.system.service;

import com.ruoyi.system.domain.ErpSalesReturn;

import java.math.BigDecimal;
import java.util.List;

/**
 * 【请填写功能名称】Service接口
 *
 * @author ruoyi
 * @date 2022-11-07
 */
public interface IErpSalesReturnService
{
    /**
     * 查询【请填写功能名称】
     *
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public ErpSalesReturn selectErpSalesReturnById(Long id);

    /**
     * 查询【请填写功能名称】列表
     *
     * @param erpSalesReturn 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<ErpSalesReturn> selectErpSalesReturnList(ErpSalesReturn erpSalesReturn);

    /**
     * 新增【请填写功能名称】
     *
     * @param erpSalesReturn 【请填写功能名称】
     * @return 结果
     */
    public int insertErpSalesReturn(ErpSalesReturn erpSalesReturn);

    /**
     * 修改【请填写功能名称】
     *
     * @param erpSalesReturn 【请填写功能名称】
     * @return 结果
     */
    public int updateErpSalesReturn(ErpSalesReturn erpSalesReturn);

    /**
     * 批量删除【请填写功能名称】
     *
     * @param ids 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteErpSalesReturnByIds(Long[] ids);

    /**
     * 删除【请填写功能名称】信息
     *
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteErpSalesReturnById(Long id);

    /**
     * 修改金额
     *
     * @param saleId 销售id
     * @param amount 退款金额
     * @return 结果
     */
    public int updateSaleAmount(Long saleId, BigDecimal amount);

    /**
     * 修改退货商品仓库关联表的库存
     *
     * @param goodId 商品id
     * @param wareHouseId 仓库id
     * @param returnNum 退货数量
     * @return 结果
     */
    int updateGoodStock(Long goodId,Long wareHouseId,Long returnNum);

    /**
     * 修改退货商品库存
     *
     * @param goodId 商品id
     * @param returnNum 退货数量
     * @return 结果
     */
    int updateStock(Long goodId,Long returnNum);

    /**
     * 修改销售退货单退货状态
     *
     * @param saleId 销售id
     * @return 结果
     * */
    int updateIsReturn(Long saleId);
}
