package com.ruoyi.system.controller;

import com.alibaba.nacos.shaded.org.checkerframework.checker.units.qual.A;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.domain.ErpCustomer;
import com.ruoyi.system.service.IErpCustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 客户管理Controller
 *
 * @author 刘策
 * @date 2022-10-24
 */
@RestController
@RequestMapping("/customer")
public class ErpCustomerController extends BaseController
{
    @Autowired
    private IErpCustomerService erpCustomerService;

    /**
     * 查询客户管理列表
     */
    @RequiresPermissions("erp:customer:list")
    @GetMapping("/list")
    public TableDataInfo list(ErpCustomer erpCustomer)
    {
        startPage();
        List<ErpCustomer> list = erpCustomerService.selectErpCustomerList(erpCustomer);
        return getDataTable(list);
    }

    /**
     * 导出客户管理列表
     */
    @RequiresPermissions("erp:customer:export")
    @Log(title = "客户管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ErpCustomer erpCustomer)
    {
        List<ErpCustomer> list = erpCustomerService.selectErpCustomerList(erpCustomer);
        ExcelUtil<ErpCustomer> util = new ExcelUtil<ErpCustomer>(ErpCustomer.class);
        util.exportExcel(response, list, "客户管理数据");
    }

    /**
     * 获取客户管理详细信息
     */
    @RequiresPermissions("erp:customer:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(erpCustomerService.selectErpCustomerById(id));
    }

    /**
     * 新增客户管理
     */
    @RequiresPermissions("erp:customer:add")
    @Log(title = "客户管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ErpCustomer erpCustomer)
    {
        return toAjax(erpCustomerService.insertErpCustomer(erpCustomer));
    }

    /**
     * 修改客户管理
     */
    @RequiresPermissions("erp:customer:edit")
    @Log(title = "客户管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ErpCustomer erpCustomer)
    {
        return toAjax(erpCustomerService.updateErpCustomer(erpCustomer));
    }

    /**
     * 删除客户管理
     */
    @RequiresPermissions("erp:customer:remove")
    @Log(title = "客户管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        for (Long id : ids) {
            Long i = erpCustomerService.selectCustomerAssociation(id);
            if (i != null){
                return AjaxResult.error("客户已分配禁止删除!");
            }
        }
        return toAjax(erpCustomerService.deleteErpCustomerByIds(ids));
    }
}
