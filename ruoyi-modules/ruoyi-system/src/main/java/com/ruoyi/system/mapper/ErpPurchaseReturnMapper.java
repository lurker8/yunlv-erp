package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.ErpPurchaseReturn;

/**
 * 采购退货Mapper接口
 * 
 * @author ruoyi
 * @date 2022-11-16
 */
public interface ErpPurchaseReturnMapper 
{
    /**
     * 查询采购退货
     * 
     * @param id 采购退货主键
     * @return 采购退货
     */
    public ErpPurchaseReturn selectErpPurchaseReturnById(Long id);

    /**
     * 查询采购退货列表
     * 
     * @param erpPurchaseReturn 采购退货
     * @return 采购退货集合
     */
    public List<ErpPurchaseReturn> selectErpPurchaseReturnList(ErpPurchaseReturn erpPurchaseReturn);

    /**
     * 新增采购退货
     * 
     * @param erpPurchaseReturn 采购退货
     * @return 结果
     */
    public int insertErpPurchaseReturn(ErpPurchaseReturn erpPurchaseReturn);

    /**
     * 修改采购退货
     * 
     * @param erpPurchaseReturn 采购退货
     * @return 结果
     */
    public int updateErpPurchaseReturn(ErpPurchaseReturn erpPurchaseReturn);

    /**
     * 删除采购退货
     * 
     * @param id 采购退货主键
     * @return 结果
     */
    public int deleteErpPurchaseReturnById(Long id);

    /**
     * 批量删除采购退货
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteErpPurchaseReturnByIds(Long[] ids);
}
