package com.ruoyi.system.controller;

import com.alibaba.nacos.shaded.org.checkerframework.checker.units.qual.A;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.domain.ErpWarehouse;
import com.ruoyi.system.service.IErpWarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 仓库管理Controller
 *
 * @author 刘策
 * @date 2022-10-26
 */
@RestController
@RequestMapping("/warehouse")
public class ErpWarehouseController extends BaseController
{
    @Autowired
    private IErpWarehouseService erpWarehouseService;

    /**
     * 查询仓库管理列表
     */
    @RequiresPermissions("system:warehouse:list")
    @GetMapping("/list")
    public TableDataInfo list(ErpWarehouse erpWarehouse)
    {
        startPage();
        List<ErpWarehouse> list = erpWarehouseService.selectErpWarehouseList(erpWarehouse);
        return getDataTable(list);
    }

    /**
     * 导出仓库管理列表
     */
    @RequiresPermissions("system:warehouse:export")
    @Log(title = "仓库管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ErpWarehouse erpWarehouse)
    {
        List<ErpWarehouse> list = erpWarehouseService.selectErpWarehouseList(erpWarehouse);
        ExcelUtil<ErpWarehouse> util = new ExcelUtil<ErpWarehouse>(ErpWarehouse.class);
        util.exportExcel(response, list, "仓库管理数据");
    }

    /**
     * 获取仓库管理详细信息
     */
    @RequiresPermissions("system:warehouse:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(erpWarehouseService.selectErpWarehouseById(id));
    }

    /**
     * 新增仓库管理
     */
    @RequiresPermissions("system:warehouse:add")
    @Log(title = "仓库管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ErpWarehouse erpWarehouse)
    {
        return toAjax(erpWarehouseService.insertErpWarehouse(erpWarehouse));
    }

    /**
     * 修改仓库管理
     */
    @RequiresPermissions("system:warehouse:edit")
    @Log(title = "仓库管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ErpWarehouse erpWarehouse)
    {
        return toAjax(erpWarehouseService.updateErpWarehouse(erpWarehouse));
    }

    /**
     * 删除仓库管理
     */
    @RequiresPermissions("system:warehouse:remove")
    @Log(title = "仓库管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        for (Long id : ids) {
            Long aLong = erpWarehouseService.selectWarehouseAssociation(id);
            if (aLong != null){
                return AjaxResult.error("仓库已分配禁止删除!");
            }
        }
        return toAjax(erpWarehouseService.deleteErpWarehouseByIds(ids));
    }
}
