package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.system.api.model.LoginUser;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.mapper.*;
import com.ruoyi.system.service.IErpSaleGoodsService;
import com.ruoyi.system.service.IErpSaleService;
import com.ruoyi.system.service.IErpSalesInvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.ruoyi.common.security.utils.SecurityUtils.getLoginUser;

/**
 * 销售订单Service业务层处理
 *
 * @author 刘策
 * @date 2022-10-28
 */
@Service
public class ErpSaleServiceImpl implements IErpSaleService {
    @Autowired
    private ErpSaleMapper erpSaleMapper;

    @Autowired
    private ErpGoodsMapper erpGoodsMapper;

    @Autowired
    private ErpSalesInvoiceMapper erpSalesInvoiceMapper;

    @Autowired
    private IErpSaleGoodsService erpSaleGoodsService;

    @Autowired
    private IErpSalesInvoiceService erpSalesInvoiceService;

    @Autowired
    private ErpGoodsWarehouseMapper erpGoodsWarehouseMapper;

    @Autowired
    private ErpSaleGoodsMapper erpSaleGoodsMapper;

    @Autowired
    private ErpCollectionMapper erpCollectionMapper;

    @Autowired
    private ErpLssueMapper erpLssueMapper;

    /**
     * 查询销售订单
     *
     * @param id 销售订单主键
     * @return 销售订单
     */
    @Override
    public ErpSale selectErpSaleById(Long id) {
        return erpSaleMapper.selectErpSaleById(id);
    }

    /**
     * 查询销售订单列表
     *
     * @param erpSale 销售订单
     * @return 销售订单
     */
    @Override
    public List<ErpSale> selectErpSaleList(ErpSale erpSale) {
        return erpSaleMapper.selectErpSaleList(erpSale);
    }

    /*首页折线图销售单数*/
    @Override
    public List<Integer> selectSaleCount(Map map) {
        return erpSaleMapper.selectSaleCount(map);
    }

 /*   @Override
    public List<ErpSale> selectErpSaleDeliveryList(ErpSale erpSale) {
        return erpSaleMapper.selectErpSaleDeliveryList(erpSale);
    }*/

    /**
     * 新增销售订单
     *
     * @param erpSale 销售订单
     * @return 结果
     */
    @Override
    public int insertErpSale(ErpSale erpSale) {
        erpSale.setCreateTime(DateUtils.getNowDate());
        LoginUser loginUser = getLoginUser();
        erpSale.setCreateUser(loginUser.getUsername());
        erpSale.setType(1L);
        erpSaleMapper.insertErpSale(erpSale);
        List<SaleGoods> goodsList = erpSale.getGoodsList();
        BigDecimal total = new BigDecimal(0);
        for (int i1 = 0; i1 < goodsList.size(); i1++) {
            SaleGoods saleGoods = goodsList.get(i1);
            ErpSaleGoods erpSaleGoods = new ErpSaleGoods();
            erpSaleGoods.setGoodsName(saleGoods.getName());
            erpSaleGoods.setGoodsNumber(saleGoods.getId().toString());
            erpSaleGoods.setMonovalent(saleGoods.getPrice());
            erpSaleGoods.setQuantity(saleGoods.getQuantity());
            erpSaleGoods.setOneGoodPrice(saleGoods.getOneGoodPrice());
            erpSaleGoods.setPurchaseId(erpSale.getId());
            erpSaleGoods.setCompany(saleGoods.getCompanyName());
            total = total.add(saleGoods.getOneGoodPrice());
            erpSaleGoodsService.insertErpSaleGoods(erpSaleGoods);
        }
        erpSale.setSaleBonus(total);
        erpSale.setReturnGoods(1L);
        return erpSaleMapper.updateErpSale(erpSale);
    }

    /**
     * 修改销售订单
     *
     * @param erpSale 销售订单
     * @return 结果
     */
    @Override
    public int updateErpSale(ErpSale erpSale) {
        erpSale.setUpdateTime(DateUtils.getNowDate());
        List<SaleGoods> goodsList = erpSale.getGoodsList();
        BigDecimal total = new BigDecimal(0);
        for (int i1 = 0; i1 < goodsList.size(); i1++) {
            SaleGoods saleGoods = goodsList.get(i1);
            ErpSaleGoods erpSaleGoods = new ErpSaleGoods();
            erpSaleGoods.setGoodsName(saleGoods.getName());
            erpSaleGoods.setGoodsNumber(saleGoods.getId().toString());
            erpSaleGoods.setMonovalent(saleGoods.getPrice());
            erpSaleGoods.setQuantity(saleGoods.getQuantity());
            erpSaleGoods.setOneGoodPrice(saleGoods.getOneGoodPrice());
            erpSaleGoods.setPurchaseId(erpSale.getId());
            erpSaleGoods.setCompany(saleGoods.getCompanyName());
            total = total.add(saleGoods.getOneGoodPrice());
            erpSaleGoodsService.updateErpSaleGoods(erpSaleGoods);
        }
        erpSale.setSaleBonus(total);
        return erpSaleMapper.updateErpSale(erpSale);
    }

    /**
     * 批量删除销售订单
     *
     * @param ids 需要删除的销售订单主键
     * @return 结果
     */
    @Override
    public int deleteErpSaleByIds(Long[] ids) {
        return erpSaleMapper.deleteErpSaleByIds(ids);
    }

    /**
     * 删除销售订单信息
     *
     * @param id 销售订单主键
     * @return 结果
     */
    @Override
    public int deleteErpSaleById(Long id) {
        return erpSaleMapper.deleteErpSaleById(id);
    }

    @Override
    public int deleteErpSaleGoodById(SaleGoods saleGoods) {
        return erpSaleMapper.deleteErpSaleGoodById(saleGoods);
    }

    @Override
    public int deleteErpSaleGoodBySaleId(Long saleId) {
        return erpSaleMapper.deleteErpSaleGoodBySaleId(saleId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateErpSaleType(Long saleId, Long type, String num) {
        Long T = 2L;
        Long L = 3L;
        //商品发货出库时进行库存减少操作
        if (type.equals(T)) {
            ErpSale erpSale = erpSaleMapper.selectErpSaleById(saleId);
            ErpSalesInvoice erpSalesInvoice = new ErpSalesInvoice();
            erpSalesInvoice.setSaleId(erpSale.getId());
            erpSalesInvoice.setSaleOrderNum(erpSale.getSaleNumber());
            erpSalesInvoice.setShipmentNum(num);
            erpSalesInvoice.setType(2L);
            erpSalesInvoice.setCustomerId(erpSale.getCustomerId());
            erpSalesInvoice.setPaymentMethod(erpSale.getPaymentMethod());
            erpSalesInvoice.setCustomerPhone(erpSale.getCustomerPhone());
            LoginUser loginUser = getLoginUser();
            erpSalesInvoice.setCreateUser(loginUser.getUsername());
            erpSalesInvoice.setCreateTime(DateUtils.getNowDate());
            erpSalesInvoice.setSaleBonus(erpSale.getSaleBonus());
            erpSalesInvoice.setIsReturn(1L);
            erpSalesInvoiceService.insertErpSalesInvoice(erpSalesInvoice);
            //进行库存减少的操作
            List<ErpGoodsSaleWarehouse> erpGoodsSaleWarehouses = erpSaleMapper.sSaleGood(saleId);
            for (ErpGoodsSaleWarehouse erpGoodsSaleWarehouse : erpGoodsSaleWarehouses) {
                Long goodId = erpGoodsSaleWarehouse.getGoodId();
                Long goodsNumber = erpGoodsSaleWarehouse.getGoodsNumber();
                String warehouseId = erpGoodsSaleWarehouse.getWarehouseId();
                allocation allocation = new allocation();
                allocation.setGoodNum(goodsNumber);
                allocation.setGoodId(goodId);
                allocation.setLssueId(Long.valueOf(warehouseId));
                erpGoodsWarehouseMapper.updateGoodNum(allocation);
            }
            List<ErpSaleGoodNum> erpSaleGoodNums = erpSaleMapper.selectGoodNumBySaleIdFromSale(saleId);
            for (ErpSaleGoodNum erpSaleGoodNum : erpSaleGoodNums) {
                Long goodId = erpSaleGoodNum.getGoodId();
                Long goodNum = erpSaleGoodNum.getGoodNum();
                erpGoodsMapper.updateGoodStock(goodId, goodNum);
            }
        }
        //商品确认收货时操作
        if (type.equals(L)) {
            erpSalesInvoiceMapper.updateErpSalesInvoiceType(saleId, type);
            ErpSale erpSale = erpSaleMapper.selectErpSaleById(saleId);
            ErpCollection erpCollection = new ErpCollection();
            erpCollection.setSaleOrderNum(erpSale.getSaleNumber());
            erpCollection.setSaleId(erpSale.getId());
            erpCollection.setAmountReceivable(erpSale.getSaleBonus());
            erpCollection.setPayMethod(erpSale.getPaymentMethod());
            erpCollectionMapper.insertErpCollection(erpCollection);
            List<String> LssueWarehouse = erpSaleMapper.selectSaleLssueWarehouse(saleId);
            ErpLssue erpLssue = new ErpLssue();
            erpLssue.setLssueTime(DateUtils.getNowDate());
            erpLssue.setSaleId(saleId);
            erpLssue.setLssueAmount(erpSale.getSaleBonus());
            erpLssue.setIssueOrder("Lss" + new Date().getTime());
            erpLssue.setLssueWarehouse(StringUtils.join(LssueWarehouse, ","));
            LoginUser loginUser = getLoginUser();
            erpLssue.setCreateUser(loginUser.getUsername());
            erpLssue.setIsSaleAllocation(0L);
            erpLssueMapper.insertErpLssue(erpLssue);
        }
        return erpSaleMapper.updateErpSaleType(saleId, type);
    }

    @Override
    public int addGoodsSaleWarehouse(ErpGoodsSaleWarehouse erpGoodsSaleWarehouse) {
        return erpSaleMapper.addGoodsSaleWarehouse(erpGoodsSaleWarehouse);
    }

    @Override
    public int returnGoods(ErpGoodsSaleWarehouse erpGoodsSaleWarehouse) {
        Long sellId = erpGoodsSaleWarehouse.getSellId();
        //计算退货金额
        BigDecimal returnAmount = erpGoodsSaleWarehouse.getItemPricing().multiply(new BigDecimal(erpGoodsSaleWarehouse.getGoodReturnWarehouse()));
        //销售订单修改金额与状态
        erpSaleMapper.modifySales(sellId, returnAmount);
        //销售发货单修改金额与状态
        erpSalesInvoiceMapper.modifySales(sellId, returnAmount);
        //销售商品修改销售金额
        erpSaleGoodsMapper.updateSaleGoodAmount(erpGoodsSaleWarehouse.getGoodId(), sellId, returnAmount, erpGoodsSaleWarehouse.getGoodReturnWarehouse());
        //销售商品仓库关联表进行添加退货数量以及退货金额操作
        erpGoodsSaleWarehouse.setReturnGoodAmount(returnAmount);
        return erpSaleMapper.returnGoods(erpGoodsSaleWarehouse);
    }

    @Override
    public List<ErpSaleGoodNum> selectGoodNumBySaleIdFromSale(Long saleId) {
        return erpSaleMapper.selectGoodNumBySaleIdFromSale(saleId);
    }

    @Override
    public List<ErpSaleGoodNum> selectGoodNumBySaleIdFromWarehouse(Long saleId) {
        return erpSaleMapper.selectGoodNumBySaleIdFromWarehouse(saleId);
    }
}
