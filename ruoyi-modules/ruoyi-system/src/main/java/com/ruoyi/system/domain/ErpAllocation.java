package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

import java.util.List;

/**
 * 库存调拨对象 erp_allocation
 * 
 * @author licue
 * @date 2022-11-17
 */
public class ErpAllocation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 调拨id */
    private Long id;

    /** 调拨单号 */
    @Excel(name = "调拨单号")
    private String allocationOrder;

    /** 出库仓库id */
    private Long lssueWarehouse;

    /** 出库仓库 */
    @Excel(name = "出库仓库")
    private String lssueWarehouseName;

    /** 入库仓库id */
    private Long warehousingId;

    /** 入库仓库 */
    @Excel(name = "入库仓库")
    private String warehousingName;

    /** 操作人 */
    @Excel(name = "操作人")
    private String creatUser;

    /** 调拨状态（1未审核，0已调拨） */
    @Excel(name = "调拨状态", readConverterExp = "1=未审核,0=已调拨")
    private Long type;

    /** 调拨商品列表*/
    private List<ErpAllocationGoods> erpAllocationGoods;

    public String getLssueWarehouseName() {
        return lssueWarehouseName;
    }

    public void setLssueWarehouseName(String lssueWarehouseName) {
        this.lssueWarehouseName = lssueWarehouseName;
    }

    public String getWarehousingName() {
        return warehousingName;
    }

    public void setWarehousingName(String warehousingName) {
        this.warehousingName = warehousingName;
    }

    public List<ErpAllocationGoods> getErpAllocationGoods() {
        return erpAllocationGoods;
    }

    public void setErpAllocationGoods(List<ErpAllocationGoods> erpAllocationGoods) {
        this.erpAllocationGoods = erpAllocationGoods;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setAllocationOrder(String allocationOrder) 
    {
        this.allocationOrder = allocationOrder;
    }

    public String getAllocationOrder() 
    {
        return allocationOrder;
    }
    public void setLssueWarehouse(Long lssueWarehouse) 
    {
        this.lssueWarehouse = lssueWarehouse;
    }

    public Long getLssueWarehouse() 
    {
        return lssueWarehouse;
    }
    public void setWarehousingId(Long warehousingId) 
    {
        this.warehousingId = warehousingId;
    }

    public Long getWarehousingId() 
    {
        return warehousingId;
    }
    public void setCreatUser(String creatUser) 
    {
        this.creatUser = creatUser;
    }

    public String getCreatUser() 
    {
        return creatUser;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("allocationOrder", getAllocationOrder())
            .append("lssueWarehouse", getLssueWarehouse())
            .append("warehousingId", getWarehousingId())
            .append("creatUser", getCreatUser())
            .append("createTime", getCreateTime())
            .append("type", getType())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
