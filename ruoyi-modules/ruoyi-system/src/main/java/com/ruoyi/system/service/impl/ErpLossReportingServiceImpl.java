package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ErpLossReportingMapper;
import com.ruoyi.system.domain.ErpLossReporting;
import com.ruoyi.system.service.IErpLossReportingService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 报损单Service业务层处理
 * 
 * @author liuce
 * @date 2022-11-21
 */
@Service
public class ErpLossReportingServiceImpl implements IErpLossReportingService 
{
    @Autowired
    private ErpLossReportingMapper erpLossReportingMapper;

    /**
     * 查询报损单
     * 
     * @param id 报损单主键
     * @return 报损单
     */
    @Override
    public ErpLossReporting selectErpLossReportingById(Long id)
    {
        return erpLossReportingMapper.selectErpLossReportingById(id);
    }

    /**
     * 查询报损单列表
     * 
     * @param erpLossReporting 报损单
     * @return 报损单
     */
    @Override
    public List<ErpLossReporting> selectErpLossReportingList(ErpLossReporting erpLossReporting)
    {
        return erpLossReportingMapper.selectErpLossReportingList(erpLossReporting);
    }

    /**
     * 新增报损单
     * 
     * @param erpLossReporting 报损单
     * @return 结果
     */
    @Override
    public int insertErpLossReporting(ErpLossReporting erpLossReporting)
    {
        erpLossReporting.setCreateTime(DateUtils.getNowDate());
        erpLossReporting.setType(1L);
        return erpLossReportingMapper.insertErpLossReporting(erpLossReporting);
    }

    /**
     * 修改报损单
     * 
     * @param erpLossReporting 报损单
     * @return 结果
     */
    @Override
    public int updateErpLossReporting(ErpLossReporting erpLossReporting)
    {
        erpLossReporting.setUpdateTime(DateUtils.getNowDate());
        return erpLossReportingMapper.updateErpLossReporting(erpLossReporting);
    }

    /**
     * 批量删除报损单
     * 
     * @param ids 需要删除的报损单主键
     * @return 结果
     */
    @Override
    public int deleteErpLossReportingByIds(Long[] ids)
    {
        return erpLossReportingMapper.deleteErpLossReportingByIds(ids);
    }

    /**
     * 删除报损单信息
     * 
     * @param id 报损单主键
     * @return 结果
     */
    @Override
    public int deleteErpLossReportingById(Long id)
    {
        return erpLossReportingMapper.deleteErpLossReportingById(id);
    }

    @Override
    @Transactional
    public int confirmReporting(ErpLossReporting erpLossReporting) {
        erpLossReportingMapper.updateGoodWarehouseNum(erpLossReporting.getGoodId(),erpLossReporting.getWarehouseId(),erpLossReporting.getDamageNum());
        erpLossReportingMapper.updateGoodNum(erpLossReporting.getGoodId(),erpLossReporting.getDamageNum());
        return erpLossReportingMapper.confirmReporting(erpLossReporting);
    }
}
