package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.ErpGoodsPurchaseWarehouse;

/**
 * 采购商品仓库关联Service接口
 * 
 * @author ruoyi
 * @date 2022-11-10
 */
public interface IErpGoodsPurchaseWarehouseService 
{
    /**
     * 查询采购商品仓库关联
     * 
     * @param id 采购商品仓库关联主键
     * @return 采购商品仓库关联
     */
    public ErpGoodsPurchaseWarehouse selectErpGoodsPurchaseWarehouseById(Long id);

    /**
     * 查询采购商品仓库关联列表
     * 
     * @param erpGoodsPurchaseWarehouse 采购商品仓库关联
     * @return 采购商品仓库关联集合
     */
    public List<ErpGoodsPurchaseWarehouse> selectErpGoodsPurchaseWarehouseList(ErpGoodsPurchaseWarehouse erpGoodsPurchaseWarehouse);

    /**
     * 新增采购商品仓库关联
     * 
     * @param erpGoodsPurchaseWarehouse 采购商品仓库关联
     * @return 结果
     */
    public int insertErpGoodsPurchaseWarehouse(ErpGoodsPurchaseWarehouse erpGoodsPurchaseWarehouse);

    /**
     * 修改采购商品仓库关联
     * 
     * @param erpGoodsPurchaseWarehouse 采购商品仓库关联
     * @return 结果
     */
    public int updateErpGoodsPurchaseWarehouse(ErpGoodsPurchaseWarehouse erpGoodsPurchaseWarehouse);

    /**
     * 批量删除采购商品仓库关联
     * 
     * @param ids 需要删除的采购商品仓库关联主键集合
     * @return 结果
     */
    public int deleteErpGoodsPurchaseWarehouseByIds(Long[] ids);

    /**
     * 删除采购商品仓库关联信息
     * 
     * @param id 采购商品仓库关联主键
     * @return 结果
     */
    public int deleteErpGoodsPurchaseWarehouseById(Long id);
}
