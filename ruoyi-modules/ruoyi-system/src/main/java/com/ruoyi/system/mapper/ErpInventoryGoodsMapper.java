package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.ErpInventoryGoods;

/**
 * 库存盘点商品Mapper接口
 * 
 * @author ruoyi
 * @date 2022-11-21
 */
public interface ErpInventoryGoodsMapper 
{
    /**
     * 查询库存盘点商品
     * 
     * @param id 库存盘点商品主键
     * @return 库存盘点商品
     */
    public ErpInventoryGoods selectErpInventoryGoodsById(Long id);

    /**
     * 查询库存盘点商品列表
     * 
     * @param erpInventoryGoods 库存盘点商品
     * @return 库存盘点商品集合
     */
    public List<ErpInventoryGoods> selectErpInventoryGoodsList(ErpInventoryGoods erpInventoryGoods);

    /**
     * 新增库存盘点商品
     * 
     * @param erpInventoryGoods 库存盘点商品
     * @return 结果
     */
    public int insertErpInventoryGoods(ErpInventoryGoods erpInventoryGoods);

    /**
     * 修改库存盘点商品
     * 
     * @param erpInventoryGoods 库存盘点商品
     * @return 结果
     */
    public int updateErpInventoryGoods(ErpInventoryGoods erpInventoryGoods);

    /**
     * 删除库存盘点商品
     * 
     * @param id 库存盘点商品主键
     * @return 结果
     */
    public int deleteErpInventoryGoodsById(Long id);

    /**
     * 批量删除库存盘点商品
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteErpInventoryGoodsByIds(Long[] ids);
}
