package com.ruoyi.system.domain;

/**
 * @author dell
 * 订单商品销售数量实体类
 */
public class ErpSaleGoodNum {
    private Long goodNum;

    private Long goodId;

    public Long getGoodNum() {
        return goodNum;
    }

    public void setGoodNum(Long goodNum) {
        this.goodNum = goodNum;
    }

    public Long getGoodId() {
        return goodId;
    }

    public void setGoodId(Long goodId) {
        this.goodId = goodId;
    }
}
