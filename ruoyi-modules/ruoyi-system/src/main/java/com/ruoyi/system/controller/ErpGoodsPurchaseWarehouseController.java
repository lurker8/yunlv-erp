package com.ruoyi.system.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import jdk.nashorn.internal.runtime.Undefined;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.domain.ErpGoodsPurchaseWarehouse;
import com.ruoyi.system.service.IErpGoodsPurchaseWarehouseService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 采购商品仓库关联Controller
 * 
 * @author ruoyi
 * @date 2022-11-10
 */
@RestController
@RequestMapping("/goodsPurchaseWarehouse")
public class ErpGoodsPurchaseWarehouseController extends BaseController
{
    @Autowired
    private IErpGoodsPurchaseWarehouseService erpGoodsPurchaseWarehouseService;

    /**
     * 查询采购商品仓库关联列表
     */
    @RequiresPermissions("system:goodsPurchaseWarehouse:list")
    @GetMapping("/list")
    public TableDataInfo list(ErpGoodsPurchaseWarehouse erpGoodsPurchaseWarehouse)
    {
        startPage();
        List<ErpGoodsPurchaseWarehouse> list = erpGoodsPurchaseWarehouseService.selectErpGoodsPurchaseWarehouseList(erpGoodsPurchaseWarehouse);
        return getDataTable(list);
    }

    /**
     * 导出采购商品仓库关联列表
     */
    @RequiresPermissions("system:goodsPurchaseWarehouse:export")
    @Log(title = "采购商品仓库关联", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ErpGoodsPurchaseWarehouse erpGoodsPurchaseWarehouse)
    {
        List<ErpGoodsPurchaseWarehouse> list = erpGoodsPurchaseWarehouseService.selectErpGoodsPurchaseWarehouseList(erpGoodsPurchaseWarehouse);
        ExcelUtil<ErpGoodsPurchaseWarehouse> util = new ExcelUtil<ErpGoodsPurchaseWarehouse>(ErpGoodsPurchaseWarehouse.class);
        util.exportExcel(response, list, "采购商品仓库关联数据");
    }

    /**
     * 获取采购商品仓库关联详细信息
     */
    @RequiresPermissions("system:goodsPurchaseWarehouse:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(erpGoodsPurchaseWarehouseService.selectErpGoodsPurchaseWarehouseById(id));
    }

    /**
     * 新增采购商品仓库关联
     */
    @RequiresPermissions("system:goodsPurchaseWarehouse:add")
    @Log(title = "采购商品仓库关联", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ErpGoodsPurchaseWarehouse erpGoodsPurchaseWarehouse)
    {
        return toAjax(erpGoodsPurchaseWarehouseService.insertErpGoodsPurchaseWarehouse(erpGoodsPurchaseWarehouse));
    }

    /**
     * 修改采购商品仓库关联
     */
    @RequiresPermissions("system:goodsPurchaseWarehouse:edit")
    @Log(title = "采购商品仓库关联", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ErpGoodsPurchaseWarehouse erpGoodsPurchaseWarehouse)
    {
        if(erpGoodsPurchaseWarehouse.getId() == null){
            erpGoodsPurchaseWarehouseService.insertErpGoodsPurchaseWarehouse(erpGoodsPurchaseWarehouse);
        }
        return toAjax(erpGoodsPurchaseWarehouseService.updateErpGoodsPurchaseWarehouse(erpGoodsPurchaseWarehouse));
    }

    /**
     * 删除采购商品仓库关联
     */
    @RequiresPermissions("system:goodsPurchaseWarehouse:remove")
    @Log(title = "采购商品仓库关联", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(erpGoodsPurchaseWarehouseService.deleteErpGoodsPurchaseWarehouseByIds(ids));
    }
}
