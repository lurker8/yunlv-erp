package com.ruoyi.system.service;

import com.ruoyi.system.domain.ErpSaleGoods;

import java.util.List;

/**
 * 销售商品Service接口
 *
 * @author liuce
 * @date 2022-10-31
 */
public interface IErpSaleGoodsService
{
    /**
     * 查询销售商品
     *
     * @param id 销售商品主键
     * @return 销售商品
     */
    public ErpSaleGoods selectErpSaleGoodsById(Long saleId,Long goodId);

    /**
     * 查询销售商品列表
     *
     * @param erpSaleGoods 销售商品
     * @return 销售商品集合
     */
    public List<ErpSaleGoods> selectErpSaleGoodsList(ErpSaleGoods erpSaleGoods);

    /**
     * 新增销售商品
     *
     * @param erpSaleGoods 销售商品
     * @return 结果
     */
    public int insertErpSaleGoods(ErpSaleGoods erpSaleGoods);

    /**
     * 修改销售商品
     *
     * @param erpSaleGoods 销售商品
     * @return 结果
     */
    public int updateErpSaleGoods(ErpSaleGoods erpSaleGoods);

    /**
     * 批量删除销售商品
     *
     * @param ids 需要删除的销售商品主键集合
     * @return 结果
     */
    public int deleteErpSaleGoodsByIds(Long[] ids);

    /**
     * 删除销售商品信息
     *
     * @param id 销售商品主键
     * @return 结果
     */
    public int deleteErpSaleGoodsById(Long id);
}
