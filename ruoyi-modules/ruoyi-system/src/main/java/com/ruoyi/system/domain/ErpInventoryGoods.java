package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 库存盘点商品对象 erp_inventory_goods
 * 
 * @author ruoyi
 * @date 2022-11-21
 */
public class ErpInventoryGoods extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 盘点商品关联表id */
    private Long id;

    /** 盘点id */
    @Excel(name = "盘点id")
    private Long inventoryId;

    /** 商品id */
    @Excel(name = "商品id")
    private Long goodId;

    /** 盘点前数量 */
    @Excel(name = "盘点前数量")
    private Long beforeNum;

    /** 盘点后数量 */
    @Excel(name = "盘点后数量")
    private Long afterNum;

    /** 创建人 */
    @Excel(name = "创建人")
    private String creatUser;

    /** 品牌name*/
    private String brandName;

    /** 分类name*/
    private String categoryName;

    /** 单位name*/
    private String companyName;

    /** 仓库name*/
    private String warehouseName;

    /** 商品name*/
    private String goodName;

    public String getGoodName() {
        return goodName;
    }

    public void setGoodName(String goodName) {
        this.goodName = goodName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setInventoryId(Long inventoryId) 
    {
        this.inventoryId = inventoryId;
    }

    public Long getInventoryId() 
    {
        return inventoryId;
    }
    public void setGoodId(Long goodId) 
    {
        this.goodId = goodId;
    }

    public Long getGoodId() 
    {
        return goodId;
    }
    public void setBeforeNum(Long beforeNum) 
    {
        this.beforeNum = beforeNum;
    }

    public Long getBeforeNum() 
    {
        return beforeNum;
    }
    public void setAfterNum(Long afterNum) 
    {
        this.afterNum = afterNum;
    }

    public Long getAfterNum() 
    {
        return afterNum;
    }
    public void setCreatUser(String creatUser) 
    {
        this.creatUser = creatUser;
    }

    public String getCreatUser() 
    {
        return creatUser;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("inventoryId", getInventoryId())
            .append("goodId", getGoodId())
            .append("beforeNum", getBeforeNum())
            .append("afterNum", getAfterNum())
            .append("creatUser", getCreatUser())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
