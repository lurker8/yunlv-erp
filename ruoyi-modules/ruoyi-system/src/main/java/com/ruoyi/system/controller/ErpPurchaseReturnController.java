package com.ruoyi.system.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.system.api.model.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.domain.ErpPurchaseReturn;
import com.ruoyi.system.service.IErpPurchaseReturnService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

import static com.ruoyi.common.security.utils.SecurityUtils.getLoginUser;

/**
 * 采购退货Controller
 * 
 * @author ruoyi
 * @date 2022-11-16
 */
@RestController
@RequestMapping("/returnPurchase")
public class ErpPurchaseReturnController extends BaseController
{
    @Autowired
    private IErpPurchaseReturnService erpPurchaseReturnService;

    /**
     * 查询采购退货列表
     */
    @RequiresPermissions("system:returnPurchase:list")
    @GetMapping("/list")
    public TableDataInfo list(ErpPurchaseReturn erpPurchaseReturn)
    {
        startPage();
        List<ErpPurchaseReturn> list = erpPurchaseReturnService.selectErpPurchaseReturnList(erpPurchaseReturn);
        return getDataTable(list);
    }

    /**
     * 导出采购退货列表
     */
    @RequiresPermissions("system:returnPurchase:export")
    @Log(title = "采购退货", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ErpPurchaseReturn erpPurchaseReturn)
    {
        List<ErpPurchaseReturn> list = erpPurchaseReturnService.selectErpPurchaseReturnList(erpPurchaseReturn);
        ExcelUtil<ErpPurchaseReturn> util = new ExcelUtil<ErpPurchaseReturn>(ErpPurchaseReturn.class);
        util.exportExcel(response, list, "采购退货数据");
    }

    /**
     * 获取采购退货详细信息
     */
    @RequiresPermissions("system:returnPurchase:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(erpPurchaseReturnService.selectErpPurchaseReturnById(id));
    }

    /**
     * 新增采购退货
     */
    @RequiresPermissions("system:returnPurchase:add")
    @Log(title = "采购退货", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ErpPurchaseReturn erpPurchaseReturn)
    {
        LoginUser loginUser = getLoginUser();
        erpPurchaseReturn.setCreateUser(loginUser.getUsername());
        return toAjax(erpPurchaseReturnService.insertErpPurchaseReturn(erpPurchaseReturn));
    }

    /**
     * 修改采购退货
     */
    @RequiresPermissions("system:returnPurchase:edit")
    @Log(title = "采购退货", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ErpPurchaseReturn erpPurchaseReturn)
    {
        return toAjax(erpPurchaseReturnService.updateErpPurchaseReturn(erpPurchaseReturn));
    }

    /**
     * 删除采购退货
     */
    @RequiresPermissions("system:returnPurchase:remove")
    @Log(title = "采购退货", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(erpPurchaseReturnService.deleteErpPurchaseReturnByIds(ids));
    }
}
