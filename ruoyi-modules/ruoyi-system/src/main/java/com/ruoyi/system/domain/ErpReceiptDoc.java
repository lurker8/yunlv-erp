package com.ruoyi.system.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 入库单对象 erp_receipt_doc
 * 
 * @author ruoyi
 * @date 2022-11-17
 */
public class ErpReceiptDoc extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 入库id */
    private Long id;

    /** 入库单号 */
    @Excel(name = "入库单号")
    private String receiptDocOrder;

    /** 入库金额 */
    @Excel(name = "入库金额")
    private BigDecimal receiptDocAmount;

    /** 入库仓库 */
    @Excel(name = "入库仓库")
    private String receiptDocWarehouse;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "入库时间" , width = 30, dateFormat = "yyyy-MM-dd  HH:mm:ss")
    private Date createTime;

    /** 采购id */
    private Long purchaseId;

    @Excel(name = "销售/采购" , readConverterExp = "1=调拨单,2=采购单")
    private Long isSaleAllocation;

    /** 操作人 */
    @Excel(name = "操作人")
    private String createUser;


    @Override
    public Date getCreateTime() {
        return createTime;
    }

    @Override
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getIsSaleAllocation() {
        return isSaleAllocation;
    }

    public void setIsSaleAllocation(Long isSaleAllocation) {
        this.isSaleAllocation = isSaleAllocation;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setReceiptDocOrder(String receiptDocOrder) 
    {
        this.receiptDocOrder = receiptDocOrder;
    }

    public String getReceiptDocOrder() 
    {
        return receiptDocOrder;
    }
    public void setReceiptDocAmount(BigDecimal receiptDocAmount) 
    {
        this.receiptDocAmount = receiptDocAmount;
    }

    public BigDecimal getReceiptDocAmount() 
    {
        return receiptDocAmount;
    }
    public void setReceiptDocWarehouse(String receiptDocWarehouse) 
    {
        this.receiptDocWarehouse = receiptDocWarehouse;
    }

    public String getReceiptDocWarehouse() 
    {
        return receiptDocWarehouse;
    }
    public void setCreateUser(String createUser) 
    {
        this.createUser = createUser;
    }

    public String getCreateUser() 
    {
        return createUser;
    }

    public Long getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(Long purchaseId) {
        this.purchaseId = purchaseId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("receiptDocOrder", getReceiptDocOrder())
            .append("receiptDocAmount", getReceiptDocAmount())
            .append("receiptDocWarehouse", getReceiptDocWarehouse())
            .append("createUser", getCreateUser())
            .append("purchaseId", getPurchaseId())
            .append("createTime", getCreateTime())
            .toString();
    }
}
