package com.ruoyi.system.domain;

import com.ruoyi.common.core.web.domain.BaseEntity;

import java.math.BigDecimal;

/**
 * 销售仓库商品关联表对象 erp_goods_sale_warehouse
 *
 * @author 刘策
 * @date 2022-11-02
 */
public class ErpGoodsSaleWarehouse extends BaseEntity {

    /**
     * 商品id
     */
    private Long goodId;

    /**
     * 仓库id
     * */
    private String warehouseId;

    /**
     * 一个仓库商品出库数量
     * */
    private Long goodsNumber;

    /**
     * 销售id
     * */
    private Long sellId;

    /**
     * 一个仓库商品出库数量(适应前端)
     * */
    private Long goodSaleWarehouse;

    /**
     * 仓库name
     * */
    private String warehouseName;

    /**
     * 商品退货数量
     * */
    private Long returnGoodNumber;

    /**
     * 商品退货数量(适应前端)
     * */
    private Long goodReturnWarehouse;

    /**
     * 商品销售单价
     * */
    private BigDecimal itemPricing;

    /**
     * 商品退货金额
     * */
    private BigDecimal returnGoodAmount;

    public BigDecimal getReturnGoodAmount() {
        return returnGoodAmount;
    }

    public void setReturnGoodAmount(BigDecimal returnGoodAmount) {
        this.returnGoodAmount = returnGoodAmount;
    }

    public BigDecimal getItemPricing() {
        return itemPricing;
    }

    public void setItemPricing(BigDecimal itemPricing) {
        this.itemPricing = itemPricing;
    }

    public Long getGoodReturnWarehouse() {
        return goodReturnWarehouse;
    }

    public void setGoodReturnWarehouse(Long goodReturnWarehouse) {
        this.goodReturnWarehouse = goodReturnWarehouse;
    }

    public Long getReturnGoodNumber() {
        return returnGoodNumber;
    }

    public void setReturnGoodNumber(Long returnGoodNumber) {
        this.returnGoodNumber = returnGoodNumber;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public Long getGoodSaleWarehouse() {
        return goodSaleWarehouse;
    }

    public void setGoodSaleWarehouse(Long goodSaleWarehouse) {
        this.goodSaleWarehouse = goodSaleWarehouse;
    }

    public Long getGoodId() {
        return goodId;
    }

    public void setGoodId(Long goodId) {
        this.goodId = goodId;
    }

    public Long getSellId() {
        return sellId;
    }

    public void setSellId(Long sellId) {
        this.sellId = sellId;
    }

    public Long getGoodsNumber() {
        return goodsNumber;
    }

    public void setGoodsNumber(Long goodsNumber) {
        this.goodsNumber = goodsNumber;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

}
