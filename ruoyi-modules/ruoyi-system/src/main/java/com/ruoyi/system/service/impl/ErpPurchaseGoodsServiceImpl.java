package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.system.domain.ErpPurchaseGoods;
import com.ruoyi.system.mapper.ErpPurchaseGoodsMapper;
import com.ruoyi.system.service.IErpPurchaseGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 采购商品Service业务层处理
 *
 * @author ruoyi
 * @date 2022-10-31
 */
@Service
public class ErpPurchaseGoodsServiceImpl implements IErpPurchaseGoodsService
{
    @Autowired
    private ErpPurchaseGoodsMapper erpPurchaseGoodsMapper;

    /**
     * 查询采购商品
     *
     * @param id 采购商品主键
     * @return 采购商品
     */
    @Override
    public ErpPurchaseGoods selectErpPurchaseGoodsById(Long id)
    {
        return erpPurchaseGoodsMapper.selectErpPurchaseGoodsById(id);
    }

    /*根据采购订单id获取采购商品详细信息列表*/
    @Override
    public List<ErpPurchaseGoods> selectErpPurchaseGoodsPurchaseId(ErpPurchaseGoods erpPurchaseGoods)
    {
        return erpPurchaseGoodsMapper.selectErpPurchaseGoodsPurchaseId(erpPurchaseGoods);
    }

    /**
     * 查询采购商品列表
     *
     * @param erpPurchaseGoods 采购商品
     * @return 采购商品
     */
    @Override
    public List<ErpPurchaseGoods> selectErpPurchaseGoodsList(ErpPurchaseGoods erpPurchaseGoods)
    {
        return erpPurchaseGoodsMapper.selectErpPurchaseGoodsList(erpPurchaseGoods);
    }

    /**
     * 新增采购商品
     *
     * @param erpPurchaseGoods 采购商品
     * @return 结果
     */
    @Override
    public int insertErpPurchaseGoods(ErpPurchaseGoods erpPurchaseGoods)
    {
        erpPurchaseGoods.setCreateTime(DateUtils.getNowDate());
        return erpPurchaseGoodsMapper.insertErpPurchaseGoods(erpPurchaseGoods);
    }

    /**
     * 修改采购商品
     *
     * @param erpPurchaseGoods 采购商品
     * @return 结果
     */
    @Override
    public int updateErpPurchaseGoods(ErpPurchaseGoods erpPurchaseGoods)
    {
        erpPurchaseGoods.setUpdateTime(DateUtils.getNowDate());
        return erpPurchaseGoodsMapper.updateErpPurchaseGoods(erpPurchaseGoods);
    }

    /*修改采购商品数量专用*/
    @Override
    public int updateErpPurchaseGoodsQuantity(ErpPurchaseGoods erpPurchaseGoods)
    {
        erpPurchaseGoods.setUpdateTime(DateUtils.getNowDate());
        return erpPurchaseGoodsMapper.updateErpPurchaseGoodsQuantity(erpPurchaseGoods);
    }

    /**
     * 批量删除采购商品
     *
     * @param ids 需要删除的采购商品主键
     * @return 结果
     */
    @Override
    public int deleteErpPurchaseGoodsByIds(Long[] ids)
    {
        return erpPurchaseGoodsMapper.deleteErpPurchaseGoodsByIds(ids);
    }

    /**
     * 删除采购商品信息
     *
     * @param id 采购商品主键
     * @return 结果
     */
    @Override
    public int deleteErpPurchaseGoodsById(Long id)
    {
        return erpPurchaseGoodsMapper.deleteErpPurchaseGoodsById(id);
    }
}
