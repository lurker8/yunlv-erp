package com.ruoyi.system.service;

import com.ruoyi.system.domain.ErpGoodsWarehouse;

import java.util.List;

/**
 * 商品仓库管理Service接口
 *
 * @author 刘策
 * @date 2022-11-02
 */
public interface IErpGoodsWarehouseService
{
    /**
     * 查询商品仓库管理
     *
     * @param id 商品id
     * @return 商品仓库管理
     */
    public List<ErpGoodsWarehouse> selectErpGoodsWarehouseById(Long id);

    /*根据商品id查询*/
    public List<ErpGoodsWarehouse> selectErpGoodsWarehouseByGoodsId(ErpGoodsWarehouse erpGoodsWarehouse);

    /**
     * 查询商品仓库管理列表
     *
     * @param erpGoodsWarehouse 商品仓库管理
     * @return 商品仓库管理集合
     */
    public List<ErpGoodsWarehouse> selectErpGoodsWarehouseList(ErpGoodsWarehouse erpGoodsWarehouse);

    /*查询商品名称*/
    public List<ErpGoodsWarehouse> selectErpGoodsWarehouseGoodsNameList(ErpGoodsWarehouse erpGoodsWarehouse);
    /*查询仓库名称*/
    public List<ErpGoodsWarehouse> selectErpGoodsWarehouseNameList(ErpGoodsWarehouse erpGoodsWarehouse);

    /**
     * 新增商品仓库管理
     *
     * @param erpGoodsWarehouse 商品仓库管理
     * @return 结果
     */
    public int insertErpGoodsWarehouse(ErpGoodsWarehouse erpGoodsWarehouse);

    /**
     * 修改商品仓库管理
     *
     * @param erpGoodsWarehouse 商品仓库管理
     * @return 结果
     */
    public int updateErpGoodsWarehouse(ErpGoodsWarehouse erpGoodsWarehouse);
    public int updateErpGoodsWarehouses(ErpGoodsWarehouse erpGoodsWarehouse);

    /**
     * 批量删除商品仓库管理
     *
     * @param ids 需要删除的商品仓库管理主键集合
     * @return 结果
     */
    public int deleteErpGoodsWarehouseByIds(Long[] ids);

    /**
     * 删除商品仓库管理信息
     *
     * @param id 商品仓库管理主键
     * @return 结果
     */
    public int deleteErpGoodsWarehouseById(Long id);

    /**
     * 查询销售的商品分别从哪个仓库发了多少的货
     * @param warehouseId 商品仓库id
     * @param saleId 销售id
     * @param saleGoodId 商品id
     * @return 结果
     * */
    ErpGoodsWarehouse selectGoodWarehouseNumber(Long warehouseId,Long saleId,Long saleGoodId);

    int deleteGoodNumberInfo(Long goodId,Long saleId);

}
