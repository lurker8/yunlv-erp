package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.ErpAllocationGoods;

/**
 * 仓库调拨商品关联Mapper接口
 *
 * @author ruoyi
 * @date 2022-11-17
 */
public interface ErpAllocationGoodsMapper
{
    /**
     * 查询仓库调拨商品关联
     *
     * @param id 仓库调拨商品关联主键
     * @return 仓库调拨商品关联
     */
    public ErpAllocationGoods selectErpAllocationGoodsById(Long id);

    /**
     * 查询仓库调拨商品关联列表
     *
     * @param erpAllocationGoods 仓库调拨商品关联
     * @return 仓库调拨商品关联集合
     */
    public List<ErpAllocationGoods> selectErpAllocationGoodsList(ErpAllocationGoods erpAllocationGoods);

    /**
     * 新增仓库调拨商品关联
     *
     * @param erpAllocationGoods 仓库调拨商品关联
     * @return 结果
     */
    public int insertErpAllocationGoods(ErpAllocationGoods erpAllocationGoods);

    /**
     * 修改仓库调拨商品关联
     *
     * @param erpAllocationGoods 仓库调拨商品关联
     * @return 结果
     */
    public int updateErpAllocationGoods(ErpAllocationGoods erpAllocationGoods);

    /**
     * 删除仓库调拨商品关联
     *
     * @param id 仓库调拨商品关联主键
     * @return 结果
     */
    public int deleteErpAllocationGoodsById(Long id);

    /**
     * 批量删除仓库调拨商品关联
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteErpAllocationGoodsByIds(Long[] ids);

}
