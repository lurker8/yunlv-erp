package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.ErpPurchaseInvoice;

/**
 * 采购入库单Service接口
 * 
 * @author ruoyi
 * @date 2022-11-14
 */
public interface IErpPurchaseInvoiceService 
{
    /**
     * 查询采购入库单
     * 
     * @param id 采购入库单主键
     * @return 采购入库单
     */
    public ErpPurchaseInvoice selectErpPurchaseInvoiceById(Long id);

    /**
     * 查询采购入库单列表
     * 
     * @param erpPurchaseInvoice 采购入库单
     * @return 采购入库单集合
     */
    public List<ErpPurchaseInvoice> selectErpPurchaseInvoiceList(ErpPurchaseInvoice erpPurchaseInvoice);

    /**
     * 新增采购入库单
     * 
     * @param erpPurchaseInvoice 采购入库单
     * @return 结果
     */
    public int insertErpPurchaseInvoice(ErpPurchaseInvoice erpPurchaseInvoice);

    /**
     * 修改采购入库单
     * 
     * @param erpPurchaseInvoice 采购入库单
     * @return 结果
     */
    public int updateErpPurchaseInvoice(ErpPurchaseInvoice erpPurchaseInvoice);

    /**
     * 批量删除采购入库单
     * 
     * @param ids 需要删除的采购入库单主键集合
     * @return 结果
     */
    public int deleteErpPurchaseInvoiceByIds(Long[] ids);

    /**
     * 删除采购入库单信息
     * 
     * @param id 采购入库单主键
     * @return 结果
     */
    public int deleteErpPurchaseInvoiceById(Long id);
}
