package com.ruoyi.system.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.system.api.model.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.domain.ErpReceiptDoc;
import com.ruoyi.system.service.IErpReceiptDocService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

import static com.ruoyi.common.security.utils.SecurityUtils.getLoginUser;

/**
 * 入库单Controller
 * 
 * @author ruoyi
 * @date 2022-11-17
 */
@RestController
@RequestMapping("/receiptDoc")
public class ErpReceiptDocController extends BaseController
{
    @Autowired
    private IErpReceiptDocService erpReceiptDocService;

    /**
     * 查询入库单列表
     */
    @RequiresPermissions("system:receiptDoc:list")
    @GetMapping("/list")
    public TableDataInfo list(ErpReceiptDoc erpReceiptDoc)
    {
        startPage();
        List<ErpReceiptDoc> list = erpReceiptDocService.selectErpReceiptDocList(erpReceiptDoc);
        return getDataTable(list);
    }

    /**
     * 导出入库单列表
     */
    @RequiresPermissions("system:receiptDoc:export")
    @Log(title = "入库单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ErpReceiptDoc erpReceiptDoc)
    {
        List<ErpReceiptDoc> list = erpReceiptDocService.selectErpReceiptDocList(erpReceiptDoc);
        ExcelUtil<ErpReceiptDoc> util = new ExcelUtil<ErpReceiptDoc>(ErpReceiptDoc.class);
        util.exportExcel(response, list, "入库单数据");
    }

    /**
     * 获取入库单详细信息
     */
    @RequiresPermissions("system:receiptDoc:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(erpReceiptDocService.selectErpReceiptDocById(id));
    }

    /**
     * 新增入库单
     */
    @RequiresPermissions("system:receiptDoc:add")
    @Log(title = "入库单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ErpReceiptDoc erpReceiptDoc)
    {
        LoginUser loginUser = getLoginUser();
        erpReceiptDoc.setCreateUser(loginUser.getUsername());
        return toAjax(erpReceiptDocService.insertErpReceiptDoc(erpReceiptDoc));
    }

    /**
     * 修改入库单
     */
    @RequiresPermissions("system:receiptDoc:edit")
    @Log(title = "入库单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ErpReceiptDoc erpReceiptDoc)
    {
        return toAjax(erpReceiptDocService.updateErpReceiptDoc(erpReceiptDoc));
    }

    /**
     * 删除入库单
     */
    @RequiresPermissions("system:receiptDoc:remove")
    @Log(title = "入库单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(erpReceiptDocService.deleteErpReceiptDocByIds(ids));
    }
}
