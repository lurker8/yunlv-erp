package com.ruoyi.system.service;

import com.ruoyi.system.domain.ErpBrand;

import java.util.List;

/**
 * 品牌管理Service接口
 *
 * @author 刘策
 * @date 2022-10-26
 */
public interface IErpBrandService
{
    /**
     * 查询品牌管理
     *
     * @param id 品牌管理主键
     * @return 品牌管理
     */
    public ErpBrand selectErpBrandById(Long id);

    /**
     * 查询品牌管理列表
     *
     * @param erpBrand 品牌管理
     * @return 品牌管理集合
     */
    public List<ErpBrand> selectErpBrandList(ErpBrand erpBrand);

    /**
     * 新增品牌管理
     *
     * @param erpBrand 品牌管理
     * @return 结果
     */
    public int insertErpBrand(ErpBrand erpBrand);

    /**
     * 修改品牌管理
     *
     * @param erpBrand 品牌管理
     * @return 结果
     */
    public int updateErpBrand(ErpBrand erpBrand);

    /**
     * 批量删除品牌管理
     *
     * @param ids 需要删除的品牌管理主键集合
     * @return 结果
     */
    public int deleteErpBrandByIds(Long[] ids);

    /**
     * 删除品牌管理信息
     *
     * @param id 品牌管理主键
     * @return 结果
     */
    public int deleteErpBrandById(Long id);

    /** 查询品牌信息关联
     *
     * @param id 品牌id
     * @return 结果
     * */
    Long selectBrandAssociation(Long id);
}
