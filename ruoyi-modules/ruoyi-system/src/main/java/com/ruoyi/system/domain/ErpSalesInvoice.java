package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 销售发货单对象 erp_sales_invoice
 *
 * @author liuce
 * @date 2022-11-07
 */
public class ErpSalesInvoice extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 销售单号 */
    @Excel(name = "销售单号")
    private String saleOrderNum;

    /** 发货单号 */
    @Excel(name = "发货单号")
    private String shipmentNum;

    /** 订单id */
    @Excel(name = "订单id")
    private Long saleId;

    /** 客户id */
    private Long customerId;

    /** 客户NAME */
    @Excel(name = "客户NAME")
    private String customerName;

    /** 收款方式id */
    @Excel(name = "收款方式id")
    private Long paymentMethod;

    /** 客户电话 */
    @Excel(name = "客户电话")
    private String customerPhone;

    /** 销售电话 */
    @Excel(name = "销售电话")
    private String fixedTelephone;

    /** 销售总额 */
    @Excel(name = "销售总额")
    private BigDecimal saleBonus;

    /** 状态 */
    @Excel(name = "状态")
    private Long type;

    /** 修改时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "修改时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updateTime;

    /** 创建人 */
    @Excel(name = "创建人")
    private String createUser;

    /** 是否退货*/
    private Long isReturn;

    /** 退货商品是否入库 */

    private Long isWarehousing;

    public Long getIsWarehousing() {
        return isWarehousing;
    }

    public void setIsWarehousing(Long isWarehousing) {
        this.isWarehousing = isWarehousing;
    }

    public Long getIsReturn() {
        return isReturn;
    }

    public void setIsReturn(Long isReturn) {
        this.isReturn = isReturn;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setSaleOrderNum(String saleOrderNum)
    {
        this.saleOrderNum = saleOrderNum;
    }

    public String getSaleOrderNum()
    {
        return saleOrderNum;
    }
    public void setShipmentNum(String shipmentNum)
    {
        this.shipmentNum = shipmentNum;
    }

    public String getShipmentNum()
    {
        return shipmentNum;
    }
    public void setSaleId(Long saleId)
    {
        this.saleId = saleId;
    }

    public Long getSaleId()
    {
        return saleId;
    }
    public void setCustomerId(Long customerId)
    {
        this.customerId = customerId;
    }

    public Long getCustomerId()
    {
        return customerId;
    }
    public void setPaymentMethod(Long paymentMethod)
    {
        this.paymentMethod = paymentMethod;
    }

    public Long getPaymentMethod()
    {
        return paymentMethod;
    }
    public void setCustomerPhone(String customerPhone)
    {
        this.customerPhone = customerPhone;
    }

    public String getCustomerPhone()
    {
        return customerPhone;
    }
    public void setFixedTelephone(String fixedTelephone)
    {
        this.fixedTelephone = fixedTelephone;
    }

    public String getFixedTelephone()
    {
        return fixedTelephone;
    }
    public void setSaleBonus(BigDecimal saleBonus)
    {
        this.saleBonus = saleBonus;
    }

    public BigDecimal getSaleBonus()
    {
        return saleBonus;
    }
    public void setType(Long type)
    {
        this.type = type;
    }

    public Long getType()
    {
        return type;
    }

    @Override
    public Date getUpdateTime() {
        return updateTime;
    }

    @Override
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public void setCreateUser(String createUser)
    {
        this.createUser = createUser;
    }

    public String getCreateUser()
    {
        return createUser;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("saleOrderNum", getSaleOrderNum())
            .append("shipmentNum", getShipmentNum())
            .append("saleId", getSaleId())
            .append("customerId", getCustomerId())
            .append("paymentMethod", getPaymentMethod())
            .append("customerPhone", getCustomerPhone())
            .append("fixedTelephone", getFixedTelephone())
            .append("saleBonus", getSaleBonus())
            .append("type", getType())
            .append("createTime", getCreateTime())
            .append("uodateTime", getUpdateTime())
            .append("createUser", getCreateUser())
            .toString();
    }
}
