package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.ErpLossReporting;
import io.lettuce.core.dynamic.annotation.Param;

/**
 * 报损单Mapper接口
 * 
 * @author liuce
 * @date 2022-11-21
 */
public interface ErpLossReportingMapper 
{
    /**
     * 查询报损单
     * 
     * @param id 报损单主键
     * @return 报损单
     */
    public ErpLossReporting selectErpLossReportingById(Long id);

    /**
     * 查询报损单列表
     * 
     * @param erpLossReporting 报损单
     * @return 报损单集合
     */
    public List<ErpLossReporting> selectErpLossReportingList(ErpLossReporting erpLossReporting);

    /**
     * 新增报损单
     * 
     * @param erpLossReporting 报损单
     * @return 结果
     */
    public int insertErpLossReporting(ErpLossReporting erpLossReporting);

    /**
     * 修改报损单
     * 
     * @param erpLossReporting 报损单
     * @return 结果
     */
    public int updateErpLossReporting(ErpLossReporting erpLossReporting);

    /**
     * 删除报损单
     * 
     * @param id 报损单主键
     * @return 结果
     */
    public int deleteErpLossReportingById(Long id);

    /**
     * 批量删除报损单
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteErpLossReportingByIds(Long[] ids);

    /** 确认报损单操作
     * @param erpLossReporting 报损单
     *
     * @return  结果*/
    int confirmReporting(ErpLossReporting erpLossReporting);

    /** 报损减少库存操作
     *
     * @param goodId 商品id
     * @param warehouseId 仓库id
     * @param goodNum 报损数量
     *
     * @return 结果
     * */
    int updateGoodWarehouseNum(@Param("goodId")Long goodId,@Param("warehouseId")Long warehouseId,@Param("goodNum")Long goodNum);

    /** 报损减少库存操作
     *
     * @param goodId 商品id
     * @param goodNum 报损数量
     *
     * @return 结果
     * */
    int updateGoodNum(@Param("goodId")Long goodId,@Param("goodNum")Long goodNum);
}
