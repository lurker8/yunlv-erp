package com.ruoyi.system.domain;

import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;

/**
 * 【请填写功能名称】对象 erp_sales_return
 *
 * @author ruoyi
 * @date 2022-11-07
 */
public class ErpSalesReturn extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 销售id */
    @Excel(name = "销售id")
    private Long saleId;

    /** 销售单号 */
    @Excel(name = "销售单号")
    private String saleOrderNum;

    /** 发货单号 */
    @Excel(name = "发货单号")
    private String shipmentNum;

    /** 客户id */
    private Long customerId;

    /**客户name*/
    @Excel(name = "客户")
    private String customerName;

    /** 收款方式id */
    @Excel(name = "收款方式id")
    private Long paymentMethod;

    /** 退货总额 */
    @Excel(name = "退货总额")
    private BigDecimal returnBonus;

    /** 客户电话 */
    @Excel(name = "客户电话")
    private String customerPhone;

    /** 创建人 */
    @Excel(name = "创建人")
    private String createUser;

    private Long isReturn;

    public Long getIsReturn() {
        return isReturn;
    }

    public void setIsReturn(Long isReturn) {
        this.isReturn = isReturn;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setSaleId(Long saleId)
    {
        this.saleId = saleId;
    }

    public Long getSaleId()
    {
        return saleId;
    }
    public void setSaleOrderNum(String saleOrderNum)
    {
        this.saleOrderNum = saleOrderNum;
    }

    public String getSaleOrderNum()
    {
        return saleOrderNum;
    }
    public void setShipmentNum(String shipmentNum)
    {
        this.shipmentNum = shipmentNum;
    }

    public String getShipmentNum()
    {
        return shipmentNum;
    }
    public void setCustomerId(Long customerId)
    {
        this.customerId = customerId;
    }

    public Long getCustomerId()
    {
        return customerId;
    }
    public void setPaymentMethod(Long paymentMethod)
    {
        this.paymentMethod = paymentMethod;
    }

    public Long getPaymentMethod()
    {
        return paymentMethod;
    }
    public void setReturnBonus(BigDecimal returnBonus)
    {
        this.returnBonus = returnBonus;
    }

    public BigDecimal getReturnBonus()
    {
        return returnBonus;
    }
    public void setCustomerPhone(String customerPhone)
    {
        this.customerPhone = customerPhone;
    }

    public String getCustomerPhone()
    {
        return customerPhone;
    }
    public void setCreateUser(String createUser)
    {
        this.createUser = createUser;
    }

    public String getCreateUser()
    {
        return createUser;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("saleId", getSaleId())
            .append("saleOrderNum", getSaleOrderNum())
            .append("shipmentNum", getShipmentNum())
            .append("customerId", getCustomerId())
            .append("paymentMethod", getPaymentMethod())
            .append("returnBonus", getReturnBonus())
            .append("customerPhone", getCustomerPhone())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createUser", getCreateUser())
            .toString();
    }
}
