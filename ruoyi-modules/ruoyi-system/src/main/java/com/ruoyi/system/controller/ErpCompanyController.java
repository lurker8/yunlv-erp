package com.ruoyi.system.controller;

import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.domain.ErpCompany;
import com.ruoyi.system.service.IErpCompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * company Controller
 *
 * @author 刘策
 * @date 2022-11-01
 */
@RestController
@RequestMapping("/company")
public class ErpCompanyController extends BaseController
{
    @Autowired
    private IErpCompanyService erpCompanyService;

    /**
     * 查询company 列表
     */
    @RequiresPermissions("system:company:list")
    @GetMapping("/list")
    public TableDataInfo list(ErpCompany erpCompany)
    {
        startPage();
        List<ErpCompany> list = erpCompanyService.selectErpCompanyList(erpCompany);
        return getDataTable(list);
    }
    /*查询计量单位专用，不能有分页*/
    @GetMapping("/listCompanys")
    public TableDataInfo listCompanys(ErpCompany erpCompany)
    {
        List<ErpCompany> list = erpCompanyService.selectErpCompanyList(erpCompany);
        return getDataTable(list);
    }

    /**
     * 导出company 列表
     */
    @RequiresPermissions("system:company:export")
    @Log(title = "company ", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ErpCompany erpCompany)
    {
        List<ErpCompany> list = erpCompanyService.selectErpCompanyList(erpCompany);
        ExcelUtil<ErpCompany> util = new ExcelUtil<ErpCompany>(ErpCompany.class);
        util.exportExcel(response, list, "company 数据");
    }

    /**
     * 获取company 详细信息
     */
    @RequiresPermissions("system:company:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(erpCompanyService.selectErpCompanyById(id));
    }

    /**
     * 新增company
     */
    @RequiresPermissions("system:company:add")
    @Log(title = "company ", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ErpCompany erpCompany)
    {
        return toAjax(erpCompanyService.insertErpCompany(erpCompany));
    }

    /**
     * 修改company
     */
    @RequiresPermissions("system:company:edit")
    @Log(title = "company ", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ErpCompany erpCompany)
    {
        return toAjax(erpCompanyService.updateErpCompany(erpCompany));
    }

    /**
     * 删除company
     */
    @RequiresPermissions("system:company:remove")
    @Log(title = "company ", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(erpCompanyService.deleteErpCompanyByIds(ids));
    }
}
