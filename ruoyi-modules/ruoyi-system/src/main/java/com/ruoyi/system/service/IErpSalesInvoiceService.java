package com.ruoyi.system.service;

import com.ruoyi.system.domain.ErpSalesInvoice;

import java.util.List;

/**
 * 销售发货单Service接口
 *
 * @author liuce
 * @date 2022-11-07
 */
public interface IErpSalesInvoiceService
{
    /**
     * 查询销售发货单
     *
     * @param id 销售发货单主键
     * @return 销售发货单
     */
    public ErpSalesInvoice selectErpSalesInvoiceById(Long id);

    /**
     * 查询销售发货单列表
     *
     * @param erpSalesInvoice 销售发货单
     * @return 销售发货单集合
     */
    public List<ErpSalesInvoice> selectErpSalesInvoiceList(ErpSalesInvoice erpSalesInvoice);

    /**
     * 新增销售发货单
     *
     * @param erpSalesInvoice 销售发货单
     * @return 结果
     */
    public int insertErpSalesInvoice(ErpSalesInvoice erpSalesInvoice);

    /**
     * 修改销售发货单
     *
     * @param erpSalesInvoice 销售发货单
     * @return 结果
     */
    public int updateErpSalesInvoice(ErpSalesInvoice erpSalesInvoice);


    /**
     * 批量删除销售发货单
     *
     * @param ids 需要删除的销售发货单主键集合
     * @return 结果
     */
    public int deleteErpSalesInvoiceByIds(Long[] ids);

    /**
     * 删除销售发货单信息
     *
     * @param id 销售发货单主键
     * @return 结果
     */
    public int deleteErpSalesInvoiceById(Long id);
}
