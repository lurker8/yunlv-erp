package com.ruoyi.system.domain;

import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;

/**
 * 销售商品对象 erp_sale_goods
 *
 * @author liuce
 * @date 2022-10-31
 */
public class ErpSaleGoods extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 资金账户id */
    private Long id;

    /** 采购订单Id */
    @Excel(name = "采购订单Id")
    private Long purchaseId;

    /** 商品编号 */
    @Excel(name = "商品编号")
    private String goodsNumber;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String goodsName;

    /** 商品规格 */
    @Excel(name = "商品规格")
    private String goodsSpecs;

    /** 商品单位 */
    @Excel(name = "商品单位")
    private String company;

    /** 销售单价 */
    @Excel(name = "单价")
    private BigDecimal monovalent;

    /** 采购数量 */
    @Excel(name = "采购数量")
    private Long quantity;

    /** 税金 */
    @Excel(name = "税金")
    private BigDecimal taxes;

    /** 销售总价 */
    @Excel(name = "销售总价")
    private BigDecimal oneGoodPrice;

    /** 创建人id */
    @Excel(name = "创建人id")
    private Long createUser;

    /** 仓库id */
    private Long warehouseId;

    public Long getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Long warehouseId) {
        this.warehouseId = warehouseId;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setPurchaseId(Long purchaseId)
    {
        this.purchaseId = purchaseId;
    }

    public Long getPurchaseId()
    {
        return purchaseId;
    }
    public void setGoodsNumber(String goodsNumber)
    {
        this.goodsNumber = goodsNumber;
    }

    public String getGoodsNumber()
    {
        return goodsNumber;
    }
    public void setGoodsName(String goodsName)
    {
        this.goodsName = goodsName;
    }

    public String getGoodsName()
    {
        return goodsName;
    }
    public void setGoodsSpecs(String goodsSpecs)
    {
        this.goodsSpecs = goodsSpecs;
    }

    public String getGoodsSpecs()
    {
        return goodsSpecs;
    }
    public void setCompany(String company)
    {
        this.company = company;
    }

    public String getCompany()
    {
        return company;
    }
    public void setMonovalent(BigDecimal monovalent)
    {
        this.monovalent = monovalent;
    }

    public BigDecimal getMonovalent()
    {
        return monovalent;
    }
    public void setQuantity(Long quantity)
    {
        this.quantity = quantity;
    }

    public Long getQuantity()
    {
        return quantity;
    }
    public void setTaxes(BigDecimal taxes)
    {
        this.taxes = taxes;
    }

    public BigDecimal getTaxes()
    {
        return taxes;
    }
    public void setOneGoodPrice(BigDecimal oneGoodPrice)
    {
        this.oneGoodPrice = oneGoodPrice;
    }

    public BigDecimal getOneGoodPrice()
    {
        return oneGoodPrice;
    }
    public void setCreateUser(Long createUser)
    {
        this.createUser = createUser;
    }

    public Long getCreateUser()
    {
        return createUser;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("purchaseId", getPurchaseId())
            .append("goodsNumber", getGoodsNumber())
            .append("goodsName", getGoodsName())
            .append("goodsSpecs", getGoodsSpecs())
            .append("company", getCompany())
            .append("monovalent", getMonovalent())
            .append("quantity", getQuantity())
            .append("taxes", getTaxes())
            .append("total", getOneGoodPrice())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createUser", getCreateUser())
            .toString();
    }
}
