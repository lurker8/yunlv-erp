package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.system.api.model.LoginUser;
import com.ruoyi.system.domain.ErpSupplier;
import com.ruoyi.system.mapper.ErpSupplierMapper;
import com.ruoyi.system.service.IErpSupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.ruoyi.common.security.utils.SecurityUtils.getLoginUser;

/**
 * 供应商管理Service业务层处理
 *
 * @author 刘策
 * @date 2022-10-25
 */
@Service
public class ErpSupplierServiceImpl implements IErpSupplierService
{
    @Autowired
    private ErpSupplierMapper erpSupplierMapper;

    /**
     * 查询供应商管理
     *
     * @param id 供应商管理主键
     * @return 供应商管理
     */
    @Override
    public ErpSupplier selectErpSupplierById(Long id)
    {
        return erpSupplierMapper.selectErpSupplierById(id);
    }

    /**
     * 查询供应商管理列表
     *
     * @param erpSupplier 供应商管理
     * @return 供应商管理
     */
    @Override
    public List<ErpSupplier> selectErpSupplierList(ErpSupplier erpSupplier)
    {
        return erpSupplierMapper.selectErpSupplierList(erpSupplier);
    }

    /*首页供应商数量统计*/
    @Override
    public int selectSupplierNum() {
        return erpSupplierMapper.selectSupplierNum();
    }

    /**
     * 新增供应商管理
     *
     * @param erpSupplier 供应商管理
     * @return 结果
     */
    @Override
    public int insertErpSupplier(ErpSupplier erpSupplier)
    {
        erpSupplier.setCreatTime(DateUtils.getNowDate());
        LoginUser loginUser = getLoginUser();
        erpSupplier.setCreatUser(loginUser.getUserid());
        return erpSupplierMapper.insertErpSupplier(erpSupplier);
    }

    /**
     * 修改供应商管理
     *
     * @param erpSupplier 供应商管理
     * @return 结果
     */
    @Override
    public int updateErpSupplier(ErpSupplier erpSupplier)
    {
        erpSupplier.setUpdateTime(DateUtils.getNowDate());
        return erpSupplierMapper.updateErpSupplier(erpSupplier);
    }

    /**
     * 批量删除供应商管理
     *
     * @param ids 需要删除的供应商管理主键
     * @return 结果
     */
    @Override
    public int deleteErpSupplierByIds(Long[] ids)
    {
        return erpSupplierMapper.deleteErpSupplierByIds(ids);
    }

    /**
     * 删除供应商管理信息
     *
     * @param id 供应商管理主键
     * @return 结果
     */
    @Override
    public int deleteErpSupplierById(Long id)
    {
        return erpSupplierMapper.deleteErpSupplierById(id);
    }

    @Override
    public Long selectSupplierAssociation(Long id) {
        return erpSupplierMapper.selectSupplierAssociation(id);
    }
}
