package com.ruoyi.system.service;

import com.ruoyi.system.domain.ErpGoods;

import java.util.List;
import java.util.Map;

/**
 * 商品信息Service接口
 *
 * @author 刘策
 * @date 2022-10-26
 */
public interface IErpGoodsService
{
    /**
     * 查询商品信息
     *
     * @param id 商品信息主键
     * @return 商品信息
     */
    public ErpGoods selectErpGoodsById(Long id);

    /**
     * 查询商品信息列表
     *
     * @param erpGoods 商品信息
     * @return 商品信息集合
     */
    public List<ErpGoods> selectErpGoodsList(ErpGoods erpGoods);

    /**
     * 查询阈值商品信息列表
     *
     * @param erpGoods 商品信息
     * @return 商品信息集合
     */
    public List<ErpGoods> selectThresholdGoodsList(ErpGoods erpGoods);

    /*首页商品数量统计*/
    public int selectGoodsNum();
    /*首页阈值商品数量统计*/
    public int selectThresholdNum();

    /*首页饼状图商品品牌名称*/
    public List<String> selectBrandName(Map map);

    /*首页饼状图商品品牌数量*/
    public List<Integer> selectBrandCount(Map map);

    /**
     * 查询商品仓库库存
     * @param erpGoods 商品信息
     *  @return 商品信息集合
     *  */
    public List<ErpGoods> selectErpGoodsWarehouseList(ErpGoods erpGoods);


    // 查询商品信息列表采购订单专用
    public List<ErpGoods> listGoodsPurchase(ErpGoods erpGoods);

    /**
     * 新增商品信息
     *
     * @param erpGoods 商品信息
     * @return 结果
     */
    public int insertErpGoods(ErpGoods erpGoods);

    /**
     * 修改商品信息
     *
     * @param erpGoods 商品信息
     * @return 结果
     */
    public int updateErpGoods(ErpGoods erpGoods);

    /*修改商品信息(采购入库按钮专用)*/
    public int updateErpGoodsWarehousing(ErpGoods erpGoods);

    /**
     * 批量删除商品信息
     *
     * @param ids 需要删除的商品信息主键集合
     * @return 结果
     */
    public int deleteErpGoodsByIds(Long[] ids);

    /**
     * 删除商品信息信息
     *
     * @param id 商品信息主键
     * @return 结果
     */
    public int deleteErpGoodsById(Long id);

    /**
     * 查询商品关联关系
     *
     * @param id 商品id
     * @return 结果
     * */
    Long selectGoodAssociation(Long id );
}
