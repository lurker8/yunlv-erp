package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.ErpAllocation;

/**
 * 库存调拨Service接口
 * 
 * @author licue
 * @date 2022-11-17
 */
public interface IErpAllocationService 
{
    /**
     * 查询库存调拨
     * 
     * @param id 库存调拨主键
     * @return 库存调拨
     */
    public ErpAllocation selectErpAllocationById(Long id);

    /**
     * 查询库存调拨列表
     * 
     * @param erpAllocation 库存调拨
     * @return 库存调拨集合
     */
    public List<ErpAllocation> selectErpAllocationList(ErpAllocation erpAllocation);

    /**
     * 新增库存调拨
     * 
     * @param erpAllocation 库存调拨
     * @return 结果
     */
    public int insertErpAllocation(ErpAllocation erpAllocation);

    /**
     * 修改库存调拨
     * 
     * @param erpAllocation 库存调拨
     * @return 结果
     */
    public int updateErpAllocation(ErpAllocation erpAllocation);

    /**
     * 修改库存调拨状态
     *
     * @param allocationId 库存调拨id
     * @param type 库存调拨状态
     * @return 结果
     */
    public int updateAllocationType(Long allocationId,Long type);

    /**
     * 批量删除库存调拨
     * 
     * @param ids 需要删除的库存调拨主键集合
     * @return 结果
     */
    public int deleteErpAllocationByIds(Long[] ids);

    /**
     * 删除库存调拨信息
     * 
     * @param id 库存调拨主键
     * @return 结果
     */
    public int deleteErpAllocationById(Long id);
}
