package com.ruoyi.system.controller;

import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.api.model.LoginUser;
import com.ruoyi.system.domain.ErpPurchase;
import com.ruoyi.system.service.IErpPurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static com.ruoyi.common.security.utils.SecurityUtils.getLoginUser;

/**
 * 采购订单Controller
 *
 * @author ruoyi
 * @date 2022-10-27
 */
@RestController
@RequestMapping("/purchase")
public class ErpPurchaseController extends BaseController
{
    @Autowired
    private IErpPurchaseService erpPurchaseService;

    /**
     * 查询采购订单列表
     */
    @RequiresPermissions("system:purchase:list")
    @GetMapping("/list")
    public TableDataInfo list(ErpPurchase erpPurchase)
    {
        startPage();
        List<ErpPurchase> list = erpPurchaseService.selectErpPurchaseList(erpPurchase);
        return getDataTable(list);
    }

    /**
     * 导出采购订单列表
     */
    @RequiresPermissions("system:purchase:export")
    @Log(title = "采购订单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ErpPurchase erpPurchase)
    {
        List<ErpPurchase> list = erpPurchaseService.selectErpPurchaseList(erpPurchase);
        ExcelUtil<ErpPurchase> util = new ExcelUtil<ErpPurchase>(ErpPurchase.class);
        util.exportExcel(response, list, "采购订单数据");
    }

    /**
     * 获取采购订单详细信息
     */
    @RequiresPermissions("system:purchase:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(erpPurchaseService.selectErpPurchaseById(id));
    }

    /**
     * 新增采购订单
     */
    @RequiresPermissions("system:purchase:add")
    @Log(title = "采购订单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ErpPurchase erpPurchase)
    {
        LoginUser loginUser = getLoginUser();
        erpPurchase.setCreateUser(loginUser.getUsername());
        return toAjax(erpPurchaseService.insertErpPurchase(erpPurchase));
    }

    /**
     * 修改采购订单
     */
    @RequiresPermissions("system:purchase:edit")
    @Log(title = "采购订单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ErpPurchase erpPurchase)
    {
        return toAjax(erpPurchaseService.updateErpPurchase(erpPurchase));
    }

    /**
     * 删除采购订单
     */
    @RequiresPermissions("system:purchase:remove")
    @Log(title = "采购订单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(erpPurchaseService.deleteErpPurchaseByIds(ids));
    }
    /*采购入库修改专用*/
    @PostMapping("/purchaseReceipt")
    public AjaxResult purchaseReceipt(@RequestBody ErpPurchase erpPurchase)
    {
        return toAjax(erpPurchaseService.updateErpPurchaseState(erpPurchase));
    }
    /**
     * 审核上下架
     */
    @PostMapping("/editFlag")
    public AjaxResult editFlag(@RequestBody ErpPurchase params)
    {
        //这里用PaymentType字段作为type使用
        if (params.getProcessType()==null || params.getId()==null) {
            return toAjax(0);
        }
        ErpPurchase erpPurchase = new ErpPurchase();
        erpPurchase.setId(params.getId());
        //审核通过
        if (params.getProcessType()==0) {
            erpPurchase.setProcessType(0L);
        }
        //取消审核
        if (params.getProcessType()==1) {
            erpPurchase.setProcessType(1L);
        }
        return toAjax(erpPurchaseService.updateErpPurchaseState(erpPurchase));
    }
}
