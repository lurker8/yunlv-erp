package com.ruoyi.system.domain;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author liuce
 * 新增销售订单商品关联表
 */
public class SaleGoods {
    private Long id;
    private String name;
    private Long quantity;
    private BigDecimal price;
    private BigDecimal oneGoodPrice;
    private String companyName;
    private String categoryName;
    private String brandName;
    private BigDecimal cost;
    private Long saleId;

    private List<ErpGoodsSaleWarehouse> goodWarehouseList;

    public List<ErpGoodsSaleWarehouse> getGoodWarehouseList() {
        return goodWarehouseList;
    }

    public void setGoodWarehouseList(List<ErpGoodsSaleWarehouse> goodWarehouseList) {
        this.goodWarehouseList = goodWarehouseList;
    }

    public Long getSaleId() {
        return saleId;
    }

    public void setSaleId(Long saleId) {
        this.saleId = saleId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getOneGoodPrice() {
        return oneGoodPrice;
    }

    public void setOneGoodPrice(BigDecimal oneGoodPrice) {
        this.oneGoodPrice = oneGoodPrice;
    }
}
