package com.ruoyi.system.controller;

import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.domain.ErpGoodLabel;
import com.ruoyi.system.service.IErpGoodLabelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 商品标签Controller
 *
 * @author ruoyi
 * @date 2022-10-27
 */
@RestController
@RequestMapping("/label")
public class ErpGoodLabelController extends BaseController
{
    @Autowired
    private IErpGoodLabelService erpGoodLabelService;

    /**
     * 查询商品标签列表
     */
    @RequiresPermissions("system:label:list")
    @GetMapping("/list")
    public TableDataInfo list(ErpGoodLabel erpGoodLabel)
    {
        startPage();
        List<ErpGoodLabel> list = erpGoodLabelService.selectErpGoodLabelList(erpGoodLabel);
        return getDataTable(list);
    }

    /**
     * 导出商品标签列表
     */
    @RequiresPermissions("system:label:export")
    @Log(title = "商品标签", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ErpGoodLabel erpGoodLabel)
    {
        List<ErpGoodLabel> list = erpGoodLabelService.selectErpGoodLabelList(erpGoodLabel);
        ExcelUtil<ErpGoodLabel> util = new ExcelUtil<ErpGoodLabel>(ErpGoodLabel.class);
        util.exportExcel(response, list, "商品标签数据");
    }

    /**
     * 获取商品标签详细信息
     */
    @RequiresPermissions("system:label:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return AjaxResult.success(erpGoodLabelService.selectErpGoodLabelById(id));
    }

    /**
     * 新增商品标签
     */
    @RequiresPermissions("system:label:add")
    @Log(title = "商品标签", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ErpGoodLabel erpGoodLabel)
    {
        String labelKey = erpGoodLabel.getLabelKey();
        ErpGoodLabel erpGoodLabel1 = new ErpGoodLabel();
        erpGoodLabel1.setLabelKey(labelKey);
        List<ErpGoodLabel> erpGoodLabels = erpGoodLabelService.selectErpGoodLabelList(erpGoodLabel1);
        if (erpGoodLabels.size() != 0 ){
            return AjaxResult.error("请保证唯一标识唯一!");
        }
        return toAjax(erpGoodLabelService.insertErpGoodLabel(erpGoodLabel));
    }

    /**
     * 修改商品标签
     */
    @RequiresPermissions("system:label:edit")
    @Log(title = "商品标签", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ErpGoodLabel erpGoodLabel)
    {
        return toAjax(erpGoodLabelService.updateErpGoodLabel(erpGoodLabel));
    }

    /**
     * 删除商品标签
     */
    @RequiresPermissions("system:label:remove")
    @Log(title = "商品标签", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        for (Integer id : ids) {
            ErpGoodLabel erpGoodLabel = erpGoodLabelService.selectErpGoodLabelById(id);
            String labelKey = erpGoodLabel.getLabelKey();
            Long aLong = erpGoodLabelService.selectGoodLabelAssociation(labelKey);
            if (aLong != null){
                return AjaxResult.error("标签已分配禁止删除");
            }
        }
        return toAjax(erpGoodLabelService.deleteErpGoodLabelByIds(ids));
    }
}
