package com.ruoyi.system.controller;

import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.domain.ErpSalesInvoice;
import com.ruoyi.system.service.IErpSalesInvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 销售发货单Controller
 *
 * @author liuce
 * @date 2022-11-07
 */
@RestController
@RequestMapping("/invoice")
public class ErpSalesInvoiceController extends BaseController
{
    @Autowired
    private IErpSalesInvoiceService erpSalesInvoiceService;

    /**
     * 查询销售发货单列表
     */
    @RequiresPermissions("system:invoice:list")
    @GetMapping("/list")
    public TableDataInfo list(ErpSalesInvoice erpSalesInvoice)
    {
        startPage();
        List<ErpSalesInvoice> list = erpSalesInvoiceService.selectErpSalesInvoiceList(erpSalesInvoice);
        return getDataTable(list);
    }

    /**
     * 导出销售发货单列表
     */
    @RequiresPermissions("system:invoice:export")
    @Log(title = "销售发货单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ErpSalesInvoice erpSalesInvoice)
    {
        List<ErpSalesInvoice> list = erpSalesInvoiceService.selectErpSalesInvoiceList(erpSalesInvoice);
        ExcelUtil<ErpSalesInvoice> util = new ExcelUtil<ErpSalesInvoice>(ErpSalesInvoice.class);
        util.exportExcel(response, list, "销售发货单数据");
    }

    /**
     * 获取销售发货单详细信息
     */
    @RequiresPermissions("system:invoice:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(erpSalesInvoiceService.selectErpSalesInvoiceById(id));
    }

    /**
     * 新增销售发货单
     */
    @RequiresPermissions("system:invoice:add")
    @Log(title = "销售发货单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ErpSalesInvoice erpSalesInvoice)
    {
        return toAjax(erpSalesInvoiceService.insertErpSalesInvoice(erpSalesInvoice));
    }

    /**
     * 修改销售发货单
     */
    @RequiresPermissions("system:invoice:edit")
    @Log(title = "销售发货单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ErpSalesInvoice erpSalesInvoice)
    {
        return toAjax(erpSalesInvoiceService.updateErpSalesInvoice(erpSalesInvoice));
    }

    /**
     * 删除销售发货单
     */
    @RequiresPermissions("system:invoice:remove")
    @Log(title = "销售发货单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(erpSalesInvoiceService.deleteErpSalesInvoiceByIds(ids));
    }
}
