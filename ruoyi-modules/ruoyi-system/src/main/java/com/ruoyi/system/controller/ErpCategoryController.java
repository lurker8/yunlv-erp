package com.ruoyi.system.controller;

import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.domain.ErpCategory;
import com.ruoyi.system.service.IErpCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 分类Controller
 *
 * @author 刘策
 * @date 2022-10-27
 */
@RestController
@RequestMapping("/category")
public class ErpCategoryController extends BaseController
{
    @Autowired
    private IErpCategoryService erpCategoryService;

    /**
     * 查询分类列表
     */
    @RequiresPermissions("system:category:list")
    @GetMapping("/list")
    public AjaxResult list(ErpCategory erpCategory)
    {
        List<ErpCategory> list = erpCategoryService.selectErpCategoryList(erpCategory);
        return AjaxResult.success(list);
    }

    /**
     * 导出分类列表
     */
    @RequiresPermissions("system:category:export")
    @Log(title = "分类", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ErpCategory erpCategory)
    {
        List<ErpCategory> list = erpCategoryService.selectErpCategoryList(erpCategory);
        ExcelUtil<ErpCategory> util = new ExcelUtil<ErpCategory>(ErpCategory.class);
        util.exportExcel(response, list, "分类数据");
    }

    /**
     * 获取分类详细信息
     */
    @RequiresPermissions("system:category:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return AjaxResult.success(erpCategoryService.selectErpCategoryById(id));
    }

    /**
     * 新增分类
     */
    @RequiresPermissions("system:category:add")
    @Log(title = "分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ErpCategory erpCategory)
    {
        return toAjax(erpCategoryService.insertErpCategory(erpCategory));
    }

    /**
     * 修改分类
     */
    @RequiresPermissions("system:category:edit")
    @Log(title = "分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ErpCategory erpCategory)
    {
        return toAjax(erpCategoryService.updateErpCategory(erpCategory));
    }

    /**
     * 删除分类
     */
    @RequiresPermissions("system:category:remove")
    @Log(title = "分类", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        Integer id = ids[0];
        ErpCategory erpCategory = erpCategoryService.selectErpCategoryById(id);
        Long parentId = erpCategory.getParentId();
        if (parentId == 0){
            ErpCategory erpCategory1 = new ErpCategory();
            erpCategory1.setParentId(id.longValue());
            List<ErpCategory> erpCategories = erpCategoryService.selectErpCategoryList(erpCategory1);
            if (erpCategories != null){
              return AjaxResult.error("检测到该分类下有二级分类不能删除");
            }
        }
        Long aLong = erpCategoryService.selectCategoryAssociation(id.longValue());
        if (aLong != null){
            return AjaxResult.error("分类已分配禁止删除!");
        }
        return toAjax(erpCategoryService.deleteErpCategoryByIds(ids));
    }
}
