package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.ErpReceiptDoc;

/**
 * 入库单Service接口
 * 
 * @author ruoyi
 * @date 2022-11-17
 */
public interface IErpReceiptDocService 
{
    /**
     * 查询入库单
     * 
     * @param id 入库单主键
     * @return 入库单
     */
    public ErpReceiptDoc selectErpReceiptDocById(Long id);

    /**
     * 查询入库单列表
     * 
     * @param erpReceiptDoc 入库单
     * @return 入库单集合
     */
    public List<ErpReceiptDoc> selectErpReceiptDocList(ErpReceiptDoc erpReceiptDoc);

    /**
     * 新增入库单
     * 
     * @param erpReceiptDoc 入库单
     * @return 结果
     */
    public int insertErpReceiptDoc(ErpReceiptDoc erpReceiptDoc);

    /**
     * 修改入库单
     * 
     * @param erpReceiptDoc 入库单
     * @return 结果
     */
    public int updateErpReceiptDoc(ErpReceiptDoc erpReceiptDoc);

    /**
     * 批量删除入库单
     * 
     * @param ids 需要删除的入库单主键集合
     * @return 结果
     */
    public int deleteErpReceiptDocByIds(Long[] ids);

    /**
     * 删除入库单信息
     * 
     * @param id 入库单主键
     * @return 结果
     */
    public int deleteErpReceiptDocById(Long id);
}
