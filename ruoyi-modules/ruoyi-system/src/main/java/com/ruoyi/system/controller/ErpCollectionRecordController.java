package com.ruoyi.system.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.domain.ErpCollectionRecord;
import com.ruoyi.system.service.IErpCollectionRecordService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 收款记录Controller
 * 
 * @author 刘策
 * @date 2022-11-16
 */
@RestController
@RequestMapping("/record")
public class ErpCollectionRecordController extends BaseController
{
    @Autowired
    private IErpCollectionRecordService erpCollectionRecordService;

    /**
     * 查询收款记录列表
     */
    @RequiresPermissions("system:record:list")
    @GetMapping("/list")
    public TableDataInfo list(ErpCollectionRecord erpCollectionRecord)
    {
        startPage();
        List<ErpCollectionRecord> list = erpCollectionRecordService.selectErpCollectionRecordList(erpCollectionRecord);
        return getDataTable(list);
    }

    /**
     * 导出收款记录列表
     */
    @RequiresPermissions("system:record:export")
    @Log(title = "收款记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ErpCollectionRecord erpCollectionRecord)
    {
        List<ErpCollectionRecord> list = erpCollectionRecordService.selectErpCollectionRecordList(erpCollectionRecord);
        ExcelUtil<ErpCollectionRecord> util = new ExcelUtil<ErpCollectionRecord>(ErpCollectionRecord.class);
        util.exportExcel(response, list, "收款记录数据");
    }

    /**
     * 获取收款记录详细信息
     */
    @RequiresPermissions("system:record:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(erpCollectionRecordService.selectErpCollectionRecordById(id));
    }

    /**
     * 新增收款记录
     */
    @RequiresPermissions("system:record:add")
    @Log(title = "收款记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ErpCollectionRecord erpCollectionRecord)
    {
        return toAjax(erpCollectionRecordService.insertErpCollectionRecord(erpCollectionRecord));
    }

    /**
     * 修改收款记录
     */
    @RequiresPermissions("system:record:edit")
    @Log(title = "收款记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ErpCollectionRecord erpCollectionRecord)
    {
        return toAjax(erpCollectionRecordService.updateErpCollectionRecord(erpCollectionRecord));
    }

    /**
     * 删除收款记录
     */
    @RequiresPermissions("system:record:remove")
    @Log(title = "收款记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(erpCollectionRecordService.deleteErpCollectionRecordByIds(ids));
    }
}
