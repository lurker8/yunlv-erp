package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.system.api.RemoteUserService;
import com.ruoyi.system.api.model.LoginUser;
import com.ruoyi.system.domain.ErpCustomer;
import com.ruoyi.system.mapper.ErpCustomerMapper;
import com.ruoyi.system.service.IErpCustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.ruoyi.common.security.utils.SecurityUtils.getLoginUser;

/**
 * 客户管理Service业务层处理
 *
 * @author 刘策
 * @date 2022-10-24
 */
@Service
public class ErpCustomerServiceImpl implements IErpCustomerService
{
    @Autowired
    private ErpCustomerMapper erpCustomerMapper;

    @Autowired
    private RemoteUserService remoteUserService;

    /**
     * 查询客户管理
     *
     * @param id 客户管理主键
     * @return 客户管理
     */
    @Override
    public ErpCustomer selectErpCustomerById(Long id)
    {
        return erpCustomerMapper.selectErpCustomerById(id);
    }

    /**
     * 查询客户管理列表
     *
     * @param erpCustomer 客户管理
     * @return 客户管理
     */
    @Override
    public List<ErpCustomer> selectErpCustomerList(ErpCustomer erpCustomer)
    {
        return erpCustomerMapper.selectErpCustomerList(erpCustomer);
    }

    /**
     * 新增客户管理
     *
     * @param erpCustomer 客户管理
     * @return 结果
     */
    @Override
    public int insertErpCustomer(ErpCustomer erpCustomer)
    {
        erpCustomer.setCreateTime(DateUtils.getNowDate());
        LoginUser loginUser = getLoginUser();
        erpCustomer.setCreateUser(loginUser.getUserid());
        return erpCustomerMapper.insertErpCustomer(erpCustomer);
    }

    /*首页客户数量统计*/
    @Override
    public int selectCustomerNum() {
        return erpCustomerMapper.selectCustomerNum();
    }

    /**
     * 修改客户管理
     *
     * @param erpCustomer 客户管理
     * @return 结果
     */
    @Override
    public int updateErpCustomer(ErpCustomer erpCustomer)
    {
        erpCustomer.setUpdateTime(DateUtils.getNowDate());
        return erpCustomerMapper.updateErpCustomer(erpCustomer);
    }

    /**
     * 批量删除客户管理
     *
     * @param ids 需要删除的客户管理主键
     * @return 结果
     */
    @Override
    public int deleteErpCustomerByIds(Long[] ids)
    {
        return erpCustomerMapper.deleteErpCustomerByIds(ids);
    }

    /**
     * 删除客户管理信息
     *
     * @param id 客户管理主键
     * @return 结果
     */
    @Override
    public int deleteErpCustomerById(Long id)
    {
        return erpCustomerMapper.deleteErpCustomerById(id);
    }

    @Override
    public Long selectCustomerAssociation(Long id) {
        return erpCustomerMapper.selectCustomerAssociation(id);
    }
}
