package com.ruoyi.system.service;

import com.ruoyi.system.domain.ErpCategory;

import java.util.List;

/**
 * 分类Service接口
 *
 * @author 刘策
 * @date 2022-10-27
 */
public interface IErpCategoryService
{
    /**
     * 查询分类
     *
     * @param id 分类主键
     * @return 分类
     */
    public ErpCategory selectErpCategoryById(Integer id);

    /**
     * 查询分类列表
     *
     * @param erpCategory 分类
     * @return 分类集合
     */
    public List<ErpCategory> selectErpCategoryList(ErpCategory erpCategory);

    /**
     * 新增分类
     *
     * @param erpCategory 分类
     * @return 结果
     */
    public int insertErpCategory(ErpCategory erpCategory);

    /**
     * 修改分类
     *
     * @param erpCategory 分类
     * @return 结果
     */
    public int updateErpCategory(ErpCategory erpCategory);

    /**
     * 批量删除分类
     *
     * @param ids 需要删除的分类主键集合
     * @return 结果
     */
    public int deleteErpCategoryByIds(Integer[] ids);

    /**
     * 删除分类信息
     *
     * @param id 分类主键
     * @return 结果
     */
    public int deleteErpCategoryById(Integer id);

    /** 查询分类信息关联
     *
     * @param id 分类id
     * @return 结果
     * */
    Long selectCategoryAssociation(Long id);
}
