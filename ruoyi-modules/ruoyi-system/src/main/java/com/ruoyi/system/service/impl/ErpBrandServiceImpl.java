package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.system.api.model.LoginUser;
import com.ruoyi.system.domain.ErpBrand;
import com.ruoyi.system.mapper.ErpBrandMapper;
import com.ruoyi.system.service.IErpBrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.ruoyi.common.security.utils.SecurityUtils.getLoginUser;

/**
 * 品牌管理Service业务层处理
 *
 * @author 刘策
 * @date 2022-10-26
 */
@Service
public class ErpBrandServiceImpl implements IErpBrandService
{
    @Autowired
    private ErpBrandMapper erpBrandMapper;

    /**
     * 查询品牌管理
     *
     * @param id 品牌管理主键
     * @return 品牌管理
     */
    @Override
    public ErpBrand selectErpBrandById(Long id)
    {
        return erpBrandMapper.selectErpBrandById(id);
    }

    /**
     * 查询品牌管理列表
     *
     * @param erpBrand 品牌管理
     * @return 品牌管理
     */
    @Override
    public List<ErpBrand> selectErpBrandList(ErpBrand erpBrand)
    {
        return erpBrandMapper.selectErpBrandList(erpBrand);
    }

    /**
     * 新增品牌管理
     *
     * @param erpBrand 品牌管理
     * @return 结果
     */
    @Override
    public int insertErpBrand(ErpBrand erpBrand)
    {
        erpBrand.setCreateTime(DateUtils.getNowDate());
        LoginUser loginUser = getLoginUser();
        erpBrand.setCreateUser(loginUser.getUsername());
        return erpBrandMapper.insertErpBrand(erpBrand);
    }

    /**
     * 修改品牌管理
     *
     * @param erpBrand 品牌管理
     * @return 结果
     */
    @Override
    public int updateErpBrand(ErpBrand erpBrand)
    {
        erpBrand.setUpdateTime(DateUtils.getNowDate());
        return erpBrandMapper.updateErpBrand(erpBrand);
    }

    /**
     * 批量删除品牌管理
     *
     * @param ids 需要删除的品牌管理主键
     * @return 结果
     */
    @Override
    public int deleteErpBrandByIds(Long[] ids)
    {
        return erpBrandMapper.deleteErpBrandByIds(ids);
    }

    /**
     * 删除品牌管理信息
     *
     * @param id 品牌管理主键
     * @return 结果
     */
    @Override
    public int deleteErpBrandById(Long id)
    {
        return erpBrandMapper.deleteErpBrandById(id);
    }

    @Override
    public Long selectBrandAssociation(Long id) {
        return erpBrandMapper.selectBrandAssociation(id);
    }
}
