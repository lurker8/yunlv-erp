package com.ruoyi.system.service;

import com.ruoyi.system.domain.ErpPurchase;

import java.util.List;
import java.util.Map;

/**
 * 采购订单Service接口
 *
 * @author ruoyi
 * @date 2022-10-27
 */
public interface IErpPurchaseService
{
    /**
     * 查询采购订单
     *
     * @param id 采购订单主键
     * @return 采购订单
     */
    public ErpPurchase selectErpPurchaseById(Long id);

    /**
     * 查询采购订单列表
     *
     * @param erpPurchase 采购订单
     * @return 采购订单集合
     */
    public List<ErpPurchase> selectErpPurchaseList(ErpPurchase erpPurchase);

    /*首页折线图日期数据*/
    public List<String> selectxDate(Map map);
    /*首页折线图采购单数*/
    public List<Integer> selectPurchaseCount(Map map);

    /**
     * 新增采购订单
     *
     * @param erpPurchase 采购订单
     * @return 结果
     */
    public int insertErpPurchase(ErpPurchase erpPurchase);

    /**
     * 修改采购订单
     *
     * @param erpPurchase 采购订单
     * @return 结果
     */
    public int updateErpPurchase(ErpPurchase erpPurchase);
    public int updateErpPurchaseState(ErpPurchase erpPurchase);

    /**
     * 批量删除采购订单
     *
     * @param ids 需要删除的采购订单主键集合
     * @return 结果
     */
    public int deleteErpPurchaseByIds(Long[] ids);

    /**
     * 删除采购订单信息
     *
     * @param id 采购订单主键
     * @return 结果
     */
    public int deleteErpPurchaseById(Long id);
}
