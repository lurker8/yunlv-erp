package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.ErpPurchaseReturn;

/**
 * 采购退货Service接口
 * 
 * @author ruoyi
 * @date 2022-11-16
 */
public interface IErpPurchaseReturnService 
{
    /**
     * 查询采购退货
     * 
     * @param id 采购退货主键
     * @return 采购退货
     */
    public ErpPurchaseReturn selectErpPurchaseReturnById(Long id);

    /**
     * 查询采购退货列表
     * 
     * @param erpPurchaseReturn 采购退货
     * @return 采购退货集合
     */
    public List<ErpPurchaseReturn> selectErpPurchaseReturnList(ErpPurchaseReturn erpPurchaseReturn);

    /**
     * 新增采购退货
     * 
     * @param erpPurchaseReturn 采购退货
     * @return 结果
     */
    public int insertErpPurchaseReturn(ErpPurchaseReturn erpPurchaseReturn);

    /**
     * 修改采购退货
     * 
     * @param erpPurchaseReturn 采购退货
     * @return 结果
     */
    public int updateErpPurchaseReturn(ErpPurchaseReturn erpPurchaseReturn);

    /**
     * 批量删除采购退货
     * 
     * @param ids 需要删除的采购退货主键集合
     * @return 结果
     */
    public int deleteErpPurchaseReturnByIds(Long[] ids);

    /**
     * 删除采购退货信息
     * 
     * @param id 采购退货主键
     * @return 结果
     */
    public int deleteErpPurchaseReturnById(Long id);
}
