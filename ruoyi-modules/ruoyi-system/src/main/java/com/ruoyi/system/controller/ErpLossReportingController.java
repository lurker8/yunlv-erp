package com.ruoyi.system.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.domain.ErpLossReporting;
import com.ruoyi.system.service.IErpLossReportingService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 报损单Controller
 * 
 * @author liuce
 * @date 2022-11-21
 */
@RestController
@RequestMapping("/reporting")
public class ErpLossReportingController extends BaseController
{
    @Autowired
    private IErpLossReportingService erpLossReportingService;

    /**
     * 查询报损单列表
     */
    @RequiresPermissions("system:reporting:list")
    @GetMapping("/list")
    public TableDataInfo list(ErpLossReporting erpLossReporting)
    {
        startPage();
        List<ErpLossReporting> list = erpLossReportingService.selectErpLossReportingList(erpLossReporting);
        return getDataTable(list);
    }

    /**
     * 导出报损单列表
     */
    @RequiresPermissions("system:reporting:export")
    @Log(title = "报损单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ErpLossReporting erpLossReporting)
    {
        List<ErpLossReporting> list = erpLossReportingService.selectErpLossReportingList(erpLossReporting);
        ExcelUtil<ErpLossReporting> util = new ExcelUtil<ErpLossReporting>(ErpLossReporting.class);
        util.exportExcel(response, list, "报损单数据");
    }

    /**
     * 获取报损单详细信息
     */
    @RequiresPermissions("system:reporting:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(erpLossReportingService.selectErpLossReportingById(id));
    }

    /**
     * 新增报损单
     */
    @RequiresPermissions("system:reporting:add")
    @Log(title = "报损单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ErpLossReporting erpLossReporting)
    {
        return toAjax(erpLossReportingService.insertErpLossReporting(erpLossReporting));
    }

    /**
     * 修改报损单
     */
    @RequiresPermissions("system:reporting:edit")
    @Log(title = "报损单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ErpLossReporting erpLossReporting)
    {
        return toAjax(erpLossReportingService.updateErpLossReporting(erpLossReporting));
    }

    @PutMapping("/confirmReporting")
    public AjaxResult confirmReporting(@RequestBody ErpLossReporting erpLossReporting)
    {
        return toAjax(erpLossReportingService.confirmReporting(erpLossReporting));
    }
    /**
     * 删除报损单
     */
    @RequiresPermissions("system:reporting:remove")
    @Log(title = "报损单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(erpLossReportingService.deleteErpLossReportingByIds(ids));
    }
}
