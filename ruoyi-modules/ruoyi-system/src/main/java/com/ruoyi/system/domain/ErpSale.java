package com.ruoyi.system.domain;

import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.List;

/**
 * 销售订单对象 erp_sale
 *
 * @author 刘策
 * @date 2022-10-28
 */
public class ErpSale extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 销售单号 */
    @Excel(name = "销售单号")
    private String saleNumber;

    /** 客户id */
    private Long customerId;

    /** 客户name */
    @Excel(name = "客户")
    private String customerName;

    /** 收款方式id */
    @Excel(name = "收款方式id")
    private Long paymentMethod;

    /** 备注 */
    @Excel(name = "备注")
    private String customerContact;

    /** 客户手机 */
    @Excel(name = "客户手机")
    private String customerPhone;

    /** 固定电话 */
    @Excel(name = "固定电话")
    private String fixedTelephone;

    /** 客户地址 */
    @Excel(name = "客户地址")
    private String customerAddress;

    /** 省 */
    @Excel(name = "省")
    private Long province;

    /** 市 */
    @Excel(name = "市")
    private Long city;

    /** 区 */
    @Excel(name = "区")
    private Long area;

    /** 详细地址 */
    @Excel(name = "详细地址")
    private String detailedAddress;

    /** 创建人 */
    @Excel(name = "创建人")
    private String createUser;

    /** 销售总额 */
    @Excel(name = "销售总额")
    private BigDecimal saleBonus;

    /** 状态(0已确认1未确认) */
    @Excel(name = "状态(0已确认1未确认)")
    private Long type;

    /** 是否退货（0有退货 1 未退货） */
    @Excel(name = "是否退货", readConverterExp = "0=有退货,1=,未=退货")
    private Long returnGoods;

    /** 是否发货（0已发货，1确认收货） */
    @Excel(name = "是否发货", readConverterExp = "0=已发货，1确认收货")
    private Long deliverGoods;

    private List<SaleGoods> goodsList;

    /** 税金 */
    @Excel(name = "税金")
    private BigDecimal taxes;

    /** 发货单号 */
    private String shipmentNum;

    public String getShipmentNum() {
        return shipmentNum;
    }

    public void setShipmentNum(String shipmentNum) {
        this.shipmentNum = shipmentNum;
    }

    public List<SaleGoods> getGoodsList() {
        return goodsList;
    }

    public void setGoodsList(List<SaleGoods> goodsList) {
        this.goodsList = goodsList;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setSaleNumber(String saleNumber)
    {
        this.saleNumber = saleNumber;
    }

    public String getSaleNumber()
    {
        return saleNumber;
    }
    public void setCustomerId(Long customerId)
    {
        this.customerId = customerId;
    }

    public Long getCustomerId()
    {
        return customerId;
    }
    public void setPaymentMethod(Long paymentMethod)
    {
        this.paymentMethod = paymentMethod;
    }

    public Long getPaymentMethod()
    {
        return paymentMethod;
    }
    public void setCustomerContact(String customerContact)
    {
        this.customerContact = customerContact;
    }

    public String getCustomerContact()
    {
        return customerContact;
    }
    public void setCustomerPhone(String customerPhone)
    {
        this.customerPhone = customerPhone;
    }

    public String getCustomerPhone()
    {
        return customerPhone;
    }
    public void setFixedTelephone(String fixedTelephone)
    {
        this.fixedTelephone = fixedTelephone;
    }

    public String getFixedTelephone()
    {
        return fixedTelephone;
    }
    public void setCustomerAddress(String customerAddress)
    {
        this.customerAddress = customerAddress;
    }

    public String getCustomerAddress()
    {
        return customerAddress;
    }
    public void setProvince(Long province)
    {
        this.province = province;
    }

    public Long getProvince()
    {
        return province;
    }
    public void setCity(Long city)
    {
        this.city = city;
    }

    public Long getCity()
    {
        return city;
    }
    public void setArea(Long area)
    {
        this.area = area;
    }

    public Long getArea()
    {
        return area;
    }
    public void setDetailedAddress(String detailedAddress)
    {
        this.detailedAddress = detailedAddress;
    }

    public String getDetailedAddress()
    {
        return detailedAddress;
    }
    public void setCreateUser(String createUser)
    {
        this.createUser = createUser;
    }

    public String getCreateUser()
    {
        return createUser;
    }
    public void setSaleBonus(BigDecimal saleBonus)
    {
        this.saleBonus = saleBonus;
    }

    public BigDecimal getSaleBonus()
    {
        return saleBonus;
    }
    public void setType(Long type)
    {
        this.type = type;
    }

    public Long getType()
    {
        return type;
    }
    public void setReturnGoods(Long returnGoods)
    {
        this.returnGoods = returnGoods;
    }

    public Long getReturnGoods()
    {
        return returnGoods;
    }
    public void setDeliverGoods(Long deliverGoods)
    {
        this.deliverGoods = deliverGoods;
    }

    public Long getDeliverGoods()
    {
        return deliverGoods;
    }
    public void setTaxes(BigDecimal taxes)
    {
        this.taxes = taxes;
    }

    public BigDecimal getTaxes()
    {
        return taxes;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("saleNumber", getSaleNumber())
            .append("customerId", getCustomerId())
            .append("paymentMethod", getPaymentMethod())
            .append("customerContact", getCustomerContact())
            .append("customerPhone", getCustomerPhone())
            .append("fixedTelephone", getFixedTelephone())
            .append("customerAddress", getCustomerAddress())
            .append("province", getProvince())
            .append("city", getCity())
            .append("area", getArea())
            .append("detailedAddress", getDetailedAddress())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createUser", getCreateUser())
            .append("saleBonus", getSaleBonus())
            .append("type", getType())
            .append("returnGoods", getReturnGoods())
            .append("deliverGoods", getDeliverGoods())
            .append("taxes", getTaxes())
            .toString();
    }
}
