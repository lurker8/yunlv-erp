package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.ErpLossReporting;

/**
 * 报损单Service接口
 * 
 * @author liuce
 * @date 2022-11-21
 */
public interface IErpLossReportingService 
{
    /**
     * 查询报损单
     * 
     * @param id 报损单主键
     * @return 报损单
     */
    public ErpLossReporting selectErpLossReportingById(Long id);

    /**
     * 查询报损单列表
     * 
     * @param erpLossReporting 报损单
     * @return 报损单集合
     */
    public List<ErpLossReporting> selectErpLossReportingList(ErpLossReporting erpLossReporting);

    /**
     * 新增报损单
     * 
     * @param erpLossReporting 报损单
     * @return 结果
     */
    public int insertErpLossReporting(ErpLossReporting erpLossReporting);

    /**
     * 修改报损单
     * 
     * @param erpLossReporting 报损单
     * @return 结果
     */
    public int updateErpLossReporting(ErpLossReporting erpLossReporting);

    /**
     * 批量删除报损单
     * 
     * @param ids 需要删除的报损单主键集合
     * @return 结果
     */
    public int deleteErpLossReportingByIds(Long[] ids);

    /**
     * 删除报损单信息
     * 
     * @param id 报损单主键
     * @return 结果
     */
    public int deleteErpLossReportingById(Long id);

    /** 确认报损单操作
     * */
    int confirmReporting(ErpLossReporting erpLossReporting);

}
