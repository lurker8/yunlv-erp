package com.ruoyi.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 收款记录对象 erp_collection_record
 * 
 * @author 刘策
 * @date 2022-11-16
 */
public class ErpCollectionRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 销售id */
    @Excel(name = "销售id")
    private Long saleId;

    /** 收款时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "收款时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date collectionTime;

    /** 收款金额 */
    @Excel(name = "收款金额")
    private BigDecimal amountCollected;

    /** 收款人 */
    @Excel(name = "收款人")
    private String payee;

    /** 收款凭证 */
    @Excel(name = "收款凭证")
    private String collectionVoucher;

    /** 收款备注 */
    @Excel(name = "收款备注")
    private String collectionRemarks;

    /** 创建人 */
    @Excel(name = "创建人")
    private String createUser;

    /** 收款账户 */
    @Excel(name = "收款账户")
    private String collectedAccount;

    private String collectedAccountName;

    /** 收款账单id*/
    private Long collectionId;

    public String getCollectedAccountName() {
        return collectedAccountName;
    }

    public void setCollectedAccountName(String collectedAccountName) {
        this.collectedAccountName = collectedAccountName;
    }

    public Long getCollectionId() {
        return collectionId;
    }

    public void setCollectionId(Long collectionId) {
        this.collectionId = collectionId;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setSaleId(Long saleId) 
    {
        this.saleId = saleId;
    }

    public Long getSaleId() 
    {
        return saleId;
    }
    public void setCollectionTime(Date collectionTime) 
    {
        this.collectionTime = collectionTime;
    }

    public Date getCollectionTime() 
    {
        return collectionTime;
    }
    public void setAmountCollected(BigDecimal amountCollected) 
    {
        this.amountCollected = amountCollected;
    }

    public BigDecimal getAmountCollected() 
    {
        return amountCollected;
    }
    public void setPayee(String payee) 
    {
        this.payee = payee;
    }

    public String getPayee() 
    {
        return payee;
    }
    public void setCollectionVoucher(String collectionVoucher) 
    {
        this.collectionVoucher = collectionVoucher;
    }

    public String getCollectionVoucher() 
    {
        return collectionVoucher;
    }
    public void setCollectionRemarks(String collectionRemarks) 
    {
        this.collectionRemarks = collectionRemarks;
    }

    public String getCollectionRemarks() 
    {
        return collectionRemarks;
    }
    public void setCreateUser(String createUser) 
    {
        this.createUser = createUser;
    }

    public String getCreateUser() 
    {
        return createUser;
    }
    public void setCollectedAccount(String collectedAccount) 
    {
        this.collectedAccount = collectedAccount;
    }

    public String getCollectedAccount() 
    {
        return collectedAccount;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("saleId", getSaleId())
            .append("collectionTime", getCollectionTime())
            .append("amountCollected", getAmountCollected())
            .append("payee", getPayee())
            .append("collectionVoucher", getCollectionVoucher())
            .append("collectionRemarks", getCollectionRemarks())
            .append("createTime", getCreateTime())
            .append("createUser", getCreateUser())
            .append("collectedAccount", getCollectedAccount())
            .toString();
    }
}
