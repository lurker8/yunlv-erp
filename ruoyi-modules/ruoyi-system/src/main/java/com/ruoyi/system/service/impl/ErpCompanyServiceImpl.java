package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.system.domain.ErpCompany;
import com.ruoyi.system.mapper.ErpCompanyMapper;
import com.ruoyi.system.service.IErpCompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * company Service业务层处理
 *
 * @author 刘策
 * @date 2022-11-01
 */
@Service
public class ErpCompanyServiceImpl implements IErpCompanyService
{
    @Autowired
    private ErpCompanyMapper erpCompanyMapper;

    /**
     * 查询company
     *
     * @param id company 主键
     * @return company
     */
    @Override
    public ErpCompany selectErpCompanyById(Long id)
    {
        return erpCompanyMapper.selectErpCompanyById(id);
    }

    /**
     * 查询company 列表
     *
     * @param erpCompany company
     * @return company
     */
    @Override
    public List<ErpCompany> selectErpCompanyList(ErpCompany erpCompany)
    {
        return erpCompanyMapper.selectErpCompanyList(erpCompany);
    }

    /**
     * 新增company
     *
     * @param erpCompany company
     * @return 结果
     */
    @Override
    public int insertErpCompany(ErpCompany erpCompany)
    {
        erpCompany.setCreateTime(DateUtils.getNowDate());
        return erpCompanyMapper.insertErpCompany(erpCompany);
    }

    /**
     * 修改company
     *
     * @param erpCompany company
     * @return 结果
     */
    @Override
    public int updateErpCompany(ErpCompany erpCompany)
    {
        erpCompany.setUpdateTime(DateUtils.getNowDate());
        return erpCompanyMapper.updateErpCompany(erpCompany);
    }

    /**
     * 批量删除company
     *
     * @param ids 需要删除的company 主键
     * @return 结果
     */
    @Override
    public int deleteErpCompanyByIds(Long[] ids)
    {
        return erpCompanyMapper.deleteErpCompanyByIds(ids);
    }

    /**
     * 删除company 信息
     *
     * @param id company 主键
     * @return 结果
     */
    @Override
    public int deleteErpCompanyById(Long id)
    {
        return erpCompanyMapper.deleteErpCompanyById(id);
    }
}
