package com.ruoyi.system.controller;

import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.api.model.LoginUser;
import com.ruoyi.system.domain.ErpPurchaseGoods;
import com.ruoyi.system.service.IErpPurchaseGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static com.ruoyi.common.security.utils.SecurityUtils.getLoginUser;

/**
 * 采购商品Controller
 *
 * @author ruoyi
 * @date 2022-10-31
 */
@RestController
@RequestMapping("/purchaseoods")
public class ErpPurchaseGoodsController extends BaseController
{
    @Autowired
    private IErpPurchaseGoodsService erpPurchaseGoodsService;

    /**
     * 查询采购商品列表
     */
    @RequiresPermissions("system:purchaseoods:list")
    @GetMapping("/list")
    public TableDataInfo list(ErpPurchaseGoods erpPurchaseGoods)
    {
        startPage();
        List<ErpPurchaseGoods> list = erpPurchaseGoodsService.selectErpPurchaseGoodsList(erpPurchaseGoods);
        return getDataTable(list);
    }

    /**
     * 导出采购商品列表
     */
    @RequiresPermissions("system:purchaseoods:export")
    @Log(title = "采购商品", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ErpPurchaseGoods erpPurchaseGoods)
    {
        List<ErpPurchaseGoods> list = erpPurchaseGoodsService.selectErpPurchaseGoodsList(erpPurchaseGoods);
        ExcelUtil<ErpPurchaseGoods> util = new ExcelUtil<ErpPurchaseGoods>(ErpPurchaseGoods.class);
        util.exportExcel(response, list, "采购商品数据");
    }

    /**
     * 获取采购商品详细信息
     */
    @RequiresPermissions("system:purchaseoods:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(erpPurchaseGoodsService.selectErpPurchaseGoodsById(id));
    }

    /*根据采购订单id获取采购商品详细信息列表*/
    @GetMapping(value = "/purchaseGoods")
    public TableDataInfo getPurchaseGoodsInfo(ErpPurchaseGoods erpPurchaseGoods)
    {
        erpPurchaseGoods.setPurchaseId(erpPurchaseGoods.getPurchaseId());
        List<ErpPurchaseGoods> list = erpPurchaseGoodsService.selectErpPurchaseGoodsPurchaseId(erpPurchaseGoods);
        return getDataTable(list);
    }

    /**
     * 新增采购商品
     */
    @RequiresPermissions("system:purchaseoods:add")
    @Log(title = "采购商品", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ErpPurchaseGoods erpPurchaseGoods)
    {
        LoginUser loginUser = getLoginUser();
        erpPurchaseGoods.setCreateUser(loginUser.getUsername());
        return toAjax(erpPurchaseGoodsService.insertErpPurchaseGoods(erpPurchaseGoods));
    }

    /**
     * 修改采购商品
     */
    @RequiresPermissions("system:purchaseoods:edit")
    @Log(title = "采购商品", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ErpPurchaseGoods erpPurchaseGoods)
    {
        return toAjax(erpPurchaseGoodsService.updateErpPurchaseGoods(erpPurchaseGoods));
    }

    /*修改采购商品数量专用*/
    @PutMapping("/updatePurchaseGoods")
    public AjaxResult updatePurchaseGoods(@RequestBody ErpPurchaseGoods erpPurchaseGoods)
    {
        return toAjax(erpPurchaseGoodsService.updateErpPurchaseGoodsQuantity(erpPurchaseGoods));
    }

    /**
     * 删除采购商品
     */
    @RequiresPermissions("system:purchaseoods:remove")
    @Log(title = "采购商品", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(erpPurchaseGoodsService.deleteErpPurchaseGoodsByIds(ids));
    }
}
