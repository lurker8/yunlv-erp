package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 报损单对象 erp_loss_reporting
 * 
 * @author liuce
 * @date 2022-11-21
 */
public class ErpLossReporting extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 报损单id */
    private Long id;

    /** 商品id */
    private Long goodId;

    /** 商品name*/
    @Excel(name = "商品")
    private String GoodName;

    /** 仓库id */
    private Long warehouseId;

    /** 仓库name*/
    @Excel(name = "仓库")
    private String warehouseName;

    /** 损坏数量 */
    @Excel(name = "损坏数量")
    private Long damageNum;

    /** 报损原因 */
    @Excel(name = "报损原因")
    private String reason;

    /** 创建人 */
    @Excel(name = "报损人")
    private String createUser;

    /** 报损状态*/
    @Excel(name = "报损单状态" ,readConverterExp = "0=已确认,1=未确认")
    private Long type;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public String getGoodName() {
        return GoodName;
    }

    public void setGoodName(String goodName) {
        GoodName = goodName;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setGoodId(Long goodId) 
    {
        this.goodId = goodId;
    }

    public Long getGoodId() 
    {
        return goodId;
    }
    public void setWarehouseId(Long warehouseId) 
    {
        this.warehouseId = warehouseId;
    }

    public Long getWarehouseId() 
    {
        return warehouseId;
    }
    public void setDamageNum(Long damageNum) 
    {
        this.damageNum = damageNum;
    }

    public Long getDamageNum() 
    {
        return damageNum;
    }
    public void setCreateUser(String createUser) 
    {
        this.createUser = createUser;
    }

    public String getCreateUser() 
    {
        return createUser;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("goodId", getGoodId())
            .append("warehouseId", getWarehouseId())
            .append("damageNum", getDamageNum())
            .append("createUser", getCreateUser())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
