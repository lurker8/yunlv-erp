package com.ruoyi.system.controller;

import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.domain.ErpGoodsSaleWarehouse;
import com.ruoyi.system.domain.ErpSalesReturn;
import com.ruoyi.system.domain.SaleGoods;
import com.ruoyi.system.service.IErpSalesReturnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 【请填写功能名称】Controller
 *
 * @author ruoyi
 * @date 2022-11-07
 */
@RestController
@RequestMapping("/return")
public class ErpSalesReturnController extends BaseController {
    @Autowired
    private IErpSalesReturnService erpSalesReturnService;

    /**
     * 查询【请填写功能名称】列表
     */
    @RequiresPermissions("system:return:list")
    @GetMapping("/list")
    public TableDataInfo list(ErpSalesReturn erpSalesReturn) {
        startPage();
        List<ErpSalesReturn> list = erpSalesReturnService.selectErpSalesReturnList(erpSalesReturn);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @RequiresPermissions("system:return:export")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ErpSalesReturn erpSalesReturn) {
        List<ErpSalesReturn> list = erpSalesReturnService.selectErpSalesReturnList(erpSalesReturn);
        ExcelUtil<ErpSalesReturn> util = new ExcelUtil<ErpSalesReturn>(ErpSalesReturn.class);
        util.exportExcel(response, list, "【请填写功能名称】数据");
    }

    /**
     * 获取【请填写功能名称】详细信息
     */
    @RequiresPermissions("system:return:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(erpSalesReturnService.selectErpSalesReturnById(id));
    }

    /**
     * 新增【请填写功能名称】
     */
    @RequiresPermissions("system:return:add")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ErpSalesReturn erpSalesReturn) {
        return toAjax(erpSalesReturnService.insertErpSalesReturn(erpSalesReturn));
    }

    /**
     * 修改【请填写功能名称】
     */
    @RequiresPermissions("system:return:edit")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ErpSalesReturn erpSalesReturn) {
        return toAjax(erpSalesReturnService.updateErpSalesReturn(erpSalesReturn));
    }

    /**
     * 删除【请填写功能名称】
     */
    @RequiresPermissions("system:return:remove")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(erpSalesReturnService.deleteErpSalesReturnByIds(ids));
    }

    @PostMapping("/returnGood")
    public AjaxResult returnGood(@RequestBody List<SaleGoods> saleGoods) {
        for (SaleGoods saleGood : saleGoods) {
            Long num = 0L;
            List<ErpGoodsSaleWarehouse> goodWarehouseList = saleGood.getGoodWarehouseList();
            for (ErpGoodsSaleWarehouse erpGoodsSaleWarehouse : goodWarehouseList) {
                String warehouseId = erpGoodsSaleWarehouse.getWarehouseId();
                Long goodId = erpGoodsSaleWarehouse.getGoodId();
                Long returnGoodNumber = erpGoodsSaleWarehouse.getReturnGoodNumber();
                erpSalesReturnService.updateGoodStock(goodId, Long.valueOf(warehouseId), returnGoodNumber);
                num = num + returnGoodNumber;
            }
            erpSalesReturnService.updateStock(saleGood.getId(), num);
        }
     return toAjax(erpSalesReturnService.updateIsReturn(saleGoods.get(0).getSaleId()));
    }
}

