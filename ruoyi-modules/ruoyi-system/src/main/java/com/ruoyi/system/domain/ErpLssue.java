package com.ruoyi.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 出库单对象 erp_lssue
 * 
 * @author liuce
 * @date 2022-11-17
 */
public class ErpLssue extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 出库id */
    private Long id;

    /** 出库单号 */
    @Excel(name = "出库单号")
    private String issueOrder;

    /** 出库金额 */
    @Excel(name = "出库金额")
    private BigDecimal lssueAmount;

    /** 出库仓库 */
    @Excel(name = "出库仓库")
    private String lssueWarehouse;

    /** 出库时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "出库时间", width = 30, dateFormat = "yyyy-MM-dd  HH:mm:ss")
    private Date lssueTime;

    /** 销售id */
    private Long saleId;

    /** 是销售出库还是调拨出库*/
    @Excel(name = "销售/调拨" , readConverterExp = "1=调拨单,0=销售单")
    private Long isSaleAllocation;

    /** 操作人 */
    @Excel(name = "操作人")
    private String createUser;

    public Long getIsSaleAllocation() {
        return isSaleAllocation;
    }

    public void setIsSaleAllocation(Long isSaleAllocation) {
        this.isSaleAllocation = isSaleAllocation;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setIssueOrder(String issueOrder) 
    {
        this.issueOrder = issueOrder;
    }

    public String getIssueOrder() 
    {
        return issueOrder;
    }
    public void setLssueAmount(BigDecimal lssueAmount) 
    {
        this.lssueAmount = lssueAmount;
    }

    public BigDecimal getLssueAmount() 
    {
        return lssueAmount;
    }
    public void setLssueWarehouse(String lssueWarehouse)
    {
        this.lssueWarehouse = lssueWarehouse;
    }

    public String getLssueWarehouse()
    {
        return lssueWarehouse;
    }
    public void setLssueTime(Date lssueTime) 
    {
        this.lssueTime = lssueTime;
    }

    public Date getLssueTime() 
    {
        return lssueTime;
    }
    public void setCreateUser(String createUser) 
    {
        this.createUser = createUser;
    }

    public String getCreateUser() 
    {
        return createUser;
    }
    public void setSaleId(Long saleId) 
    {
        this.saleId = saleId;
    }

    public Long getSaleId() 
    {
        return saleId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("issueOrder", getIssueOrder())
            .append("lssueAmount", getLssueAmount())
            .append("lssueWarehouse", getLssueWarehouse())
            .append("lssueTime", getLssueTime())
            .append("createUser", getCreateUser())
            .append("saleId", getSaleId())
            .toString();
    }
}
