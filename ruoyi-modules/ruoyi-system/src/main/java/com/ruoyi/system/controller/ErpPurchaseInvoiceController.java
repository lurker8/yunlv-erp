package com.ruoyi.system.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.system.api.model.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.domain.ErpPurchaseInvoice;
import com.ruoyi.system.service.IErpPurchaseInvoiceService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

import static com.ruoyi.common.security.utils.SecurityUtils.getLoginUser;

/**
 * 采购入库单Controller
 * 
 * @author ruoyi
 * @date 2022-11-14
 */
@RestController
@RequestMapping("/invoicePurchase")
public class ErpPurchaseInvoiceController extends BaseController
{
    @Autowired
    private IErpPurchaseInvoiceService erpPurchaseInvoiceService;

    /**
     * 查询采购入库单列表
     */
    @RequiresPermissions("system:invoicePurchase:list")
    @GetMapping("/list")
    public TableDataInfo list(ErpPurchaseInvoice erpPurchaseInvoice)
    {
        startPage();
        List<ErpPurchaseInvoice> list = erpPurchaseInvoiceService.selectErpPurchaseInvoiceList(erpPurchaseInvoice);
        return getDataTable(list);
    }

    /**
     * 导出采购入库单列表
     */
    @RequiresPermissions("system:invoicePurchase:export")
    @Log(title = "采购入库单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ErpPurchaseInvoice erpPurchaseInvoice)
    {
        List<ErpPurchaseInvoice> list = erpPurchaseInvoiceService.selectErpPurchaseInvoiceList(erpPurchaseInvoice);
        ExcelUtil<ErpPurchaseInvoice> util = new ExcelUtil<ErpPurchaseInvoice>(ErpPurchaseInvoice.class);
        util.exportExcel(response, list, "采购入库单数据");
    }

    /**
     * 获取采购入库单详细信息
     */
    @RequiresPermissions("system:invoicePurchase:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(erpPurchaseInvoiceService.selectErpPurchaseInvoiceById(id));
    }

    /**
     * 新增采购入库单
     */
    @RequiresPermissions("system:invoicePurchase:add")
    @Log(title = "采购入库单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ErpPurchaseInvoice erpPurchaseInvoice)
    {
        LoginUser loginUser = getLoginUser();
        erpPurchaseInvoice.setCreateUser(loginUser.getUsername());
        return toAjax(erpPurchaseInvoiceService.insertErpPurchaseInvoice(erpPurchaseInvoice));
    }

    /**
     * 修改采购入库单
     */
    @RequiresPermissions("system:invoicePurchase:edit")
    @Log(title = "采购入库单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ErpPurchaseInvoice erpPurchaseInvoice)
    {
        return toAjax(erpPurchaseInvoiceService.updateErpPurchaseInvoice(erpPurchaseInvoice));
    }

    /**
     * 删除采购入库单
     */
    @RequiresPermissions("system:invoicePurchase:remove")
    @Log(title = "采购入库单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(erpPurchaseInvoiceService.deleteErpPurchaseInvoiceByIds(ids));
    }
}
