package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.system.domain.ErpSalesReturn;
import com.ruoyi.system.mapper.ErpSaleMapper;
import com.ruoyi.system.mapper.ErpSalesInvoiceMapper;
import com.ruoyi.system.mapper.ErpSalesReturnMapper;
import com.ruoyi.system.service.IErpSalesReturnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * 【请填写功能名称】Service业务层处理
 *
 * @author ruoyi
 * @date 2022-11-07
 */
@Service
public class ErpSalesReturnServiceImpl implements IErpSalesReturnService
{
    @Autowired
    private ErpSalesReturnMapper erpSalesReturnMapper;
    @Autowired
    private ErpSaleMapper erpSaleMapper;
    @Autowired
    private ErpSalesInvoiceMapper erpSalesInvoiceMapper;

    /**
     * 查询【请填写功能名称】
     *
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public ErpSalesReturn selectErpSalesReturnById(Long id)
    {
        return erpSalesReturnMapper.selectErpSalesReturnById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     *
     * @param erpSalesReturn 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<ErpSalesReturn> selectErpSalesReturnList(ErpSalesReturn erpSalesReturn)
    {
        return erpSalesReturnMapper.selectErpSalesReturnList(erpSalesReturn);
    }

    /**
     * 新增【请填写功能名称】
     *
     * @param erpSalesReturn 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertErpSalesReturn(ErpSalesReturn erpSalesReturn)
    {
        erpSalesReturn.setCreateTime(DateUtils.getNowDate());
        Long saleId = erpSalesReturn.getSaleId();
        //修改销售订单与销售发货单退货状态
        erpSaleMapper.updateReturnType(saleId);
        erpSalesInvoiceMapper.updateReturnType(saleId);
        return erpSalesReturnMapper.insertErpSalesReturn(erpSalesReturn);
    }

    /**
     * 修改【请填写功能名称】
     *
     * @param erpSalesReturn 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateErpSalesReturn(ErpSalesReturn erpSalesReturn)
    {
        erpSalesReturn.setUpdateTime(DateUtils.getNowDate());
        return erpSalesReturnMapper.updateErpSalesReturn(erpSalesReturn);
    }

    /**
     * 批量删除【请填写功能名称】
     *
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteErpSalesReturnByIds(Long[] ids)
    {
        return erpSalesReturnMapper.deleteErpSalesReturnByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     *
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteErpSalesReturnById(Long id)
    {
        return erpSalesReturnMapper.deleteErpSalesReturnById(id);
    }

    @Override
    public int updateSaleAmount(Long saleId, BigDecimal amount) {
        return erpSalesReturnMapper.updateSaleAmount(saleId,amount);
    }

    @Override
    public int updateGoodStock(Long goodId, Long wareHouseId, Long returnNum) {
        return erpSalesReturnMapper.updateGoodStock(goodId, wareHouseId, returnNum);
    }

    @Override
    public int updateStock(Long goodId, Long returnNum) {
        return erpSalesReturnMapper.updateStock(goodId,returnNum);
    }

    @Override
    public int updateIsReturn(Long saleId) {
        return erpSalesReturnMapper.updateIsReturn(saleId);
    }
}
