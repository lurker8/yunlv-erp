package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.system.domain.ErpGoods;
import com.ruoyi.system.domain.ErpPurchase;
import com.ruoyi.system.domain.ErpPurchaseGoods;
import com.ruoyi.system.mapper.ErpGoodsMapper;
import com.ruoyi.system.mapper.ErpPurchaseGoodsMapper;
import com.ruoyi.system.mapper.ErpPurchaseMapper;
import com.ruoyi.system.service.IErpPurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 采购订单Service业务层处理
 *
 * @author ruoyi
 * @date 2022-10-27
 */
@Service
public class ErpPurchaseServiceImpl implements IErpPurchaseService
{
    @Autowired
    private ErpPurchaseMapper erpPurchaseMapper;

    @Autowired
    private ErpGoodsMapper erpGoodsMapper;

    @Autowired
    private ErpPurchaseGoodsMapper erpPurchaseGoodsMapper;

    /**
     * 查询采购订单
     *
     * @param id 采购订单主键
     * @return 采购订单
     */
    @Override
    public ErpPurchase selectErpPurchaseById(Long id)
    {
        return erpPurchaseMapper.selectErpPurchaseById(id);
    }

    /**
     * 查询采购订单列表
     *
     * @param erpPurchase 采购订单
     * @return 采购订单
     */
    @Override
    public List<ErpPurchase> selectErpPurchaseList(ErpPurchase erpPurchase)
    {
        return erpPurchaseMapper.selectErpPurchaseList(erpPurchase);
    }

    /*首页折线图日期数据*/
    @Override
    public List<String> selectxDate(Map map) {
        return erpPurchaseMapper.selectxDate(map);
    }
    /*首页折线图采购单数*/
    @Override
    public List<Integer> selectPurchaseCount(Map map) {
        return erpPurchaseMapper.selectPurchaseCount(map);
    }

    /**
     * 新增采购订单
     *
     * @param erpPurchase 采购订单
     * @return 结果
     */
    @Override
    public int insertErpPurchase(ErpPurchase erpPurchase)
    {
        erpPurchase.setCreateTime(DateUtils.getNowDate());
        int rows = erpPurchaseMapper.insertErpPurchase(erpPurchase);
        insertGoods(erpPurchase);
        return rows;
    }

    /**
     * 新增采购订单关联商品
     *
     */
    public void insertGoods(ErpPurchase erpPurchase)
    {
        /*商品ids*/
        String[] posts = erpPurchase.getGoodsIds();
        /*采购单价*/
        String[] monovalents = erpPurchase.getMonovalents();
        /*采购数量*/
        String[] quantitys = erpPurchase.getQuantitys();
        /*采购总价*/
        String[] totalPrices = erpPurchase.getTotalPrices();
        if (StringUtils.isNotNull(posts))
        {
            // 新增用户与岗位管理
            List<ErpPurchaseGoods> list = new ArrayList<ErpPurchaseGoods>();
            for (int i = 0; i < posts.length; i++) {
                /*查询基础数据*/
                ErpGoods erpGoods = erpGoodsMapper.selectErpGoodsById(Long.valueOf(posts[i]));
                /*插入数据*/
                ErpPurchaseGoods erpPurchaseGoods = new ErpPurchaseGoods();
                erpPurchaseGoods.setPurchaseId(erpPurchase.getId());
                erpPurchaseGoods.setGoodsNumber(String.valueOf(erpGoods.getId()));
                erpPurchaseGoods.setGoodsName(erpGoods.getName());
                if(monovalents[i] != null){
                    /*将采购单价转换成BigDecimal类型*/
                    BigDecimal monovalent = new BigDecimal(monovalents[i]);
                    erpPurchaseGoods.setMonovalent(monovalent);
                }
                if(quantitys[i] != null){
                    erpPurchaseGoods.setQuantity(Long.valueOf(quantitys[i]));
                }
                if(totalPrices[i] != null){
                    /*将采购总价转换成BigDecimal类型*/
                    BigDecimal totalPrice = new BigDecimal(totalPrices[i]);
                    erpPurchaseGoods.setTotal(totalPrice);
                }
                erpPurchaseGoods.setCreateTime(new Date());
                erpPurchaseGoods.setCategoryName(erpGoods.getCategoryName());
                erpPurchaseGoods.setBrandName(erpGoods.getBrandName());
                list.add(erpPurchaseGoods);
            }
            if (list.size() > 0)
            {
                erpPurchaseGoodsMapper.batchErpPurchaseGoods(list);
            }
        }
    }

    /**
     * 修改采购订单
     *
     * @param erpPurchase 采购订单
     * @return 结果
     */
    @Override
    public int updateErpPurchase(ErpPurchase erpPurchase)
    {
        long Id = erpPurchase.getId();
        erpPurchaseGoodsMapper.deleteErpPurchaseGoods(Id);
        insertGoods(erpPurchase);
        erpPurchase.setUpdateTime(DateUtils.getNowDate());
        return erpPurchaseMapper.updateErpPurchase(erpPurchase);
    }

    public int updateErpPurchaseState(ErpPurchase erpPurchase)
    {
        erpPurchase.setUpdateTime(DateUtils.getNowDate());
        return erpPurchaseMapper.updateErpPurchase(erpPurchase);
    }

    /**
     * 批量删除采购订单
     *
     * @param ids 需要删除的采购订单主键
     * @return 结果
     */
    @Override
    public int deleteErpPurchaseByIds(Long[] ids)
    {
        erpPurchaseGoodsMapper.deletePurchaseId(ids);
        return erpPurchaseMapper.deleteErpPurchaseByIds(ids);
    }

    /**
     * 删除采购订单信息
     *
     * @param id 采购订单主键
     * @return 结果
     */
    @Override
    public int deleteErpPurchaseById(Long id)
    {
        return erpPurchaseMapper.deleteErpPurchaseById(id);
    }
}
