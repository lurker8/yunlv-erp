package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.ErpSaleGoods;
import io.lettuce.core.dynamic.annotation.Param;

import java.math.BigDecimal;
import java.util.List;

/**
 * 销售商品Mapper接口
 *
 * @author liuce
 * @date 2022-10-31
 */
public interface ErpSaleGoodsMapper {
    /**
     * 查询销售商品
     *
     * @return 销售商品
     */
    public ErpSaleGoods selectErpSaleGoodsById(@Param("saleId") Long saleId, @Param("goodId") Long goodId);

    /**
     * 查询销售商品列表
     *
     * @param erpSaleGoods 销售商品
     * @return 销售商品集合
     */
    public List<ErpSaleGoods> selectErpSaleGoodsList(ErpSaleGoods erpSaleGoods);

    /**
     * 新增销售商品
     *
     * @param erpSaleGoods 销售商品
     * @return 结果
     */
    public int insertErpSaleGoods(ErpSaleGoods erpSaleGoods);

    /**
     * 修改销售商品
     *
     * @param erpSaleGoods 销售商品
     * @return 结果
     */
    public int updateErpSaleGoods(ErpSaleGoods erpSaleGoods);

    /**
     * 删除销售商品
     *
     * @param id 销售商品主键
     * @return 结果
     */
    public int deleteErpSaleGoodsById(Long id);

    /**
     * 批量删除销售商品
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteErpSaleGoodsByIds(Long[] ids);

    /**
     * 修改商品销售金额
     *
     * @param saleId 销售Id
     * @param goodId 商品Id
     * @param returnAmount 商品退货金额
     * @param goodReturnWarehouse 商品退货数量
     * @return 结果
     */
    public int updateSaleGoodAmount(@Param("goodId") Long goodId, @Param("saleId")Long saleId, @Param("returnAmount")BigDecimal returnAmount,@Param("goodReturnWarehouse")Long goodReturnWarehouse);

}
