package com.ruoyi.system.controller;

import com.alibaba.nacos.shaded.org.checkerframework.checker.units.qual.A;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.domain.ErpFundAccount;
import com.ruoyi.system.service.IErpFundAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 资金账户管理Controller
 *
 * @author ruoyi
 * @date 2022-10-26
 */
@RestController
@RequestMapping("/account")
public class ErpFundAccountController extends BaseController
{
    @Autowired
    private IErpFundAccountService erpFundAccountService;

    /**
     * 查询资金账户管理列表
     */
    @RequiresPermissions("system:account:list")
    @GetMapping("/list")
    public TableDataInfo list(ErpFundAccount erpFundAccount)
    {
        startPage();
        List<ErpFundAccount> list = erpFundAccountService.selectErpFundAccountList(erpFundAccount);
        return getDataTable(list);
    }

    /**
     * 导出资金账户管理列表
     */
    @RequiresPermissions("system:account:export")
    @Log(title = "资金账户管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ErpFundAccount erpFundAccount)
    {
        List<ErpFundAccount> list = erpFundAccountService.selectErpFundAccountList(erpFundAccount);
        ExcelUtil<ErpFundAccount> util = new ExcelUtil<ErpFundAccount>(ErpFundAccount.class);
        util.exportExcel(response, list, "资金账户管理数据");
    }

    /**
     * 获取资金账户管理详细信息
     */
    @RequiresPermissions("system:account:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(erpFundAccountService.selectErpFundAccountById(id));
    }

    /**
     * 新增资金账户管理
     */
    @RequiresPermissions("system:account:add")
    @Log(title = "资金账户管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ErpFundAccount erpFundAccount)
    {
        return toAjax(erpFundAccountService.insertErpFundAccount(erpFundAccount));
    }

    /**
     * 修改资金账户管理
     */
    @RequiresPermissions("system:account:edit")
    @Log(title = "资金账户管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ErpFundAccount erpFundAccount)
    {
        return toAjax(erpFundAccountService.updateErpFundAccount(erpFundAccount));
    }

    /**
     * 删除资金账户管理
     */
    @RequiresPermissions("system:account:remove")
    @Log(title = "资金账户管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        for (Long id : ids) {
            Long aLong = erpFundAccountService.selectAccountAssociation(id);
            if (aLong != null){
                return AjaxResult.error("资金账户已分配禁止删除!");
            }
        }
        return toAjax(erpFundAccountService.deleteErpFundAccountByIds(ids));
    }
}
