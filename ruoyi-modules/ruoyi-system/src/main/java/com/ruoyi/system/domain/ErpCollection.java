package com.ruoyi.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 应收账款对象 erp_collection
 * 
 * @author ruoyi
 * @date 2022-11-11
 */
public class ErpCollection extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** Id */
    private Long id;

    /** 销售单号 */
    @Excel(name = "销售单号")
    private String saleOrderNum;

    /** 收款时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "收款时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date collectionTime;

    /** 收款金额 */
    @Excel(name = "收款金额")
    private BigDecimal amountCollected;

    /** 应收金额 */
    @Excel(name = "应收金额")
    private BigDecimal amountReceivable;

    /** 已收金额 */
    @Excel(name = "已收金额")
    private BigDecimal receivedAmount;

    /** 未收金额 */
    @Excel(name = "未收金额")
    private BigDecimal amountNotReceived;

    /** 收款人name */
    @Excel(name = "收款人name")
    private String payee;

    /** 收款账户 */
    @Excel(name = "收款账户")
    private String collectedAccount;

    /** 收款凭证 */
    @Excel(name = "收款凭证")
    private String collectionVoucher;

    /** 收款备注 */
    @Excel(name = "收款备注")
    private String collectionRemarks;

    /**付款方式*/
    private Long payMethod;

    /** 收款状态 */
    @Excel(name = "收款状态")
    private Long collectionType;

    /** 操作人 */
    @Excel(name = "操作人")
    private String operator;

    /** 销售ID */
    private Long saleId;

    /** 客户name*/
    private String customerName;

    public BigDecimal getAmountNotReceived() {
        return amountNotReceived;
    }

    public void setAmountNotReceived(BigDecimal amountNotReceived) {
        this.amountNotReceived = amountNotReceived;
    }

    public BigDecimal getReceivedAmount() {
        return receivedAmount;
    }

    public void setReceivedAmount(BigDecimal receivedAmount) {
        this.receivedAmount = receivedAmount;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getSaleOrderNum() {
        return saleOrderNum;
    }

    public Date getCollectionTime() {
        return collectionTime;
    }

    public void setCollectionTime(Date collectionTime) {
        this.collectionTime = collectionTime;
    }

    public void setAmountCollected(BigDecimal amountCollected) {
        this.amountCollected = amountCollected;
    }

    public String getPayee() {
        return payee;
    }

    public void setPayee(String payee) {
        this.payee = payee;
    }

    public String getCollectedAccount() {
        return collectedAccount;
    }

    public void setCollectedAccount(String collectedAccount) {
        this.collectedAccount = collectedAccount;
    }

    public String getCollectionVoucher() {
        return collectionVoucher;
    }

    public void setCollectionVoucher(String collectionVoucher) {
        this.collectionVoucher = collectionVoucher;
    }

    public String getCollectionRemarks() {
        return collectionRemarks;
    }

    public void setCollectionRemarks(String collectionRemarks) {
        this.collectionRemarks = collectionRemarks;
    }

    public void setCollectionType(Long collectionType) {
        this.collectionType = collectionType;
    }

    public Long getSaleId() {
        return saleId;
    }

    public void setSaleId(Long saleId) {
        this.saleId = saleId;
    }

    public BigDecimal getAmountReceivable() {
        return amountReceivable;
    }

    public void setAmountReceivable(BigDecimal amountReceivable) {
        this.amountReceivable = amountReceivable;
    }

    public Long getPayMethod() {
        return payMethod;
    }

    public void setPayMethod(Long payMethod) {
        this.payMethod = payMethod;
    }


    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setSaleOrderNum(String saleOrderNum)
    {
        this.saleOrderNum = saleOrderNum;
    }


    public BigDecimal getAmountCollected() 
    {
        return amountCollected;
    }

    public Long getCollectionType() 
    {
        return collectionType;
    }
    public void setOperator(String operator) 
    {
        this.operator = operator;
    }

    public String getOperator() 
    {
        return operator;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("amountCollected", getAmountCollected())
            .append("collectionType", getCollectionType())
            .append("operator", getOperator())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
