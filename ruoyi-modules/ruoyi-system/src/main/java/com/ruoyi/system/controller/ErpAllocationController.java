package com.ruoyi.system.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.domain.ErpAllocation;
import com.ruoyi.system.service.IErpAllocationService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 库存调拨Controller
 * 
 * @author licue
 * @date 2022-11-17
 */
@RestController
@RequestMapping("/allocation")
public class ErpAllocationController extends BaseController
{
    @Autowired
    private IErpAllocationService erpAllocationService;

    /**
     * 查询库存调拨列表
     */
    @RequiresPermissions("system:allocation:list")
    @GetMapping("/list")
    public TableDataInfo list(ErpAllocation erpAllocation)
    {
        startPage();
        List<ErpAllocation> list = erpAllocationService.selectErpAllocationList(erpAllocation);
        return getDataTable(list);
    }

    /**
     * 导出库存调拨列表
     */
    @RequiresPermissions("system:allocation:export")
    @Log(title = "库存调拨", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ErpAllocation erpAllocation)
    {
        List<ErpAllocation> list = erpAllocationService.selectErpAllocationList(erpAllocation);
        ExcelUtil<ErpAllocation> util = new ExcelUtil<ErpAllocation>(ErpAllocation.class);
        util.exportExcel(response, list, "库存调拨数据");
    }

    /**
     * 获取库存调拨详细信息
     */
    @RequiresPermissions("system:allocation:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(erpAllocationService.selectErpAllocationById(id));
    }

    /**
     * 新增库存调拨
     */
    @RequiresPermissions("system:allocation:add")
    @Log(title = "库存调拨", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ErpAllocation erpAllocation)
    {
        return toAjax(erpAllocationService.insertErpAllocation(erpAllocation));
    }

    /**
     * 修改库存调拨
     */
    @RequiresPermissions("system:allocation:edit")
    @Log(title = "库存调拨", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ErpAllocation erpAllocation)
    {
        return toAjax(erpAllocationService.updateErpAllocation(erpAllocation));
    }

    /**
     * 修改库存调拨状态
     */
    @PutMapping("/updateAllocationType")
    public AjaxResult updateAllocationType(Long allocationId,Long type)
    {
        return toAjax(erpAllocationService.updateAllocationType(allocationId, type));
    }

    /**
     * 删除库存调拨
     */
    @RequiresPermissions("system:allocation:remove")
    @Log(title = "库存调拨", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(erpAllocationService.deleteErpAllocationByIds(ids));
    }
}
