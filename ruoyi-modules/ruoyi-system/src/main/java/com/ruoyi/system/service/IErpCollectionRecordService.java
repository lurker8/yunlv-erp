package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.ErpCollectionRecord;

/**
 * 收款记录Service接口
 * 
 * @author 刘策
 * @date 2022-11-16
 */
public interface IErpCollectionRecordService 
{
    /**
     * 查询收款记录
     * 
     * @param id 收款记录主键
     * @return 收款记录
     */
    public ErpCollectionRecord selectErpCollectionRecordById(Long id);

    /**
     * 查询收款记录列表
     * 
     * @param erpCollectionRecord 收款记录
     * @return 收款记录集合
     */
    public List<ErpCollectionRecord> selectErpCollectionRecordList(ErpCollectionRecord erpCollectionRecord);

    /**
     * 新增收款记录
     * 
     * @param erpCollectionRecord 收款记录
     * @return 结果
     */
    public int insertErpCollectionRecord(ErpCollectionRecord erpCollectionRecord);

    /**
     * 修改收款记录
     * 
     * @param erpCollectionRecord 收款记录
     * @return 结果
     */
    public int updateErpCollectionRecord(ErpCollectionRecord erpCollectionRecord);

    /**
     * 批量删除收款记录
     * 
     * @param ids 需要删除的收款记录主键集合
     * @return 结果
     */
    public int deleteErpCollectionRecordByIds(Long[] ids);

    /**
     * 删除收款记录信息
     * 
     * @param id 收款记录主键
     * @return 结果
     */
    public int deleteErpCollectionRecordById(Long id);
}
