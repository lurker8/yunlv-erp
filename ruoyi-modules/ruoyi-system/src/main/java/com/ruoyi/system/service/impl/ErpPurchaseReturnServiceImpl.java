package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ErpPurchaseReturnMapper;
import com.ruoyi.system.domain.ErpPurchaseReturn;
import com.ruoyi.system.service.IErpPurchaseReturnService;

/**
 * 采购退货Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-11-16
 */
@Service
public class ErpPurchaseReturnServiceImpl implements IErpPurchaseReturnService 
{
    @Autowired
    private ErpPurchaseReturnMapper erpPurchaseReturnMapper;

    /**
     * 查询采购退货
     * 
     * @param id 采购退货主键
     * @return 采购退货
     */
    @Override
    public ErpPurchaseReturn selectErpPurchaseReturnById(Long id)
    {
        return erpPurchaseReturnMapper.selectErpPurchaseReturnById(id);
    }

    /**
     * 查询采购退货列表
     * 
     * @param erpPurchaseReturn 采购退货
     * @return 采购退货
     */
    @Override
    public List<ErpPurchaseReturn> selectErpPurchaseReturnList(ErpPurchaseReturn erpPurchaseReturn)
    {
        return erpPurchaseReturnMapper.selectErpPurchaseReturnList(erpPurchaseReturn);
    }

    /**
     * 新增采购退货
     * 
     * @param erpPurchaseReturn 采购退货
     * @return 结果
     */
    @Override
    public int insertErpPurchaseReturn(ErpPurchaseReturn erpPurchaseReturn)
    {
        erpPurchaseReturn.setCreateTime(DateUtils.getNowDate());
        return erpPurchaseReturnMapper.insertErpPurchaseReturn(erpPurchaseReturn);
    }

    /**
     * 修改采购退货
     * 
     * @param erpPurchaseReturn 采购退货
     * @return 结果
     */
    @Override
    public int updateErpPurchaseReturn(ErpPurchaseReturn erpPurchaseReturn)
    {
        erpPurchaseReturn.setUpdateTime(DateUtils.getNowDate());
        return erpPurchaseReturnMapper.updateErpPurchaseReturn(erpPurchaseReturn);
    }

    /**
     * 批量删除采购退货
     * 
     * @param ids 需要删除的采购退货主键
     * @return 结果
     */
    @Override
    public int deleteErpPurchaseReturnByIds(Long[] ids)
    {
        return erpPurchaseReturnMapper.deleteErpPurchaseReturnByIds(ids);
    }

    /**
     * 删除采购退货信息
     * 
     * @param id 采购退货主键
     * @return 结果
     */
    @Override
    public int deleteErpPurchaseReturnById(Long id)
    {
        return erpPurchaseReturnMapper.deleteErpPurchaseReturnById(id);
    }
}
