package com.ruoyi.system.controller;

import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.domain.ErpBrand;
import com.ruoyi.system.service.IErpBrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 品牌管理Controller
 *
 * @author 刘策
 * @date 2022-10-26
 */
@RestController
@RequestMapping("/brand")
public class ErpBrandController extends BaseController
{
    @Autowired
    private IErpBrandService erpBrandService;

    /**
     * 查询品牌管理列表
     */
    @RequiresPermissions("system:brand:list")
    @GetMapping("/list")
    public TableDataInfo list(ErpBrand erpBrand)
    {
        startPage();
        List<ErpBrand> list = erpBrandService.selectErpBrandList(erpBrand);
        return getDataTable(list);
    }

    /**
     * 导出品牌管理列表
     */
    @RequiresPermissions("system:brand:export")
    @Log(title = "品牌管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ErpBrand erpBrand)
    {
        List<ErpBrand> list = erpBrandService.selectErpBrandList(erpBrand);
        ExcelUtil<ErpBrand> util = new ExcelUtil<ErpBrand>(ErpBrand.class);
        util.exportExcel(response, list, "品牌管理数据");
    }

    /**
     * 获取品牌管理详细信息
     */
    @RequiresPermissions("system:brand:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(erpBrandService.selectErpBrandById(id));
    }

    /**
     * 新增品牌管理
     */
    @RequiresPermissions("system:brand:add")
    @Log(title = "品牌管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ErpBrand erpBrand)
    {
        return toAjax(erpBrandService.insertErpBrand(erpBrand));
    }

    /**
     * 修改品牌管理
     */
    @RequiresPermissions("system:brand:edit")
    @Log(title = "品牌管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ErpBrand erpBrand)
    {
        return toAjax(erpBrandService.updateErpBrand(erpBrand));
    }

    /**
     * 删除品牌管理
     */
    @RequiresPermissions("system:brand:remove")
    @Log(title = "品牌管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        for (Long id : ids) {
            Long aLong = erpBrandService.selectBrandAssociation(id);
            if (aLong != null){
                return AjaxResult.error("品牌已分配禁止删除");
            }
        }
        return toAjax(erpBrandService.deleteErpBrandByIds(ids));
    }
}
