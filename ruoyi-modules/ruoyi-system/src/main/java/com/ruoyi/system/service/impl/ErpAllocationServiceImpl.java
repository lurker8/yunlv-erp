package com.ruoyi.system.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.alibaba.nacos.shaded.org.checkerframework.checker.units.qual.A;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.system.api.model.LoginUser;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.mapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.service.IErpAllocationService;
import org.springframework.transaction.annotation.Transactional;

import static com.ruoyi.common.security.utils.SecurityUtils.getLoginUser;

/**
 * 库存调拨Service业务层处理
 * 
 * @author licue
 * @date 2022-11-17
 */
@Service
public class ErpAllocationServiceImpl implements IErpAllocationService 
{
    @Autowired
    private ErpAllocationMapper erpAllocationMapper;

    @Autowired
    private ErpAllocationGoodsMapper erpAllocationGoodsMapper;

    @Autowired
    private ErpLssueMapper erpLssueMapper;

    @Autowired
    private ErpGoodsWarehouseMapper erpGoodsWarehouseMapper;

    @Autowired
    private ErpReceiptDocMapper erpReceiptDocMapper;

    /**
     * 查询库存调拨
     * 
     * @param id 库存调拨主键
     * @return 库存调拨
     */
    @Override
    public ErpAllocation selectErpAllocationById(Long id)
    {
        return erpAllocationMapper.selectErpAllocationById(id);
    }

    /**
     * 查询库存调拨列表
     * 
     * @param erpAllocation 库存调拨
     * @return 库存调拨
     */
    @Override
    public List<ErpAllocation> selectErpAllocationList(ErpAllocation erpAllocation)
    {
        return erpAllocationMapper.selectErpAllocationList(erpAllocation);
    }

    /**
     * 新增库存调拨
     * 
     * @param erpAllocation 库存调拨
     * @return 结果
     */
    @Override
    public int insertErpAllocation(ErpAllocation erpAllocation)
    {
        erpAllocation.setCreateTime(DateUtils.getNowDate());
        erpAllocation.setType(1L);
        int i = erpAllocationMapper.insertErpAllocation(erpAllocation);
        for (ErpAllocationGoods erpAllocationGood : erpAllocation.getErpAllocationGoods()) {
            erpAllocationGood.setCreateUser(getLoginUser().getUsername());
            erpAllocationGood.setCreateTime(DateUtils.getNowDate());
            erpAllocationGood.setAllocationId(erpAllocation.getId());
            erpAllocationGoodsMapper.insertErpAllocationGoods(erpAllocationGood);
        }
        return i;
    }

    /**
     * 修改库存调拨
     * 
     * @param erpAllocation 库存调拨
     * @return 结果
     */
    @Override
    public int updateErpAllocation(ErpAllocation erpAllocation)
    {
        erpAllocationGoodsMapper.deleteErpAllocationGoodsById(erpAllocation.getId());
        for (ErpAllocationGoods erpAllocationGood : erpAllocation.getErpAllocationGoods()) {
            erpAllocationGood.setCreateUser(getLoginUser().getUsername());
            erpAllocationGood.setCreateTime(DateUtils.getNowDate());
            erpAllocationGood.setAllocationId(erpAllocation.getId());
            erpAllocationGoodsMapper.insertErpAllocationGoods(erpAllocationGood);
        }
        erpAllocation.setUpdateTime(DateUtils.getNowDate());
        return erpAllocationMapper.updateErpAllocation(erpAllocation);
    }

    @Override
    @Transactional
    public int updateAllocationType(Long allocationId, Long type) {
        ErpAllocation erpAllocation = erpAllocationMapper.selectErpAllocationById(allocationId);
        //新增出库单
        ErpLssue erpLssue = new ErpLssue();
        erpLssue.setLssueWarehouse(erpAllocation.getLssueWarehouseName());
        erpLssue.setLssueTime(DateUtils.getNowDate());
        erpLssue.setIssueOrder("Lss" + new Date().getTime());
        erpLssue.setSaleId(erpAllocation.getId());
        erpLssue.setIsSaleAllocation(1L);
        erpLssue.setCreateUser(getLoginUser().getUsername());
        erpLssue.setLssueAmount(new BigDecimal(0));
        erpLssueMapper.insertErpLssue(erpLssue);
        //新增入库单
        ErpReceiptDoc erpReceiptDoc = new ErpReceiptDoc();
        erpReceiptDoc.setReceiptDocOrder("Erd" + new Date().getTime());
        erpReceiptDoc.setCreateUser(getLoginUser().getUsername());
        erpReceiptDoc.setReceiptDocAmount(new BigDecimal(0));
        erpReceiptDoc.setIsSaleAllocation(1L);
        erpReceiptDoc.setPurchaseId(erpAllocation.getId());
        erpReceiptDoc.setCreateTime(DateUtils.getNowDate());
        erpReceiptDoc.setReceiptDocWarehouse(erpAllocation.getWarehousingName());
        erpReceiptDocMapper.insertErpReceiptDoc(erpReceiptDoc);
        List<ErpAllocationGoods> erpAllocationGoods = erpAllocation.getErpAllocationGoods();
        //对于调拨商品进行商品仓库库存操作
        for (ErpAllocationGoods erpAllocationGood : erpAllocationGoods) {
            ErpGoodsWarehouse erpGoodsWarehouse = erpGoodsWarehouseMapper.selectAllByGidWid(erpAllocationGood.getGoodId(), erpAllocation.getWarehousingId());
            //入库仓库没有此商品新增一条数据
            if (erpGoodsWarehouse == null){
                ErpGoodsWarehouse erpGoodsWarehouse1 = new ErpGoodsWarehouse();
                erpGoodsWarehouse1.setCreateUser(getLoginUser().getUsername());
                erpGoodsWarehouse1.setGoodId(erpAllocationGood.getGoodId());
                erpGoodsWarehouse1.setGoodsNumber(erpAllocationGood.getGoodNumber());
                erpGoodsWarehouse1.setWarehouseId(erpAllocation.getWarehousingId());
                erpGoodsWarehouse1.setWarehouseName(erpAllocation.getWarehousingName());
                erpGoodsWarehouse1.setCreateTime(DateUtils.getNowDate());
                erpGoodsWarehouseMapper.insertErpGoodsWarehouse(erpGoodsWarehouse1);
            }else {
                //入库仓库有此商品进行库存增加操作
                allocation allocation = new allocation();
                allocation.setGoodNum(erpAllocationGood.getGoodNumber());
                allocation.setWarehousingId(erpAllocation.getWarehousingId());
                allocation.setGoodId(erpAllocationGood.getGoodId());
                erpGoodsWarehouseMapper.warehousingGoodNum(allocation);
            }
            //出库商品进行库存减少操作
            com.ruoyi.system.domain.allocation allocation1 = new allocation();
            allocation1.setGoodId(erpAllocationGood.getGoodId());
            allocation1.setLssueId(erpAllocation.getLssueWarehouse());
            allocation1.setGoodNum(erpAllocationGood.getGoodNumber());
            erpGoodsWarehouseMapper.updateGoodNum(allocation1);
        }
        //修改调拨单的状态
        return erpAllocationMapper.updateAllocationType(allocationId, type);
    }

    /**
     * 批量删除库存调拨
     * 
     * @param ids 需要删除的库存调拨主键
     * @return 结果
     */
    @Override
    public int deleteErpAllocationByIds(Long[] ids)
    {
        for (Long id : ids) {
            erpAllocationGoodsMapper.deleteErpAllocationGoodsById(id);
        }
        return erpAllocationMapper.deleteErpAllocationByIds(ids);
    }

    /**
     * 删除库存调拨信息
     * 
     * @param id 库存调拨主键
     * @return 结果
     */
    @Override
    public int deleteErpAllocationById(Long id)
    {
        return erpAllocationMapper.deleteErpAllocationById(id);
    }
}
