package com.ruoyi.system.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 采购退货对象 erp_purchase_return
 * 
 * @author ruoyi
 * @date 2022-11-16
 */
public class ErpPurchaseReturn extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 订单id */
    @Excel(name = "订单id")
    private Long purchaseId;

    /** 订单单号 */
    @Excel(name = "订单单号")
    private String purchaseOrderNum;

    /** 发货单号 */
    @Excel(name = "发货单号")
    private String shipmentNum;

    /** 退货总额 */
    @Excel(name = "退货总额")
    private BigDecimal returnBonus;

    /** 创建人 */
    @Excel(name = "创建人")
    private String createUser;

    /** 是否退货扣除库存（0是1否） */
    @Excel(name = "是否退货扣除库存", readConverterExp = "0=是1否")
    private Long isReturn;

    /** 供应商id */
    @Excel(name = "供应商id")
    private Long supplierId;

    /** 付款类型 */
    @Excel(name = "付款类型")
    private Long paymentType;

    /** 供应商联系人 */
    @Excel(name = "供应商联系人")
    private String supplieruser;

    /** 供应商手机号 */
    @Excel(name = "供应商手机号")
    private String supplierphone;
    /*供应商名称*/
    private String supplierName;

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setPurchaseId(Long purchaseId) 
    {
        this.purchaseId = purchaseId;
    }

    public Long getPurchaseId() 
    {
        return purchaseId;
    }
    public void setPurchaseOrderNum(String purchaseOrderNum) 
    {
        this.purchaseOrderNum = purchaseOrderNum;
    }

    public String getPurchaseOrderNum() 
    {
        return purchaseOrderNum;
    }
    public void setShipmentNum(String shipmentNum) 
    {
        this.shipmentNum = shipmentNum;
    }

    public String getShipmentNum() 
    {
        return shipmentNum;
    }
    public void setReturnBonus(BigDecimal returnBonus) 
    {
        this.returnBonus = returnBonus;
    }

    public BigDecimal getReturnBonus() 
    {
        return returnBonus;
    }
    public void setCreateUser(String createUser) 
    {
        this.createUser = createUser;
    }

    public String getCreateUser() 
    {
        return createUser;
    }
    public void setIsReturn(Long isReturn) 
    {
        this.isReturn = isReturn;
    }

    public Long getIsReturn() 
    {
        return isReturn;
    }
    public void setSupplierId(Long supplierId) 
    {
        this.supplierId = supplierId;
    }

    public Long getSupplierId() 
    {
        return supplierId;
    }
    public void setPaymentType(Long paymentType) 
    {
        this.paymentType = paymentType;
    }

    public Long getPaymentType() 
    {
        return paymentType;
    }
    public void setSupplieruser(String supplieruser) 
    {
        this.supplieruser = supplieruser;
    }

    public String getSupplieruser() 
    {
        return supplieruser;
    }
    public void setSupplierphone(String supplierphone) 
    {
        this.supplierphone = supplierphone;
    }

    public String getSupplierphone() 
    {
        return supplierphone;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("purchaseId", getPurchaseId())
            .append("purchaseOrderNum", getPurchaseOrderNum())
            .append("shipmentNum", getShipmentNum())
            .append("returnBonus", getReturnBonus())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createUser", getCreateUser())
            .append("isReturn", getIsReturn())
            .append("supplierId", getSupplierId())
            .append("paymentType", getPaymentType())
            .append("supplieruser", getSupplieruser())
            .append("supplierphone", getSupplierphone())
            .toString();
    }
}
