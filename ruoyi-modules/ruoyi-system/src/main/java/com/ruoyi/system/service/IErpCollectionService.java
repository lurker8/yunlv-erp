package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.ErpCollection;

/**
 * 应收账款Service接口
 * 
 * @author ruoyi
 * @date 2022-11-11
 */
public interface IErpCollectionService 
{
    /**
     * 查询应收账款
     * 
     * @param id 应收账款主键
     * @return 应收账款
     */
    public ErpCollection selectErpCollectionById(Long id);

    /**
     * 查询应收账款列表
     * 
     * @param erpCollection 应收账款
     * @return 应收账款集合
     */
    public List<ErpCollection> selectErpCollectionList(ErpCollection erpCollection);

    /**
     * 新增应收账款
     * 
     * @param erpCollection 应收账款
     * @return 结果
     */
    public int insertErpCollection(ErpCollection erpCollection);

    /**
     * 修改应收账款
     * 
     * @param erpCollection 应收账款
     * @return 结果
     */
    public int updateErpCollection(ErpCollection erpCollection);

    /**
     * 批量删除应收账款
     * 
     * @param ids 需要删除的应收账款主键集合
     * @return 结果
     */
    public int deleteErpCollectionByIds(Long[] ids);

    /**
     * 删除应收账款信息
     * 
     * @param id 应收账款主键
     * @return 结果
     */
    public int deleteErpCollectionById(Long id);
}
