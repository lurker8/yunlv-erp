package com.ruoyi.system.controller;

import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.domain.ErpGoodsWarehouse;
import com.ruoyi.system.service.IErpGoodsWarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 商品仓库管理Controller
 *
 * @author 刘策
 * @date 2022-11-02
 */
@RestController
@RequestMapping("/goodWarehouse")
public class ErpGoodsWarehouseController extends BaseController
{
    @Autowired
    private IErpGoodsWarehouseService erpGoodsWarehouseService;

    /**
     * 查询商品仓库管理列表
     */
    @RequiresPermissions("system:warehouse:list")
    @GetMapping("/list")
    public TableDataInfo list(ErpGoodsWarehouse erpGoodsWarehouse)
    {
        startPage();
        List<ErpGoodsWarehouse> list = erpGoodsWarehouseService.selectErpGoodsWarehouseList(erpGoodsWarehouse);
        return getDataTable(list);
    }

    /**
     * 查询商品名称
     */
    @GetMapping("/goodsName")
    public TableDataInfo goodsName(ErpGoodsWarehouse erpGoodsWarehouse)
    {
        startPage();
        List<ErpGoodsWarehouse> list = erpGoodsWarehouseService.selectErpGoodsWarehouseGoodsNameList(erpGoodsWarehouse);
        return getDataTable(list);
    }
    /**
     * 查询仓库名称
     */
    @GetMapping("/warehouseName")
    public TableDataInfo warehouseName(ErpGoodsWarehouse erpGoodsWarehouse)
    {
        startPage();
        List<ErpGoodsWarehouse> list = erpGoodsWarehouseService.selectErpGoodsWarehouseNameList(erpGoodsWarehouse);
        return getDataTable(list);
    }

    /**
     * 导出商品仓库管理列表
     */
    @RequiresPermissions("system:warehouse:export")
    @Log(title = "商品仓库管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ErpGoodsWarehouse erpGoodsWarehouse)
    {
        List<ErpGoodsWarehouse> list = erpGoodsWarehouseService.selectErpGoodsWarehouseList(erpGoodsWarehouse);
        ExcelUtil<ErpGoodsWarehouse> util = new ExcelUtil<ErpGoodsWarehouse>(ErpGoodsWarehouse.class);
        util.exportExcel(response, list, "商品仓库管理数据");
    }

    /**
     * 获取商品仓库管理详细
     */
    @RequiresPermissions("system:warehouse:query")
    @GetMapping(value = "/{id}")
    public TableDataInfo getInfo(@PathVariable("id") Long id)
    {
        startPage();
        List<ErpGoodsWarehouse> erpGoodsWarehouses = erpGoodsWarehouseService.selectErpGoodsWarehouseById(id);
        return getDataTable(erpGoodsWarehouses);
    }

    @GetMapping(value = "/getGoodNumberInfo")
    public TableDataInfo getGoodNumberInfo(Long id,Long saleId)
    {
        //查询仓库中商品的库存
        List<ErpGoodsWarehouse> erpGoodsWarehouses = erpGoodsWarehouseService.selectErpGoodsWarehouseById(id);
        for (ErpGoodsWarehouse erpGoodsWarehouse : erpGoodsWarehouses) {
            //查询销售的商品分别从哪个仓库发了多少的货
            ErpGoodsWarehouse erpGoodsWarehouse1 = erpGoodsWarehouseService.selectGoodWarehouseNumber(erpGoodsWarehouse.getWarehouseId(), saleId, id);
            if (erpGoodsWarehouse1 != null){
                erpGoodsWarehouse.setGoodSaleWarehouse(erpGoodsWarehouse1.getGoodSaleWarehouse());
                erpGoodsWarehouse.setGoodReturnWarehouse(erpGoodsWarehouse1.getGoodReturnWarehouse());
            }
        }
        return getDataTable(erpGoodsWarehouses);
    }

    @DeleteMapping(value = "/deleteGoodNumberInfo")
    public AjaxResult deleteGoodNumberInfo(Long goodId,Long saleId)
    {
        return toAjax(erpGoodsWarehouseService.deleteGoodNumberInfo(goodId, saleId));
    }


    /**
     * 新增商品仓库管理
     */
    @RequiresPermissions("system:warehouse:add")
    @Log(title = "商品仓库管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ErpGoodsWarehouse erpGoodsWarehouse)
    {
        return toAjax(erpGoodsWarehouseService.insertErpGoodsWarehouse(erpGoodsWarehouse));
    }

    /**
     * 新增采购入库商品仓库管理
     */
    @PostMapping(value = "/addPurchaseWarehouse")
    public AjaxResult addPurchaseWarehouse(@RequestBody ErpGoodsWarehouse erpGoodsWarehouse)
    {
        ErpGoodsWarehouse erpGoodsWarehouse1 = new ErpGoodsWarehouse();
        erpGoodsWarehouse1.setGoodId(erpGoodsWarehouse.getGoodId());
        erpGoodsWarehouse1.setWarehouseId(erpGoodsWarehouse.getWarehouseId());
        List<ErpGoodsWarehouse> erpGoodsWarehouses = erpGoodsWarehouseService.selectErpGoodsWarehouseByGoodsId(erpGoodsWarehouse1);
        if(erpGoodsWarehouses.size() > 0){
            for (ErpGoodsWarehouse goodsWarehouse : erpGoodsWarehouses) {
                erpGoodsWarehouse.setId(goodsWarehouse.getId());

            }
            erpGoodsWarehouseService.updateErpGoodsWarehouses(erpGoodsWarehouse);
        }
        if(erpGoodsWarehouses.size() <=0){
            erpGoodsWarehouseService.insertErpGoodsWarehouse(erpGoodsWarehouse);
        }
        return AjaxResult.success();
    }

    /**
     * 修改商品仓库管理
     */
    @RequiresPermissions("system:warehouse:edit")
    @Log(title = "商品仓库管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ErpGoodsWarehouse erpGoodsWarehouse)
    {
        return toAjax(erpGoodsWarehouseService.updateErpGoodsWarehouse(erpGoodsWarehouse));
    }

    /**
     * 删除商品仓库管理
     */
    @RequiresPermissions("system:warehouse:remove")
    @Log(title = "商品仓库管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(erpGoodsWarehouseService.deleteErpGoodsWarehouseByIds(ids));
    }
}
