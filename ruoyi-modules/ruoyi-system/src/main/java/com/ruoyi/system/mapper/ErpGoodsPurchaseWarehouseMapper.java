package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.ErpGoodsPurchaseWarehouse;

/**
 * 采购商品仓库关联Mapper接口
 * 
 * @author ruoyi
 * @date 2022-11-10
 */
public interface ErpGoodsPurchaseWarehouseMapper 
{
    /**
     * 查询采购商品仓库关联
     * 
     * @param id 采购商品仓库关联主键
     * @return 采购商品仓库关联
     */
    public ErpGoodsPurchaseWarehouse selectErpGoodsPurchaseWarehouseById(Long id);

    /**
     * 查询采购商品仓库关联列表
     * 
     * @param erpGoodsPurchaseWarehouse 采购商品仓库关联
     * @return 采购商品仓库关联集合
     */
    public List<ErpGoodsPurchaseWarehouse> selectErpGoodsPurchaseWarehouseList(ErpGoodsPurchaseWarehouse erpGoodsPurchaseWarehouse);

    /**
     * 新增采购商品仓库关联
     * 
     * @param erpGoodsPurchaseWarehouse 采购商品仓库关联
     * @return 结果
     */
    public int insertErpGoodsPurchaseWarehouse(ErpGoodsPurchaseWarehouse erpGoodsPurchaseWarehouse);

    /**
     * 修改采购商品仓库关联
     * 
     * @param erpGoodsPurchaseWarehouse 采购商品仓库关联
     * @return 结果
     */
    public int updateErpGoodsPurchaseWarehouse(ErpGoodsPurchaseWarehouse erpGoodsPurchaseWarehouse);

    /**
     * 删除采购商品仓库关联
     * 
     * @param id 采购商品仓库关联主键
     * @return 结果
     */
    public int deleteErpGoodsPurchaseWarehouseById(Long id);

    /**
     * 批量删除采购商品仓库关联
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteErpGoodsPurchaseWarehouseByIds(Long[] ids);
}
