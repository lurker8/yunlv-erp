package com.ruoyi.system.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.domain.ErpPaymentRecord;
import com.ruoyi.system.service.IErpPaymentRecordService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 付款记录Controller
 * 
 * @author ruoyi
 * @date 2022-11-18
 */
@RestController
@RequestMapping("/paymentRecord")
public class ErpPaymentRecordController extends BaseController
{
    @Autowired
    private IErpPaymentRecordService erpPaymentRecordService;

    /**
     * 查询付款记录列表
     */
    @RequiresPermissions("system:paymentRecord:list")
    @GetMapping("/list")
    public TableDataInfo list(ErpPaymentRecord erpPaymentRecord)
    {
        startPage();
        List<ErpPaymentRecord> list = erpPaymentRecordService.selectErpPaymentRecordList(erpPaymentRecord);
        return getDataTable(list);
    }

    /**
     * 导出付款记录列表
     */
    @RequiresPermissions("system:paymentRecord:export")
    @Log(title = "付款记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ErpPaymentRecord erpPaymentRecord)
    {
        List<ErpPaymentRecord> list = erpPaymentRecordService.selectErpPaymentRecordList(erpPaymentRecord);
        ExcelUtil<ErpPaymentRecord> util = new ExcelUtil<ErpPaymentRecord>(ErpPaymentRecord.class);
        util.exportExcel(response, list, "付款记录数据");
    }

    /**
     * 获取付款记录详细信息
     */
    @RequiresPermissions("system:paymentRecord:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(erpPaymentRecordService.selectErpPaymentRecordById(id));
    }

    /**
     * 新增付款记录
     */
    @RequiresPermissions("system:paymentRecord:add")
    @Log(title = "付款记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ErpPaymentRecord erpPaymentRecord)
    {
        return toAjax(erpPaymentRecordService.insertErpPaymentRecord(erpPaymentRecord));
    }

    /**
     * 修改付款记录
     */
    @RequiresPermissions("system:paymentRecord:edit")
    @Log(title = "付款记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ErpPaymentRecord erpPaymentRecord)
    {
        return toAjax(erpPaymentRecordService.updateErpPaymentRecord(erpPaymentRecord));
    }

    /**
     * 删除付款记录
     */
    @RequiresPermissions("system:paymentRecord:remove")
    @Log(title = "付款记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(erpPaymentRecordService.deleteErpPaymentRecordByIds(ids));
    }
}
