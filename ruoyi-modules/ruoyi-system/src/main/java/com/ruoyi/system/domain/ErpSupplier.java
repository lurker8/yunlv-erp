package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 供应商管理对象 erp_supplier
 *
 * @author 刘策
 * @date 2022-10-25
 */
public class ErpSupplier extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 供应商id
     */
    private Long id;

    /**
     * 供应商名称
     */
    @Excel(name = "供应商名称")
    private String supplierName;

    /**
     * 联系人
     */
    @Excel(name = "联系人")
    private String supplierContacts;

    /**
     * 联系人电话
     */
    @Excel(name = "联系人电话")
    private String supplierPhone;

    /**
     * 供应商分类
     */
    @Excel(name = "供应商分类"  ,readConverterExp = "0=服饰,1=鞋靴")
    private Long supplierCategory;

    /**
     * 供应商地址
     */
    @Excel(name = "供应商地址")
    private String supplierAddress;

    /**
     * 详细地址
     */
    @Excel(name = "详细地址")
    private String detailedAddress;

    /**
     * 供应商开户银行
     */
    @Excel(name = "供应商开户银行")
    private String supplierBank;

    /**
     * 供应商银行账户
     */
    @Excel(name = "供应商银行账户")
    private String supplierBankAccount;

    /**
     * 供应商税号
     */
    @Excel(name = "供应商税号")
    private String supplierDutyParagraph;

    /**
     * 备注信息
     */
    @Excel(name = "备注信息")
    private String remarks;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date creatTime;

    /**
     * 创建人
     */
    private Long creatUser;

    /**
     * 创建人Name
     */
    private String creatUserName;

    /**
     * 省
     */
    private Long province;

    /**
     * 市
     */
    private Long city;

    /**
     * 区
     */
    private Long area;


    public String getCreatUserName() {
        return creatUserName;
    }

    public void setCreatUserName(String creatUserName) {
        this.creatUserName = creatUserName;
    }

    public String getDetailedAddress() {
        return detailedAddress;
    }

    public void setDetailedAddress(String detailedAddress) {
        this.detailedAddress = detailedAddress;
    }

    public Long getProvince() {
        return province;
    }

    public void setProvince(Long province) {
        this.province = province;
    }

    public Long getCity() {
        return city;
    }

    public void setCity(Long city) {
        this.city = city;
    }

    public Long getArea() {
        return area;
    }

    public void setArea(Long area) {
        this.area = area;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierContacts(String supplierContacts) {
        this.supplierContacts = supplierContacts;
    }

    public String getSupplierContacts() {
        return supplierContacts;
    }

    public void setSupplierPhone(String supplierPhone) {
        this.supplierPhone = supplierPhone;
    }

    public String getSupplierPhone() {
        return supplierPhone;
    }

    public void setSupplierCategory(Long supplierCategory) {
        this.supplierCategory = supplierCategory;
    }

    public Long getSupplierCategory() {
        return supplierCategory;
    }

    public void setSupplierAddress(String supplierAddress) {
        this.supplierAddress = supplierAddress;
    }

    public String getSupplierAddress() {
        return supplierAddress;
    }

    public void setSupplierBank(String supplierBank) {
        this.supplierBank = supplierBank;
    }

    public String getSupplierBank() {
        return supplierBank;
    }

    public void setSupplierBankAccount(String supplierBankAccount) {
        this.supplierBankAccount = supplierBankAccount;
    }

    public String getSupplierBankAccount() {
        return supplierBankAccount;
    }

    public void setSupplierDutyParagraph(String supplierDutyParagraph) {
        this.supplierDutyParagraph = supplierDutyParagraph;
    }

    public String getSupplierDutyParagraph() {
        return supplierDutyParagraph;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setCreatTime(Date creatTime) {
        this.creatTime = creatTime;
    }

    public Date getCreatTime() {
        return creatTime;
    }

    public void setCreatUser(Long creatUser) {
        this.creatUser = creatUser;
    }

    public Long getCreatUser() {
        return creatUser;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("supplierName", getSupplierName())
                .append("supplierContacts", getSupplierContacts())
                .append("supplierPhone", getSupplierPhone())
                .append("supplierCategory", getSupplierCategory())
                .append("supplierAddress", getSupplierAddress())
                .append("supplierBank", getSupplierBank())
                .append("supplierBankAccount", getSupplierBankAccount())
                .append("supplierDutyParagraph", getSupplierDutyParagraph())
                .append("remarks", getRemarks())
                .append("creatTime", getCreatTime())
                .append("updateTime", getUpdateTime())
                .append("创建人", getCreatUser())
                .toString();
    }
}
