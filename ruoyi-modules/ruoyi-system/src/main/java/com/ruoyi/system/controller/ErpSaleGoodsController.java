package com.ruoyi.system.controller;

import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.domain.ErpSaleGoods;
import com.ruoyi.system.domain.SaleGoods;
import com.ruoyi.system.service.IErpSaleGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 销售商品Controller
 *
 * @author liuce
 * @date 2022-10-31
 */
@RestController
@RequestMapping("/saleGoods")
public class ErpSaleGoodsController extends BaseController
{
    @Autowired
    private IErpSaleGoodsService erpSaleGoodsService;

    /**
     * 查询销售商品列表
     */
    @RequiresPermissions("system:goods:list")
    @GetMapping("/list")
    public TableDataInfo list(ErpSaleGoods erpSaleGoods)
    {
        startPage();
        List<ErpSaleGoods> list = erpSaleGoodsService.selectErpSaleGoodsList(erpSaleGoods);
        return getDataTable(list);
    }

    /**
     * 导出销售商品列表
     */
    @RequiresPermissions("system:goods:export")
    @Log(title = "销售商品", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ErpSaleGoods erpSaleGoods)
    {
        List<ErpSaleGoods> list = erpSaleGoodsService.selectErpSaleGoodsList(erpSaleGoods);
        ExcelUtil<ErpSaleGoods> util = new ExcelUtil<ErpSaleGoods>(ErpSaleGoods.class);
        util.exportExcel(response, list, "销售商品数据");
    }

/*
    @RequiresPermissions("system:goods:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(erpSaleGoodsService.selectErpSaleGoodsById(id));
    }*/

    /**
     * 新增销售商品
     */
    @RequiresPermissions("system:goods:add")
    @Log(title = "销售商品", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SaleGoods saleGoods)
    {
        ErpSaleGoods erpSaleGoods = new ErpSaleGoods();
        erpSaleGoods.setGoodsName(saleGoods.getName());
        erpSaleGoods.setGoodsNumber(saleGoods.getId().toString());
        erpSaleGoods.setMonovalent(saleGoods.getPrice());
        erpSaleGoods.setQuantity(saleGoods.getQuantity());
        erpSaleGoods.setOneGoodPrice(saleGoods.getOneGoodPrice());
        erpSaleGoods.setPurchaseId(saleGoods.getSaleId());
        erpSaleGoods.setCompany(saleGoods.getCompanyName());
        return toAjax(erpSaleGoodsService.insertErpSaleGoods(erpSaleGoods));
    }

    /**
     * 修改销售商品
     */
    @RequiresPermissions("system:goods:edit")
    @Log(title = "销售商品", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ErpSaleGoods erpSaleGoods)
    {
        return toAjax(erpSaleGoodsService.updateErpSaleGoods(erpSaleGoods));
    }

    /**
     * 删除销售商品
     */
    @RequiresPermissions("system:goods:remove")
    @Log(title = "销售商品", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(erpSaleGoodsService.deleteErpSaleGoodsByIds(ids));
    }
}
