package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.ErpSalesInvoice;
import io.lettuce.core.dynamic.annotation.Param;

import java.math.BigDecimal;
import java.util.List;

/**
 * 销售发货单Mapper接口
 *
 * @author liuce
 * @date 2022-11-07
 */
public interface ErpSalesInvoiceMapper
{
    /**
     * 查询销售发货单
     *
     * @param id 销售发货单主键
     * @return 销售发货单
     */
    public ErpSalesInvoice selectErpSalesInvoiceById(Long id);

    /**
     * 查询销售发货单列表
     *
     * @param erpSalesInvoice 销售发货单
     * @return 销售发货单集合
     */
    public List<ErpSalesInvoice> selectErpSalesInvoiceList(ErpSalesInvoice erpSalesInvoice);

    /**
     * 新增销售发货单
     *
     * @param erpSalesInvoice 销售发货单
     * @return 结果
     */
    public int insertErpSalesInvoice(ErpSalesInvoice erpSalesInvoice);

    /**
     * 修改销售发货单
     *
     * @param erpSalesInvoice 销售发货单
     * @return 结果
     */
    public int updateErpSalesInvoice(ErpSalesInvoice erpSalesInvoice);

    /**
     * 删除销售发货单
     *
     * @param id 销售发货单主键
     * @return 结果
     */
    public int deleteErpSalesInvoiceById(Long id);

    /**
     * 批量删除销售发货单
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteErpSalesInvoiceByIds(Long[] ids);

    /**
     * 修改销售发货单状态
     *
     * @param saleId 销售id
     * @param type  订单状态
     * @return 结果
     */
    public int updateErpSalesInvoiceType(@Param("saleId") Long saleId, @Param("type")Long type);

    /**
     * 退货时修改订单的销售额
     * @param saleId 销售id
     * @param returnAmount 退货金额
     * @return 结果
     * */
    int modifySales(@Param("saleId") Long saleId, @Param("returnAmount") BigDecimal returnAmount);

    int updateReturnType(Long saleId);
}
