package com.ruoyi.system.service.impl;

import java.math.BigDecimal;
import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.system.api.model.LoginUser;
import com.ruoyi.system.domain.ErpCollectionRecord;
import com.ruoyi.system.mapper.ErpCollectionRecordMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ErpCollectionMapper;
import com.ruoyi.system.domain.ErpCollection;
import com.ruoyi.system.service.IErpCollectionService;

import static com.ruoyi.common.security.utils.SecurityUtils.getLoginUser;

/**
 * 应收账款Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-11-11
 */
@Service
public class ErpCollectionServiceImpl implements IErpCollectionService 
{
    @Autowired
    private ErpCollectionMapper erpCollectionMapper;

    @Autowired
    private ErpCollectionRecordMapper erpCollectionRecordMapper;


    /**
     * 查询应收账款
     * 
     * @param id 应收账款主键
     * @return 应收账款
     */
    @Override
    public ErpCollection selectErpCollectionById(Long id)
    {
        ErpCollection erpCollection = erpCollectionMapper.selectErpCollectionById(id);
        BigDecimal amountReceivable = erpCollection.getAmountReceivable();
        BigDecimal receivedAmount = erpCollection.getReceivedAmount();
        if (amountReceivable != null && receivedAmount != null){
            BigDecimal AmountNotReceived = amountReceivable.subtract(receivedAmount);
            erpCollection.setAmountNotReceived(AmountNotReceived);
        }
        if (amountReceivable != null && receivedAmount == null){
            erpCollection.setAmountNotReceived(amountReceivable);
        }
        return erpCollection;
    }

    /**
     * 查询应收账款列表
     * 
     * @param erpCollection 应收账款
     * @return 应收账款
     */
    @Override
    public List<ErpCollection> selectErpCollectionList(ErpCollection erpCollection)
    {
        return erpCollectionMapper.selectErpCollectionList(erpCollection);
    }

    /**
     * 新增应收账款
     * 
     * @param erpCollection 应收账款
     * @return 结果
     */
    @Override
    public int insertErpCollection(ErpCollection erpCollection)
    {
        erpCollection.setCreateTime(DateUtils.getNowDate());
        return erpCollectionMapper.insertErpCollection(erpCollection);
    }

    /**
     * 修改应收账款
     * 
     * @param erpCollection 应收账款
     * @return 结果
     */
    @Override
    public int updateErpCollection(ErpCollection erpCollection)
    {
        erpCollection.setUpdateTime(DateUtils.getNowDate());
        ErpCollectionRecord erpCollectionRecord = new ErpCollectionRecord();
        erpCollectionRecord.setSaleId(erpCollection.getSaleId());
        erpCollectionRecord.setCollectionTime(erpCollection.getCollectionTime());
        erpCollectionRecord.setAmountCollected(erpCollection.getAmountCollected());
        erpCollectionRecord.setPayee(erpCollection.getPayee());
        erpCollectionRecord.setCollectionVoucher(erpCollection.getCollectionVoucher());
        erpCollectionRecord.setCollectionRemarks(erpCollection.getCollectionRemarks());
        erpCollectionRecord.setCreateTime(DateUtils.getNowDate());
        LoginUser loginUser = getLoginUser();
        erpCollectionRecord.setCreateUser(loginUser.getUsername());
        erpCollectionRecord.setCollectedAccount(erpCollection.getCollectedAccount());
        erpCollectionRecord.setCollectionId(erpCollection.getId());
        erpCollectionRecordMapper.insertErpCollectionRecord(erpCollectionRecord);
        return erpCollectionMapper.updateErpCollection(erpCollection);
    }

    /**
     * 批量删除应收账款
     * 
     * @param ids 需要删除的应收账款主键
     * @return 结果
     */
    @Override
    public int deleteErpCollectionByIds(Long[] ids)
    {
        return erpCollectionMapper.deleteErpCollectionByIds(ids);
    }

    /**
     * 删除应收账款信息
     * 
     * @param id 应收账款主键
     * @return 结果
     */
    @Override
    public int deleteErpCollectionById(Long id)
    {
        return erpCollectionMapper.deleteErpCollectionById(id);
    }
}
