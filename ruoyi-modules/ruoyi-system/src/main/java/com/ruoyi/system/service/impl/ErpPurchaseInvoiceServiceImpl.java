package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ErpPurchaseInvoiceMapper;
import com.ruoyi.system.domain.ErpPurchaseInvoice;
import com.ruoyi.system.service.IErpPurchaseInvoiceService;

/**
 * 采购入库单Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-11-14
 */
@Service
public class ErpPurchaseInvoiceServiceImpl implements IErpPurchaseInvoiceService 
{
    @Autowired
    private ErpPurchaseInvoiceMapper erpPurchaseInvoiceMapper;

    /**
     * 查询采购入库单
     * 
     * @param id 采购入库单主键
     * @return 采购入库单
     */
    @Override
    public ErpPurchaseInvoice selectErpPurchaseInvoiceById(Long id)
    {
        return erpPurchaseInvoiceMapper.selectErpPurchaseInvoiceById(id);
    }

    /**
     * 查询采购入库单列表
     * 
     * @param erpPurchaseInvoice 采购入库单
     * @return 采购入库单
     */
    @Override
    public List<ErpPurchaseInvoice> selectErpPurchaseInvoiceList(ErpPurchaseInvoice erpPurchaseInvoice)
    {
        return erpPurchaseInvoiceMapper.selectErpPurchaseInvoiceList(erpPurchaseInvoice);
    }

    /**
     * 新增采购入库单
     * 
     * @param erpPurchaseInvoice 采购入库单
     * @return 结果
     */
    @Override
    public int insertErpPurchaseInvoice(ErpPurchaseInvoice erpPurchaseInvoice)
    {
        erpPurchaseInvoice.setCreateTime(DateUtils.getNowDate());
        return erpPurchaseInvoiceMapper.insertErpPurchaseInvoice(erpPurchaseInvoice);
    }

    /**
     * 修改采购入库单
     * 
     * @param erpPurchaseInvoice 采购入库单
     * @return 结果
     */
    @Override
    public int updateErpPurchaseInvoice(ErpPurchaseInvoice erpPurchaseInvoice)
    {
        erpPurchaseInvoice.setUpdateTime(DateUtils.getNowDate());
        return erpPurchaseInvoiceMapper.updateErpPurchaseInvoice(erpPurchaseInvoice);
    }

    /**
     * 批量删除采购入库单
     * 
     * @param ids 需要删除的采购入库单主键
     * @return 结果
     */
    @Override
    public int deleteErpPurchaseInvoiceByIds(Long[] ids)
    {
        return erpPurchaseInvoiceMapper.deleteErpPurchaseInvoiceByIds(ids);
    }

    /**
     * 删除采购入库单信息
     * 
     * @param id 采购入库单主键
     * @return 结果
     */
    @Override
    public int deleteErpPurchaseInvoiceById(Long id)
    {
        return erpPurchaseInvoiceMapper.deleteErpPurchaseInvoiceById(id);
    }
}
