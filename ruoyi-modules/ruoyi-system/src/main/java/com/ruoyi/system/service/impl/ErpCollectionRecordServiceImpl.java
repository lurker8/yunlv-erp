package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ErpCollectionRecordMapper;
import com.ruoyi.system.domain.ErpCollectionRecord;
import com.ruoyi.system.service.IErpCollectionRecordService;

/**
 * 收款记录Service业务层处理
 * 
 * @author 刘策
 * @date 2022-11-16
 */
@Service
public class ErpCollectionRecordServiceImpl implements IErpCollectionRecordService 
{
    @Autowired
    private ErpCollectionRecordMapper erpCollectionRecordMapper;

    /**
     * 查询收款记录
     * 
     * @param id 收款记录主键
     * @return 收款记录
     */
    @Override
    public ErpCollectionRecord selectErpCollectionRecordById(Long id)
    {
        return erpCollectionRecordMapper.selectErpCollectionRecordById(id);
    }

    /**
     * 查询收款记录列表
     * 
     * @param erpCollectionRecord 收款记录
     * @return 收款记录
     */
    @Override
    public List<ErpCollectionRecord> selectErpCollectionRecordList(ErpCollectionRecord erpCollectionRecord)
    {
        return erpCollectionRecordMapper.selectErpCollectionRecordList(erpCollectionRecord);
    }

    /**
     * 新增收款记录
     * 
     * @param erpCollectionRecord 收款记录
     * @return 结果
     */
    @Override
    public int insertErpCollectionRecord(ErpCollectionRecord erpCollectionRecord)
    {
        erpCollectionRecord.setCreateTime(DateUtils.getNowDate());
        return erpCollectionRecordMapper.insertErpCollectionRecord(erpCollectionRecord);
    }

    /**
     * 修改收款记录
     * 
     * @param erpCollectionRecord 收款记录
     * @return 结果
     */
    @Override
    public int updateErpCollectionRecord(ErpCollectionRecord erpCollectionRecord)
    {
        return erpCollectionRecordMapper.updateErpCollectionRecord(erpCollectionRecord);
    }

    /**
     * 批量删除收款记录
     * 
     * @param ids 需要删除的收款记录主键
     * @return 结果
     */
    @Override
    public int deleteErpCollectionRecordByIds(Long[] ids)
    {
        return erpCollectionRecordMapper.deleteErpCollectionRecordByIds(ids);
    }

    /**
     * 删除收款记录信息
     * 
     * @param id 收款记录主键
     * @return 结果
     */
    @Override
    public int deleteErpCollectionRecordById(Long id)
    {
        return erpCollectionRecordMapper.deleteErpCollectionRecordById(id);
    }
}
