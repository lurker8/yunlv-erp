package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.system.domain.ErpSalesInvoice;
import com.ruoyi.system.mapper.ErpSalesInvoiceMapper;
import com.ruoyi.system.service.IErpSalesInvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 销售发货单Service业务层处理
 *
 * @author liuce
 * @date 2022-11-07
 */
@Service
public class ErpSalesInvoiceServiceImpl implements IErpSalesInvoiceService
{
    @Autowired
    private ErpSalesInvoiceMapper erpSalesInvoiceMapper;

    /**
     * 查询销售发货单
     *
     * @param id 销售发货单主键
     * @return 销售发货单
     */
    @Override
    public ErpSalesInvoice selectErpSalesInvoiceById(Long id)
    {
        return erpSalesInvoiceMapper.selectErpSalesInvoiceById(id);
    }

    /**
     * 查询销售发货单列表
     *
     * @param erpSalesInvoice 销售发货单
     * @return 销售发货单
     */
    @Override
    public List<ErpSalesInvoice> selectErpSalesInvoiceList(ErpSalesInvoice erpSalesInvoice)
    {
        return erpSalesInvoiceMapper.selectErpSalesInvoiceList(erpSalesInvoice);
    }

    /**
     * 新增销售发货单
     *
     * @param erpSalesInvoice 销售发货单
     * @return 结果
     */
    @Override
    public int insertErpSalesInvoice(ErpSalesInvoice erpSalesInvoice)
    {
        erpSalesInvoice.setCreateTime(DateUtils.getNowDate());
        return erpSalesInvoiceMapper.insertErpSalesInvoice(erpSalesInvoice);
    }

    /**
     * 修改销售发货单
     *
     * @param erpSalesInvoice 销售发货单
     * @return 结果
     */
    @Override
    public int updateErpSalesInvoice(ErpSalesInvoice erpSalesInvoice)
    {
        return erpSalesInvoiceMapper.updateErpSalesInvoice(erpSalesInvoice);
    }

    /**
     * 批量删除销售发货单
     *
     * @param ids 需要删除的销售发货单主键
     * @return 结果
     */
    @Override
    public int deleteErpSalesInvoiceByIds(Long[] ids)
    {
        return erpSalesInvoiceMapper.deleteErpSalesInvoiceByIds(ids);
    }

    /**
     * 删除销售发货单信息
     *
     * @param id 销售发货单主键
     * @return 结果
     */
    @Override
    public int deleteErpSalesInvoiceById(Long id)
    {
        return erpSalesInvoiceMapper.deleteErpSalesInvoiceById(id);
    }
}
