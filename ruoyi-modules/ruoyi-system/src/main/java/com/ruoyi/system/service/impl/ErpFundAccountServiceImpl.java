package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.system.domain.ErpFundAccount;
import com.ruoyi.system.mapper.ErpFundAccountMapper;
import com.ruoyi.system.service.IErpFundAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 资金账户管理Service业务层处理
 *
 * @author ruoyi
 * @date 2022-10-26
 */
@Service
public class ErpFundAccountServiceImpl implements IErpFundAccountService
{
    @Autowired
    private ErpFundAccountMapper erpFundAccountMapper;

    /**
     * 查询资金账户管理
     *
     * @param id 资金账户管理主键
     * @return 资金账户管理
     */
    @Override
    public ErpFundAccount selectErpFundAccountById(Long id)
    {
        return erpFundAccountMapper.selectErpFundAccountById(id);
    }

    /**
     * 查询资金账户管理列表
     *
     * @param erpFundAccount 资金账户管理
     * @return 资金账户管理
     */
    @Override
    public List<ErpFundAccount> selectErpFundAccountList(ErpFundAccount erpFundAccount)
    {
        return erpFundAccountMapper.selectErpFundAccountList(erpFundAccount);
    }

    /**
     * 新增资金账户管理
     *
     * @param erpFundAccount 资金账户管理
     * @return 结果
     */
    @Override
    public int insertErpFundAccount(ErpFundAccount erpFundAccount)
    {
        erpFundAccount.setCreateTime(DateUtils.getNowDate());
        return erpFundAccountMapper.insertErpFundAccount(erpFundAccount);
    }

    /**
     * 修改资金账户管理
     *
     * @param erpFundAccount 资金账户管理
     * @return 结果
     */
    @Override
    public int updateErpFundAccount(ErpFundAccount erpFundAccount)
    {
        erpFundAccount.setUpdateTime(DateUtils.getNowDate());
        return erpFundAccountMapper.updateErpFundAccount(erpFundAccount);
    }

    /**
     * 批量删除资金账户管理
     *
     * @param ids 需要删除的资金账户管理主键
     * @return 结果
     */
    @Override
    public int deleteErpFundAccountByIds(Long[] ids)
    {
        return erpFundAccountMapper.deleteErpFundAccountByIds(ids);
    }

    /**
     * 删除资金账户管理信息
     *
     * @param id 资金账户管理主键
     * @return 结果
     */
    @Override
    public int deleteErpFundAccountById(Long id)
    {
        return erpFundAccountMapper.deleteErpFundAccountById(id);
    }

    @Override
    public Long selectAccountAssociation(Long id) {
        return erpFundAccountMapper.selectAccountAssociation(id);
    }
}
