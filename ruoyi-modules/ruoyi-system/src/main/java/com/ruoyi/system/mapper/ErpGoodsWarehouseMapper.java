package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.ErpGoodsWarehouse;
import com.ruoyi.system.domain.allocation;
import io.lettuce.core.dynamic.annotation.Param;

import java.util.List;

/**
 * 商品仓库管理Mapper接口
 *
 * @author 刘策
 * @date 2022-11-02
 */
public interface ErpGoodsWarehouseMapper
{
    /**
     * 查询商品仓库管理
     *
     * @param id 商品id
     * @return 商品仓库管理
     */
    public List<ErpGoodsWarehouse> selectErpGoodsWarehouseById(Long id);

    /*根据商品id查询*/
    public List<ErpGoodsWarehouse> selectErpGoodsWarehouseByGoodsId(ErpGoodsWarehouse erpGoodsWarehouse);


    /**
     * 查询销售的商品分别从哪个仓库发了多少的货
     *
     * @param warehouseId 仓库id
     * @param saleId 销售id
     * @param saleGoodId 销售商品id
     *
     * @return 结果
     */
    ErpGoodsWarehouse selectGoodWarehouseNumber(@Param("warehouseId") Long warehouseId,@Param("saleId")Long saleId,@Param("saleGoodId")Long saleGoodId);
    /**
     * 查询商品仓库管理列表
     *
     * @param erpGoodsWarehouse 商品仓库管理
     * @return 商品仓库管理集合
     */
    public List<ErpGoodsWarehouse> selectErpGoodsWarehouseList(ErpGoodsWarehouse erpGoodsWarehouse);

    /*查询商品名称*/
    public List<ErpGoodsWarehouse> selectErpGoodsWarehouseGoodsNameList(ErpGoodsWarehouse erpGoodsWarehouse);
    /*查询仓库名称*/
    public List<ErpGoodsWarehouse> selectErpGoodsWarehouseNameList(ErpGoodsWarehouse erpGoodsWarehouse);

    /**
     * 新增商品仓库管理
     *
     * @param erpGoodsWarehouse 商品仓库管理
     * @return 结果
     */
    public int insertErpGoodsWarehouse(ErpGoodsWarehouse erpGoodsWarehouse);

    /**
     * 修改商品仓库管理
     *
     * @param erpGoodsWarehouse 商品仓库管理
     * @return 结果
     */
    public int updateErpGoodsWarehouse(ErpGoodsWarehouse erpGoodsWarehouse);
    public int updateErpGoodsWarehouses(ErpGoodsWarehouse erpGoodsWarehouse);

    /**
     * 删除商品仓库管理
     *
     * @param id 商品仓库管理主键
     * @return 结果
     */
    public int deleteErpGoodsWarehouseById(Long id);

    /**
     * 批量删除商品仓库管理
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteErpGoodsWarehouseByIds(Long[] ids);

    int deleteGoodNumberInfo(@Param("goodId") Long goodId,@Param("saleId")Long saleId);

    /**
     * 出库商品库存减少操作
     *
     * @return 成功结果
     * */
    int updateGoodNum(allocation allocation);

    /**
     * 入库商品库存增加操作
     *
     * @return 成功结果
     * */
    int warehousingGoodNum(allocation allocation);

    ErpGoodsWarehouse selectAllByGidWid(@Param("goodId")Long goodId,@Param("warehouseId")Long warehouseId);

}
