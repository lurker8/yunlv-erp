package com.ruoyi.system.domain;

import org.apache.catalina.LifecycleState;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

import java.util.List;

/**
 * 库存盘点对象 erp_inventory
 * 
 * @author ruoyi
 * @date 2022-11-21
 */
public class ErpInventory extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 盘点id */
    private Long id;

    /** 盘点单号 */
    @Excel(name = "盘点单号")
    private String inventoryOrder;

    /** 仓库id */
    @Excel(name = "仓库id")
    private Long warehouseId;

    /** 仓库name*/
    private String warehouseName;

    /** 盘点人 */
    @Excel(name = "盘点人")
    private String inventoryUser;

    /** 盘点状态 */
    @Excel(name = "盘点状态")
    private Long type;

    /** 创建人 */
    @Excel(name = "创建人")
    private String createUser;

    /** 盘点商品列表*/
    private List<ErpInventoryGoods> erpInventoryGoodsList;

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public List<ErpInventoryGoods> getErpInventoryGoodsList() {
        return erpInventoryGoodsList;
    }

    public void setErpInventoryGoodsList(List<ErpInventoryGoods> erpInventoryGoodsList) {
        this.erpInventoryGoodsList = erpInventoryGoodsList;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setInventoryOrder(String inventoryOrder) 
    {
        this.inventoryOrder = inventoryOrder;
    }

    public String getInventoryOrder() 
    {
        return inventoryOrder;
    }
    public void setWarehouseId(Long warehouseId) 
    {
        this.warehouseId = warehouseId;
    }

    public Long getWarehouseId() 
    {
        return warehouseId;
    }
    public void setInventoryUser(String inventoryUser) 
    {
        this.inventoryUser = inventoryUser;
    }

    public String getInventoryUser() 
    {
        return inventoryUser;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }
    public void setCreateUser(String createUser) 
    {
        this.createUser = createUser;
    }

    public String getCreateUser() 
    {
        return createUser;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("inventoryOrder", getInventoryOrder())
            .append("warehouseId", getWarehouseId())
            .append("inventoryUser", getInventoryUser())
            .append("remark", getRemark())
            .append("type", getType())
            .append("createUser", getCreateUser())
            .append("createTime", getCreateTime())
            .toString();
    }
}
