package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

import java.math.BigDecimal;

/**
 * 采购入库单对象 erp_purchase_invoice
 * 
 * @author ruoyi
 * @date 2022-11-14
 */
public class ErpPurchaseInvoice extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 采购单号 */
    @Excel(name = "采购单号")
    private String purchaseOrderNum;

    /** 入库单号 */
    @Excel(name = "入库单号")
    private String shipmentNum;

    /** 订单id */
    @Excel(name = "订单id")
    private Long purchaseId;

    /** 供应商id */
    @Excel(name = "供应商id")
    private Long supplierId;

    /** 付款类型(0应付账款1现金付款2预付款) */
    @Excel(name = "付款类型(0应付账款1现金付款2预付款)")
    private Long paymentType;

    /** 创建人 */
    @Excel(name = "创建人")
    private String createUser;

    /** 有无退货 */
    @Excel(name = "有无退货")
    private Long isReturn;

    /** 供应商联系人 */
    @Excel(name = "供应商联系人")
    private String supplieruser;

    /** 供应商手机号 */
    @Excel(name = "供应商手机号")
    private String supplierphone;
    /*供应商名称*/
    private String supplierName;

    /** 采购总价 */
    @Excel(name = "采购总价")
    private BigDecimal totalPrice;

    /** 是否入库(0是1否) */
    @Excel(name = "是否入库(0是1否)")
    private Long isWarehousing;

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public Long getIsWarehousing() {
        return isWarehousing;
    }

    public void setIsWarehousing(Long isWarehousing) {
        this.isWarehousing = isWarehousing;
    }

    public String getSupplieruser() {
        return supplieruser;
    }

    public void setSupplieruser(String supplieruser) {
        this.supplieruser = supplieruser;
    }

    public String getSupplierphone() {
        return supplierphone;
    }

    public void setSupplierphone(String supplierphone) {
        this.supplierphone = supplierphone;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setPurchaseOrderNum(String purchaseOrderNum) 
    {
        this.purchaseOrderNum = purchaseOrderNum;
    }

    public String getPurchaseOrderNum() 
    {
        return purchaseOrderNum;
    }
    public void setShipmentNum(String shipmentNum) 
    {
        this.shipmentNum = shipmentNum;
    }

    public String getShipmentNum() 
    {
        return shipmentNum;
    }
    public void setPurchaseId(Long purchaseId) 
    {
        this.purchaseId = purchaseId;
    }

    public Long getPurchaseId() 
    {
        return purchaseId;
    }
    public void setSupplierId(Long supplierId) 
    {
        this.supplierId = supplierId;
    }

    public Long getSupplierId() 
    {
        return supplierId;
    }
    public void setPaymentType(Long paymentType) 
    {
        this.paymentType = paymentType;
    }

    public Long getPaymentType() 
    {
        return paymentType;
    }
    public void setCreateUser(String createUser) 
    {
        this.createUser = createUser;
    }

    public String getCreateUser() 
    {
        return createUser;
    }
    public void setIsReturn(Long isReturn) 
    {
        this.isReturn = isReturn;
    }

    public Long getIsReturn() 
    {
        return isReturn;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("purchaseOrderNum", getPurchaseOrderNum())
            .append("shipmentNum", getShipmentNum())
            .append("purchaseId", getPurchaseId())
            .append("supplierId", getSupplierId())
            .append("paymentType", getPaymentType())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createUser", getCreateUser())
            .append("isReturn", getIsReturn())
            .toString();
    }
}
