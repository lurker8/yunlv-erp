package com.ruoyi.system.service;

import com.ruoyi.system.domain.ErpPurchaseGoods;

import java.util.List;

/**
 * 采购商品Service接口
 *
 * @author ruoyi
 * @date 2022-10-31
 */
public interface IErpPurchaseGoodsService
{
    /**
     * 查询采购商品
     *
     * @param id 采购商品主键
     * @return 采购商品
     */
    public ErpPurchaseGoods selectErpPurchaseGoodsById(Long id);

    /*根据采购订单id获取采购商品详细信息列表*/
    public List<ErpPurchaseGoods> selectErpPurchaseGoodsPurchaseId(ErpPurchaseGoods erpPurchaseGoods);

    /**
     * 查询采购商品列表
     *
     * @param erpPurchaseGoods 采购商品
     * @return 采购商品集合
     */
    public List<ErpPurchaseGoods> selectErpPurchaseGoodsList(ErpPurchaseGoods erpPurchaseGoods);

    /**
     * 新增采购商品
     *
     * @param erpPurchaseGoods 采购商品
     * @return 结果
     */
    public int insertErpPurchaseGoods(ErpPurchaseGoods erpPurchaseGoods);

    /**
     * 修改采购商品
     *
     * @param erpPurchaseGoods 采购商品
     * @return 结果
     */
    public int updateErpPurchaseGoods(ErpPurchaseGoods erpPurchaseGoods);
    /*修改采购商品数量专用*/
    public int updateErpPurchaseGoodsQuantity(ErpPurchaseGoods erpPurchaseGoods);

    /**
     * 批量删除采购商品
     *
     * @param ids 需要删除的采购商品主键集合
     * @return 结果
     */
    public int deleteErpPurchaseGoodsByIds(Long[] ids);

    /**
     * 删除采购商品信息
     *
     * @param id 采购商品主键
     * @return 结果
     */
    public int deleteErpPurchaseGoodsById(Long id);
}
