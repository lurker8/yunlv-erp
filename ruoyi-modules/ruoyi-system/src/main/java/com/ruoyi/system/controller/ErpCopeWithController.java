package com.ruoyi.system.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.system.api.model.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.domain.ErpCopeWith;
import com.ruoyi.system.service.IErpCopeWithService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

import static com.ruoyi.common.security.utils.SecurityUtils.getLoginUser;

/**
 * 应付账款Controller
 * 
 * @author ruoyi
 * @date 2022-11-18
 */
@RestController
@RequestMapping("/copeWith")
public class ErpCopeWithController extends BaseController
{
    @Autowired
    private IErpCopeWithService erpCopeWithService;

    /**
     * 查询应付账款列表
     */
    @RequiresPermissions("system:copeWith:list")
    @GetMapping("/list")
    public TableDataInfo list(ErpCopeWith erpCopeWith)
    {
        startPage();
        List<ErpCopeWith> list = erpCopeWithService.selectErpCopeWithList(erpCopeWith);
        return getDataTable(list);
    }

    /**
     * 导出应付账款列表
     */
    @RequiresPermissions("system:copeWith:export")
    @Log(title = "应付账款", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ErpCopeWith erpCopeWith)
    {
        List<ErpCopeWith> list = erpCopeWithService.selectErpCopeWithList(erpCopeWith);
        ExcelUtil<ErpCopeWith> util = new ExcelUtil<ErpCopeWith>(ErpCopeWith.class);
        util.exportExcel(response, list, "应付账款数据");
    }

    /**
     * 获取应付账款详细信息
     */
    @RequiresPermissions("system:copeWith:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(erpCopeWithService.selectErpCopeWithById(id));
    }
    /*财务管理查看付款页面信息*/
    @GetMapping(value = "/LeftPaymentRecordLeftPaymentRecord/{id}")
    public AjaxResult selectErpCopeWithByIdLeftPaymentRecord(@PathVariable("id") Long id)
    {
        return AjaxResult.success(erpCopeWithService.selectErpCopeWithByIdLeftPaymentRecord(id));
    }

    /**
     * 新增应付账款
     */
    @RequiresPermissions("system:copeWith:add")
    @Log(title = "应付账款", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ErpCopeWith erpCopeWith)
    {
        LoginUser loginUser = getLoginUser();
        erpCopeWith.setOperator(loginUser.getUsername());
        return toAjax(erpCopeWithService.insertErpCopeWith(erpCopeWith));
    }

    /**
     * 修改应付账款
     */
    @RequiresPermissions("system:copeWith:edit")
    @Log(title = "应付账款", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ErpCopeWith erpCopeWith)
    {
        return toAjax(erpCopeWithService.updateErpCopeWith(erpCopeWith));
    }

    /**
     * 删除应付账款
     */
    @RequiresPermissions("system:copeWith:remove")
    @Log(title = "应付账款", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(erpCopeWithService.deleteErpCopeWithByIds(ids));
    }
}
