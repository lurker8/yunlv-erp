package com.ruoyi.system.controller;

import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.domain.ErpSupplier;
import com.ruoyi.system.service.IErpSupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 供应商管理Controller
 *
 * @author 刘策
 * @date 2022-10-25
 */
@RestController
@RequestMapping("/supplier")
public class ErpSupplierController extends BaseController
{
    @Autowired
    private IErpSupplierService erpSupplierService;

    /**
     * 查询供应商管理列表
     */
    @RequiresPermissions("system:supplier:list")
    @GetMapping("/list")
    public TableDataInfo list(ErpSupplier erpSupplier)
    {
        startPage();
        List<ErpSupplier> list = erpSupplierService.selectErpSupplierList(erpSupplier);
        return getDataTable(list);
    }

    /**
     * 导出供应商管理列表
     */
    @RequiresPermissions("system:supplier:export")
    @Log(title = "供应商管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ErpSupplier erpSupplier)
    {
        List<ErpSupplier> list = erpSupplierService.selectErpSupplierList(erpSupplier);
        ExcelUtil<ErpSupplier> util = new ExcelUtil<ErpSupplier>(ErpSupplier.class);
        util.exportExcel(response, list, "供应商管理数据");
    }

    /**
     * 获取供应商管理详细信息
     */
    @RequiresPermissions("system:supplier:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(erpSupplierService.selectErpSupplierById(id));
    }

    /**
     * 新增供应商管理
     */
    @RequiresPermissions("system:supplier:add")
    @Log(title = "供应商管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ErpSupplier erpSupplier)
    {
        return toAjax(erpSupplierService.insertErpSupplier(erpSupplier));
    }

    /**
     * 修改供应商管理
     */
    @RequiresPermissions("system:supplier:edit")
    @Log(title = "供应商管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ErpSupplier erpSupplier)
    {
        return toAjax(erpSupplierService.updateErpSupplier(erpSupplier));
    }

    /**
     * 删除供应商管理
     */
    @RequiresPermissions("system:supplier:remove")
    @Log(title = "供应商管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        for (Long id : ids) {
            Long aLong = erpSupplierService.selectSupplierAssociation(id);
            if (aLong != null){
                return AjaxResult.error("供应商已分配禁止删除!");
            }
        }
        return toAjax(erpSupplierService.deleteErpSupplierByIds(ids));
    }
}
