package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.ErpInventory;

/**
 * 库存盘点Mapper接口
 * 
 * @author ruoyi
 * @date 2022-11-21
 */
public interface ErpInventoryMapper 
{
    /**
     * 查询库存盘点
     * 
     * @param id 库存盘点主键
     * @return 库存盘点
     */
    public ErpInventory selectErpInventoryById(Long id);

    /**
     * 查询库存盘点列表
     * 
     * @param erpInventory 库存盘点
     * @return 库存盘点集合
     */
    public List<ErpInventory> selectErpInventoryList(ErpInventory erpInventory);

    /**
     * 新增库存盘点
     * 
     * @param erpInventory 库存盘点
     * @return 结果
     */
    public int insertErpInventory(ErpInventory erpInventory);

    /**
     * 修改库存盘点
     * 
     * @param erpInventory 库存盘点
     * @return 结果
     */
    public int updateErpInventory(ErpInventory erpInventory);

    /**
     * 删除库存盘点
     * 
     * @param id 库存盘点主键
     * @return 结果
     */
    public int deleteErpInventoryById(Long id);

    /**
     * 批量删除库存盘点
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteErpInventoryByIds(Long[] ids);

    /**
     * 修改盘点库存状态
     *
     * @param inventoryId 盘点库存id
     * @param type 状态
     *
     * @return 结果
     * */
    int updateInventoryType(Long inventoryId,Long type);

}
