package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.system.domain.ErpCategory;
import com.ruoyi.system.mapper.ErpCategoryMapper;
import com.ruoyi.system.service.IErpCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 分类Service业务层处理
 *
 * @author 刘策
 * @date 2022-10-27
 */
@Service
public class ErpCategoryServiceImpl implements IErpCategoryService
{
    @Autowired
    private ErpCategoryMapper erpCategoryMapper;

    /**
     * 查询分类
     *
     * @param id 分类主键
     * @return 分类
     */
    @Override
    public ErpCategory selectErpCategoryById(Integer id)
    {
        return erpCategoryMapper.selectErpCategoryById(id);
    }

    /**
     * 查询分类列表
     *
     * @param erpCategory 分类
     * @return 分类
     */
    @Override
    public List<ErpCategory> selectErpCategoryList(ErpCategory erpCategory)
    {
        return erpCategoryMapper.selectErpCategoryList(erpCategory);
    }

    /**
     * 新增分类
     *
     * @param erpCategory 分类
     * @return 结果
     */
    @Override
    public int insertErpCategory(ErpCategory erpCategory)
    {
        erpCategory.setCreateTime(DateUtils.getNowDate());
        return erpCategoryMapper.insertErpCategory(erpCategory);
    }

    /**
     * 修改分类
     *
     * @param erpCategory 分类
     * @return 结果
     */
    @Override
    public int updateErpCategory(ErpCategory erpCategory)
    {
        erpCategory.setUpdateTime(DateUtils.getNowDate());
        return erpCategoryMapper.updateErpCategory(erpCategory);
    }

    /**
     * 批量删除分类
     *
     * @param ids 需要删除的分类主键
     * @return 结果
     */
    @Override
    public int deleteErpCategoryByIds(Integer[] ids)
    {
        return erpCategoryMapper.deleteErpCategoryByIds(ids);
    }

    /**
     * 删除分类信息
     *
     * @param id 分类主键
     * @return 结果
     */
    @Override
    public int deleteErpCategoryById(Integer id)
    {
        return erpCategoryMapper.deleteErpCategoryById(id);
    }

    @Override
    public Long selectCategoryAssociation(Long id) {
        return erpCategoryMapper.selectCategoryAssociation(id);
    }
}
