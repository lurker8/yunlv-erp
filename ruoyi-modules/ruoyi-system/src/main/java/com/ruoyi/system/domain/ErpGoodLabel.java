package com.ruoyi.system.domain;

import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 商品标签对象 erp_good_label
 *
 * @author ruoyi
 * @date 2022-10-27
 */
public class ErpGoodLabel extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Integer id;

    /** 唯一标识 */
    @Excel(name = "唯一标识")
    private String labelKey;

    /** 标签名称 */
    @Excel(name = "标签名称")
    private String name;

    /** 创建人 */
    @Excel(name = "创建人")
    private String createUser;

    public void setId(Integer id)
    {
        this.id = id;
    }

    public Integer getId()
    {
        return id;
    }
    public void setLabelKey(String labelKey)
    {
        this.labelKey = labelKey;
    }

    public String getLabelKey()
    {
        return labelKey;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setCreateUser(String createUser)
    {
        this.createUser = createUser;
    }

    public String getCreateUser()
    {
        return createUser;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("labelKey", getLabelKey())
            .append("name", getName())
            .append("createUser", getCreateUser())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
