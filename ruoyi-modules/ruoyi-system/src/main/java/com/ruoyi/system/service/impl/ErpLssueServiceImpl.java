package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ErpLssueMapper;
import com.ruoyi.system.domain.ErpLssue;
import com.ruoyi.system.service.IErpLssueService;

/**
 * 出库单Service业务层处理
 * 
 * @author liuce
 * @date 2022-11-17
 */
@Service
public class ErpLssueServiceImpl implements IErpLssueService 
{
    @Autowired
    private ErpLssueMapper erpLssueMapper;

    /**
     * 查询出库单
     * 
     * @param id 出库单主键
     * @return 出库单
     */
    @Override
    public ErpLssue selectErpLssueById(Long id)
    {
        return erpLssueMapper.selectErpLssueById(id);
    }

    /**
     * 查询出库单列表
     * 
     * @param erpLssue 出库单
     * @return 出库单
     */
    @Override
    public List<ErpLssue> selectErpLssueList(ErpLssue erpLssue)
    {
        return erpLssueMapper.selectErpLssueList(erpLssue);
    }

    /**
     * 新增出库单
     * 
     * @param erpLssue 出库单
     * @return 结果
     */
    @Override
    public int insertErpLssue(ErpLssue erpLssue)
    {
        return erpLssueMapper.insertErpLssue(erpLssue);
    }

    /**
     * 修改出库单
     * 
     * @param erpLssue 出库单
     * @return 结果
     */
    @Override
    public int updateErpLssue(ErpLssue erpLssue)
    {
        return erpLssueMapper.updateErpLssue(erpLssue);
    }

    /**
     * 批量删除出库单
     * 
     * @param ids 需要删除的出库单主键
     * @return 结果
     */
    @Override
    public int deleteErpLssueByIds(Long[] ids)
    {
        return erpLssueMapper.deleteErpLssueByIds(ids);
    }

    /**
     * 删除出库单信息
     * 
     * @param id 出库单主键
     * @return 结果
     */
    @Override
    public int deleteErpLssueById(Long id)
    {
        return erpLssueMapper.deleteErpLssueById(id);
    }
}
