package com.ruoyi.system.controller;

import java.util.Date;
import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.domain.ErpLssue;
import com.ruoyi.system.service.IErpLssueService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 出库单Controller
 * 
 * @author liuce
 * @date 2022-11-17
 */
@RestController
@RequestMapping("/lssue")
public class ErpLssueController extends BaseController
{
    @Autowired
    private IErpLssueService erpLssueService;

    /**
     * 查询出库单列表
     */
    @RequiresPermissions("system:lssue:list")
    @GetMapping("/list")
    public TableDataInfo list(ErpLssue erpLssue)
    {
        startPage();
        List<ErpLssue> list = erpLssueService.selectErpLssueList(erpLssue);
        return getDataTable(list);
    }

    /**
     * 导出出库单列表
     */
    @RequiresPermissions("system:lssue:export")
    @Log(title = "出库单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ErpLssue erpLssue)
    {
        List<ErpLssue> list = erpLssueService.selectErpLssueList(erpLssue);
        ExcelUtil<ErpLssue> util = new ExcelUtil<ErpLssue>(ErpLssue.class);
        util.exportExcel(response, list, "出库单数据");
    }

    /**
     * 获取出库单详细信息
     */
    @RequiresPermissions("system:lssue:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(erpLssueService.selectErpLssueById(id));
    }

    /**
     * 新增出库单
     */
    @RequiresPermissions("system:lssue:add")
    @Log(title = "出库单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ErpLssue erpLssue)
    {
        return toAjax(erpLssueService.insertErpLssue(erpLssue));
    }

    /**
     * 修改出库单
     */
    @RequiresPermissions("system:lssue:edit")
    @Log(title = "出库单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ErpLssue erpLssue)
    {
        return toAjax(erpLssueService.updateErpLssue(erpLssue));
    }

    /**
     * 删除出库单
     */
    @RequiresPermissions("system:lssue:remove")
    @Log(title = "出库单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(erpLssueService.deleteErpLssueByIds(ids));
    }

}
