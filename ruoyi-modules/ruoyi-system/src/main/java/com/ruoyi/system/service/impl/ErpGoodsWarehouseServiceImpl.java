package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.system.domain.ErpGoodsWarehouse;
import com.ruoyi.system.mapper.ErpGoodsWarehouseMapper;
import com.ruoyi.system.service.IErpGoodsWarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 商品仓库管理Service业务层处理
 *
 * @author 刘策
 * @date 2022-11-02
 */
@Service
public class ErpGoodsWarehouseServiceImpl implements IErpGoodsWarehouseService
{
    @Autowired
    private ErpGoodsWarehouseMapper erpGoodsWarehouseMapper;

    /**
     * 查询商品仓库管理
     *
     * @param id 商品仓库管理主键
     * @return 商品仓库管理
     */
    @Override
    public List<ErpGoodsWarehouse> selectErpGoodsWarehouseById(Long id)
    {
        return erpGoodsWarehouseMapper.selectErpGoodsWarehouseById(id);
    }

    /*根据商品id查询*/
    @Override
    public List<ErpGoodsWarehouse> selectErpGoodsWarehouseByGoodsId(ErpGoodsWarehouse erpGoodsWarehouse)
    {
        return erpGoodsWarehouseMapper.selectErpGoodsWarehouseByGoodsId(erpGoodsWarehouse);
    }

    /**
     * 查询商品仓库管理列表
     *
     * @param erpGoodsWarehouse 商品仓库管理
     * @return 商品仓库管理
     */
    @Override
    public List<ErpGoodsWarehouse> selectErpGoodsWarehouseList(ErpGoodsWarehouse erpGoodsWarehouse)
    {
        return erpGoodsWarehouseMapper.selectErpGoodsWarehouseList(erpGoodsWarehouse);
    }

    /*查询商品名称*/
    @Override
    public List<ErpGoodsWarehouse> selectErpGoodsWarehouseGoodsNameList(ErpGoodsWarehouse erpGoodsWarehouse)
    {
        return erpGoodsWarehouseMapper.selectErpGoodsWarehouseGoodsNameList(erpGoodsWarehouse);
    }
    /*查询仓库名称*/
    @Override
    public List<ErpGoodsWarehouse> selectErpGoodsWarehouseNameList(ErpGoodsWarehouse erpGoodsWarehouse)
    {
        return erpGoodsWarehouseMapper.selectErpGoodsWarehouseNameList(erpGoodsWarehouse);
    }

    /**
     * 新增商品仓库管理
     *
     * @param erpGoodsWarehouse 商品仓库管理
     * @return 结果
     */
    @Override
    public int insertErpGoodsWarehouse(ErpGoodsWarehouse erpGoodsWarehouse)
    {
        erpGoodsWarehouse.setCreateTime(DateUtils.getNowDate());
        return erpGoodsWarehouseMapper.insertErpGoodsWarehouse(erpGoodsWarehouse);
    }

    /**
     * 修改商品仓库管理
     *
     * @param erpGoodsWarehouse 商品仓库管理
     * @return 结果
     */
    @Override
    public int updateErpGoodsWarehouse(ErpGoodsWarehouse erpGoodsWarehouse)
    {
        erpGoodsWarehouse.setUpdateTime(DateUtils.getNowDate());
        return erpGoodsWarehouseMapper.updateErpGoodsWarehouse(erpGoodsWarehouse);
    }

    @Override
    public int updateErpGoodsWarehouses(ErpGoodsWarehouse erpGoodsWarehouse)
    {
        erpGoodsWarehouse.setUpdateTime(DateUtils.getNowDate());
        return erpGoodsWarehouseMapper.updateErpGoodsWarehouses(erpGoodsWarehouse);
    }

    /**
     * 批量删除商品仓库管理
     *
     * @param ids 需要删除的商品仓库管理主键
     * @return 结果
     */
    @Override
    public int deleteErpGoodsWarehouseByIds(Long[] ids)
    {
        return erpGoodsWarehouseMapper.deleteErpGoodsWarehouseByIds(ids);
    }

    /**
     * 删除商品仓库管理信息
     *
     * @param id 商品仓库管理主键
     * @return 结果
     */
    @Override
    public int deleteErpGoodsWarehouseById(Long id)
    {
        return erpGoodsWarehouseMapper.deleteErpGoodsWarehouseById(id);
    }

    @Override
    public ErpGoodsWarehouse selectGoodWarehouseNumber(Long warehouseId, Long saleId, Long saleGoodId) {
        return erpGoodsWarehouseMapper.selectGoodWarehouseNumber(warehouseId, saleId, saleGoodId);
    }

    @Override
    public int deleteGoodNumberInfo(Long goodId, Long saleId) {
        return erpGoodsWarehouseMapper.deleteGoodNumberInfo(goodId, saleId);
    }

}
