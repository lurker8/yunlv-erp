package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.system.domain.ErpWarehouse;
import com.ruoyi.system.mapper.ErpWarehouseMapper;
import com.ruoyi.system.service.IErpWarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 仓库管理Service业务层处理
 *
 * @author 刘策
 * @date 2022-10-26
 */
@Service
public class ErpWarehouseServiceImpl implements IErpWarehouseService
{
    @Autowired
    private ErpWarehouseMapper erpWarehouseMapper;

    /**
     * 查询仓库管理
     *
     * @param id 仓库管理主键
     * @return 仓库管理
     */
    @Override
    public ErpWarehouse selectErpWarehouseById(Long id)
    {
        return erpWarehouseMapper.selectErpWarehouseById(id);
    }

    /**
     * 查询仓库管理列表
     *
     * @param erpWarehouse 仓库管理
     * @return 仓库管理
     */
    @Override
    public List<ErpWarehouse> selectErpWarehouseList(ErpWarehouse erpWarehouse)
    {
        return erpWarehouseMapper.selectErpWarehouseList(erpWarehouse);
    }

    /**
     * 新增仓库管理
     *
     * @param erpWarehouse 仓库管理
     * @return 结果
     */
    @Override
    public int insertErpWarehouse(ErpWarehouse erpWarehouse)
    {
        erpWarehouse.setCreateTime(DateUtils.getNowDate());
        return erpWarehouseMapper.insertErpWarehouse(erpWarehouse);
    }

    /**
     * 修改仓库管理
     *
     * @param erpWarehouse 仓库管理
     * @return 结果
     */
    @Override
    public int updateErpWarehouse(ErpWarehouse erpWarehouse)
    {
        erpWarehouse.setUpdateTime(DateUtils.getNowDate());
        return erpWarehouseMapper.updateErpWarehouse(erpWarehouse);
    }

    /**
     * 批量删除仓库管理
     *
     * @param ids 需要删除的仓库管理主键
     * @return 结果
     */
    @Override
    public int deleteErpWarehouseByIds(Long[] ids)
    {
        return erpWarehouseMapper.deleteErpWarehouseByIds(ids);
    }

    /**
     * 删除仓库管理信息
     *
     * @param id 仓库管理主键
     * @return 结果
     */
    @Override
    public int deleteErpWarehouseById(Long id)
    {
        return erpWarehouseMapper.deleteErpWarehouseById(id);
    }

    @Override
    public Long selectWarehouseAssociation(Long id) {
        return erpWarehouseMapper.selectWarehouseAssociation(id);
    }
}
