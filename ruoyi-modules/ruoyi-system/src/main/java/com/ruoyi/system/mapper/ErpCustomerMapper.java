package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.ErpCustomer;

import java.util.List;

/**
 * 客户管理Mapper接口
 *
 * @author 刘策
 * @date 2022-10-24
 */
public interface ErpCustomerMapper
{
    /**
     * 查询客户管理
     *
     * @param id 客户管理主键
     * @return 客户管理
     */
    public ErpCustomer selectErpCustomerById(Long id);

    /**
     * 查询客户管理列表
     *
     * @param erpCustomer 客户管理
     * @return 客户管理集合
     */
    public List<ErpCustomer> selectErpCustomerList(ErpCustomer erpCustomer);

    /**
     * 新增客户管理
     *
     * @param erpCustomer 客户管理
     * @return 结果
     */
    public int insertErpCustomer(ErpCustomer erpCustomer);

    /*首页客户数量统计*/
    public int selectCustomerNum();

    /**
     * 修改客户管理
     *
     * @param erpCustomer 客户管理
     * @return 结果
     */
    public int updateErpCustomer(ErpCustomer erpCustomer);

    /**
     * 删除客户管理
     *
     * @param id 客户管理主键
     * @return 结果
     */
    public int deleteErpCustomerById(Long id);

    /**
     * 批量删除客户管理
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteErpCustomerByIds(Long[] ids);


    /**
     * 查询客户关联属性
     *
     * @param id 客户id
     *
     * @return 结果*/

    Long selectCustomerAssociation(Long id);
}
