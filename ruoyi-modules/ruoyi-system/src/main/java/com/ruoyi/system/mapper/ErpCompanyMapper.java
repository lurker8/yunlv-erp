package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.ErpCompany;

import java.util.List;

/**
 * company Mapper接口
 *
 * @author 刘策
 * @date 2022-11-01
 */
public interface ErpCompanyMapper
{
    /**
     * 查询company
     *
     * @param id company 主键
     * @return company
     */
    public ErpCompany selectErpCompanyById(Long id);

    /**
     * 查询company 列表
     *
     * @param erpCompany company
     * @return company 集合
     */
    public List<ErpCompany> selectErpCompanyList(ErpCompany erpCompany);

    /**
     * 新增company
     *
     * @param erpCompany company
     * @return 结果
     */
    public int insertErpCompany(ErpCompany erpCompany);

    /**
     * 修改company
     *
     * @param erpCompany company
     * @return 结果
     */
    public int updateErpCompany(ErpCompany erpCompany);

    /**
     * 删除company
     *
     * @param id company 主键
     * @return 结果
     */
    public int deleteErpCompanyById(Long id);

    /**
     * 批量删除company
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteErpCompanyByIds(Long[] ids);
}
