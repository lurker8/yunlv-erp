package com.ruoyi.system.domain;

import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 商品仓库管理对象 erp_goods_warehouse
 *
 * @author 刘策
 * @date 2022-11-02
 */
public class ErpGoodsWarehouse extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 商品id */
    private Long goodId;

    /** 商品NAME */
    @Excel(name = "商品名称")
    private String goodName;

    /** 仓库id */
    private Long warehouseId;

    /** 仓库name */
    @Excel(name = "仓库")
    private String warehouseName;

    /** 库存数量 */
    @Excel(name = "库存数量")
    private Long goodsNumber;

    /** 创建人 */
    private String createUser;

    /** 商品从仓库发了多少的货*/
    private Long goodSaleWarehouse;

    /** 商品从仓库退了多少的货*/
    private Long goodReturnWarehouse;

    public Long getGoodReturnWarehouse() {
        return goodReturnWarehouse;
    }

    public void setGoodReturnWarehouse(Long goodReturnWarehouse) {
        this.goodReturnWarehouse = goodReturnWarehouse;
    }

    public Long getGoodSaleWarehouse() {
        return goodSaleWarehouse;
    }

    public void setGoodSaleWarehouse(Long goodSaleWarehouse) {
        this.goodSaleWarehouse = goodSaleWarehouse;
    }

    public String getGoodName() {
        return goodName;
    }

    public void setGoodName(String goodName) {
        this.goodName = goodName;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setGoodId(Long goodId)
    {
        this.goodId = goodId;
    }

    public Long getGoodId()
    {
        return goodId;
    }
    public void setWarehouseId(Long warehouseId)
    {
        this.warehouseId = warehouseId;
    }

    public Long getWarehouseId()
    {
        return warehouseId;
    }
    public void setWarehouseName(String warehouseName)
    {
        this.warehouseName = warehouseName;
    }

    public String getWarehouseName()
    {
        return warehouseName;
    }
    public void setGoodsNumber(Long goodsNumber)
    {
        this.goodsNumber = goodsNumber;
    }

    public Long getGoodsNumber()
    {
        return goodsNumber;
    }
    public void setCreateUser(String createUser)
    {
        this.createUser = createUser;
    }

    public String getCreateUser()
    {
        return createUser;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("goodId", getGoodId())
            .append("warehouseId", getWarehouseId())
            .append("warehouseName", getWarehouseName())
            .append("goodsNumber", getGoodsNumber())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createUser", getCreateUser())
            .toString();
    }
}
