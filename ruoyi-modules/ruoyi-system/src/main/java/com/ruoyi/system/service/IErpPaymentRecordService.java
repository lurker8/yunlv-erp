package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.ErpPaymentRecord;

/**
 * 付款记录Service接口
 * 
 * @author ruoyi
 * @date 2022-11-18
 */
public interface IErpPaymentRecordService 
{
    /**
     * 查询付款记录
     * 
     * @param id 付款记录主键
     * @return 付款记录
     */
    public ErpPaymentRecord selectErpPaymentRecordById(Long id);

    /**
     * 查询付款记录列表
     * 
     * @param erpPaymentRecord 付款记录
     * @return 付款记录集合
     */
    public List<ErpPaymentRecord> selectErpPaymentRecordList(ErpPaymentRecord erpPaymentRecord);

    /**
     * 新增付款记录
     * 
     * @param erpPaymentRecord 付款记录
     * @return 结果
     */
    public int insertErpPaymentRecord(ErpPaymentRecord erpPaymentRecord);

    /**
     * 修改付款记录
     * 
     * @param erpPaymentRecord 付款记录
     * @return 结果
     */
    public int updateErpPaymentRecord(ErpPaymentRecord erpPaymentRecord);

    /**
     * 批量删除付款记录
     * 
     * @param ids 需要删除的付款记录主键集合
     * @return 结果
     */
    public int deleteErpPaymentRecordByIds(Long[] ids);

    /**
     * 删除付款记录信息
     * 
     * @param id 付款记录主键
     * @return 结果
     */
    public int deleteErpPaymentRecordById(Long id);
}
